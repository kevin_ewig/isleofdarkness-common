import "mocha";
import {Point} from "../../src/math/point";
import {ObjectModel, ObjectModelType} from "../../src/model/objectmodel";
import AnimatedCharacter from "../../src/model/animatedcharacter";
import {World} from "../../src/model/world";
import {Pathfinder} from "../../src/pf/pathfinder";
import {Circle} from "../../src/math/circle";
import {Shape} from "../../src/math/shape";

let assert = require('assert');
let TWEEN = require('@tweenjs/tween.js');

describe("Pathfinder + Tween Test", () => {

    it("should be able to tween from point A to point B", (done: Function) => {

        TWEEN.removeAll();

        let world = new World();
        let pathfinder = new Pathfinder(world);

        let circle = new Circle( new Point(0, 0, 0), 5 );
        let objModel = new AnimatedCharacter("OBJ1", circle, "MODEL");

        // Path should be like 0,0 -> 10,10 -> 20,20 -> 30,30 -> ... 80,80 -> 90, 90 -> 100,100
        let path = pathfinder.findPath(objModel, new Point(100, 100, 0));
        objModel.setPath(path);

        let now = new Date();
        TWEEN.update( now.getTime() + 2000 );

        assert( Math.round(circle.x) == 30 && Math.round(circle.y) == 30 );

        done();

    });

    it("should be able to tween two objects from point A to point B", (done: Function) => {

        let world = new World();
        let pathfinder = new Pathfinder(world);
        let now = new Date();

        // ====================================================================
        // Object 1
        let circle1 = new Circle( new Point(0, 0, 0), 5 );
        let objModel1 = new AnimatedCharacter("OBJ1", circle1, "MODEL");
        
        // Path should be like 0,0 -> 10,10 -> 20,20 -> 30,30 -> ... 80,80 -> 90, 90 -> 100,100
        let path1 = pathfinder.findPath(objModel1, new Point(100, 100, 0));
        objModel1.setPath(path1);

        TWEEN.update( now.getTime() + 2000 );

        // ====================================================================
        // Object 2
        let circle2 = new Circle( new Point(0, 10, 0), 5 );
        let objModel2 = new AnimatedCharacter("OBJ2", circle2, "MODEL");        
        let path2 = pathfinder.findPath(objModel2, new Point(0, 200, 0));
        objModel2.setPath(path2);

        TWEEN.update( now.getTime() + 4000 );

        assert( Math.round(circle1.x) == 70 && Math.round(circle1.y) == 70 );
        assert( Math.round(circle2.x) == 0 && Math.round(circle2.y) == 80 );

        done();

    });

    it("should be able to tween a serialized animated character from point A to point B", (done: Function) => {

        TWEEN.removeAll();

        let world = new World();
        let pathfinder = new Pathfinder(world);

        let circle = new Circle( new Point(0, 0, 0), 5 );
        let animatedCharacter = new AnimatedCharacter("OBJ1", circle, "MODEL");

        // Path should be like 0,0 -> 10,10 -> 20,20 -> 30,30 -> ... 80,80 -> 90, 90 -> 100,100
        let path = pathfinder.findPath(animatedCharacter, new Point(100, 100, 0));
        animatedCharacter.setPath(path);

        // Serialize object model.
        let serializedString:string = JSON.stringify(animatedCharacter.toJSON());

        // Unserialize the object.
        let tempJson = JSON.parse(serializedString);
        let seriObject:AnimatedCharacter = AnimatedCharacter.fromJSON(tempJson);

        let now = new Date();
        TWEEN.update( now.getTime() + 2000 );

        let allTweens = TWEEN.getAll();
        assert( Math.round(seriObject.shape.x) == 30 && Math.round(seriObject.shape.y) == 30 );

        done();

    });    

    it("should be able to call a function after tween stops", (done: Function) => {

        TWEEN.removeAll();

        let world = new World();
        let pathfinder = new Pathfinder(world);

        let circle = new Circle( new Point(0, 0, 0), 5 );
        let objModel = new AnimatedCharacter("OBJ1", circle, "MODEL");

        // Path should be like 0,0 -> 10,10 -> 20,20 -> 30,30 -> ... 80,80 -> 90, 90 -> 100,100
        let path = pathfinder.findPath(objModel, new Point(100, 100, 0));
        
        objModel.speed = 1; // super fast. Total travel time = speed * path length.
        objModel.onTweenComplete = function( objShape:any ) {
            let tempShape: Shape = objShape as Shape;
            assert( tempShape !== null );
            done();
        }
        objModel.setPath(path);
        
        let now = new Date();
        TWEEN.update( now.getTime() + 1000 );

    });

    it("should be able to call a function on tween update", (done: Function) => {

        TWEEN.removeAll();

        let world = new World();
        let pathfinder = new Pathfinder(world);

        let circle = new Circle( new Point(0, 0, 0), 5 );
        let objModel = new AnimatedCharacter("OBJ1", circle, "MODEL");

        // Path should be like 0,0 -> 10,10 -> 20,20 -> 30,30 -> ... 80,80 -> 90, 90 -> 100,100
        let path = pathfinder.findPath(objModel, new Point(100, 100, 0));
        
        let numberOfUpdates: number = 0;
        objModel.speed = 500; // super slow. Total travel time = speed * path length.
        objModel.onTweenUpdate = function( tweenObjectModel: any ) {
            numberOfUpdates += 1;
            if( numberOfUpdates == 4 ) {
                done();
            }
        }
        objModel.setPath(path);
        
        let now = new Date();
        TWEEN.update( now.getTime() + objModel.speed*1 );
        TWEEN.update( now.getTime() + objModel.speed*2 );
        TWEEN.update( now.getTime() + objModel.speed*3 );
        TWEEN.update( now.getTime() + objModel.speed*4 );

    });  
    
    it("should be able to find a complex path from point A to point B", (done:Function) => {

        let now = new Date();
        let kalaxId = "KALAX";
        let kalax = {"id":kalaxId,"shape":{"center":{"x":0,"y":50,"z":0,"type":"point"},"radius":10,"type":"circle"},"modelName":"Alice","modelTexture":null,"objectType":"2","ownerUserId":"B1FOV2--m","isAvatar":true,"speed":500,"type":"animatedcharacter"};
        let heepburn = {"id":"HEEPBURN","shape":{"center":{"x":0,"y":10,"z":0,"type":"point"},"radius":10,"type":"circle"},"modelName":"Alice","modelTexture":null,"objectType":"2","ownerUserId":"S1TiN3bW7","isAvatar":true,"speed":500,"type":"animatedcharacter"};
        let jsonObject = {"objectModels":[kalax, heepburn],"users":[]};
        let world: World = World.fromJSON(jsonObject);

        let avatar = <AnimatedCharacter><any>world.getObjectModel(kalaxId);

        // Find a path to the world.
        let pathfinder = new Pathfinder(world);
        let endPoint = new Point(0, -50, 0);

        let path = pathfinder.findPath(avatar, endPoint);
        assert.ok( path.length == 6 );
        assert.ok( path[0].x == 0 && path[0].y == 50 );
        assert.ok( path[1].x == 0 && path[1].y == 30 );
        assert.ok( path[2].x == -20 && path[2].y == 10 );
        assert.ok( path[3].x == 0 && path[3].y == -10 );
        assert.ok( path[4].x == 0 && path[4].y == -30 );
        assert.ok( path[5].x == 0 && path[5].y == -50 );

        avatar.setPath(path);
        TWEEN.update( now.getTime() + 4000 );

        endPoint = new Point(0, 50, 0);
        path = pathfinder.findPath(avatar, endPoint);

        assert.ok( path.length == 6 );
        assert.ok( path[0].x == 0 && path[0].y == -50 );
        assert.ok( path[1].x == 0 && path[1].y == -30 );
        assert.ok( path[2].x == 0 && path[2].y == -10 );
        assert.ok( path[3].x == 20 && path[3].y == 10 );
        assert.ok( path[4].x == 0 && path[4].y == 30 );
        assert.ok( path[5].x == 0 && path[5].y == 50 );
        
        avatar.setPath(path);
        TWEEN.update( now.getTime() + 8000 );        

        done();

    });     
    
});
