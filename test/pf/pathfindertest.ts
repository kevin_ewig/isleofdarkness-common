import "mocha";
import {Point} from "../../src/math/point";
import {RelativePoint} from "../../src/math/relativepoint";
import {Polygon} from "../../src/math/polygon";
import {ObjectModel, ObjectModelType} from "../../src/model/objectmodel";
import {World} from "../../src/model/world";
import {Pathfinder} from "../../src/pf/pathfinder";
import {Circle} from "../../src/math/circle";
import AnimatedCharacter from "../../src/model/animatedcharacter";

let assert = require('assert');

describe("Pathfinder Test", () => {

    function points2PolygonConverter( startPoint:Point, arrayOfPoints:Array<any>, angle:number, heightArray?:Array<number> ) {
        let tempArrayOfPoints = [];
        for( let i = 0; i < arrayOfPoints.length; i++ ) {
            let obj = arrayOfPoints[i];
            tempArrayOfPoints.push( new RelativePoint(obj.x, obj.y, obj.z) );
        }
        return Polygon.fromTiled(startPoint, tempArrayOfPoints, angle, heightArray);
    }

    function box2PolygonConverter( startPoint:Point, width:number, length:number, z:number ) {
        let tempArrayOfPoints = [];
        let heightArray = [];
        tempArrayOfPoints.push( new RelativePoint(0, 0, z) );
        tempArrayOfPoints.push( new RelativePoint(width, 0, z) );
        tempArrayOfPoints.push( new RelativePoint(width, length, z) );
        tempArrayOfPoints.push( new RelativePoint(0, length, z) );
        tempArrayOfPoints.push( new RelativePoint(0, 0, z) );
        heightArray[0] = z;
        heightArray[1] = z;
        heightArray[2] = z;
        heightArray[3] = z;
        heightArray[4] = z;
        return Polygon.fromTiled(startPoint, tempArrayOfPoints, 0, heightArray);
    }

    function objects2ObjectModelConverter( arrayOfObjects:Array<any> ):Array<ObjectModel> {

        let arrayOfObjectModels = [];
        for( let i = 0; i < arrayOfObjects.length; i++ ) {

            let tempObj = arrayOfObjects[i];
            if( !tempObj.ellipse ) {

                let tempObjId = ("ID" + tempObj.id);
                let tempObjName = tempObj.name;
                let tempX = tempObj.x;
                let tempY = tempObj.y;
                let tempObjectType = tempObj.type;
                let tempObjData = tempObj.polyline;
                let tempObjRotation = tempObj.rotation;

                let objectModelType:ObjectModelType = ObjectModelType.Passable;
                if( tempObjectType == "impassable" ) {
                    objectModelType = ObjectModelType.Impassable;
                } else if( tempObjectType == "climbable" ) {
                    objectModelType = ObjectModelType.Climbable;
                }

                // Polyline
                if( tempObj.polyline) {

                    let heightArray = null;
                    if( tempObj["properties"] ) {
                        heightArray = new Array<number>();
                        let tempHeightsString = tempObj.properties['heights'];
                        let array = tempHeightsString.replace("[","").replace("]","").split(",");
                        for( let k = 0; k < array.length; k++ ) {
                            heightArray[k] = parseFloat(array[k]);
                        }
                        let tempShape = points2PolygonConverter( new Point(tempX, tempY, 0), tempObjData, tempObjRotation, heightArray );
                        let tempObjectModel = new AnimatedCharacter(tempObjId, tempShape, tempObjName);
                        tempObjectModel.objectType = objectModelType;
                        arrayOfObjectModels.push(tempObjectModel);
                    } else {
                        let tempShape = points2PolygonConverter( new Point(tempX, tempY, 0), tempObjData, tempObjRotation );
                        let tempObjectModel = new AnimatedCharacter(tempObjId, tempShape, tempObjName);
                        tempObjectModel.objectType = objectModelType;
                        arrayOfObjectModels.push(tempObjectModel);
                    }
                }

                // Rectangle
                else {

                    let rectLength = tempObj.height;
                    let rectWidth = tempObj.width;
                    let rectZ = 0;

                    if( tempObj.properties ) {
                        rectZ = parseInt(tempObj.properties['height']);
                    }

                    let tempShape = box2PolygonConverter( new Point(tempX, tempY, 0), rectWidth, rectLength, rectZ );
                    let tempObjectModel = new ObjectModel(tempObjId, tempShape, tempObjName);
                    tempObjectModel.objectType = objectModelType;
                    arrayOfObjectModels.push(tempObjectModel);

                }

            }

        }

        return arrayOfObjectModels;

    };

    it("should be able to find a simple path from point A to point B", (done:Function) => {

        let world = new World();
        let pathfinder = new Pathfinder(world);
        let circle = new Circle( new Point(0, 0, 0), 5 );
        let objModel = new ObjectModel("OBJID", circle, "None");

        // Path should be like 0,0 -> 10,10 -> 20,20 -> 30,30 -> ... 80,80 -> 90, 90 -> 100,100
        let path = pathfinder.findPath(objModel, new Point(100, 100, 0));

        assert.deepEqual( path.length, 11 );
        assert( path[0].x == 0 && path[0].y == 0 );
        for( let i = 1; i <= 10; i++ ) {
            assert( path[i].x == i*10 && path[i].y == i*10 );
        }
        done();

    });

    it("should be able to find a complex path from point A to point B - 1", (done:Function) => {

        let world = new World();

        let wallData = [{"x":0,"y":0},{"x":0,"y":80},{"x":16,"y":80},{"x":16,"y":0},{"x":0,"y":0}];
        let wall = points2PolygonConverter( new Point(64,32,0), wallData, 0 );
        let wallObj = new AnimatedCharacter("id1", wall, "wall");
        wallObj.objectType = ObjectModelType.Impassable;
        world.addObjectModel(wallObj);

        let smallWallData = [{"x":0,"y":0},{"x":32,"y":0},{"x":32,"y":-16},{"x":0,"y":-16},{"x":0,"y":0}];
        let smallWall = points2PolygonConverter( new Point(32,112,0), smallWallData, 0 );
        let smallWallObj = new ObjectModel("id2", smallWall, "smallwall");
        smallWallObj.objectType = ObjectModelType.Impassable;
        world.addObjectModel(smallWallObj);

        let pathfinder = new Pathfinder(world);
        let startPoint = new Point(32, 64, 0);
        let endPoint = new Point(112, 64, 0);
        let circle = new Circle(startPoint, 10);
        let objModel = new ObjectModel("OBJID", circle, "None");

        let path = pathfinder.findPath(objModel, endPoint);
        assert( path.length == 7 );

        assert( path[0].x == 32 && path[0].y == 64 );
        assert( path[1].x == 52 && path[1].y == 44 );
        assert( path[2].x == 52 && path[2].y == 24 );
        assert( path[3].x == 72 && path[3].y == 4 );
        assert( path[4].x == 92 && path[4].y == 24 );
        assert( path[5].x == 92 && path[5].y == 44 );
        assert( path[6].x == 112 && path[6].y == 64 );

        done();

    });

    it("should be able to find a complex path from point A to point B - 2", (done:Function) => {

        let objects = [{"height":0,"id":38,"name":"wall1","polyline":[{"x":0,"y":0},{"x":0,"y":10},{"x":100,"y":10},{"x":100,"y":0},{"x":0,"y":0}],"rotation":0,"type":"impassable","visible":true,"width":0,"x":20,"y":10},{"height":0,"id":40,"name":"wall2","polyline":[{"x":0,"y":0},{"x":0,"y":-60},{"x":-10,"y":-60},{"x":-10,"y":0},{"x":0,"y":0}],"rotation":0,"type":"impassable","visible":true,"width":0,"x":90,"y":100},{"height":0,"id":42,"name":"wall3","polyline":[{"x":0,"y":0},{"x":30,"y":0},{"x":30,"y":10},{"x":0,"y":10},{"x":0,"y":0}],"rotation":0,"type":"impassable","visible":true,"width":0,"x":20,"y":50},{"height":0,"id":43,"name":"wall4","polyline":[{"x":0,"y":0},{"x":0,"y":10},{"x":100,"y":10},{"x":100,"y":0},{"x":0,"y":0}],"rotation":0,"type":"impassable","visible":true,"width":0,"x":20,"y":100},{"height":0,"id":44,"name":"wall5","polyline":[{"x":0,"y":0},{"x":0,"y":60},{"x":10,"y":60},{"x":10,"y":0},{"x":0,"y":0}],"rotation":0,"type":"impassable","visible":true,"width":0,"x":110,"y":20},{"height":0,"id":45,"name":"wall6","polyline":[{"x":0,"y":0},{"x":0,"y":60},{"x":10,"y":60},{"x":10,"y":0},{"x":0,"y":0}],"rotation":0,"type":"impassable","visible":true,"width":0,"x":50,"y":20},{"height":0,"id":46,"name":"wall7","polyline":[{"x":0,"y":0},{"x":60,"y":0},{"x":60,"y":10},{"x":0,"y":10},{"x":0,"y":0}],"rotation":0,"type":"impassable","visible":true,"width":0,"x":0,"y":130},{"height":0,"id":47,"name":"wall8","polyline":[{"x":0,"y":0},{"x":0,"y":30},{"x":10,"y":30},{"x":10,"y":0},{"x":0,"y":0}],"rotation":0,"type":"impassable","visible":true,"width":0,"x":60,"y":110},{"ellipse":true,"height":0,"id":48,"name":"start","rotation":0,"type":"","visible":true,"width":0,"x":30,"y":40},{"ellipse":true,"height":0,"id":49,"name":"end","rotation":0,"type":"","visible":true,"width":0,"x":130,"y":90}];
        let listOfObjModels = objects2ObjectModelConverter(objects);
        assert( listOfObjModels.length == 8 );

        // Add all object models to the world.
        let world = new World();
        for( let i = 0; i < listOfObjModels.length; i++ ) {
            world.addObjectModel(listOfObjModels[i]);
        }

        // Find a path to the world.
        let pathfinder = new Pathfinder(world);
        let startPoint = new Point(30, 40, 0);
        let endPoint = new Point(130, 90, 0);
        let circle = new Circle(startPoint, 5);
        let objModel = new ObjectModel("OBJID", circle, "None");

        let path = pathfinder.findPath(objModel, endPoint);
        assert( path.length == 24 );

        // Should be no negative numbers and all of the x and y should be divisible by 10.
        for( let i = 0; i < path.length; i++ ) {
            let tempX = path[i].x; let tempY = path[i].y;
            assert( tempX > 0 && tempY > 0 && tempX % 10 == 0 && tempY % 10 == 0 );
        }

        done();

    });   

    it("should be able to go up and down another floor", (done:Function) => {

        let world = new World();

        let floor1Data = [{"height":100,"id":1,"name":"basefloor","rotation":0,"type":"climbable","visible":true,"width":100,"x":10,"y":10},{"ellipse":true,"height":20,"id":2,"name":"start","rotation":0,"type":"","visible":true,"width":20,"x":10,"y":20},{"height":0,"id":12,"name":"stairs","polyline":[{"x":0,"y":0},{"x":40,"y":0},{"x":40,"y":20},{"x":0,"y":20},{"x":0,"y":0}],"properties":{"heights":"[0,50,50,0,0]"},"propertytypes":{"heights":"string"},"rotation":0,"type":"","visible":true,"width":0,"x":30,"y":50}];
        let floor1Objects = objects2ObjectModelConverter(floor1Data);
        assert( floor1Objects.length == 2 );

        // Add all first floor object models to the world.
        for( let i = 0; i < floor1Objects.length; i++ ) {
            world.addObjectModel(floor1Objects[i]);
        }

        let floor2Data = [{"height":80,"id":5,"name":"balcony","properties":{"height":"50"},"propertytypes":{"height":"string"},"rotation":0,"type":"climbable","visible":true,"width":30,"x":70,"y":20},{"ellipse":true,"height":20,"id":10,"name":"end","rotation":0,"type":"","visible":true,"width":20,"x":80,"y":50}];
        let floor2Objects = objects2ObjectModelConverter(floor2Data);
        assert( floor2Objects.length == 1 );

        // Add all second floor object models to the world.
        for( let i = 0; i < floor2Objects.length; i++ ) {
            world.addObjectModel(floor2Objects[i]);
        }

        // Create pathfinder.
        let pathfinder = new Pathfinder(world);

        // ==================================================
        // Go up another floor.
        // ==================================================
        let startPoint = new Point(20, 30, 0);
        let endPoint = new Point(90, 60, 50);
        let circle = new Circle(startPoint, 5);
        let objModel = new ObjectModel("OBJID", circle, "None");

        let path = pathfinder.findPath(objModel, endPoint, 20);
        assert( path.length == 8 );
        assert( path[0].x == 20 && path[0].y == 30 && path[0].z == 0 );
        assert( path[7].x == 90 && path[7].y == 60 && path[7].z == 50 );

        // ==================================================
        // Go down another floor.
        // ==================================================
        startPoint = new Point(90, 60, 50);
        endPoint = new Point(20, 30, 0);
        circle = new Circle(startPoint, 5);
        objModel = new ObjectModel("OBJID", circle, "None");

        path = pathfinder.findPath(objModel, endPoint, 20);
        assert( path.length == 8 );
        assert( path[0].x == 90 && path[0].y == 60 && path[0].z == 50 );
        assert( path[7].x == 20 && path[7].y == 30 && path[7].z == 0 );

        done();

    });

    it("should be able to go up and down multiple floors", (done:Function) => {

        let world = new World();

        let floor1Data = [{"ellipse":true,"height":0,"id":50,"name":"start","rotation":0,"type":"","visible":true,"width":0,"x":20,"y":30},{"height":0,"id":51,"name":"stairs1","polyline":[{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":20},{"x":0,"y":20},{"x":0,"y":0}],"properties":{"heights":"[0,50,50,0,0]"},"propertytypes":{"heights":"string"},"rotation":0,"type":"climbable","visible":true,"width":0,"x":40,"y":20},{"height":0,"id":74,"name":"firstfloor","polyline":[{"x":0,"y":0},{"x":140,"y":0},{"x":140,"y":110},{"x":0,"y":110}],"rotation":0,"type":"climbable","visible":true,"width":0,"x":0,"y":0}];
        let floor1Objects = objects2ObjectModelConverter(floor1Data);
        assert( floor1Objects.length == 2 );

        // Add all first floor object models to the world.
        for( let i = 0; i < floor1Objects.length; i++ ) {
            world.addObjectModel(floor1Objects[i]);
        }

        let floor2Data = [{"height":0,"id":53,"name":"landing2","polyline":[{"x":0,"y":0},{"x":0,"y":110},{"x":70,"y":110},{"x":70,"y":0},{"x":0,"y":0}],"properties":{"heights":"[50,50,50,50,50]"},"propertytypes":{"heights":"string"},"rotation":0,"type":"climbable","visible":true,"width":0,"x":90,"y":10},{"height":0,"id":55,"name":"stairs2","polyline":[{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":20},{"x":0,"y":20},{"x":0,"y":0}],"properties":{"heights":"[100,50,50,100,100]"},"propertytypes":{"heights":"string"},"rotation":0,"type":"climbable","visible":true,"width":0,"x":40,"y":90}];
        let floor2Objects = objects2ObjectModelConverter(floor2Data);
        assert( floor2Objects.length == 2 );

        // Add all second floor object models to the world.
        for( let i = 0; i < floor2Objects.length; i++ ) {
            floor2Objects[i].shape.z += 50;
            world.addObjectModel(floor2Objects[i]);
        }

        let floor3Data = [{"height":0,"id":66,"name":"landing3a","polyline":[{"x":0,"y":0},{"x":40,"y":0},{"x":40,"y":110},{"x":0,"y":110},{"x":0,"y":0}],"properties":{"heights":"[100,100,100,100,100]"},"propertytypes":{"heights":"string"},"rotation":0,"type":"climbable","visible":true,"width":0,"x":0,"y":0},{"height":0,"id":67,"name":"stairs3a","polyline":[{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":20},{"x":0,"y":20},{"x":0,"y":0}],"properties":{"heights":"[100,150,150,100,100]"},"propertytypes":{"heights":"string"},"rotation":0,"type":"climbable","visible":true,"width":0,"x":40,"y":50}];
        let floor3Objects = objects2ObjectModelConverter(floor3Data);
        assert( floor3Objects.length == 2 );

        // Add all third floor object models to the world.
        for( let i = 0; i < floor3Objects.length; i++ ) {
            floor3Objects[i].shape.z += 100;
            world.addObjectModel(floor3Objects[i]);
        }

        let floor4Data = [{"height":0,"id":72,"name":"landing4","polyline":[{"x":0,"y":0},{"x":0,"y":110},{"x":70,"y":110},{"x":70,"y":0},{"x":0,"y":0}],"properties":{"heights":"[150,150,150,150,150]"},"propertytypes":{"heights":"string"},"rotation":0,"type":"climbable","visible":true,"width":0,"x":90,"y":10},{"ellipse":true,"height":20,"id":73,"name":"end","rotation":0,"type":"","visible":true,"width":20,"x":110,"y":20}];
        let floor4Objects = objects2ObjectModelConverter(floor4Data);
        assert( floor4Objects.length == 1 );

        // Add all fourth floor object models to the world.
        for( let i = 0; i < floor4Objects.length; i++ ) {
            floor4Objects[i].shape.z += 150;
            world.addObjectModel(floor4Objects[i]);
        }

        // Create pathfinder.
        let pathfinder = new Pathfinder(world);

        // ==================================================
        // Go up another floor.
        // ==================================================
        let startPoint = new Point(20, 30, 0);
        let endPoint = new Point(110, 20, 150);
        let circle = new Circle(startPoint, 5);
        let objModel = new ObjectModel("OBJID", circle, "None");

        let path = pathfinder.findPath(objModel, endPoint, 10);
        assert( path.length == 27 );
        assert( path[0].x == 20 && path[0].y == 30 && path[0].z == 0 );
        assert( path[26].x == 110 && path[26].y == 20 && path[26].z == 150 );

        // ==================================================
        // Go down another floor.
        // ==================================================
        startPoint = new Point(110, 20, 150);
        endPoint = new Point(20, 30, 0);
        circle = new Circle(startPoint, 5);
        objModel = new ObjectModel("OBJID", circle, "None");

        path = pathfinder.findPath(objModel, endPoint, 10);
        assert( path.length == 27 );
        assert( path[0].x == 110 && path[0].y == 20 && path[0].z == 150 );
        assert( path[26].x == 20 && path[26].y == 30 && path[26].z == 0 );

        done();

    });

    it("should be able to treat rectangles same as polylines", (done:Function) => {

        let world = new World();

        let floorData = [{"height":80,"id":1,"name":"wall","rotation":0,"type":"impassable","visible":true,"width":10,"x":40,"y":20},{"ellipse":true,"height":20,"id":2,"name":"start","rotation":0,"type":"","visible":true,"width":20,"x":10,"y":50},{"ellipse":true,"height":20,"id":3,"name":"end","rotation":0,"type":"","visible":true,"width":20,"x":60,"y":50}];
        let floorObjects = objects2ObjectModelConverter(floorData);
        assert( floorObjects.length == 1 );

        // Add all floor object models to the world.
        for( let i = 0; i < floorObjects.length; i++ ) {
            world.addObjectModel(floorObjects[i]);
        }

        // Create pathfinder.
        let pathfinder = new Pathfinder(world);

        let startPoint = new Point(20, 60, 0);
        let endPoint = new Point(70, 60, 0);
        let circle = new Circle(startPoint, 5);
        let objModel = new ObjectModel("OBJID", circle, "None");

        let path = pathfinder.findPath(objModel, endPoint, 10);
        assert( path.length == 12 );
        assert( path[0].x == 20 && path[0].y == 60 && path[0].z == 0 );
        assert( path[11].x == 70 && path[11].y == 60 && path[11].z == 0 );

        done();

    });

    it("should be able to find a simple path for an object model from point A to point B", (done:Function) => {

        let world = new World();

        let circle: Circle = new Circle( new Point(0, 10, 0), 10);
        let objectModel: ObjectModel = new ObjectModel("ID123", circle, "Model Name");
        world.addObjectModel(objectModel);

        let pathfinder = new Pathfinder(world);
        let path = pathfinder.findPath(objectModel, new Point(-64, 70, 0));

        done();

    });

});
