import 'mocha';
import {Point} from "../../src/math/point";
import {Intersecter} from "../../src/math/intersecter";
import {Circle} from "../../src/math/circle";
import {Polygon} from "../../src/math/polygon";
import {Rectangle} from "../../src/math/rectangle";
import {Line} from "../../src/math/line";

let assert = require('assert');

describe("Intersecter Test", () =>{

    it('should check if a point intersects a circle', (done:Function) => {
        let point = new Point(60, 60);
        let circle = new Circle(new Point(20, 20), 60);
        let result = Intersecter.isPointIntersectCircle(point, circle);
        assert(result == true);
        done();
    });

    it('should check if a point does not intersect a circle', (done:Function) => {
        let point = new Point(60, 80);
        let circle = new Circle(new Point(20, 20), 50);
        let result = Intersecter.isPointIntersectCircle(point, circle);
        assert(result == false);
        done();
    });

    it('should check if a point intersects a polygon', (done:Function) => {
        let point = new Point(60, 60);
        let array = [ new Point(20, 20), new Point(100, 20), new Point(90, 120), new Point(30, 110)];
        let poly = new Polygon(array);
        let result = Intersecter.isPointIntersectPolygon(point, poly);
        assert(result == true);
        done();
    });

    it('should check if a point does not intersect a polygon', (done:Function) => {
        let point = new Point(60, 121);
        let array = [new Point(20, 20), new Point(100, 20), new Point(100, 120), new Point(20, 120)];
        let poly = new Polygon(array);
        let result = Intersecter.isPointIntersectPolygon(point, poly);
        assert(result == false);
        done();
    });

    it('should check if a line intersects a circle', (done:Function) => {
        let circle = new Circle( new Point(100, 30), 10 );
        let line = new Line( new Point(115, 20), new Point(90, 50) );
        let result = Intersecter.isLineIntersectCircle(line, circle);
        assert(result == true);
        done();
    });

    it('should check if a line does not intersects a circle', (done:Function) => {
        let circle = new Circle( new Point(100, 30), 10 );
        let line = new Line( new Point(135.25, 30.25), new Point(110.25, 60.25) );
        let result = Intersecter.isLineIntersectCircle(line, circle);
        assert(result == false);
        done();
    });

    it('should check if a line intersects a rectangle', (done:Function) => {
        let rect = new Rectangle( new Point(10, 10), 20, 30 );
        let line = new Line( new Point(20, 50), new Point(35, 10) );
        let result = Intersecter.isLineIntersectRectangle(line, rect);
        assert(result == true);
        done();
    });

    it('should check if a line does not intersects a rectangle', (done:Function) => {
        let rect = new Rectangle( new Point(10, 10), 20, 30 );
        let line = new Line( new Point(20, 60), new Point(40, 30) );
        let result = Intersecter.isLineIntersectRectangle(line, rect);
        assert(result == false);
        done();
    });

});
