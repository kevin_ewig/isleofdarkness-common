import "mocha"
import {Point} from "../../src/math/point";
import {Rectangle} from "../../src/math/rectangle";
import {Circle} from "../../src/math/circle";
import {Intersecter} from "../../src/math/intersecter";

let assert = require('assert');

describe("Rectangle Test", () => {

    it('should check if two rectangles intersect', (done:Function) => {
        let rect1 = new Rectangle( new Point(10, 10), 30, 30 );
        let rect2 = new Rectangle( new Point(15, 15), 30, 30 );
        let result = Intersecter.isRectangleIntersectRectangle(rect1, rect2);
        assert(result == true);
        done();
    });

    it('should check if two rectangles do not intersect', (done:Function) => {
        let rect1 = new Rectangle( new Point(10, 10), 30, 30 );
        let rect2 = new Rectangle( new Point(150, 150), 30, 30 );
        let result = Intersecter.isRectangleIntersectRectangle(rect1, rect2);
        assert(result == false);
        done();
    });

    it('should check if a rectangle and a circle intersects', (done:Function) => {
        let rect = new Rectangle( new Point(10, 10), 30, 30 );
        let circle = new Circle( new Point(15, 15), 3 );
        let result = Intersecter.isRectangleIntersectCircle(rect, circle);
        assert(result == true);
        done();
    });

    it('should check if a rectangle and a circle does not intersect', (done:Function) => {
        let rect = new Rectangle( new Point(10, 10), 30, 30 );
        let circle = new Circle( new Point(45, 45), 3 );
        let result = Intersecter.isRectangleIntersectCircle(rect, circle);
        assert(result == false);
        done();
    });      

});

