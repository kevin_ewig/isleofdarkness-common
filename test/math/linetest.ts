
import {Intersecter} from "../../src/math/intersecter";
import {Line} from "../../src/math/line";
import {Point} from "../../src/math/point";

let assert = require('assert');

describe("Line Test", () => {

    it('should return the correct length of the line', (done:Function) => {
        let line = new Line( new Point(5, 5), new Point(10, 5) );
        assert(line.getLength() == 5);
        done();
    });

    it('should return the correct angle of the line', (done:Function) => {
        let line = new Line( new Point(5, 5), new Point(10, 10) );
        assert(line.getAngle() == 45);
        done();
    });

    it('should check if a line intersects another line', (done:Function) => {
        var line1 = new Line( new Point(5, 5), new Point(10, 5) );
        var line2 = new Line( new Point(6, 6), new Point(6, 10) );
        var result = Intersecter.isLineIntersectLine(line1, line2);
        assert(result == true);
        done();
    });

    it('should check a parallel line does not intersect another line', (done:Function) => {
        var line1 = new Line( new Point(5, 5), new Point(10, 5) );
        var line2 = new Line( new Point(6, 5), new Point(10, 5) );
        var result = Intersecter.isLineIntersectLine(line1, line2);
        assert(result == false);
        done();
    });

    it('should check if a line intersects another line', (done:Function) =>{
        var line1 = new Line( new Point(5, 5), new Point(10, 5) );
        var line2 = new Line( new Point(11, 5), new Point(15, 5) );
        var result = Intersecter.isLineIntersectLine(line1, line2);
        assert(result == false);
        done();
    });

});
