import "mocha"
import {RelativePoint} from "../../src/math/relativepoint";
import {Point} from "../../src/math/point";

let assert = require('assert');

describe("RelativePoint Test", () => {

    it('should be able to make a point relative to another point', (done:Function) => { 
        let relativePoint = new RelativePoint( 4, 5, 0 );
        let point = new Point(1, 2);
        relativePoint.setRelativePoint(point);
        assert(relativePoint.x == 5);
        assert(relativePoint.y == 7);
        done();
    });

    it('should be able to make a point relative to another point even after modification', (done:Function) => { 
        let relativePoint1 = new RelativePoint( 4, 5, 0 );
        let relativePoint2 = new RelativePoint( 9, 10, 11 );

        let point = new Point(1, 2);
        relativePoint1.setRelativePoint(point);
        relativePoint2.setRelativePoint(point);

        point.x = 0;
        point.y = 0;
        point.z = 10;

        assert(relativePoint1.x == 4);
        assert(relativePoint1.y == 5);
        assert(relativePoint1.z == 10);

        assert(relativePoint2.x == 9);
        assert(relativePoint2.y == 10);
        assert(relativePoint2.z == 21);

        done();
    });

    it('should be able to convert a point to JSON and back', (done:Function) => { 

        let relativePoint1 = new RelativePoint( 4, 5, 0 );
        let relativePoint1Clone = RelativePoint.fromJSON(JSON.parse(JSON.stringify(relativePoint1.toJSON())));
        assert( relativePoint1.x == relativePoint1Clone.x );
        assert( relativePoint1.y == relativePoint1Clone.y );
        assert( relativePoint1.z == relativePoint1Clone.z );

        let relativePoint2 = new RelativePoint( 4, 5, 0 );
        let relativePoint2Clone = RelativePoint.fromJSON(JSON.parse(JSON.stringify(relativePoint2.toJSON())));
        assert( relativePoint2.x == relativePoint2Clone.x );
        assert( relativePoint2.y == relativePoint2Clone.y );
        assert( relativePoint2.z == relativePoint2Clone.z );

        done();
    });

});
