import "mocha";
import Point from "../../src/math/point";

let assert = require('assert');

describe("Point Test", () => {

    it('should be able to convert Point to JSON and back.', (done:Function) => {

        let pnt = new Point( 3, 2, 1 );
        let jsonObj = pnt.toJSON();
        let jsonStr = JSON.stringify(jsonObj);
        assert( jsonStr == '{"x":3,"y":2,"z":1,"type":"point"}' );

        let pnt2 = Point.fromJSON(jsonObj);
        assert( pnt.equals(pnt2) );

        done();

    });

    it('should be able to get section coordinate from point.', (done:Function) => {

        let pnt = new Point( 300, 200 );
        assert.ok( pnt.getSectionCoordinate().x === 3 && pnt.getSectionCoordinate().y === 2 && pnt.getSectionCoordinate().z === 0 );
        done();

    });  

});
