import 'mocha';
import {Point} from "../../src/math/point";
import {Circle} from "../../src/math/circle";
import {Intersecter} from "../../src/math/intersecter";

let assert = require('assert');

describe('Circle Test', () => {

    it('should check if two circles intersect', (done:Function) => {
        let circle1 = new Circle( new Point(10, 10), 10 );
        let circle2 = new Circle( new Point(15, 15), 10 );
        let result = Intersecter.isCircleIntersectCircle(circle1, circle2);
        assert(result == true);
        done();
    });

    it('should check if two circles do not intersect', (done:Function) => {
        let circle1 = new Circle( new Point(10, 10), 10 );
        let circle2 = new Circle( new Point(215, 215), 10 );
        let result = Intersecter.isCircleIntersectCircle(circle1, circle2);
        assert(result == false);
        done();
    });

    it('should convert circle to JSON and back again', (done:Function) => {
        let circle1 = new Circle( new Point(11, 12, 0), 10 );
        let json = circle1.toJSON();

        let jsonString = JSON.stringify(json);
        assert(jsonString == '{"center":{"x":11,"y":12,"z":0,"type":"point"},"radius":10,"type":"circle"}');

        let json2 = JSON.parse(jsonString);
        let circle2 = Circle.fromJSON(json2);
        assert(circle1.equals(circle2));

        done();
    });

    it('should check if a circle can be cloned', (done:Function) => {
        let circle1 = new Circle( new Point(10, 10), 10 );
        let circle2 = circle1.clone() as Circle;
        assert(circle1.equals(circle2));
        done();
    });

    it('should check if adjacent circles can be created successfully', (done:Function) => {

        let circle = new Circle( new Point(0, 0, 0), 5 );
        let listOfCircles = circle.getAdjacentShapes();
        assert(listOfCircles.length == 8);

        assert(listOfCircles[0].x == -10 && listOfCircles[0].y == -10);
        assert(listOfCircles[1].x == -10 && listOfCircles[1].y == 0);
        assert(listOfCircles[2].x == -10 && listOfCircles[2].y == 10);

        assert(listOfCircles[3].x == 0 && listOfCircles[3].y == -10);
        assert(listOfCircles[4].x == 0 && listOfCircles[4].y == 10);

        assert(listOfCircles[5].x == 10 && listOfCircles[5].y == -10);
        assert(listOfCircles[6].x == 10 && listOfCircles[6].y == 0);
        assert(listOfCircles[7].x == 10 && listOfCircles[7].y == 10);

        done();

    });

    it('should update link position', (done:Function) => {
        let circle1 = new Circle( new Point(10, 10), 10 );
        let linkPosition = { "x" : 0, "y": 0, "z": 0 };
        circle1.linkPosition = linkPosition;
        circle1.x = 10;
        circle1.y = 20;
        circle1.z = 30;
        assert(circle1.linkPosition.x == 10);
        assert(circle1.linkPosition.y == 20);
        assert(circle1.linkPosition.z == 30);

        done();
    });

});

