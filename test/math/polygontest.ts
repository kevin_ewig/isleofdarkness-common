import "mocha";
import {Point} from "../../src/math/point";
import {RelativePoint} from "../../src/math/relativepoint";
import {Polygon} from "../../src/math/polygon";
import {Intersecter} from "../../src/math/intersecter";
import {Circle} from "../../src/math/circle";

let assert = require('assert');

describe("Polygon Test", () => {

    function points2PolygonConverter( startPoint:Point, arrayOfPoints:Array<any>, angle:number, heights?:number[] ) {

        let tempArrayOfPoints = [];

        for( let i = 0; i < arrayOfPoints.length; i++ ) {
            let obj = arrayOfPoints[i];
            tempArrayOfPoints.push( new RelativePoint(obj.x, obj.y, obj.z) );
        }
        return Polygon.fromTiled(startPoint, tempArrayOfPoints, angle, heights);

    }

    it('should be able to convert from tiled', (done:Function) => {
        let startPoint = new Point(0, 0, 0);
        let arrayOfPoints = [ new Point(10, 10, 0), new Point(50, 10, 0), new Point(50, 50, 0), new Point(10, 50, 0) ];

        let polygon = Polygon.fromTiled(startPoint, arrayOfPoints, 45);
        assert(Math.round(polygon.getArrayOfPoints()[0].x) == 10);
        assert(Math.round(polygon.getArrayOfPoints()[0].y) == 10);
        assert(Math.round(polygon.getArrayOfPoints()[1].x) == 38);
        assert(Math.round(polygon.getArrayOfPoints()[1].y) == 38);
        assert(Math.round(polygon.getArrayOfPoints()[2].x) == 10);
        assert(Math.round(polygon.getArrayOfPoints()[2].y) == 67);
        assert(Math.round(polygon.getArrayOfPoints()[3].x) == -18);
        assert(Math.round(polygon.getArrayOfPoints()[3].y) == 38);
        done();
    });

    it('should be able to convert to JSON and back', (done:Function) => {

        let startPoint = new Point(5, 5, 0);
        let arrayOfPoints = [ new Point(15, 15, 0), new Point(50, 15, 0), new Point(50, 50, 0), new Point(15, 50, 0) ];

        let tiledPolygon = Polygon.fromTiled(startPoint, arrayOfPoints, 45);
        let jsonObject = tiledPolygon.toJSON();
        assert(jsonObject['arrayOfPoints'].length == 4);

        let polygon = Polygon.fromJSON(jsonObject);
        assert(polygon.getArrayOfPoints().length == 4);

        done();

    });

    it('should be able to tell if two polygons do not intersect 1', (done:Function) => {

        let startPoint1 = new Point(32, 15, 0);
        let data1 = [{"x":0,"y":0},{"x":48,"y":0},{"x":48,"y":32},{"x":0,"y":32},{"x":0,"y":0}];
        let polygon1 = points2PolygonConverter(startPoint1, data1, 45);

        let startPoint2 = new Point(48, 16, 0);
        let data2 = [{"x":0,"y":0},{"x":0,"y":32},{"x":32,"y":32},{"x":32,"y":0},{"x":0,"y":0}];
        let polygon2 = points2PolygonConverter(startPoint2, data2, -45);

        let result = Intersecter.isPolygonIntersectPolygon(polygon1, polygon2);
        assert(result == false);

        done();

    });

    it('should be able to tell if two polygons do not intersect 2', (done:Function) => {

        let startPoint1 = new Point(44, 48, 0);
        let data1 = [{"x":0,"y":0},{"x":0,"y":48},{"x":48,"y":32},{"x":48,"y":0},{"x":32,"y":-16},{"x":16,"y":-16},{"x":0,"y":0}];
        let polygon1 = points2PolygonConverter(startPoint1, data1, 45);

        let startPoint2 = new Point(80, 45, 0);
        let data2 = [{"x":0,"y":0},{"x":16,"y":-16},{"x":48,"y":0},{"x":48,"y":48},{"x":16,"y":64},{"x":0,"y":48},{"x":0,"y":0}];
        let polygon2 = points2PolygonConverter(startPoint2, data2, 0);

        let result = Intersecter.isPolygonIntersectPolygon(polygon1, polygon2);
        assert(result == false);

        done();

    });

    it('should be able to tell if two polygons intersect 1', (done:Function) => {

        let startPoint1 = new Point(32, 15, 0);
        let data1 = [{"x":0,"y":0},{"x":48,"y":0},{"x":48,"y":32},{"x":0,"y":32},{"x":0,"y":0}];
        let polygon1 = points2PolygonConverter(startPoint1, data1, 45);

        let startPoint2 = new Point(32, 30, 0);
        let data2 = [{"x":0,"y":0},{"x":0,"y":32},{"x":32,"y":32},{"x":32,"y":0},{"x":0,"y":0}];
        let polygon2 = points2PolygonConverter(startPoint2, data2, -45);

        let result = Intersecter.isPolygonIntersectPolygon(polygon1, polygon2);
        assert(result == true);

        done();

    });

    it('should be able to tell if two polygons intersect 2', (done:Function) => {

        let startPoint1 = new Point(48, 80, 0);
        let data1 = [{"x":0,"y":0},{"x":0,"y":48},{"x":48,"y":32},{"x":48,"y":0},{"x":32,"y":-16},{"x":16,"y":-16},{"x":0,"y":0}];
        let polygon1 = points2PolygonConverter(startPoint1, data1, 45);

        let startPoint2 = new Point(80, 45, 0);
        let data2 = [{"x":0,"y":0},{"x":16,"y":-16},{"x":48,"y":0},{"x":48,"y":48},{"x":16,"y":64},{"x":0,"y":48},{"x":0,"y":0}];
        let polygon2 = points2PolygonConverter(startPoint2, data2, 0);

        let result = Intersecter.isPolygonIntersectPolygon(polygon1, polygon2);
        assert(result == true);

        done();

    });

    it('should be able to tell if two polygons do not intersect 3', (done:Function) => {

        let startPoint = new Point(0, 0, 0);
        let data = [{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":50},{"x":0,"y":50},{"x":0,"y":0}];
        let polygon1 = points2PolygonConverter(startPoint, data, 0);

        let startPoint2 = new Point(1000, 1000, 0);
        let data2 = [{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":50},{"x":0,"y":50},{"x":0,"y":0}];
        let polygon2 = points2PolygonConverter(startPoint2, data2, 0);

        let result = Intersecter.isPolygonIntersectPolygon(polygon1, polygon2);
        assert(result == false);

        done();

    });

    it('should be able to move polygon by changing their center points.', (done:Function) => {

        let startPoint = new Point(0, 0, 0);
        let data = [{"x": 0, "y": 0}, {"x": 50, "y": 0}, {"x": 50, "y": 50}, {"x": 0, "y": 50}, {"x": 0, "y": 0}];
        let polygon = points2PolygonConverter(startPoint, data, 0);

        assert( polygon.x == 25 );
        assert( polygon.y == 25 );

        // Now move the center 100 units.
        polygon.x = 125;
        polygon.y = 125;

        let arrayOfPoints:RelativePoint[] = polygon.getArrayOfPoints();
        assert( arrayOfPoints[0].x == 100 );
        assert( arrayOfPoints[0].y == 100 );
        assert( arrayOfPoints[1].x == 150 );
        assert( arrayOfPoints[1].y == 100 );
        assert( arrayOfPoints[2].x == 150 );
        assert( arrayOfPoints[2].y == 150 );

        done();

    });

    it('should be able to tell if two polygons intersect after moving their center points', (done:Function) => {

        let startPoint = new Point(0, 0, 0);
        let data = [{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":50},{"x":0,"y":50},{"x":0,"y":0}];
        let polygon1 = points2PolygonConverter(startPoint, data, 0);

        let startPoint2 = new Point(1000, 1000, 0);
        let data2 = [{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":50},{"x":0,"y":50},{"x":0,"y":0}];
        let polygon2 = points2PolygonConverter(startPoint2, data2, 0);

        // In the beginning these two polygons will not intersect.
        let result = Intersecter.isPolygonIntersectPolygon(polygon1, polygon2);
        assert(result == false);

        // Now move the center point of the second polygon.
        polygon2.x = 1;
        polygon2.y = 1;

        // The two polygons should now intersect.
        let result2 = Intersecter.isPolygonIntersectPolygon(polygon1, polygon2);
        assert(result2 == true);

        done();

    });

    it('should be able to clone the polygon', (done:Function) => {

        let startPoint = new Point(10, 10, 10);
        let data = [{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":50},{"x":0,"y":50},{"x":0,"y":0}];
        let polygon1 = points2PolygonConverter(startPoint, data, 0);
        let polygon2 = polygon1.clone();

        assert(polygon1.equals(polygon2));

        done();

    });

    it('should check if adjacent polygons can be created successfully', (done:Function) => {

        let dataPoint = new Array<Point>();
        dataPoint.push(new Point(0,0,0));
        dataPoint.push(new Point(2,0,0));
        dataPoint.push(new Point(2,2,0));
        dataPoint.push(new Point(0,2,0));

        let polygon = new Polygon(dataPoint);
        let listOfPolygons = polygon.getAdjacentShapes();

        assert(listOfPolygons.length == 8);

        assert(listOfPolygons[0].x == -1 && listOfPolygons[0].y == -1);
        assert(listOfPolygons[1].x == -1 && listOfPolygons[1].y == 1);
        assert(listOfPolygons[2].x == -1 && listOfPolygons[2].y == 3);
        assert(listOfPolygons[3].x == 1 && listOfPolygons[3].y == -1);
        assert(listOfPolygons[4].x == 1 && listOfPolygons[4].y == 3);
        assert(listOfPolygons[5].x == 3 && listOfPolygons[5].y == -1);
        assert(listOfPolygons[6].x == 3 && listOfPolygons[6].y == 1);
        assert(listOfPolygons[7].x == 3 && listOfPolygons[7].y == 3);

        done();

    });

    it('should check if a polygon intersects a circle', (done:Function) => {

        let circle = new Circle( new Point(50, 24, 0), 10 );

        let wallData = [{"x":0,"y":0},{"x":0,"y":80},{"x":16,"y":80},{"x":16,"y":0},{"x":0,"y":0}];
        let wall = points2PolygonConverter( new Point(64,32,0), wallData, 0 );
        let result = Intersecter.isShapeIntersectShape(wall, circle);

        assert(result == false);
        done();

    });

    it('should calculate polygon z value', (done:Function) => {

        let circle = new Circle( new Point(50, 24, 0), 10 );

        let startPoint = new Point(0, 0, 0);
        let data = [{"x":0,"y":0},{"x":100,"y":0},{"x":100,"y":20},{"x":0,"y":20},{"x":0,"y":0}];
        let heightData = [0, 50, 50, 0, 0];
        let polygon1 = points2PolygonConverter(startPoint, data, 0, heightData);

        let z1 = polygon1.getZRelativeToPoint(new Point(0,0,0));
        assert(z1 == 0);

        let z2 = polygon1.getZRelativeToPoint(new Point(100,0,0));
        assert(z2 == 50);

        let z3 = polygon1.getZRelativeToPoint(new Point(100,20,0));
        assert(z3 == 50);

        let z4 = polygon1.getZRelativeToPoint(new Point(0,20,0));
        assert(z4 == 0);

        let z5 = polygon1.getZRelativeToPoint(new Point(50,10,0));
        assert(z5 == 25);

        let z6 = polygon1.getZRelativeToPoint(new Point(75,10,0));
        assert(z6 == 37.5);

        done();

    });

    it('should calculate polygon z value that is a flat surface', (done:Function) => {

        let circle = new Circle( new Point(50, 24, 0), 10 );

        let startPoint = new Point(0, 0, 0);
        let data = [{"x":0,"y":0},{"x":100,"y":0},{"x":100,"y":20},{"x":0,"y":20},{"x":0,"y":0}];
        let heightData = [50, 50, 50, 50, 50];
        let polygon1 = points2PolygonConverter(startPoint, data, 0, heightData);

        let z1 = polygon1.getZRelativeToPoint(new Point(0,0,0));
        assert(z1 == 50);

        let z2 = polygon1.getZRelativeToPoint(new Point(100,0,0));
        assert(z2 == 50);

        let z3 = polygon1.getZRelativeToPoint(new Point(100,20,0));
        assert(z3 == 50);

        let z4 = polygon1.getZRelativeToPoint(new Point(0,20,0));
        assert(z4 == 50);

        let z5 = polygon1.getZRelativeToPoint(new Point(50,10,0));
        assert(z5 == 50);

        let z6 = polygon1.getZRelativeToPoint(new Point(75,10,0));
        assert(z6 == 50);

        done();

    });

    it('should be able to link position', (done:Function) => {

        let startPoint = new Point(10, 10, 10);
        let data = [{"x":0,"y":0},{"x":50,"y":0},{"x":50,"y":50},{"x":0,"y":50},{"x":0,"y":0}];
        let polygon1 = points2PolygonConverter(startPoint, data, 0);

        let linkPosition = { "x": 30, "y": 60, "z": 90 };
        polygon1.linkPosition = linkPosition;

        polygon1.x = 100;
        polygon1.y = 200;
        polygon1.z = 300;

        assert(polygon1.linkPosition.x == 100);
        assert(polygon1.linkPosition.y == 200);
        assert(polygon1.linkPosition.z == 300);

        done();

    });

});
