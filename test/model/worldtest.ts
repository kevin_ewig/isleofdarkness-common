import "mocha";
import {World} from "../../src/model/world";
import {ObjectModel} from "../../src/model/objectmodel";
import {Point} from "../../src/math/point";
import {Circle} from "../../src/math/circle";
import {Polygon} from "../../src/math/polygon";
import {UserMetaData} from "../../src/model/usermetadata";

let assert = require('assert');

describe("World Test", () => {

    it('should check if an object model intersects another model', (done:Function) => {

        let world = new World();

        let circle1 = new Circle( new Point(5,5), 10);
        let objModel1 = new ObjectModel("ID123", circle1, "model3d");
        world.addObjectModel(objModel1);

        let circle2 = new Circle( new Point(8,8), 10);
        let intersection = world.getAllIntersection(circle2);
        assert(intersection.length == 1);

        circle2 = new Circle( new Point(30,8), 9);
        intersection = world.getAllIntersection(circle2);
        assert(intersection.length == 0);

        done();

    });

    it('should check if an object model can be added and removed properly', (done:Function) => {

        let world = new World();

        let circle1 = new Circle( new Point(5,5), 10);
        let objModel1 = new ObjectModel("ID123", circle1, "model3d");
        world.addObjectModel(objModel1);

        world.removeObjectModel("ID123");

        let circle2 = new Circle( new Point(8,8), 10);
        let intersection = world.getAllIntersection(circle2);
        assert(intersection.length == 0);

        circle2 = new Circle( new Point(30,8), 9);
        intersection = world.getAllIntersection(circle2);
        assert(intersection.length == 0);

        done();

    });

    it('should check if it can be converted to JSON and back.', (done:Function) => {

        let world = new World();

        let circle = new Circle( new Point(5,5), 10);
        let objModel1 = new ObjectModel("ID123", circle, "model3d1");
        world.addObjectModel(objModel1);

        let data = [ new Point(0,0), new Point(100, 0), new Point(100, 20), new Point(0, 20), new Point(0, 0)];
        let polygon = new Polygon(data);
        let objModel2 = new ObjectModel("ID456", polygon, "model3d2");
        world.addObjectModel(objModel2);

        let jsonString = JSON.stringify(world.toJSON());
        let jsonObject = JSON.parse(jsonString);
        let clonedWorld = World.fromJSON(jsonObject);

        done();

    });

    it('should check if it can be converted to JSON and back (with users).', (done:Function) => {

        let world = new World();

        let circle = new Circle( new Point(5,5), 10);
        let objModel1 = new ObjectModel("ID123", circle, "model3d1");
        world.addObjectModel(objModel1);

        let data = [ new Point(0,0), new Point(100, 0), new Point(100, 20), new Point(0, 20), new Point(0, 0)];
        let polygon = new Polygon(data);
        let objModel2 = new ObjectModel("ID456", polygon, "model3d2");
        world.addObjectModel(objModel2);

        let user1 = new UserMetaData("123", "user1");
        world.addUserMetaData(user1);

        let jsonString = JSON.stringify(world.toJSON());
        let jsonObject = JSON.parse(jsonString);
        let clonedWorld = World.fromJSON(jsonObject);

        done();

    });    

});
