"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var usermetadata_1 = require("../../src/model/usermetadata");
var assert = require('assert');
describe("UserMetaData Test", function () {
    it('should check user metadata can export to JSON and back', function (done) {
        var usermetadata = new usermetadata_1.UserMetaData("123456", "james");
        var jsonstring = JSON.stringify(usermetadata);
        var expected = '{"userId":"123456","userName":"james"}';
        assert(jsonstring === expected);
        done();
    });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL3VzZXJtZXRhZGF0YXRlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpQkFBYztBQUNkLDZEQUEwRDtBQUUxRCxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7QUFFL0IsUUFBUSxDQUFDLG1CQUFtQixFQUFFO0lBRTFCLEVBQUUsQ0FBQyx3REFBd0QsRUFBRSxVQUFDLElBQWE7UUFFdkUsSUFBSSxZQUFZLEdBQUcsSUFBSSwyQkFBWSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUV2RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzlDLElBQUksUUFBUSxHQUFHLHdDQUF3QyxDQUFDO1FBQ3hELE1BQU0sQ0FBQyxVQUFVLEtBQUssUUFBUSxDQUFDLENBQUM7UUFFaEMsSUFBSSxFQUFFLENBQUM7SUFFWCxDQUFDLENBQUMsQ0FBQztBQUVQLENBQUMsQ0FBQyxDQUFDIiwiZmlsZSI6Im1vZGVsL3VzZXJtZXRhZGF0YXRlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCJtb2NoYVwiXHJcbmltcG9ydCB7VXNlck1ldGFEYXRhfSBmcm9tIFwiLi4vLi4vc3JjL21vZGVsL3VzZXJtZXRhZGF0YVwiO1xyXG5cclxubGV0IGFzc2VydCA9IHJlcXVpcmUoJ2Fzc2VydCcpO1xyXG5cclxuZGVzY3JpYmUoXCJVc2VyTWV0YURhdGEgVGVzdFwiLCAoKSA9PiB7XHJcblxyXG4gICAgaXQoJ3Nob3VsZCBjaGVjayB1c2VyIG1ldGFkYXRhIGNhbiBleHBvcnQgdG8gSlNPTiBhbmQgYmFjaycsIChkb25lOkZ1bmN0aW9uKSA9PiB7XHJcblxyXG4gICAgICAgIGxldCB1c2VybWV0YWRhdGEgPSBuZXcgVXNlck1ldGFEYXRhKFwiMTIzNDU2XCIsIFwiamFtZXNcIik7XHJcblxyXG4gICAgICAgIGxldCBqc29uc3RyaW5nID0gSlNPTi5zdHJpbmdpZnkodXNlcm1ldGFkYXRhKTtcclxuICAgICAgICBsZXQgZXhwZWN0ZWQgPSAne1widXNlcklkXCI6XCIxMjM0NTZcIixcInVzZXJOYW1lXCI6XCJqYW1lc1wifSc7XHJcbiAgICAgICAgYXNzZXJ0KGpzb25zdHJpbmcgPT09IGV4cGVjdGVkKTtcclxuICAgICAgICBcclxuICAgICAgICBkb25lKCk7XHJcblxyXG4gICAgfSk7XHJcblxyXG59KTsiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
