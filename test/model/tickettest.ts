import "mocha"

import {Ticket} from "../../src/model/ticket";

let assert = require('assert');

describe("Ticket Test", () => {

    it('should be able to create a ticket from static method NewTicket', (done:Function) => {

        let expirySeconds = 10;
        let ticket = Ticket.NewTicket("USER1", expirySeconds);
        let now = new Date();

        assert.ok( ticket.userId === "USER1" );
        assert.ok( now.getTime() - ticket.expirationDate.getTime() <= expirySeconds*1000 );

        done();

    });

});
