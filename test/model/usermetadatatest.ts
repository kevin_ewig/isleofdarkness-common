import "mocha"
import {UserMetaData} from "../../src/model/usermetadata";

let assert = require('assert');

describe("UserMetaData Test", () => {

    it('should check user metadata can export to JSON and back', (done:Function) => {

        let usermetadata = new UserMetaData("123456", "james");

        let jsonstring = JSON.stringify(usermetadata);
        let expected = '{"userId":"123456","userName":"james"}';
        assert(jsonstring === expected);
        
        done();

    });

});