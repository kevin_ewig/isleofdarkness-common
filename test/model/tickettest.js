"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var ticket_1 = require("../../src/model/ticket");
var assert = require('assert');
describe("Ticket Test", function () {
    it('should be able to create a ticket from static method NewTicket', function (done) {
        var expirySeconds = 10;
        var ticket = ticket_1.Ticket.NewTicket("USER1", expirySeconds);
        var now = new Date();
        assert.ok(ticket.userId === "USER1");
        assert.ok(now.getTime() - ticket.expirationDate.getTime() <= expirySeconds * 1000);
        done();
    });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL3RpY2tldHRlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpQkFBYztBQUVkLGlEQUE4QztBQUU5QyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7QUFFL0IsUUFBUSxDQUFDLGFBQWEsRUFBRTtJQUVwQixFQUFFLENBQUMsZ0VBQWdFLEVBQUUsVUFBQyxJQUFhO1FBRS9FLElBQUksYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLE1BQU0sR0FBRyxlQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQztRQUN0RCxJQUFJLEdBQUcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1FBRXJCLE1BQU0sQ0FBQyxFQUFFLENBQUUsTUFBTSxDQUFDLE1BQU0sS0FBSyxPQUFPLENBQUUsQ0FBQztRQUN2QyxNQUFNLENBQUMsRUFBRSxDQUFFLEdBQUcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxJQUFJLGFBQWEsR0FBQyxJQUFJLENBQUUsQ0FBQztRQUVuRixJQUFJLEVBQUUsQ0FBQztJQUVYLENBQUMsQ0FBQyxDQUFDO0FBRVAsQ0FBQyxDQUFDLENBQUMiLCJmaWxlIjoibW9kZWwvdGlja2V0dGVzdC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBcIm1vY2hhXCJcclxuXHJcbmltcG9ydCB7VGlja2V0fSBmcm9tIFwiLi4vLi4vc3JjL21vZGVsL3RpY2tldFwiO1xyXG5cclxubGV0IGFzc2VydCA9IHJlcXVpcmUoJ2Fzc2VydCcpO1xyXG5cclxuZGVzY3JpYmUoXCJUaWNrZXQgVGVzdFwiLCAoKSA9PiB7XHJcblxyXG4gICAgaXQoJ3Nob3VsZCBiZSBhYmxlIHRvIGNyZWF0ZSBhIHRpY2tldCBmcm9tIHN0YXRpYyBtZXRob2QgTmV3VGlja2V0JywgKGRvbmU6RnVuY3Rpb24pID0+IHtcclxuXHJcbiAgICAgICAgbGV0IGV4cGlyeVNlY29uZHMgPSAxMDtcclxuICAgICAgICBsZXQgdGlja2V0ID0gVGlja2V0Lk5ld1RpY2tldChcIlVTRVIxXCIsIGV4cGlyeVNlY29uZHMpO1xyXG4gICAgICAgIGxldCBub3cgPSBuZXcgRGF0ZSgpO1xyXG5cclxuICAgICAgICBhc3NlcnQub2soIHRpY2tldC51c2VySWQgPT09IFwiVVNFUjFcIiApO1xyXG4gICAgICAgIGFzc2VydC5vayggbm93LmdldFRpbWUoKSAtIHRpY2tldC5leHBpcmF0aW9uRGF0ZS5nZXRUaW1lKCkgPD0gZXhwaXJ5U2Vjb25kcyoxMDAwICk7XHJcblxyXG4gICAgICAgIGRvbmUoKTtcclxuXHJcbiAgICB9KTtcclxuXHJcbn0pO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
