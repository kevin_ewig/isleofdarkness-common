import "mocha";

import {Rectangle} from "../../src/math/rectangle";
import {Circle} from "../../src/math/circle";
import {Point} from "../../src/math/point";
import {ObjectModel, ObjectModelType} from "../../src/model/objectmodel";
import AnimatedCharacter from "../../src/model/animatedcharacter";
import { AnimationProfile } from "../../src/model/animationprofile";

let assert = require('assert');

describe("AnimatedCharacter Test", () => {

    it('should convert animated character to JSON and back', (done:Function) => {

        let rect = new Rectangle( new Point(5,1), 10, 20 );
        let objModel1 = new AnimatedCharacter("ID990", rect, "testmodel1");
        objModel1.modelName = "objModel1";
        objModel1.modelTexture = "objTexture1";
        objModel1.ownerUserId = "owner1";
        objModel1.objectType = ObjectModelType.Impassable;
        objModel1.speed = 230;

        let objModel1JsonString = JSON.stringify(objModel1.toJSON());
        let objModel1Json = JSON.parse(objModel1JsonString);
        let objModel1Clone = AnimatedCharacter.fromJSON(objModel1Json);

        assert(objModel1Clone.id == objModel1.id);
        assert(objModel1Clone.modelName == objModel1.modelName);
        assert(objModel1Clone.objectType == objModel1.objectType);
        assert(objModel1Clone.objectType == ObjectModelType.Impassable);
        assert(objModel1Clone.shape.equals(objModel1.shape));

        assert(objModel1Clone.modelName == objModel1.modelName);
        assert(objModel1Clone.modelTexture == objModel1.modelTexture);
        assert(objModel1Clone.ownerUserId == objModel1.ownerUserId);
        assert(objModel1Clone.speed == objModel1.speed);

        done();

    });  
    
    it('should convert animated character with animation profile to JSON and back', (done:Function) => {

        let circle = new Circle( new Point(5,1), 20 );
        let objModel1 = new AnimatedCharacter("ID777", circle, "testmodel2");
        objModel1.modelName = "objModel2";
        objModel1.modelTexture = "objTexture2";
        objModel1.ownerUserId = "owner2";
        objModel1.objectType = ObjectModelType.Impassable;
        objModel1.speed = 761;

        var animProfile = new AnimationProfile("1", "animprof", "act1", "act2", "act3", "act4"); 
        objModel1.animationProfile = animProfile;

        let objModel1JsonString = JSON.stringify(objModel1.toJSON());
        let objModel1Json = JSON.parse(objModel1JsonString);
        let objModel1Clone = AnimatedCharacter.fromJSON(objModel1Json);

        assert(objModel1Clone.id == objModel1.id);
        assert(objModel1Clone.modelName == objModel1.modelName);
        assert(objModel1Clone.objectType == objModel1.objectType);
        assert(objModel1Clone.shape.equals(objModel1.shape));
        assert(objModel1Clone.modelTexture == objModel1.modelTexture);
        assert(objModel1Clone.ownerUserId == objModel1.ownerUserId);
        assert(objModel1Clone.speed == objModel1.speed);

        assert(objModel1Clone.objectType == ObjectModelType.Impassable);
        assert(objModel1Clone.animationProfile != null);
        assert(objModel1Clone.animationProfile.id === objModel1.animationProfile.id);
        assert(objModel1Clone.animationProfile.die === objModel1.animationProfile.die);
        assert(objModel1Clone.animationProfile.idle === objModel1.animationProfile.idle);
        assert(objModel1Clone.animationProfile.name === objModel1.animationProfile.name);
        assert(objModel1Clone.animationProfile.run === objModel1.animationProfile.run);
        assert(objModel1Clone.animationProfile.walk === objModel1.animationProfile.walk);                                      
        done();

    });    
    
    it('should convert animated character with points to JSON and back', (done:Function) => {

        let circle = new Circle( new Point(5,10), 90 );
        let objModel1 = new AnimatedCharacter("ID111", circle, "testmodel3");
        objModel1.modelName = "objModel3";
        objModel1.modelTexture = "objTexture3";
        objModel1.ownerUserId = "owner3";
        objModel1.objectType = ObjectModelType.Impassable;
        objModel1.speed = 123;

        var animProfile = new AnimationProfile("2", "animprof", "act1", "act2", "act3", "act4"); 
        objModel1.animationProfile = animProfile;

        // Set points
        var path = [ new Point(6,10), new Point(7,10) ];
        objModel1.setPath(path);

        let objModel1JsonString = JSON.stringify(objModel1.toJSON());
        let objModel1Json = JSON.parse(objModel1JsonString);
        let objModel1Clone = AnimatedCharacter.fromJSON(objModel1Json);

        assert(objModel1Clone.id == objModel1.id);
        assert(objModel1Clone.modelName == objModel1.modelName);
        assert(objModel1Clone.objectType == objModel1.objectType);
        assert(objModel1Clone.shape.equals(objModel1.shape));
        assert(objModel1Clone.modelTexture == objModel1.modelTexture);
        assert(objModel1Clone.ownerUserId == objModel1.ownerUserId);
        assert(objModel1Clone.speed == objModel1.speed);

        assert(objModel1Clone.objectType == ObjectModelType.Impassable);
        assert(objModel1Clone.animationProfile != null);
        assert(objModel1Clone.animationProfile.id === objModel1.animationProfile.id);
        assert(objModel1Clone.animationProfile.die === objModel1.animationProfile.die);
        assert(objModel1Clone.animationProfile.idle === objModel1.animationProfile.idle);
        assert(objModel1Clone.animationProfile.name === objModel1.animationProfile.name);
        assert(objModel1Clone.animationProfile.run === objModel1.animationProfile.run);
        assert(objModel1Clone.animationProfile.walk === objModel1.animationProfile.walk);                                      

        assert(objModel1Clone.getPath().length === 2);
        assert(objModel1Clone.getPath()[0].x === objModel1.getPath()[0].x);
        assert(objModel1Clone.getPath()[0].y === objModel1.getPath()[0].y);
        assert(objModel1Clone.getPath()[1].x === objModel1.getPath()[1].x);
        assert(objModel1Clone.getPath()[1].y === objModel1.getPath()[1].y);

        done();

    });  


    it('should convert a string to an animated character', (done:Function) => {

        let str = '{"shape":{"center":{"x":0,"y":10,"z":0,"type":"point"},"radius":10,"type":"circle"},"modelName":"Alice","modelTexture":null,"objectType":"1","ownerUserId":"B1FOV2--m","isAvatar":true,"speed":400,"type":"animatedcharacter", "id": "ID1"}';
        let jsonObj = JSON.parse(str);
        let objModel1 = AnimatedCharacter.fromJSON(jsonObj);
        assert(objModel1.objectType === ObjectModelType.Passable);
        assert(objModel1.speed === 400);

        str = '{"shape":{"center":{"x":0,"y":10,"z":0,"type":"point"},"radius":10,"type":"circle"},"modelName":"Alice","modelTexture":null,"objectType":"2","ownerUserId":"B1FOV2--m","isAvatar":true,"speed":600,"type":"animatedcharacter", "id": "ID1"}';
        jsonObj = JSON.parse(str);
        objModel1 = AnimatedCharacter.fromJSON(jsonObj);
        assert(objModel1.objectType === ObjectModelType.Impassable);
        assert(objModel1.speed === 600);

        done();

    });        

    it('should be able to get the section coordinate', (done:Function) => {

        let rect = new Rectangle( new Point(200, 500), 12, 20 );
        let objModel1 = new AnimatedCharacter("ID990", rect, "testmodel1");
        objModel1.objectType = ObjectModelType.Impassable;
       
        assert(objModel1.getSectionCoordinate().x === 2 && objModel1.getSectionCoordinate().y === 5 );

        done();

    });

});
