import "mocha";

import {Circle} from "../../src/math/circle";
import {Point} from "../../src/math/point";
import {Polygon} from "../../src/math/polygon";
import BaseAnimatedCharacter from "../../src/model/baseanimatedcharacter";
import { AnimationProfile } from "../../src/model/animationprofile";
import { deepEqual } from "assert";

let assert = require('assert');

describe("BaseAnimatedCharacter Test", () => {

    it('should convert base animate character with profile to JSON and back', (done:Function) => {

        let circle = new Circle( new Point(5,5), 10);
        let animationProfile = new AnimationProfile("id1", "avatar1", "idle", "run", "", "");
        let characterProfile = new BaseAnimatedCharacter("ID222", circle, "avatar1", animationProfile);
        let characterProfileJson = JSON.stringify(characterProfile.toJSON());
        let objModelJson = JSON.parse(characterProfileJson);
        let objModelClone = BaseAnimatedCharacter.fromJSON(objModelJson);

        assert(objModelClone.id == characterProfile.id);
        assert(objModelClone.modelName == characterProfile.modelName);
        assert(objModelClone.shape.equals(characterProfile.shape));
        deepEqual(objModelClone.animationProfile, characterProfile.animationProfile);
        deepEqual(objModelClone.animationProfile.name, "avatar1");

        let data = [ new Point(0, 0), new Point(100, 0), new Point(100, 20), new Point(0, 20), new Point(0, 0)];
        let polygon = new Polygon(data);
        let animationProfile2 = new AnimationProfile("id2", "avatar2", "idle", "run", "", "");
        let characterProfile2 = new BaseAnimatedCharacter("ID111", polygon, "avatar2", animationProfile2);
        let characterProfile2Json = JSON.stringify(characterProfile2.toJSON());
        let objModel2Json = JSON.parse(characterProfile2Json);
        let objModel2Clone = BaseAnimatedCharacter.fromJSON(objModel2Json);

        assert(objModel2Clone.id == characterProfile2.id);
        assert(objModel2Clone.modelName == characterProfile2.modelName);
        assert(objModel2Clone.shape.equals(characterProfile2.shape));
        deepEqual(objModel2Clone.animationProfile, characterProfile2.animationProfile);
        deepEqual(objModel2Clone.animationProfile.name, "avatar2");

        done();

    });      

});
