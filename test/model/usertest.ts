import "mocha";
import {User} from "../../src/model/user";

let assert = require('assert');

describe("User Test", () => {

    it('should check user can export to JSON and back', (done:Function) => {

        let user = new User("123456", "james");
        user.lastLoggedInDate = new Date("1/1/2010");
        user.createdDate = new Date("1/1/2000");
        user.email = "joeuserit.com";
        user.language = "en";

        let jsonString = JSON.stringify(user.toJSON());
        let jsonObject = JSON.parse(jsonString);
        let clonedUser = User.fromJSON(jsonObject);

        assert( clonedUser.language == user.language );
        assert( clonedUser.email == user.email);
        assert( clonedUser.createdDate.getTime() == user.createdDate.getTime() );
        assert( clonedUser.language == user.language );
        assert( clonedUser.lastLoggedInDate.getTime() == user.lastLoggedInDate.getTime() );

        done();

    });

    it('should check user can export to JSON and back (with section coordinate)', (done:Function) => {

        let user = new User("123456", "james");
        user.lastLoggedInDate = new Date("1/1/2010");
        user.createdDate = new Date("1/1/2000");
        user.email = "joeuserit.com";
        user.language = "en";

        let jsonString = JSON.stringify(user.toJSON());
        let jsonObject = JSON.parse(jsonString);
        let clonedUser = User.fromJSON(jsonObject);

        assert( clonedUser.language == user.language );
        assert( clonedUser.email == user.email);
        assert( clonedUser.createdDate.getTime() == user.createdDate.getTime() );
        assert( clonedUser.language == user.language );
        assert( clonedUser.lastLoggedInDate.getTime() == user.lastLoggedInDate.getTime() );

        done();

    });    

    it('should check user can export to JSON and back (minimum data)', (done:Function) => {

        let user = new User("3434123", "Smith");

        let jsonString = JSON.stringify(user.toJSON());
        let jsonObject = JSON.parse(jsonString);
        let clonedUser = User.fromJSON(jsonObject);

        assert.equal( jsonString, JSON.stringify(clonedUser) );
        done();

    });    

});
