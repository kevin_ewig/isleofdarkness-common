import "mocha";

import {Circle} from "../../src/math/circle";
import {Point} from "../../src/math/point";
import {ObjectModel, ObjectModelType} from "../../src/model/objectmodel";
import {Polygon} from "../../src/math/polygon";
import { deepEqual } from "assert";

let assert = require('assert');

describe("ObjectModel Test", () => {

    it('should check if object model can export to JSON and back', (done:Function) => {

        let circle = new Circle( new Point(5,5), 10);
        let objModel1 = new ObjectModel("ID123", circle, "model3d1");
        objModel1.worldName = "World";
        objModel1.ownerUserId = "User1";
        objModel1.displayName = "Display1";
        objModel1.modelTexture = "Texture";
        let objModel1JsonString = JSON.stringify(objModel1.toJSON());
        let objModel1Json = JSON.parse(objModel1JsonString);
        let objModel1Clone = ObjectModel.fromJSON(objModel1Json);

        assert(objModel1Clone.id == objModel1.id);
        assert(objModel1Clone.modelName == objModel1.modelName);
        assert(objModel1Clone.objectType == objModel1.objectType);
        assert(objModel1Clone.shape.equals(objModel1.shape));
        deepEqual(objModel1Clone.worldName, objModel1.worldName);
        deepEqual(objModel1Clone.displayName, objModel1.displayName);
        deepEqual(objModel1Clone.modelTexture, objModel1.modelTexture);

        let data = [ new Point(0, 0), new Point(100, 0), new Point(100, 20), new Point(0, 20), new Point(0, 0)];
        let polygon = new Polygon(data);
        let objModel2 = new ObjectModel("ID456", polygon, "model3d2");
        let objModel2JsonString = JSON.stringify(objModel2.toJSON());
        let objModel2Json = JSON.parse(objModel2JsonString);
        let objModel2Clone = ObjectModel.fromJSON(objModel2Json);

        assert(objModel2Clone.id == objModel2.id);
        assert(objModel2Clone.modelName == objModel2.modelName);
        assert(objModel2Clone.objectType == objModel2.objectType);
        assert(objModel2Clone.shape.equals(objModel2.shape));

        done();

    });      

});
