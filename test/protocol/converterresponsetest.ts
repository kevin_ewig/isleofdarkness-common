import "mocha";

import {UserMetaData} from "../../src/model/usermetadata";
import {User} from "../../src/model/user";
import {ObjectModel} from "../../src/model/objectmodel";
import {World} from "../../src/model/world";
import {Point} from "../../src/math/point";
import {Circle} from "../../src/math/circle";
import {Ticket} from "../../src/model/ticket";
import {Converter} from "../../src/protocol/converter";

import {AddAvatarResponse} from "../../src/protocol/response/addavatarresponse";
import {OpenConnectionResponse} from "../../src/protocol/response/openconnectionresponse";
import {BroadcastMessageResponse} from "../../src/protocol/response/broadcastmessageresponse";
import {WorldModelResponse} from "../../src/protocol/response/worldmodelresponse";
import {InvalidRequestResponse} from "../../src/protocol/response/invalidrequestresponse";
import {AddObjectModelResponse} from "../../src/protocol/response/addobjectmodelresponse";
import {CloseConnectionResponse} from "../../src/protocol/response/closeconnectionresponse";
import {MoveObjectModelResponse} from "../../src/protocol/response/moveobjectmodelresponse";
import {ErrorResponse} from "../../src/protocol/response/errorresponse";
import {OkResponse} from "../../src/protocol/response/okresponse";
import {RemoveAvatarResponse} from "../../src/protocol/response/removeavatarresponse";

let assert = require('assert');

describe("Converter Response Test", () => {

    it('should convert between BroadcastMessageResponse to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("123", "John");
        let ticket = new Ticket("123", "T123", new Date());
        let broadcastMessageResponse = new BroadcastMessageResponse(userMetaData, ticket, "test message");
        broadcastMessageResponse.responseId = "abc123";

        let jsonString = JSON.stringify(broadcastMessageResponse.toJSON());
        let jsonObject = Converter.ConvertResponse(jsonString) as BroadcastMessageResponse;

        assert(jsonObject.requestId == broadcastMessageResponse.requestId);
        assert(jsonObject.message == broadcastMessageResponse.message);
        assert(jsonObject.getUserMetaData().userName == broadcastMessageResponse.getUserMetaData().userName);
        assert(jsonObject.getUserMetaData().userId == broadcastMessageResponse.getUserMetaData().userId);
        assert(jsonObject.getTicket().ticketId == broadcastMessageResponse.getTicket().ticketId);

        done();

    });

    it('should convert between InvalidRequestResponse to JSON and back', (done:Function) => {

        let invalidRequestResponse = new InvalidRequestResponse();
        invalidRequestResponse.responseId = "asdfghjkl1234567890";
        
        let jsonString = JSON.stringify(invalidRequestResponse.toJSON());
        let jsonObject = Converter.ConvertResponse(jsonString) as InvalidRequestResponse;
        assert.deepEqual(jsonObject.responseId, invalidRequestResponse.responseId);

        done();

    });

    it('should convert between LoginResponse to JSON and back', (done:Function) => {

        var tempUser = new User("123", "john");
        tempUser.createdDate = new Date();
        tempUser.email = "abc@abc.com";
        tempUser.language = "en";
        tempUser.lastLoggedInDate = new Date("1/1/2016");

        var userMetaData = tempUser.getMetaData();
        var ticket = new Ticket("123", "T123", new Date());

        let loginResponse = new OpenConnectionResponse(userMetaData, ticket, true);

        let jsonString = JSON.stringify(loginResponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as OpenConnectionResponse;

        assert(jsonObject.requestId == loginResponse.requestId);
        assert(jsonObject.result == loginResponse.result);

        done();

    });

    it('should convert between WorldModelResponse to JSON and back', (done:Function) => {

        let world = new World();

        let circle = new Circle(new Point(3, 5, 6), 1 );
        let objectModel = new ObjectModel("objectmodel1", circle, "tempModelName");
        world.addObjectModel(objectModel);

        let userMetaData = new UserMetaData("ID123", "NAME");
        let ticket = new Ticket("123", "T23", new Date());
        let worldModelResponse = new WorldModelResponse(userMetaData, ticket, world);
        worldModelResponse.requestId = "request_123";

        let jsonString = JSON.stringify(worldModelResponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as WorldModelResponse;

        assert(jsonObject.requestId == worldModelResponse.requestId);
        assert(jsonObject.world.getAllObjectModels().length == 1);

        let cloneObjectModel = jsonObject.world.getAllObjectModels()[0];
        assert( cloneObjectModel.id == objectModel.id );
        assert( cloneObjectModel.modelName == objectModel.modelName );
        assert( cloneObjectModel.shape.equals(objectModel.shape) );

        done();

    });

    it('should convert between AddObjectModelResponse to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("ID123", "NAME");
        let ticket = new Ticket("123", "T23", new Date());        
        let addObjectModelResponse = new AddObjectModelResponse(userMetaData, ticket, true);
        addObjectModelResponse.requestId = "request1234567890";

        let jsonString = JSON.stringify(addObjectModelResponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as AddObjectModelResponse;

        assert.deepEqual(jsonObject.requestId, addObjectModelResponse.requestId);
        assert.deepEqual(jsonObject.result, addObjectModelResponse.result);

        done();

    });
    
    it('should convert between LogoutResponse to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("ID123", "NAME");
        let ticket = new Ticket("123", "T23", new Date());        
        let logoutResponse = new CloseConnectionResponse(userMetaData, ticket, true);
        logoutResponse.requestId = "request1234567890";

        let jsonString = JSON.stringify(logoutResponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as CloseConnectionResponse;

        assert.deepEqual(jsonObject.requestId, logoutResponse.requestId);
        assert.deepEqual(jsonObject.result, logoutResponse.result);
        assert.deepEqual(jsonObject.getUserMetaData(), logoutResponse.getUserMetaData());
        assert.deepEqual(jsonObject.getTicket(), logoutResponse.getTicket());

        done();

    });
    
    
    it('should convert between MoveObjectModelResponse to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("ID12345", "NAME123");
        let ticket = new Ticket("ID12345", "T54123", new Date());        
        let moveObjectResponse = new MoveObjectModelResponse(userMetaData, ticket, new Point(3, 1, 2), true);
        moveObjectResponse.requestId = "ABC123ABC123ABC123";
        moveObjectResponse.objectModelId = "123-456";

        let jsonString = JSON.stringify(moveObjectResponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as MoveObjectModelResponse;

        assert.deepEqual(jsonObject.requestId, moveObjectResponse.requestId);
        assert.deepEqual(jsonObject.result, moveObjectResponse.result);
        assert.deepEqual(jsonObject.getUserMetaData(), moveObjectResponse.getUserMetaData());
        assert.deepEqual(jsonObject.getTicket(), moveObjectResponse.getTicket());
        assert.deepEqual(jsonObject.objectModelId, moveObjectResponse.objectModelId);
        assert.deepEqual(jsonObject.endPoint, moveObjectResponse.endPoint);

        done();

    });

    it('should convert between ErrorResponse to JSON and back', (done:Function) => {
 
        let errorString = "THERE IS AN ERROR HERE";
        let errorResponse = new ErrorResponse(errorString);
        errorResponse.requestId = "ABC123ABC123ABC123";

        let jsonString = JSON.stringify(errorResponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as ErrorResponse;

        assert.deepEqual(jsonObject.requestId, errorResponse.requestId);
        assert.deepEqual(jsonObject.errorMessage, errorResponse.errorMessage);

        done();

    });

    it('should convert between AddAvatarResponse to JSON and back', (done:Function) => {
 
        let userMetaData = new UserMetaData("ID12345", "NAME123");
        let ticket = new Ticket("ID12345", "T54123", new Date());     
        let addAvatarResponse = new AddAvatarResponse(userMetaData, ticket, new Point(32, 40));
        addAvatarResponse.requestId = "ABC123ABC123ABC123";

        let jsonString = JSON.stringify(addAvatarResponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as AddAvatarResponse;

        assert.deepEqual(jsonObject.requestId, addAvatarResponse.requestId);
        assert.deepEqual(jsonObject.result.x, addAvatarResponse.result.x);
        assert.deepEqual(jsonObject.result.y, addAvatarResponse.result.y);
        assert.deepEqual(jsonObject.result.z, addAvatarResponse.result.z);

        done();

    });

    it('should convert between RemoveAvatarResponse to JSON and back', (done:Function) => {
 
        let userMetaData = new UserMetaData("ID12345", "NAME123");
        let ticket = new Ticket("ID12345", "T54123", new Date());     
        let objectModelId = "MODEL1";
        let removeAvatarResponse = new RemoveAvatarResponse(userMetaData, ticket, objectModelId, true);
        removeAvatarResponse.requestId = "ABC123ABC123ABC123";

        let jsonString = JSON.stringify(removeAvatarResponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as RemoveAvatarResponse;

        assert.deepEqual(jsonObject.requestId, removeAvatarResponse.requestId);
        assert.deepEqual(jsonObject.result, removeAvatarResponse.result);
        assert.deepEqual(jsonObject.objectModelId, removeAvatarResponse.objectModelId);

        done();

    });    

    it('should convert between OkResponse to JSON and back', (done:Function) => {
 
        let okReponse = new OkResponse();
        okReponse.requestId = "ABC123ABC123ABC123";

        let jsonString = JSON.stringify(okReponse);
        let jsonObject = Converter.ConvertResponse(jsonString) as OkResponse;

        assert.deepEqual(jsonObject.requestId, okReponse.requestId);

        done();

    });

    it('should not convert between MoveObjectModelResponse with null endpoint', (done:Function) => {

        let userMetaData = new UserMetaData("ID12345", "NAME123");
        let ticket = new Ticket("ID12345", "T54123", new Date());        
        let moveObjectResponse = new MoveObjectModelResponse(userMetaData, ticket, new Point(3, 2), true);
        moveObjectResponse.requestId = "ABC123ABC123ABC123";
        moveObjectResponse.objectModelId = "123-456";

        let jsonString = JSON.stringify(moveObjectResponse);
        let tempJson = JSON.parse(jsonString);
        delete tempJson.endPoint;
        jsonString = JSON.stringify(tempJson);

        try {
            let jsonObject = Converter.ConvertResponse(jsonString) as MoveObjectModelResponse;
            assert.ok(false);
        } catch( error ) {
            assert.ok(true);
        }

        done();

    });

});