import "mocha";

import {UserMetaData} from "../../src/model/usermetadata";
import {User} from "../../src/model/user";
import {ObjectModel} from "../../src/model/objectmodel";
import {World} from "../../src/model/world";
import {Point} from "../../src/math/point";
import {Circle} from "../../src/math/circle";
import {Converter} from "../../src/protocol/converter";
import {Ticket} from "../../src/model/ticket";

import {OpenConnectionRequest} from "../../src/protocol/request/openconnectionrequest";
import {CloseConnectionRequest} from "../../src/protocol/request/closeconnectionrequest";
import {WorldModelRequest} from "../../src/protocol/request/worldmodelrequest";
import {BroadcastMessageRequest} from "../../src/protocol/request/broadcastmessagerequest";
import {AddObjectModelRequest} from "../../src/protocol/request/addobjectmodelrequest";
import {MoveObjectModelRequest} from "../../src/protocol/request/moveobjectmodelrequest";
import {AddAvatarRequest} from "../../src/protocol/request/addavatarrequest";
import {RemoveAvatarRequest} from "../../src/protocol/request/removeavatarrequest";

let assert = require('assert');

describe("Converter Request Test", () => {

    it('should convert between BroadcastMessageRequest to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("123", "John");
        let request = new BroadcastMessageRequest(userMetaData, new Ticket("123", "T", new Date()), "This is a message");

        let jsonString = JSON.stringify(request);
        let jsonObject = Converter.ConvertRequest(jsonString) as BroadcastMessageRequest;

        assert( jsonObject.message == request.message );
        assert( jsonObject.requestId == request.requestId );

        done();

    });

    it('should convert between CloseConnectionRequest to JSON and back', (done:Function) => {

        let ticket = new Ticket("123", "T", new Date());
        let userMetaData = new UserMetaData("123", "John");
        let request = new CloseConnectionRequest(userMetaData, ticket, true);

        let jsonString = JSON.stringify(request);
        let jsonObject = Converter.ConvertRequest(jsonString) as CloseConnectionRequest;

        assert( jsonObject.requestId == request.requestId );

        done();

    });

    it('should convert between OpenConnectionRequest to JSON and back', (done:Function) => {

        let userMetaData: UserMetaData = new UserMetaData("123", "John123");
        let request = new OpenConnectionRequest(userMetaData, new Ticket("123", "T", new Date()));
        request.requestId = "89898989898123";

        let jsonString = JSON.stringify(request);
        let jsonObject = Converter.ConvertRequest(jsonString) as OpenConnectionRequest;

        assert.deepEqual( jsonObject.requestId, request.requestId );
        assert.deepEqual( jsonObject.getUserMetaData().userId, request.getUserMetaData().userId );
        assert.deepEqual( jsonObject.getUserMetaData().userName, request.getUserMetaData().userName );
        assert.deepEqual( jsonObject.getTicket().ticketId, request.getTicket().ticketId );
        assert.deepEqual( jsonObject.getTicket().expirationDate.getTime(), request.getTicket().expirationDate.getTime() );

        done();

    });

    it('should convert between WorldModelRequest to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("123", "John");
        let ticket = new Ticket("123", "T123", new Date());
        let shape = new Circle(new Point(3, 2), 5);
        let request = new WorldModelRequest(userMetaData, ticket, shape);

        let jsonString = JSON.stringify(request);
        let jsonObject = Converter.ConvertRequest(jsonString) as WorldModelRequest;

        assert.deepEqual( jsonObject.requestId.valueOf(), request.requestId.valueOf() );
        assert.deepEqual( jsonObject.getUserMetaData().userName, request.getUserMetaData().userName );
        assert.deepEqual( jsonObject.getUserMetaData().userId, request.getUserMetaData().userId );
        assert( jsonObject.shape.equals(request.shape) );

        done();

    });

    it('should convert between AddObjectModelRequest to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("123", "John");
        let ticket = new Ticket("123", "T123", new Date());

        let shape = new Circle(new Point(3, 2), 5);
        let objectModel = new ObjectModel("OBJ123", shape, "Model Name ABC");
        
        let request = new AddObjectModelRequest(userMetaData, ticket, objectModel);
        request.requestId = "1234567890";

        let jsonString = JSON.stringify(request);
        let jsonObject = Converter.ConvertRequest(jsonString) as AddObjectModelRequest;

        assert.deepEqual( jsonObject.ObjectModel.id, request.ObjectModel.id );
        assert.deepEqual( jsonObject.ObjectModel.modelName, request.ObjectModel.modelName );
        assert.deepEqual( jsonObject.ObjectModel.objectType, request.ObjectModel.objectType );
        assert.deepEqual( jsonObject.ObjectModel.ownerUserId, request.ObjectModel.ownerUserId );
        assert.ok( jsonObject.ObjectModel.shape.equals(request.ObjectModel.shape) );
        assert.deepEqual( jsonObject.requestId, request.requestId );
        
        done();

    });

    it('should convert between MoveObjectModelRequest to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("123", "John");
        let ticket = new Ticket("123", "T123", new Date());

        let request = new MoveObjectModelRequest(userMetaData, ticket, "OBJ123", new Point(3, 2));
        request.requestId = "1234567890";

        let jsonString = JSON.stringify(request);
        let jsonObject = Converter.ConvertRequest(jsonString) as MoveObjectModelRequest;

        assert.deepEqual( jsonObject.objectModelId, request.objectModelId );
        assert.deepEqual( jsonObject.endPoint, request.endPoint );
        assert.deepEqual( jsonObject.getType(), request.getType() );
        assert.deepEqual( jsonObject.getUserMetaData(), request.getUserMetaData() ); 
        assert.deepEqual( jsonObject.getTicket(), request.getTicket() );               
        assert.deepEqual( jsonObject.requestId, request.requestId );
        
        done();

    });

    it('should convert between AddAvatarRequest to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("123", "John");
        let ticket = new Ticket("123", "T123", new Date());

        let request = new AddAvatarRequest(userMetaData, ticket);
        request.requestId = "1234567890";

        let jsonString = JSON.stringify(request);
        let jsonObject = Converter.ConvertRequest(jsonString) as AddAvatarRequest;

        assert.deepEqual( jsonObject.getType(), request.getType() );
        assert.deepEqual( jsonObject.getUserMetaData(), request.getUserMetaData() ); 
        assert.deepEqual( jsonObject.getTicket(), request.getTicket() );               
        assert.deepEqual( jsonObject.requestId, request.requestId );
        
        done();

    });

    it('should convert between RemoveAvatarRequest to JSON and back', (done:Function) => {

        let userMetaData = new UserMetaData("123", "John");
        let ticket = new Ticket("123", "T123", new Date());

        let request = new RemoveAvatarRequest(userMetaData, ticket);
        request.requestId = "1234567890";

        let jsonString = JSON.stringify(request);
        let jsonObject = Converter.ConvertRequest(jsonString) as RemoveAvatarRequest;

        assert.deepEqual( jsonObject.getType(), request.getType() );
        assert.deepEqual( jsonObject.getUserMetaData(), request.getUserMetaData() ); 
        assert.deepEqual( jsonObject.getTicket(), request.getTicket() );               
        assert.deepEqual( jsonObject.requestId, request.requestId );
        
        done();

    });

});
