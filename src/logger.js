"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger.error = function (className, message) {
        Logger.logMessage("ERROR", className, message);
    };
    Logger.warn = function (className, message) {
        Logger.logMessage("WARN", className, message);
    };
    Logger.info = function (className, message) {
        Logger.logMessage("INFO", className, message);
    };
    Logger.debug = function (className, message) {
        Logger.logMessage("DEBUG", className, message);
    };
    Logger.logMessage = function (type, className, message) {
        console.log("[" + type + "] " + Logger.getDateAsString() + " [" + className + "] " + message);
    };
    Logger.getDateAsString = function () {
        var now = new Date();
        var monthStr = now.getMonth() >= 9 ? now.getMonth() + 1 : ("0" + (now.getMonth() + 1));
        var dateStr = now.getDate() >= 9 ? now.getDate() : ("0" + now.getDate());
        var hoursStr = now.getHours() >= 9 ? now.getHours() : ("0" + now.getHours());
        var minuteStr = now.getMinutes() >= 9 ? now.getMinutes() : ("0" + now.getMinutes());
        var secondStr = now.getSeconds() >= 9 ? now.getSeconds() : ("0" + now.getSeconds());
        var dformat = [monthStr, dateStr, now.getFullYear()].join("/") + " " +
            [hoursStr, minuteStr, secondStr].join(":");
        return dformat;
    };
    return Logger;
}());
exports.Logger = Logger;
exports.default = Logger;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2dlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBQUE7SUFrQ0EsQ0FBQztJQWhDaUIsWUFBSyxHQUFuQixVQUFxQixTQUFpQixFQUFFLE9BQWU7UUFDbkQsTUFBTSxDQUFDLFVBQVUsQ0FBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBRSxDQUFDO0lBQ3JELENBQUM7SUFFYSxXQUFJLEdBQWxCLFVBQW9CLFNBQWlCLEVBQUUsT0FBZTtRQUNsRCxNQUFNLENBQUMsVUFBVSxDQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFFLENBQUM7SUFDcEQsQ0FBQztJQUVhLFdBQUksR0FBbEIsVUFBb0IsU0FBaUIsRUFBRSxPQUFlO1FBQ2xELE1BQU0sQ0FBQyxVQUFVLENBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUUsQ0FBQztJQUNwRCxDQUFDO0lBRWEsWUFBSyxHQUFuQixVQUFxQixTQUFpQixFQUFFLE9BQWU7UUFDbkQsTUFBTSxDQUFDLFVBQVUsQ0FBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBRSxDQUFDO0lBQ3JELENBQUM7SUFFYSxpQkFBVSxHQUF4QixVQUF5QixJQUFZLEVBQUUsU0FBaUIsRUFBRSxPQUFlO1FBQ3JFLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLElBQUksR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRSxHQUFHLElBQUksR0FBRyxTQUFTLEdBQUcsSUFBSSxHQUFHLE9BQU8sQ0FBRSxDQUFDO0lBQ25HLENBQUM7SUFFYSxzQkFBZSxHQUE3QjtRQUNJLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDckIsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2RixJQUFJLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBQ3pFLElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDN0UsSUFBSSxTQUFTLEdBQUcsR0FBRyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztRQUNwRixJQUFJLFNBQVMsR0FBRyxHQUFHLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBQ3BGLElBQUksT0FBTyxHQUFHLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRztZQUNoRSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVMLGFBQUM7QUFBRCxDQWxDQSxBQWtDQyxJQUFBO0FBbENZLHdCQUFNO0FBb0NuQixrQkFBZSxNQUFNLENBQUMiLCJmaWxlIjoibG9nZ2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIExvZ2dlciB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBlcnJvciggY2xhc3NOYW1lOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZyApIHtcclxuICAgICAgICBMb2dnZXIubG9nTWVzc2FnZSggXCJFUlJPUlwiLCBjbGFzc05hbWUsIG1lc3NhZ2UgKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIHdhcm4oIGNsYXNzTmFtZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgTG9nZ2VyLmxvZ01lc3NhZ2UoIFwiV0FSTlwiLCBjbGFzc05hbWUsIG1lc3NhZ2UgKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGluZm8oIGNsYXNzTmFtZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgTG9nZ2VyLmxvZ01lc3NhZ2UoIFwiSU5GT1wiLCBjbGFzc05hbWUsIG1lc3NhZ2UgKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGRlYnVnKCBjbGFzc05hbWU6IHN0cmluZywgbWVzc2FnZTogc3RyaW5nICkge1xyXG4gICAgICAgIExvZ2dlci5sb2dNZXNzYWdlKCBcIkRFQlVHXCIsIGNsYXNzTmFtZSwgbWVzc2FnZSApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgbG9nTWVzc2FnZSh0eXBlOiBzdHJpbmcsIGNsYXNzTmFtZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIltcIiArIHR5cGUgKyBcIl0gXCIgKyBMb2dnZXIuZ2V0RGF0ZUFzU3RyaW5nKCkgKyBcIiBbXCIgKyBjbGFzc05hbWUgKyBcIl0gXCIgKyBtZXNzYWdlICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXREYXRlQXNTdHJpbmcoKSB7XHJcbiAgICAgICAgbGV0IG5vdyA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgbGV0IG1vbnRoU3RyID0gbm93LmdldE1vbnRoKCkgPj0gOSA/IG5vdy5nZXRNb250aCgpICsgMSA6IChcIjBcIiArIChub3cuZ2V0TW9udGgoKSArIDEpKTtcclxuICAgICAgICBsZXQgZGF0ZVN0ciA9IG5vdy5nZXREYXRlKCkgPj0gOSA/IG5vdy5nZXREYXRlKCkgOiAoXCIwXCIgKyBub3cuZ2V0RGF0ZSgpKTtcclxuICAgICAgICBsZXQgaG91cnNTdHIgPSBub3cuZ2V0SG91cnMoKSA+PSA5ID8gbm93LmdldEhvdXJzKCkgOiAoXCIwXCIgKyBub3cuZ2V0SG91cnMoKSk7XHJcbiAgICAgICAgbGV0IG1pbnV0ZVN0ciA9IG5vdy5nZXRNaW51dGVzKCkgPj0gOSA/IG5vdy5nZXRNaW51dGVzKCkgOiAoXCIwXCIgKyBub3cuZ2V0TWludXRlcygpKTtcclxuICAgICAgICBsZXQgc2Vjb25kU3RyID0gbm93LmdldFNlY29uZHMoKSA+PSA5ID8gbm93LmdldFNlY29uZHMoKSA6IChcIjBcIiArIG5vdy5nZXRTZWNvbmRzKCkpO1xyXG4gICAgICAgIGxldCBkZm9ybWF0ID0gW21vbnRoU3RyLCBkYXRlU3RyLCBub3cuZ2V0RnVsbFllYXIoKV0uam9pbihcIi9cIikgKyBcIiBcIiArXHJcbiAgICAgICAgICAgIFtob3Vyc1N0ciwgbWludXRlU3RyLCBzZWNvbmRTdHJdLmpvaW4oXCI6XCIpO1xyXG4gICAgICAgIHJldHVybiBkZm9ybWF0O1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgTG9nZ2VyO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
