"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("./point");
var RelativePoint = /** @class */ (function (_super) {
    __extends(RelativePoint, _super);
    function RelativePoint(pointX, pointY, pointZ) {
        var _this = this;
        var tempX = pointX === undefined ? 0 : pointX;
        var tempY = pointY === undefined ? 0 : pointY;
        var tempZ = pointZ === undefined ? 0 : pointZ;
        _this = _super.call(this, tempX, tempY, tempZ) || this;
        _this._nX = tempX;
        _this._nY = tempY;
        _this._nZ = tempZ;
        _this._relative = new point_1.default(0, 0, 0);
        return _this;
    }
    /**
     * Convert a JSON object to a RelativePoint.
     * @param jsonObj
     * @returns {math.Point}
     */
    RelativePoint.fromJSON = function (jsonObj) {
        if (!jsonObj) {
            throw new Error("Cannot convert undefined");
        }
        else if (!jsonObj.hasOwnProperty("x")) {
            throw new Error("X property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("y")) {
            throw new Error("Y property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("z")) {
            throw new Error("Z property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("type")) {
            throw new Error("Type property does not exist");
        }
        else if (jsonObj.type !== RelativePoint.TYPE) {
            throw new Error("Invalid type");
        }
        var tempPoint = new RelativePoint(jsonObj.x, jsonObj.y, jsonObj.z);
        return tempPoint;
    };
    /**
     * Clone this point. The cloned point will be relative to the same point object.s
     */
    RelativePoint.prototype.clone = function () {
        var clonePoint = new RelativePoint(this._nX, this._nY, this._nZ);
        clonePoint.setRelativePoint(this._relative);
        return clonePoint;
    };
    /**
     * This is the point that this relative point is relative to.
     * @param point
     */
    RelativePoint.prototype.setRelativePoint = function (point) {
        this._relative = point;
    };
    Object.defineProperty(RelativePoint.prototype, "x", {
        /**
         * Get the X value.
         * @returns {number}
         */
        get: function () { return this._nX + this._relative.x; },
        /**
         * Set the X value.
         */
        set: function (value) {
            this._nX = value - this._relative.x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RelativePoint.prototype, "y", {
        /**
         * Get the Y value.
         * @returns {number}
         */
        get: function () { return this._nY + this._relative.y; },
        /**
         * Set the Y value.
         */
        set: function (value) {
            this._nY = value - this._relative.y;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RelativePoint.prototype, "z", {
        /**
         * Get the Z value.
         * @returns {number}
         */
        get: function () { return this._nZ + this._relative.z; },
        /**
         * Set the Z value.
         */
        set: function (value) {
            this._nZ = value - this._relative.z;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert this point to JSON.
     */
    RelativePoint.prototype.toJSON = function () {
        return {
            "x": this.x,
            "y": this.y,
            "z": this.z,
            "type": RelativePoint.TYPE
        };
    };
    /**
     * Type of this point.
     */
    RelativePoint.TYPE = "relativepoint";
    return RelativePoint;
}(point_1.default));
exports.RelativePoint = RelativePoint;
exports.default = RelativePoint;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvcmVsYXRpdmVwb2ludC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxpQ0FBNEI7QUFFNUI7SUFBbUMsaUNBQUs7SUFzQ3BDLHVCQUFZLE1BQWMsRUFBRSxNQUFjLEVBQUUsTUFBYztRQUExRCxpQkFVQztRQVRHLElBQUksS0FBSyxHQUFHLE1BQU0sS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzlDLElBQUksS0FBSyxHQUFHLE1BQU0sS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzlDLElBQUksS0FBSyxHQUFHLE1BQU0sS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBRTlDLFFBQUEsa0JBQU8sS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUUsU0FBQztRQUM3QixLQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztRQUNqQixLQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztRQUNqQixLQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztRQUNqQixLQUFJLENBQUMsU0FBUyxHQUFHLElBQUksZUFBSyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7O0lBQ3hDLENBQUM7SUFwQ0Q7Ozs7T0FJRztJQUNXLHNCQUFRLEdBQXRCLFVBQXdCLE9BQVk7UUFFaEMsRUFBRSxDQUFDLENBQUUsQ0FBQyxPQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2IsTUFBTSxJQUFJLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBQ2hELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUN4QyxNQUFNLElBQUksS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDakQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQztRQUNqRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDeEMsTUFBTSxJQUFJLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUMzQyxNQUFNLElBQUksS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDcEQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxPQUFPLENBQUMsSUFBSSxLQUFNLGFBQWEsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2hELE1BQU0sSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUVELElBQUksU0FBUyxHQUFHLElBQUksYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkUsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUVyQixDQUFDO0lBY0Q7O09BRUc7SUFDSSw2QkFBSyxHQUFaO1FBQ0ksSUFBSSxVQUFVLEdBQUcsSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqRSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVDLE1BQU0sQ0FBQyxVQUFVLENBQUM7SUFDdEIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLHdDQUFnQixHQUF2QixVQUF3QixLQUFZO1FBQ2hDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFNRCxzQkFBSSw0QkFBQztRQUpMOzs7V0FHRzthQUNILGNBQVUsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRS9DOztXQUVHO2FBQ0gsVUFBTSxLQUFhO1lBQ2YsSUFBSSxDQUFDLEdBQUcsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDeEMsQ0FBQzs7O09BUDhDO0lBYS9DLHNCQUFJLDRCQUFDO1FBSkw7OztXQUdHO2FBQ0gsY0FBVSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFL0M7O1dBRUc7YUFDSCxVQUFNLEtBQWE7WUFDZixJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUN4QyxDQUFDOzs7T0FQOEM7SUFhL0Msc0JBQUksNEJBQUM7UUFKTDs7O1dBR0c7YUFDSCxjQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUUvQzs7V0FFRzthQUNILFVBQU0sS0FBYTtZQUNmLElBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7OztPQVA4QztJQVMvQzs7T0FFRztJQUNJLDhCQUFNLEdBQWI7UUFDSSxNQUFNLENBQUM7WUFDSCxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDWCxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDWCxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDWCxNQUFNLEVBQUcsYUFBYSxDQUFDLElBQUk7U0FDOUIsQ0FBQztJQUNOLENBQUM7SUFsSEQ7O09BRUc7SUFDVyxrQkFBSSxHQUFHLGVBQWUsQ0FBQztJQWlIekMsb0JBQUM7Q0F0SEQsQUFzSEMsQ0F0SGtDLGVBQUssR0FzSHZDO0FBdEhZLHNDQUFhO0FBd0gxQixrQkFBZSxhQUFhLENBQUMiLCJmaWxlIjoibWF0aC9yZWxhdGl2ZXBvaW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFBvaW50IGZyb20gXCIuL3BvaW50XCI7XHJcblxyXG5leHBvcnQgY2xhc3MgUmVsYXRpdmVQb2ludCBleHRlbmRzIFBvaW50IHtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFR5cGUgb2YgdGhpcyBwb2ludC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJyZWxhdGl2ZXBvaW50XCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfblg6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX25ZOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9uWjogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfcmVsYXRpdmU6IFBvaW50O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBhIEpTT04gb2JqZWN0IHRvIGEgUmVsYXRpdmVQb2ludC5cclxuICAgICAqIEBwYXJhbSBqc29uT2JqXHJcbiAgICAgKiBAcmV0dXJucyB7bWF0aC5Qb2ludH1cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTigganNvbk9iajogYW55ICk6IFJlbGF0aXZlUG9pbnQgIHtcclxuXHJcbiAgICAgICAgaWYgKCAhanNvbk9iaiApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGNvbnZlcnQgdW5kZWZpbmVkXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoICFqc29uT2JqLmhhc093blByb3BlcnR5KFwieFwiKSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiWCBwcm9wZXJ0eSBkb2VzIG5vdCBleGlzdFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCAhanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcInlcIikgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlkgcHJvcGVydHkgZG9lcyBub3QgZXhpc3RcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICggIWpzb25PYmouaGFzT3duUHJvcGVydHkoXCJ6XCIpICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJaIHByb3BlcnR5IGRvZXMgbm90IGV4aXN0XCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoICFqc29uT2JqLmhhc093blByb3BlcnR5KFwidHlwZVwiKSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiVHlwZSBwcm9wZXJ0eSBkb2VzIG5vdCBleGlzdFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCBqc29uT2JqLnR5cGUgIT09ICBSZWxhdGl2ZVBvaW50LlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgdHlwZVwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCB0ZW1wUG9pbnQgPSBuZXcgUmVsYXRpdmVQb2ludChqc29uT2JqLngsIGpzb25PYmoueSwganNvbk9iai56KTtcclxuICAgICAgICByZXR1cm4gdGVtcFBvaW50O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwb2ludFg6IG51bWJlciwgcG9pbnRZOiBudW1iZXIsIHBvaW50WjogbnVtYmVyICkge1xyXG4gICAgICAgIGxldCB0ZW1wWCA9IHBvaW50WCA9PT0gdW5kZWZpbmVkID8gMCA6IHBvaW50WDtcclxuICAgICAgICBsZXQgdGVtcFkgPSBwb2ludFkgPT09IHVuZGVmaW5lZCA/IDAgOiBwb2ludFk7XHJcbiAgICAgICAgbGV0IHRlbXBaID0gcG9pbnRaID09PSB1bmRlZmluZWQgPyAwIDogcG9pbnRaO1xyXG5cclxuICAgICAgICBzdXBlciggdGVtcFgsIHRlbXBZLCB0ZW1wWiApO1xyXG4gICAgICAgIHRoaXMuX25YID0gdGVtcFg7XHJcbiAgICAgICAgdGhpcy5fblkgPSB0ZW1wWTtcclxuICAgICAgICB0aGlzLl9uWiA9IHRlbXBaO1xyXG4gICAgICAgIHRoaXMuX3JlbGF0aXZlID0gbmV3IFBvaW50KDAsIDAsIDApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2xvbmUgdGhpcyBwb2ludC4gVGhlIGNsb25lZCBwb2ludCB3aWxsIGJlIHJlbGF0aXZlIHRvIHRoZSBzYW1lIHBvaW50IG9iamVjdC5zXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBjbG9uZSgpOiBSZWxhdGl2ZVBvaW50IHtcclxuICAgICAgICBsZXQgY2xvbmVQb2ludCA9IG5ldyBSZWxhdGl2ZVBvaW50KHRoaXMuX25YLCB0aGlzLl9uWSwgdGhpcy5fblopO1xyXG4gICAgICAgIGNsb25lUG9pbnQuc2V0UmVsYXRpdmVQb2ludCh0aGlzLl9yZWxhdGl2ZSk7XHJcbiAgICAgICAgcmV0dXJuIGNsb25lUG9pbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGlzIGlzIHRoZSBwb2ludCB0aGF0IHRoaXMgcmVsYXRpdmUgcG9pbnQgaXMgcmVsYXRpdmUgdG8uXHJcbiAgICAgKiBAcGFyYW0gcG9pbnRcclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldFJlbGF0aXZlUG9pbnQocG9pbnQ6IFBvaW50KSB7XHJcbiAgICAgICAgdGhpcy5fcmVsYXRpdmUgPSBwb2ludDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgWCB2YWx1ZS5cclxuICAgICAqIEByZXR1cm5zIHtudW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIGdldCB4KCkgeyByZXR1cm4gdGhpcy5fblggKyB0aGlzLl9yZWxhdGl2ZS54OyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIFggdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIHNldCB4KHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9uWCA9IHZhbHVlIC0gdGhpcy5fcmVsYXRpdmUueDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgWSB2YWx1ZS5cclxuICAgICAqIEByZXR1cm5zIHtudW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIGdldCB5KCkgeyByZXR1cm4gdGhpcy5fblkgKyB0aGlzLl9yZWxhdGl2ZS55OyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIFkgdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIHNldCB5KHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9uWSA9IHZhbHVlIC0gdGhpcy5fcmVsYXRpdmUueTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgWiB2YWx1ZS5cclxuICAgICAqIEByZXR1cm5zIHtudW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIGdldCB6KCkgeyByZXR1cm4gdGhpcy5fblogKyB0aGlzLl9yZWxhdGl2ZS56OyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIFogdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIHNldCB6KHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9uWiA9IHZhbHVlIC0gdGhpcy5fcmVsYXRpdmUuejtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyBwb2ludCB0byBKU09OLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIFwieFwiOiB0aGlzLngsXHJcbiAgICAgICAgICAgIFwieVwiOiB0aGlzLnksXHJcbiAgICAgICAgICAgIFwielwiOiB0aGlzLnosXHJcbiAgICAgICAgICAgIFwidHlwZVwiIDogUmVsYXRpdmVQb2ludC5UWVBFXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFJlbGF0aXZlUG9pbnQ7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
