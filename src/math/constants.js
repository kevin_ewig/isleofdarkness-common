"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    Constants.CIRCLE = "circle";
    Constants.POLYGON = "polygon";
    Constants.RECTANGLE = "rectangle";
    return Constants;
}());
exports.Constants = Constants;
exports.default = Constants;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7SUFBQTtJQU1BLENBQUM7SUFKaUIsZ0JBQU0sR0FBRyxRQUFRLENBQUM7SUFDbEIsaUJBQU8sR0FBRyxTQUFTLENBQUM7SUFDcEIsbUJBQVMsR0FBRyxXQUFXLENBQUM7SUFFMUMsZ0JBQUM7Q0FORCxBQU1DLElBQUE7QUFOWSw4QkFBUztBQVF0QixrQkFBZSxTQUFTLENBQUMiLCJmaWxlIjoibWF0aC9jb25zdGFudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgQ29uc3RhbnRzIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIENJUkNMRSA9IFwiY2lyY2xlXCI7XHJcbiAgICBwdWJsaWMgc3RhdGljIFBPTFlHT04gPSBcInBvbHlnb25cIjtcclxuICAgIHB1YmxpYyBzdGF0aWMgUkVDVEFOR0xFID0gXCJyZWN0YW5nbGVcIjtcclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IENvbnN0YW50cztcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
