import Point from "./point";
import Shape from "./shape";
import Circle from "./circle";
import Polygon from "./polygon";
import Rectangle from "./rectangle";
import Line from "./line";
import Vector from "./vector";
import RelativePoint from "./relativepoint";
import {Common} from "./common";
import Constants from "./constants";

export class Intersecter {

    /**
     * Check if two geometry objects shapes intersect each other.
     */
    public static isShapeIntersectShape = function (geom1: Shape, geom2: Shape): boolean {

        let geom1Type = geom1.type;
        let geom2Type = geom2.type;

        if (geom1Type === Constants.CIRCLE && geom2Type === Constants.CIRCLE) {
            let circle1 = geom1 as Circle;
            let circle2 = geom2 as Circle;
            return Intersecter.isCircleIntersectCircle(circle1, circle2);
        }
        if (geom1Type === Constants.POLYGON && geom2Type === Constants.POLYGON) {
            let poly1 = geom1 as Polygon;
            let poly2 = geom2 as Polygon;
            return Intersecter.isPolygonIntersectPolygon(poly1, poly2);
        }
        if ( geom1Type === Constants.RECTANGLE && geom2Type === Constants.RECTANGLE ) {
            let rect1 = geom1 as Rectangle;
            let rect2 = geom2 as Rectangle;
            return Intersecter.isRectangleIntersectRectangle(rect1, rect2);
        }

        // Circle - Polygon
        if (geom1Type === Constants.CIRCLE && geom2Type === Constants.POLYGON) {
            let circle = geom1 as Circle;
            let poly = geom2 as Polygon;
            return Intersecter.isCircleIntersectPolygon(circle, poly);
        }
        if (geom1Type === Constants.POLYGON && geom2Type === Constants.CIRCLE) {
            let circle = geom2 as Circle;
            let poly = geom1 as Polygon;
            return Intersecter.isCircleIntersectPolygon(circle, poly);
        }

        // Circle - Rectangle
        if (geom1Type === Constants.CIRCLE && geom2Type === Constants.RECTANGLE) {
            let circle = geom1 as Circle;
            let rect = geom2 as Rectangle;
            return Intersecter.isRectangleIntersectCircle(rect, circle);
        }
        if (geom1Type === Constants.RECTANGLE && geom2Type === Constants.CIRCLE) {
            let circle = geom2 as Circle;
            let rect = geom1 as Rectangle;
            return Intersecter.isRectangleIntersectCircle(rect, circle);
        }

        // Rectangle - Polygon
        if (geom1Type === Constants.POLYGON && geom2Type === Constants.RECTANGLE) {
            let poly = geom1 as Polygon;
            let rect = geom2 as Rectangle;
            return Intersecter.isRectangleIntersectPolygon(rect, poly);
        }
        if (geom1Type === Constants.RECTANGLE && geom2Type === Constants.POLYGON) {
            let poly = geom2 as Polygon;
            let rect = geom1 as Rectangle;
            return Intersecter.isRectangleIntersectPolygon(rect, poly);
        }

        return false;

    };


    /**
     * Check if a geometry shape intersect a point.
     */
    public static isShapeIntersectPoint = function (geom: Shape, point: Point): boolean {

        let geomType = geom.type;

        if (geomType === Constants.CIRCLE) {
            let circle = geom as Circle;
            return Intersecter.isPointIntersectCircle(point, circle);
        }
        if (geomType === Constants.POLYGON) {
            let polygon = geom as Polygon;
            return Intersecter.isPointIntersectPolygon(point, polygon);
        }
        return false;

    };


    /**
     * Checks to see if a point is on a line.
     * Source: http://stackoverflow.com/questions/7050186/find-if-point-lays-on-line-segment
     * @param point
     * @param line
     * @returns {boolean}
     */
    public static isPointOnLine = function (point: Point, line: Line): boolean {

        let epsilon = 0.01;
        let pt1: Point = line.getStartPoint();
        let pt2: Point = line.getEndPoint();
        let pt: Point = point;

        if (pt.x - Math.max(pt1.x, pt2.x) > epsilon ||
            Math.min(pt1.x, pt2.x) - pt.x > epsilon ||
            pt.y - Math.max(pt1.y, pt2.y) > epsilon ||
            Math.min(pt1.y, pt2.y) - pt.y > epsilon) {
            return false;
        }

        if (Math.abs(pt2.x - pt1.x) < epsilon) {
            return Math.abs(pt1.x - pt.x) < epsilon || Math.abs(pt2.x - pt.x) < epsilon;
        }

        if (Math.abs(pt2.y - pt1.y) < epsilon) {
            return Math.abs(pt1.y - pt.y) < epsilon || Math.abs(pt2.y - pt.y) < epsilon;
        }

        let x: number = pt1.x + (pt.y - pt1.y) * (pt2.x - pt1.x) / (pt2.y - pt1.y);
        let y: number = pt1.y + (pt.x - pt1.x) * (pt2.y - pt1.y) / (pt2.x - pt1.x);

        return Math.abs(pt.x - x) < epsilon || Math.abs(pt.y - y) < epsilon;

    };

    /**
     * Check if 2 line segments intersect.
     * Source: https://stackoverflow.com/questions/4543506/algorithm-for-intersection-of-2-lines
     * @param line1
     * @param line2
     */
    public static isLineIntersectLine(line1: Line, line2: Line): boolean {
        let determinant = line1.a * line2.b - line2.a * line1.b;
        return determinant !== 0;
    }

    /**
     * Determine if two circles intersect or overlap.
     *
     * If d is the distance between the center of the two circles, and r0 is the
     * radius of circle 1 and r1 is the radius of circle 2, then:
     *
     * If d > r0 + r1: The circles are too far apart to intersect.
     * If d < |r0 – r1|: One circle is inside the other so there is no intersection.
     * If d = 0 and r0 = r1: The circles are the same.
     * If d = r0 + r1: The circles touch at a single point.
     * Otherwise: The circles touch at two points.
     * (Now d = r0 + r1 because if not the objects steer way clear of each other)
     * @param circle1
     * @param circle2
     */
    public static isCircleIntersectCircle(circle1: Circle, circle2: Circle) {

        let d: number = Common.distance2d(circle1.x, circle1.y, circle2.x, circle2.y);
        let r0: number = circle1.getRadius();
        let r1: number = circle2.getRadius();

        return (d < r0 + r1);

    }

    /**
     * Check if two rectangles intersect.
     * https://stackoverflow.com/questions/13390333/two-rectangles-intersection
     * @param rect1
     * @param rect2
     */
    public static isRectangleIntersectRectangle(rect1: Rectangle, rect2: Rectangle) {
        let x1: number = rect1.position().x;
        let y1: number = rect1.position().y;
        let width1: number = rect1.width();
        let height1: number = rect1.height();

        let x2: number = rect2.position().x;
        let y2: number = rect2.position().y;
        let width2: number = rect2.width();
        let height2: number = rect2.height();
        return !(x1 > x2 + width2 || x1 + width1 < x2 || y1 > y2 + height2 || y1 + height1 < y2);
    }

    /**
     * Check if circle intersects rectangle.
     * @param rect
     * @param circle
     */
    public static isRectangleIntersectCircle(rect: Rectangle, circle: Circle) {

        let circleDistanceX: number = Math.abs(circle.x - rect.x);
        let circleDistanceY: number =  Math.abs(circle.y - rect.y);

        if (circleDistanceX > (rect.width() / 2 + circle.getRadius())) { return false; }
        if (circleDistanceY > (rect.height() / 2 + circle.getRadius())) { return false; }

        if (circleDistanceX <= (rect.width() / 2)) { return true; }
        if (circleDistanceY <= (rect.height() / 2)) { return true; }

        let cornerDistanceSq = (circleDistanceX - rect.width() / 2) * (circleDistanceX - rect.width() / 2) +
                               (circleDistanceY - rect.height() / 2) * (circleDistanceY - rect.height() / 2);

        return (cornerDistanceSq <= circle.getRadius() * circle.getRadius());

    }

    /**
     * Determines if a point is inside a circle
     * @param point
     * @param circle
     * @returns {boolean} True if the point is inside the circle. False otherwise.
     */
    public static isPointIntersectCircle(point: Point, circle: Circle): boolean {

        let radius: number = circle.getRadius();
        let dist: number = Common.distance2d(circle.x, circle.y, point.x, point.y);
        return ( dist <= radius );

    }

    /**
     * Check if a point is inside a polygon.
     *
     * @param point
     * @param poly
     * @returns {boolean}
     */
    public static isPointIntersectPolygon(point: Point, poly: Polygon): boolean {

        let i = 0, j = 0;
        let c = false;
        let vertices: RelativePoint[] = poly.getArrayOfPoints();
        let numberOfVertices: number = vertices.length;

        for (i = 0, j = numberOfVertices - 1; i < numberOfVertices; j = i++) {
            if (((vertices[i].y > point.y) !== (vertices[j].y > point.y)) &&
                (point.x < (vertices[j].x - vertices[i].x) * (point.y - vertices[i].y) / (vertices[j].y - vertices[i].y) + vertices[i].x)) {
                c = !c;
            }
        }

        return c;

    }

    /**
     * Test to see if a line intersects a circle.
     * Source: http://stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm
     * @param line
     * @param circle
     */
    public static isLineIntersectCircle(line: Line, circle: Circle): boolean {

        // Direction vector of ray, from start to end )
        let d = Vector.subtract(line.getStartPoint(), line.getEndPoint());

        // Vector from center sphere to ray start )
        let f = Vector.subtract(circle.getCenter(), line.getStartPoint());

        let r = circle.getRadius();
        let a = d.dot(d);
        let b = 2 * f.dot(d);
        let c = f.dot(f) - r * r;

        let discriminant = b * b - 4 * a * c;
        if (discriminant < 0) {
            return false;
        } else {

            // Ray didn"t totally miss sphere, so there is a solution to the equation.
            discriminant = Math.sqrt(discriminant);

            // either solution may be on or off the ray so need to test both t1 is always the smaller value, because
            // BOTH discriminant and a are non-negative.
            let t1 = (-b - discriminant) / (2 * a);
            let t2 = (-b + discriminant) / (2 * a);

            if (t1 >= 0 && t1 <= 1) {
                return true;
            }

            // Here t1 didn"t intersect so we are either started inside the sphere or completely past it.
            if (t2 >= 0 && t2 <= 1) {
                return true;
            }

            // No intersection.
            return false;

        }
    }

    /**
     * Test to see if a circle intersects a polygon.
     * @param circle
     * @param polygon
     */
    public static isCircleIntersectPolygon(circle: Circle, polygon: Polygon): boolean {

        // If center of the circle is in the polygon, then it intersects.
        if (Intersecter.isPointIntersectPolygon(circle.getCenter(), polygon)) {
            return true;
        }

        // If line intersects the circle, then it intersects.
        let arrayOfPoints: RelativePoint[] = polygon.getArrayOfPoints();
        for (let i = 0; i < arrayOfPoints.length - 1; i++) {
            let line = new Line(arrayOfPoints[i], arrayOfPoints[i + 1]);
            if (Intersecter.isLineIntersectCircle(line, circle)) {
                return true;
            }
        }

        return false;

    }

    /**
     * Test to see if a rectangle intersects a polygon.
     * @param rect
     * @param polygon
     */
    public static isRectangleIntersectPolygon(rect: Rectangle, polygon: Polygon): boolean {

        // If center of the rectangle is in the polygon, then it intersects.
        if (Intersecter.isPointIntersectPolygon(rect.getCenter(), polygon)) {
            return true;
        }

        // If line intersects the rectangle, then it intersects.
        let arrayOfPoints: RelativePoint[] = polygon.getArrayOfPoints();
        for (let i = 0; i < arrayOfPoints.length - 1; i++) {
            let line = new Line(arrayOfPoints[i], arrayOfPoints[i + 1]);
            if (Intersecter.isLineIntersectRectangle(line, rect)) {
                return true;
            }
        }

        return false;

    }

    /**
     * Test to see if a line intersects a rectangle.
     * Source: https://stackoverflow.com/questions/99353/how-to-test-if-a-line-segment-intersects-an-axis-aligned-rectange-in-2d
     * @param line
     * @param rect
     */
    public static isLineIntersectRectangle(line: Line, rect: Rectangle): boolean {

        let a_rectangleMinX: number = rect.position().x;
        let a_rectangleMinY: number = rect.position().y;
        let a_rectangleMaxX: number = rect.position().x + rect.width();
        let a_rectangleMaxY: number = rect.position().y + rect.height();
        let a_p1x: number = line.getStartPoint().x;
        let a_p1y: number = line.getStartPoint().y;
        let a_p2x: number = line.getEndPoint().x;
        let a_p2y: number = line.getEndPoint().y;

        // Find min and max X for the segment
        let minX: number = a_p1x;
        let maxX: number = a_p2x;

        if (a_p1x > a_p2x) {
            minX = a_p2x;
            maxX = a_p1x;
        }

        // Find the intersection of the segment's and rectangle's x-projections
        if (maxX > a_rectangleMaxX) {
            maxX = a_rectangleMaxX;
        }
        if (minX < a_rectangleMinX) {
            minX = a_rectangleMinX;
        }

        // If their projections do not intersect return false
        if (minX > maxX) {
        return false;
        }

        // Find corresponding min and max Y for min and max X we found before
        let minY: number = a_p1y;
        let maxY: number = a_p2y;
        let dx: number = a_p2x - a_p1x;

        if ( Math.abs(dx) > 0.0000001 ) {
            let a: number = (a_p2y - a_p1y) / dx;
            let b: number = a_p1y - a * a_p1x;
            minY = a * minX + b;
            maxY = a * maxX + b;
        }

        if ( minY > maxY ) {
            let tmp: number = maxY;
            maxY = minY;
            minY = tmp;
        }

        // Find the intersection of the segment's and rectangle's y-projections
        if ( maxY > a_rectangleMaxY ) {
            maxY = a_rectangleMaxY;
        }
        if ( minY < a_rectangleMinY ) {
            minY = a_rectangleMinY;
        }

        // If Y-projections do not intersect return false
        if ( minY > maxY ) {
            return false;
        }

        return true;
    }

    /**
     * Checks to see if two polygons intersect. Uses the axis-separation test.
     *
     * Source: http://www.dyn4j.org/2010/01/sat/
     *
     * @param poly1
     * @param poly2
     */
    public static isPolygonIntersectPolygon = function (poly1: Polygon, poly2: Polygon) {

        //
        // Generate a list of axis (normalized vectors) from an array of points in
        // the polygon.
        //
        function generateAxis(polyPoints: any) {
            let axis = [];
            for (let i = 0; i < polyPoints.length - 1; i++) {
                let polyPointA = polyPoints[i];
                let polyPointB = polyPoints[i + 1 === polyPoints.length ? 0 : i + 1];

                let vector = new Vector(polyPointB.x - polyPointA.x, polyPointB.y - polyPointA.y);
                vector.normalize();
                vector.perpendicular();
                axis.push(vector);
            }
            return axis;
        }

        let poly1Points: RelativePoint[] = poly1.getArrayOfPoints();
        let poly2Points: RelativePoint[] = poly2.getArrayOfPoints();

        let axes1 = generateAxis(poly1Points);
        let axes2 = generateAxis(poly2Points);

        for (let i = 0; i < axes1.length; i++) {
            let axis: Vector = axes1[i];
            // project both shapes onto the axis
            let p1 = new Projection(poly1, axis);
            let p2 = new Projection(poly2, axis);
            // do the projections overlap?
            if (!p1.overlap(p2)) {
                // then we can guarantee that the shapes do not overlap
                return false;
            }
        }

        for (let i = 0; i < axes2.length; i++) {
            let axis = axes2[i];
            // project both shapes onto the axis
            let p1 = new Projection(poly1, axis);
            let p2 = new Projection(poly2, axis);
            // do the projections overlap?
            if (!p1.overlap(p2)) {
                // then we can guarantee that the shapes do not overlap
                return false;
            }
        }
        // if we get here then we know that every axis had overlap on it
        // so we can guarantee an intersection
        return true;
    };

}

/**
 * A projection object based on projecting the points of a polygon to the
 * given axis.
 * @param polygon A convex polygon.
 * @param axis A vector.
 */
class Projection {

    private _min: number;
    private _max: number;

    constructor(polygon: Polygon, axis: Vector) {
        let points = polygon.getArrayOfPoints();
        this._min = axis.dot2(points[0].x, points[0].y, points[0].z);
        this._max = this._min;
        for (let ii = 1; ii < points.length; ii++) {
            // NOTE: the axis must be normalized to get accurate projections
            let p = axis.dot2(points[ii].x, points[ii].y, points[ii].z);
            if (p < this._min) {
                this._min = p;
            } else if (p > this._max) {
                this._max = p;
            }
        }
    }

    public getMin(): number {
        return this._min;
    }

    public getMax(): number {
        return this._max;
    }

    public overlap(projection2: any): boolean {
        if (this._max > projection2.getMin()) {
            return true;
        }
        if (this._min > projection2.getMax()) {
            return true;
        }
        return false;
    }

}

export default Intersecter;
