import Point from "./point";

export class Line {

    public static TYPE = "line";

    private _a: number;
    private _b: number;
    private _c: number;
    private _pointStart: Point;
    private _pointEnd: Point;

    /**
     * Convert JSON object to a Line object.
     * @param jsonObj
     * @returns {math.Line}
     */
    public static fromJSON(jsonObj: any): Line {

        if ( !jsonObj ) {
            throw new Error("Cannot convert undefined to Line");
        } else if ( !jsonObj.hasOwnProperty("pointStart") ) {
            throw new Error("pointStart property does not exist");
        } else if ( !jsonObj.hasOwnProperty("pointEnd") ) {
            throw new Error("pointEnd property does not exist");
        } else if ( !jsonObj.hasOwnProperty("type") ) {
            throw new Error("Type property does not exist");
        } else if ( jsonObj.type !== Line.TYPE ) {
            throw new Error("Invalid type");
        }

        let pointStart = Point.fromJSON(jsonObj.pointStart);
        let pointEnd = Point.fromJSON(jsonObj.pointEnd);
        return new Line(pointStart, pointEnd);

    }

    constructor( pointStart: Point, pointEnd: Point ) {
        this._pointStart = pointStart;
        this._pointEnd = pointEnd;
        this._a = pointEnd.y - pointStart.y;
        this._b = pointStart.x - pointEnd.x;
        this._c = this._a * pointStart.x + this._b * pointStart.y;
    }

    /**
     * Get the A value.
     * @returns {number}
     */
    get a() { return this._a; }

    /**
     * Get the B value.
     * @returns {number}
     */
    get b() { return this._b; }

    /**
     * Get the C value.
     * @returns {number}
     */
    get c() { return this._c; }

    /**
     * Clone this line.
     * @returns {math.Line}
     */
    public clone(): Line {
        return new Line(this._pointStart, this._pointEnd);
    }

    /**
     * Returns the angle (in degrees) for this line
     * relative to the x-axis.
     * @returns {number}
     */
    public getAngle(): number {
        let radians = this.getRadiansAngle();
        return radians * 180 / Math.PI;
    }

    /**
     * Get the center of this line.
     */
    public getCenter(): Point {
        let mx = (this._pointStart.x + this._pointEnd.x) / 2;
        let my = (this._pointStart.y + this._pointEnd.y) / 2;
        let mz = (this._pointStart.z + this._pointEnd.z) / 2;
        return new Point(mx, my, mz);
    }

    /**
     * Get the end point of this line segment.
     * @returns {Point}
     */
    public getEndPoint(): Point {
        return this._pointEnd;
    }

    /**
     * Returns the length of this line segment.
     * @returns {number}
     */
    public getLength(): number {
        let ax = this._pointStart.x;
        let ay = this._pointStart.y;
        let bx = this._pointEnd.x;
        let by = this._pointEnd.y;
        return Math.sqrt( (ax - bx) * (ax - bx) + (ay - by) * (ay - by) );
    }

    /**
     * Returns the angle (in radians) for this line
     * relative to the x-axis.
     * @returns {number}
     */
    public getRadiansAngle(): number {

        let ax = this._pointStart.x;
        let ay = this._pointStart.y;

        // Shift the end point relative to origin and normalize it.
        let dx = (this._pointEnd.x - ax);
        let dy = (this._pointEnd.y - ay);

        return Math.atan2(dy, dx);

    }

    /**
     * Get the start point of this line segment.
     * @returns {Point}
     */
    public getStartPoint(): Point {
        return this._pointStart;
    }

    /**
     * Convert line to JSON.
     */
    public toJSON(): any {
        return {
            "pointStart": this._pointStart.toJSON(),
            "pointEnd": this._pointEnd.toJSON(),
            "type" : Line.TYPE
        };
    }


    /**
     * Get the type of this object.
     */
    get type(): string {
        return Line.TYPE;
    }

}

export default Line;
