import Point from "./point";
import Shape from "./shape";
import Constants from "./constants";

export class Circle extends Shape {

    private _centerPoint: Point;
    private _circleRadius: number;
    private _linkPosition: any;

    /**
     * Convert JSON to math.Circle object.
     * @param jsonObj
     * @returns {math.Circle}
     */
    public static fromJSON(jsonObj: any) {

        if (!jsonObj) {
            throw new Error("Cannot convert undefined to Circle");
        } else if (!jsonObj.hasOwnProperty("center")) {
            throw new Error("Center property does not exist");
        } else if (!jsonObj.hasOwnProperty("radius")) {
            throw new Error("Radius property does not exist");
        } else if (!jsonObj.hasOwnProperty("type")) {
            throw new Error("Type property does not exist");
        } else if (jsonObj.type !== Constants.CIRCLE) {
            throw new Error("Invalid type");
        }

        let centerPoint = Point.fromJSON(jsonObj.center);
        let radius = jsonObj.radius;

        return new Circle(centerPoint, radius);

    }

    /**
     * Consruct a circle object.
     * @param centerPoint
     * @param circleRadius
     */
    constructor(centerPoint: Point, circleRadius: number) {
        super();
        this._centerPoint = centerPoint;
        this._circleRadius = circleRadius;
    }

    /**
     * Get adjacent shapes. Include diagonals. This is used for path-finding.
     */
    public getAdjacentShapes(): Shape[] {
        let list = [];
        let doubleRadius = this._circleRadius * 2;
        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                if (i !== 0 || j !== 0) {
                    let point = new Point(this._centerPoint.x + i * doubleRadius, this._centerPoint.y + j * doubleRadius,
                        this._centerPoint.z);
                    let circle = new Circle(point, this._circleRadius);
                    list.push(circle);
                }
            }
        }
        return list;
    }

    /**
     * Clone this circle.
     */
    public clone(): Shape {
        return new Circle(this._centerPoint, this._circleRadius);
    }

    /**
     * Get the center of this circle.
     */
    public getCenter(): Point {
        return this._centerPoint;
    }

    /**
     * Get a point related to the center of this shape.
     */
    public getPosition(): Point {
        return this._centerPoint;
    }

    /**
     * Get the radius of this circle.
     */
    public getRadius(): number {
        return this._circleRadius;
    }

    /**
     * Get the Z value relative to a given point. In the case of a circle
     * the z is always the circle's z value.
     */
    public getZRelativeToPoint(point: Point): number {
        return this.z;
    }

    /**
     * Get the type of this polygon.
     */
    get type(): string {
        return Constants.CIRCLE;
    }

    /**
     * Get the X coordinate of the center of this shape.
     */
    get x(): number {
        return this._centerPoint.x;
    }

    /**
     * Get the Y coordinate of the center of this shape.
     */
    get y(): number {
        return this._centerPoint.y;
    }

    /**
     * Get the Z coordinate of the center of this shape.
     */
    get z(): number {
        return this._centerPoint.z;
    }

    /**
     * Set the X coordinate of the center of this shape.
     */
    set x(value: number) {
        this._centerPoint.x = value;
        if (this._linkPosition) {
            this._linkPosition.x = value;
        }
    }

    /**
     * Set the Y coordinate of the center of this shape.
     */
    set y(value: number) {
        this._centerPoint.y = value;
        if (this._linkPosition) {
            this._linkPosition.y = value;
        }
    }

    /**
     * Set the Z coordinate of the center of this shape.
     */
    set z(value: number) {
        this._centerPoint.z = value;
        if (this._linkPosition) {
            this._linkPosition.z = value;
        }
    }

    /**
     * Set the center of the circle.
     */
    public setCenter(aPoint: Point): void {
        this._centerPoint = aPoint;
        if (this._linkPosition) {
            this._linkPosition.x = aPoint.x;
            this._linkPosition.y = aPoint.y;
            this._linkPosition.z = aPoint.z;
        }
    }

    /**
     * Set the radius of the circle.
     */
    public setRadius(aRadius: number): void {
        this._circleRadius = aRadius;
    }

    /**
     * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
     * shape changes the object of this link position also changes.
     */
    get linkPosition(): any {
        return this._linkPosition;
    }

    /**
     * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
     * shape changes the object of this link position also changes.
     */
    set linkPosition(value: any) {
        if (!value) {
            throw new Error("Value is null");
        } else if (value.hasOwnProperty("x") && value.hasOwnProperty("y") && value.hasOwnProperty("z")) {
            this._linkPosition = value;
            this._linkPosition.x = this._centerPoint.x;
            this._linkPosition.y = this._centerPoint.y;
            this._linkPosition.z = this._centerPoint.z;
        } else {
            throw new Error("Value does not have all the required properties");
        }
    }

    /**
     * Determine if a circle equals this circle.
     */
    public equals(aCircleObj: Circle): boolean {
        if (aCircleObj.getCenter().equals(this.getCenter()) &&
            aCircleObj.getRadius() === this.getRadius()) {
            return true;
        }
        return false;
    }

    /**
     * Convert the circle to JSON.
     */
    public toJSON(): any {
        return {
            "center": this._centerPoint.toJSON(),
            "radius": this._circleRadius,
            "type": Constants.CIRCLE
        };
    }

}

export default Circle;
