"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Point = /** @class */ (function () {
    function Point(pointX, pointY, pointZ) {
        if (pointZ === void 0) { pointZ = 0; }
        this._x = pointX === undefined ? 0 : pointX;
        this._y = pointY === undefined ? 0 : pointY;
        this._z = pointZ === undefined ? 0 : pointZ;
    }
    /**
     * Convert a JSON object to a Point.
     * @param jsonObj
     * @returns {math.Point}
     */
    Point.fromJSON = function (jsonObj) {
        if (!jsonObj) {
            throw new Error("Cannot convert undefined to Point");
        }
        else if (!jsonObj.hasOwnProperty("x")) {
            throw new Error("X property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("y")) {
            throw new Error("Y property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("z")) {
            throw new Error("Z property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("type")) {
            throw new Error("Type property does not exist");
        }
        else if (jsonObj.type !== Point.TYPE) {
            throw new Error("Invalid type");
        }
        var tempPoint = new Point(jsonObj.x, jsonObj.y, jsonObj.z);
        return tempPoint;
    };
    /**
     * Clone this point.
     */
    Point.prototype.clone = function () {
        return new Point(this._x, this._y, this._z);
    };
    /**
     * Check if this point is equal to another point.
     */
    Point.prototype.equals = function (otherPoint) {
        if (!otherPoint) {
            return false;
        }
        return (otherPoint.x === this.x && otherPoint.y === this.y && otherPoint.z === this.z);
    };
    Object.defineProperty(Point.prototype, "x", {
        /**
         * Get the X value.
         * @returns {number}
         */
        get: function () { return this._x; },
        /**
         * Set the X value.
         */
        set: function (value) {
            this._x = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Point.prototype, "y", {
        /**
         * Get the Y value.
         * @returns {number}
         */
        get: function () { return this._y; },
        /**
         * Set the Y value.
         */
        set: function (value) {
            this._y = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Point.prototype, "z", {
        /**
         * Get the Z value.
         * @returns {number}
         */
        get: function () { return this._z; },
        /**
         * Set the Z value.
         */
        set: function (value) {
            this._z = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Get the world section related to this point.
     */
    Point.prototype.getSectionCoordinate = function () {
        var sectionX = Math.round(this._x / 100);
        var sectionY = Math.round(this._y / 100);
        var sectionZ = Math.round(this._z / 100);
        return new Point(sectionX, sectionY, sectionZ);
    };
    /**
     * Convert this point to JSON.
     */
    Point.prototype.toJSON = function () {
        return {
            "x": this._x,
            "y": this._y,
            "z": this._z,
            "type": Point.TYPE
        };
    };
    /**
     * Convert this point to string..
     */
    Point.prototype.toString = function () {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    };
    Object.defineProperty(Point.prototype, "type", {
        /**
         * Get the type of this object.
         */
        get: function () {
            return Point.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Type of this point.
     */
    Point.TYPE = "point";
    return Point;
}());
exports.Point = Point;
exports.default = Point;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvcG9pbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtJQXFDSSxlQUFhLE1BQWMsRUFBRSxNQUFjLEVBQUUsTUFBUztRQUFULHVCQUFBLEVBQUEsVUFBUztRQUNsRCxJQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzVDLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDNUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztJQUNoRCxDQUFDO0lBOUJEOzs7O09BSUc7SUFDVyxjQUFRLEdBQXRCLFVBQXVCLE9BQVk7UUFFL0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ1gsTUFBTSxJQUFJLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO1FBQ3pELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QyxNQUFNLElBQUksS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDakQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQztRQUNqRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsTUFBTSxJQUFJLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6QyxNQUFNLElBQUksS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDcEQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLE1BQU0sSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUVELElBQUksU0FBUyxHQUFHLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0QsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUVyQixDQUFDO0lBUUQ7O09BRUc7SUFDSSxxQkFBSyxHQUFaO1FBQ0ksTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVEOztPQUVHO0lBQ0ksc0JBQU0sR0FBYixVQUFlLFVBQWlCO1FBQzVCLEVBQUUsQ0FBQyxDQUFFLENBQUMsVUFBVyxDQUFDLENBQUMsQ0FBQztZQUNoQixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFDRCxNQUFNLENBQUMsQ0FBRSxVQUFVLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLElBQUksVUFBVSxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBRSxDQUFDO0lBQzdGLENBQUM7SUFNRCxzQkFBSSxvQkFBQztRQUpMOzs7V0FHRzthQUNILGNBQVUsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRTNCOztXQUVHO2FBQ0gsVUFBTSxLQUFhO1lBQ2YsSUFBSSxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUM7UUFDcEIsQ0FBQzs7O09BUDBCO0lBYTNCLHNCQUFJLG9CQUFDO1FBSkw7OztXQUdHO2FBQ0gsY0FBVSxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFM0I7O1dBRUc7YUFDSCxVQUFNLEtBQWE7WUFDZixJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztRQUNwQixDQUFDOzs7T0FQMEI7SUFhM0Isc0JBQUksb0JBQUM7UUFKTDs7O1dBR0c7YUFDSCxjQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUUzQjs7V0FFRzthQUNILFVBQU0sS0FBYTtZQUNmLElBQUksQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLENBQUM7OztPQVAwQjtJQVMzQjs7T0FFRztJQUNJLG9DQUFvQixHQUEzQjtRQUNJLElBQUksUUFBUSxHQUFXLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNqRCxJQUFJLFFBQVEsR0FBVyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDakQsSUFBSSxRQUFRLEdBQVcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRDs7T0FFRztJQUNJLHNCQUFNLEdBQWI7UUFDSSxNQUFNLENBQUM7WUFDSCxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDWixHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDWixHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDWixNQUFNLEVBQUcsS0FBSyxDQUFDLElBQUk7U0FDdEIsQ0FBQztJQUNOLENBQUM7SUFFRDs7T0FFRztJQUNJLHdCQUFRLEdBQWY7UUFDSSxNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQzlELENBQUM7SUFLRCxzQkFBSSx1QkFBSTtRQUhSOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUN0QixDQUFDOzs7T0FBQTtJQW5JRDs7T0FFRztJQUNXLFVBQUksR0FBRyxPQUFPLENBQUM7SUFrSWpDLFlBQUM7Q0F2SUQsQUF1SUMsSUFBQTtBQXZJWSxzQkFBSztBQXlJbEIsa0JBQWUsS0FBSyxDQUFDIiwiZmlsZSI6Im1hdGgvcG9pbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgUG9pbnQge1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHlwZSBvZiB0aGlzIHBvaW50LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcInBvaW50XCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfeDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfeTogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfejogbnVtYmVyO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBhIEpTT04gb2JqZWN0IHRvIGEgUG9pbnQuXHJcbiAgICAgKiBAcGFyYW0ganNvbk9ialxyXG4gICAgICogQHJldHVybnMge21hdGguUG9pbnR9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iajogYW55KTogUG9pbnQgIHtcclxuXHJcbiAgICAgICAgaWYgKCFqc29uT2JqKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbm5vdCBjb252ZXJ0IHVuZGVmaW5lZCB0byBQb2ludFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCFqc29uT2JqLmhhc093blByb3BlcnR5KFwieFwiKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJYIHByb3BlcnR5IGRvZXMgbm90IGV4aXN0XCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIWpzb25PYmouaGFzT3duUHJvcGVydHkoXCJ5XCIpKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlkgcHJvcGVydHkgZG9lcyBub3QgZXhpc3RcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICghanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcInpcIikpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiWiBwcm9wZXJ0eSBkb2VzIG5vdCBleGlzdFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCFqc29uT2JqLmhhc093blByb3BlcnR5KFwidHlwZVwiKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJUeXBlIHByb3BlcnR5IGRvZXMgbm90IGV4aXN0XCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoanNvbk9iai50eXBlICE9PSBQb2ludC5UWVBFKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgdHlwZVwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCB0ZW1wUG9pbnQgPSBuZXcgUG9pbnQoanNvbk9iai54LCBqc29uT2JqLnksIGpzb25PYmoueik7XHJcbiAgICAgICAgcmV0dXJuIHRlbXBQb2ludDtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHBvaW50WDogbnVtYmVyLCBwb2ludFk6IG51bWJlciwgcG9pbnRaPSAwICkge1xyXG4gICAgICAgIHRoaXMuX3ggPSBwb2ludFggPT09IHVuZGVmaW5lZCA/IDAgOiBwb2ludFg7XHJcbiAgICAgICAgdGhpcy5feSA9IHBvaW50WSA9PT0gdW5kZWZpbmVkID8gMCA6IHBvaW50WTtcclxuICAgICAgICB0aGlzLl96ID0gcG9pbnRaID09PSB1bmRlZmluZWQgPyAwIDogcG9pbnRaO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2xvbmUgdGhpcyBwb2ludC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGNsb25lKCk6IFBvaW50IHtcclxuICAgICAgICByZXR1cm4gbmV3IFBvaW50KHRoaXMuX3gsIHRoaXMuX3ksIHRoaXMuX3opO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgdGhpcyBwb2ludCBpcyBlcXVhbCB0byBhbm90aGVyIHBvaW50LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZXF1YWxzKCBvdGhlclBvaW50OiBQb2ludCApIHtcclxuICAgICAgICBpZiAoICFvdGhlclBvaW50ICkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAoIG90aGVyUG9pbnQueCA9PT0gdGhpcy54ICYmIG90aGVyUG9pbnQueSA9PT0gdGhpcy55ICYmIG90aGVyUG9pbnQueiA9PT0gdGhpcy56ICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFggdmFsdWUuXHJcbiAgICAgKiBAcmV0dXJucyB7bnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBnZXQgeCgpIHsgcmV0dXJuIHRoaXMuX3g7IH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCB0aGUgWCB2YWx1ZS5cclxuICAgICAqL1xyXG4gICAgc2V0IHgodmFsdWU6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX3ggPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgWSB2YWx1ZS5cclxuICAgICAqIEByZXR1cm5zIHtudW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIGdldCB5KCkgeyByZXR1cm4gdGhpcy5feTsgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSBZIHZhbHVlLlxyXG4gICAgICovXHJcbiAgICBzZXQgeSh2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5feSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBaIHZhbHVlLlxyXG4gICAgICogQHJldHVybnMge251bWJlcn1cclxuICAgICAqL1xyXG4gICAgZ2V0IHooKSB7IHJldHVybiB0aGlzLl96OyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIFogdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIHNldCB6KHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl96ID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIHdvcmxkIHNlY3Rpb24gcmVsYXRlZCB0byB0aGlzIHBvaW50LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0U2VjdGlvbkNvb3JkaW5hdGUoKTogUG9pbnQge1xyXG4gICAgICAgIGxldCBzZWN0aW9uWDogbnVtYmVyID0gTWF0aC5yb3VuZCh0aGlzLl94IC8gMTAwKTtcclxuICAgICAgICBsZXQgc2VjdGlvblk6IG51bWJlciA9IE1hdGgucm91bmQodGhpcy5feSAvIDEwMCk7XHJcbiAgICAgICAgbGV0IHNlY3Rpb25aOiBudW1iZXIgPSBNYXRoLnJvdW5kKHRoaXMuX3ogLyAxMDApO1xyXG4gICAgICAgIHJldHVybiBuZXcgUG9pbnQoc2VjdGlvblgsIHNlY3Rpb25ZLCBzZWN0aW9uWik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgcG9pbnQgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBcInhcIjogdGhpcy5feCxcclxuICAgICAgICAgICAgXCJ5XCI6IHRoaXMuX3ksXHJcbiAgICAgICAgICAgIFwielwiOiB0aGlzLl96LFxyXG4gICAgICAgICAgICBcInR5cGVcIiA6IFBvaW50LlRZUEVcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB0aGlzIHBvaW50IHRvIHN0cmluZy4uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b1N0cmluZygpIHtcclxuICAgICAgICByZXR1cm4gXCIoXCIgKyB0aGlzLnggKyBcIiwgXCIgKyB0aGlzLnkgKyBcIiwgXCIgKyB0aGlzLnogKyBcIilcIjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgdHlwZSBvZiB0aGlzIG9iamVjdC5cclxuICAgICAqL1xyXG4gICAgZ2V0IHR5cGUoKTogc3RyaW5nICB7XHJcbiAgICAgICAgcmV0dXJuIFBvaW50LlRZUEU7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBQb2ludDtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
