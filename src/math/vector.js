"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Vector = /** @class */ (function () {
    function Vector(xValue, yValue, zValue) {
        this._x = xValue;
        this._y = yValue;
        this._z = zValue ? zValue : 0;
    }
    /**
     * Get the cross product of two vectors.
     */
    Vector.cross = function (a, b) {
        var cx = a.y * b.z - a.z * b.y;
        var cy = a.z * b.x - a.x * b.z;
        var cz = a.x * b.y - a.y * b.x;
        return new Vector(cx, cy, cz);
    };
    /**
     * Subtract 2 points to get the direction of the vector.
     */
    Vector.subtract = function (a, b) {
        return new Vector(a.x - b.x, a.y - b.y, a.z - b.z);
    };
    /**
     * Clone this vector.
     */
    Vector.prototype.clone = function () {
        return new Vector(this._x, this._y, this._z);
    };
    /**
     * Get the dot product of this vector with another vector.
     * @param vector2
     */
    Vector.prototype.dot = function (vector2) {
        return this._x * vector2.x + this._y * vector2.y + this._z * vector2.z;
    };
    /**
     * Get the dot product of this vector with the x, y, z of another vector.
     */
    Vector.prototype.dot2 = function (x, y, z) {
        return this._x * x + this._y * y + this._z * z;
    };
    /**
     * Normalize this vector.
     */
    Vector.prototype.normalize = function () {
        var length = Math.sqrt(this._x * this._x) + Math.sqrt(this._y * this._y) + Math.sqrt(this._z * this._z);
        this._x /= length;
        this._y /= length;
        this._z /= length;
    };
    /**
     * Flip this vector and make it perpendicular.
     */
    Vector.prototype.perpendicular = function () {
        var a = this.y;
        var b = -this.x;
        this._x = a;
        this._y = b;
    };
    Object.defineProperty(Vector.prototype, "x", {
        /**
         * Get the X value.
         */
        get: function () {
            return this._x;
        },
        /**
         * Set the X value.
         */
        set: function (value) {
            this._x = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector.prototype, "y", {
        /**
         * Get the Y value.
         */
        get: function () {
            return this._y;
        },
        /**
         * Get the Y value.
         */
        set: function (value) {
            this._y = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector.prototype, "z", {
        /**
         * Get the Z value.
         */
        get: function () {
            return this._z;
        },
        /**
         * Get the Z value.
         */
        set: function (value) {
            this._z = value;
        },
        enumerable: true,
        configurable: true
    });
    return Vector;
}());
exports.Vector = Vector;
exports.default = Vector;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvdmVjdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUEwQkksZ0JBQVksTUFBYyxFQUFFLE1BQWMsRUFBRSxNQUFlO1FBQ3ZELElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBeEJEOztPQUVHO0lBQ1csWUFBSyxHQUFuQixVQUFxQixDQUFTLEVBQUUsQ0FBUztRQUVyQyxJQUFJLEVBQUUsR0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksRUFBRSxHQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkMsSUFBSSxFQUFFLEdBQVcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUV2QyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUVsQyxDQUFDO0lBRUQ7O09BRUc7SUFDVyxlQUFRLEdBQXRCLFVBQXdCLENBQVEsRUFBRSxDQUFRO1FBQ3RDLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBRSxDQUFDO0lBQ3pELENBQUM7SUFRRDs7T0FFRztJQUNJLHNCQUFLLEdBQVo7UUFDSSxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksb0JBQUcsR0FBVixVQUFZLE9BQWU7UUFDdkIsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLE9BQU8sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFFRDs7T0FFRztJQUNJLHFCQUFJLEdBQVgsVUFBYSxDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVM7UUFDeEMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRDs7T0FFRztJQUNJLDBCQUFTLEdBQWhCO1FBQ0ksSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDeEcsSUFBSSxDQUFDLEVBQUUsSUFBSSxNQUFNLENBQUM7UUFDbEIsSUFBSSxDQUFDLEVBQUUsSUFBSSxNQUFNLENBQUM7UUFDbEIsSUFBSSxDQUFDLEVBQUUsSUFBSSxNQUFNLENBQUM7SUFDdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksOEJBQWEsR0FBcEI7UUFDSSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDaEIsQ0FBQztJQUtELHNCQUFJLHFCQUFDO1FBSEw7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ25CLENBQUM7UUFFRDs7V0FFRzthQUNILFVBQU8sS0FBYTtZQUNoQixJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztRQUNwQixDQUFDOzs7T0FQQTtJQVlELHNCQUFJLHFCQUFDO1FBSEw7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ25CLENBQUM7UUFFRDs7V0FFRzthQUNILFVBQU8sS0FBYTtZQUNoQixJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztRQUNwQixDQUFDOzs7T0FQQTtJQVlELHNCQUFJLHFCQUFDO1FBSEw7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ25CLENBQUM7UUFFRDs7V0FFRzthQUNILFVBQU8sS0FBYTtZQUNoQixJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztRQUNwQixDQUFDOzs7T0FQQTtJQVNMLGFBQUM7QUFBRCxDQXBIQSxBQW9IQyxJQUFBO0FBcEhZLHdCQUFNO0FBc0huQixrQkFBZSxNQUFNLENBQUMiLCJmaWxlIjoibWF0aC92ZWN0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUG9pbnQgZnJvbSBcIi4vcG9pbnRcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBWZWN0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgX3g6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX3k6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX3o6IG51bWJlcjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgY3Jvc3MgcHJvZHVjdCBvZiB0d28gdmVjdG9ycy5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBjcm9zcyggYTogVmVjdG9yLCBiOiBWZWN0b3IgKTogVmVjdG9yIHtcclxuXHJcbiAgICAgICAgbGV0IGN4OiBudW1iZXIgPSBhLnkgKiBiLnogLSBhLnogKiBiLnk7XHJcbiAgICAgICAgbGV0IGN5OiBudW1iZXIgPSBhLnogKiBiLnggLSBhLnggKiBiLno7XHJcbiAgICAgICAgbGV0IGN6OiBudW1iZXIgPSBhLnggKiBiLnkgLSBhLnkgKiBiLng7XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgVmVjdG9yKGN4LCBjeSwgY3opO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFN1YnRyYWN0IDIgcG9pbnRzIHRvIGdldCB0aGUgZGlyZWN0aW9uIG9mIHRoZSB2ZWN0b3IuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgc3VidHJhY3QoIGE6IFBvaW50LCBiOiBQb2ludCApOiBWZWN0b3Ige1xyXG4gICAgICAgIHJldHVybiBuZXcgVmVjdG9yKCBhLnggLSBiLngsIGEueSAtIGIueSwgYS56IC0gYi56ICk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoeFZhbHVlOiBudW1iZXIsIHlWYWx1ZTogbnVtYmVyLCB6VmFsdWU/OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl94ID0geFZhbHVlO1xyXG4gICAgICAgIHRoaXMuX3kgPSB5VmFsdWU7XHJcbiAgICAgICAgdGhpcy5feiA9IHpWYWx1ZSA/IHpWYWx1ZSA6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDbG9uZSB0aGlzIHZlY3Rvci5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGNsb25lKCk6IFZlY3RvciB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBWZWN0b3IodGhpcy5feCwgdGhpcy5feSwgdGhpcy5feik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIGRvdCBwcm9kdWN0IG9mIHRoaXMgdmVjdG9yIHdpdGggYW5vdGhlciB2ZWN0b3IuXHJcbiAgICAgKiBAcGFyYW0gdmVjdG9yMlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZG90KCB2ZWN0b3IyOiBWZWN0b3IgKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5feCAqIHZlY3RvcjIueCArIHRoaXMuX3kgKiB2ZWN0b3IyLnkgKyB0aGlzLl96ICogdmVjdG9yMi56O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBkb3QgcHJvZHVjdCBvZiB0aGlzIHZlY3RvciB3aXRoIHRoZSB4LCB5LCB6IG9mIGFub3RoZXIgdmVjdG9yLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZG90MiggeDogbnVtYmVyLCB5OiBudW1iZXIsIHo6IG51bWJlciApOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl94ICogeCArIHRoaXMuX3kgKiB5ICsgdGhpcy5feiAqIHo7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBOb3JtYWxpemUgdGhpcyB2ZWN0b3IuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBub3JtYWxpemUoKTogdm9pZCB7XHJcbiAgICAgICAgbGV0IGxlbmd0aCA9IE1hdGguc3FydCh0aGlzLl94ICogdGhpcy5feCkgKyBNYXRoLnNxcnQodGhpcy5feSAqIHRoaXMuX3kpICsgTWF0aC5zcXJ0KHRoaXMuX3ogKiB0aGlzLl96KTtcclxuICAgICAgICB0aGlzLl94IC89IGxlbmd0aDtcclxuICAgICAgICB0aGlzLl95IC89IGxlbmd0aDtcclxuICAgICAgICB0aGlzLl96IC89IGxlbmd0aDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZsaXAgdGhpcyB2ZWN0b3IgYW5kIG1ha2UgaXQgcGVycGVuZGljdWxhci5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHBlcnBlbmRpY3VsYXIoKTogdm9pZCB7XHJcbiAgICAgICAgbGV0IGEgPSB0aGlzLnk7XHJcbiAgICAgICAgbGV0IGIgPSAtdGhpcy54O1xyXG4gICAgICAgIHRoaXMuX3ggPSBhO1xyXG4gICAgICAgIHRoaXMuX3kgPSBiO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBYIHZhbHVlLlxyXG4gICAgICovXHJcbiAgICBnZXQgeCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl94O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSBYIHZhbHVlLlxyXG4gICAgICovXHJcbiAgICBzZXQgeCggdmFsdWU6IG51bWJlciApIHtcclxuICAgICAgICB0aGlzLl94ID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFkgdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIGdldCB5KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3k7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFkgdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIHNldCB5KCB2YWx1ZTogbnVtYmVyICkge1xyXG4gICAgICAgIHRoaXMuX3kgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgWiB2YWx1ZS5cclxuICAgICAqL1xyXG4gICAgZ2V0IHooKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fejtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgWiB2YWx1ZS5cclxuICAgICAqL1xyXG4gICAgc2V0IHooIHZhbHVlOiBudW1iZXIgKSB7XHJcbiAgICAgICAgdGhpcy5feiA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgVmVjdG9yO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
