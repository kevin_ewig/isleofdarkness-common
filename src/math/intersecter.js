"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var line_1 = require("./line");
var vector_1 = require("./vector");
var common_1 = require("./common");
var constants_1 = require("./constants");
var Intersecter = /** @class */ (function () {
    function Intersecter() {
    }
    /**
     * Check if 2 line segments intersect.
     * Source: https://stackoverflow.com/questions/4543506/algorithm-for-intersection-of-2-lines
     * @param line1
     * @param line2
     */
    Intersecter.isLineIntersectLine = function (line1, line2) {
        var determinant = line1.a * line2.b - line2.a * line1.b;
        return determinant !== 0;
    };
    /**
     * Determine if two circles intersect or overlap.
     *
     * If d is the distance between the center of the two circles, and r0 is the
     * radius of circle 1 and r1 is the radius of circle 2, then:
     *
     * If d > r0 + r1: The circles are too far apart to intersect.
     * If d < |r0 – r1|: One circle is inside the other so there is no intersection.
     * If d = 0 and r0 = r1: The circles are the same.
     * If d = r0 + r1: The circles touch at a single point.
     * Otherwise: The circles touch at two points.
     * (Now d = r0 + r1 because if not the objects steer way clear of each other)
     * @param circle1
     * @param circle2
     */
    Intersecter.isCircleIntersectCircle = function (circle1, circle2) {
        var d = common_1.Common.distance2d(circle1.x, circle1.y, circle2.x, circle2.y);
        var r0 = circle1.getRadius();
        var r1 = circle2.getRadius();
        return (d < r0 + r1);
    };
    /**
     * Check if two rectangles intersect.
     * https://stackoverflow.com/questions/13390333/two-rectangles-intersection
     * @param rect1
     * @param rect2
     */
    Intersecter.isRectangleIntersectRectangle = function (rect1, rect2) {
        var x1 = rect1.position().x;
        var y1 = rect1.position().y;
        var width1 = rect1.width();
        var height1 = rect1.height();
        var x2 = rect2.position().x;
        var y2 = rect2.position().y;
        var width2 = rect2.width();
        var height2 = rect2.height();
        return !(x1 > x2 + width2 || x1 + width1 < x2 || y1 > y2 + height2 || y1 + height1 < y2);
    };
    /**
     * Check if circle intersects rectangle.
     * @param rect
     * @param circle
     */
    Intersecter.isRectangleIntersectCircle = function (rect, circle) {
        var circleDistanceX = Math.abs(circle.x - rect.x);
        var circleDistanceY = Math.abs(circle.y - rect.y);
        if (circleDistanceX > (rect.width() / 2 + circle.getRadius())) {
            return false;
        }
        if (circleDistanceY > (rect.height() / 2 + circle.getRadius())) {
            return false;
        }
        if (circleDistanceX <= (rect.width() / 2)) {
            return true;
        }
        if (circleDistanceY <= (rect.height() / 2)) {
            return true;
        }
        var cornerDistanceSq = (circleDistanceX - rect.width() / 2) * (circleDistanceX - rect.width() / 2) +
            (circleDistanceY - rect.height() / 2) * (circleDistanceY - rect.height() / 2);
        return (cornerDistanceSq <= circle.getRadius() * circle.getRadius());
    };
    /**
     * Determines if a point is inside a circle
     * @param point
     * @param circle
     * @returns {boolean} True if the point is inside the circle. False otherwise.
     */
    Intersecter.isPointIntersectCircle = function (point, circle) {
        var radius = circle.getRadius();
        var dist = common_1.Common.distance2d(circle.x, circle.y, point.x, point.y);
        return (dist <= radius);
    };
    /**
     * Check if a point is inside a polygon.
     *
     * @param point
     * @param poly
     * @returns {boolean}
     */
    Intersecter.isPointIntersectPolygon = function (point, poly) {
        var i = 0, j = 0;
        var c = false;
        var vertices = poly.getArrayOfPoints();
        var numberOfVertices = vertices.length;
        for (i = 0, j = numberOfVertices - 1; i < numberOfVertices; j = i++) {
            if (((vertices[i].y > point.y) !== (vertices[j].y > point.y)) &&
                (point.x < (vertices[j].x - vertices[i].x) * (point.y - vertices[i].y) / (vertices[j].y - vertices[i].y) + vertices[i].x)) {
                c = !c;
            }
        }
        return c;
    };
    /**
     * Test to see if a line intersects a circle.
     * Source: http://stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm
     * @param line
     * @param circle
     */
    Intersecter.isLineIntersectCircle = function (line, circle) {
        // Direction vector of ray, from start to end )
        var d = vector_1.default.subtract(line.getStartPoint(), line.getEndPoint());
        // Vector from center sphere to ray start )
        var f = vector_1.default.subtract(circle.getCenter(), line.getStartPoint());
        var r = circle.getRadius();
        var a = d.dot(d);
        var b = 2 * f.dot(d);
        var c = f.dot(f) - r * r;
        var discriminant = b * b - 4 * a * c;
        if (discriminant < 0) {
            return false;
        }
        else {
            // Ray didn"t totally miss sphere, so there is a solution to the equation.
            discriminant = Math.sqrt(discriminant);
            // either solution may be on or off the ray so need to test both t1 is always the smaller value, because
            // BOTH discriminant and a are non-negative.
            var t1 = (-b - discriminant) / (2 * a);
            var t2 = (-b + discriminant) / (2 * a);
            if (t1 >= 0 && t1 <= 1) {
                return true;
            }
            // Here t1 didn"t intersect so we are either started inside the sphere or completely past it.
            if (t2 >= 0 && t2 <= 1) {
                return true;
            }
            // No intersection.
            return false;
        }
    };
    /**
     * Test to see if a circle intersects a polygon.
     * @param circle
     * @param polygon
     */
    Intersecter.isCircleIntersectPolygon = function (circle, polygon) {
        // If center of the circle is in the polygon, then it intersects.
        if (Intersecter.isPointIntersectPolygon(circle.getCenter(), polygon)) {
            return true;
        }
        // If line intersects the circle, then it intersects.
        var arrayOfPoints = polygon.getArrayOfPoints();
        for (var i = 0; i < arrayOfPoints.length - 1; i++) {
            var line = new line_1.default(arrayOfPoints[i], arrayOfPoints[i + 1]);
            if (Intersecter.isLineIntersectCircle(line, circle)) {
                return true;
            }
        }
        return false;
    };
    /**
     * Test to see if a rectangle intersects a polygon.
     * @param rect
     * @param polygon
     */
    Intersecter.isRectangleIntersectPolygon = function (rect, polygon) {
        // If center of the rectangle is in the polygon, then it intersects.
        if (Intersecter.isPointIntersectPolygon(rect.getCenter(), polygon)) {
            return true;
        }
        // If line intersects the rectangle, then it intersects.
        var arrayOfPoints = polygon.getArrayOfPoints();
        for (var i = 0; i < arrayOfPoints.length - 1; i++) {
            var line = new line_1.default(arrayOfPoints[i], arrayOfPoints[i + 1]);
            if (Intersecter.isLineIntersectRectangle(line, rect)) {
                return true;
            }
        }
        return false;
    };
    /**
     * Test to see if a line intersects a rectangle.
     * Source: https://stackoverflow.com/questions/99353/how-to-test-if-a-line-segment-intersects-an-axis-aligned-rectange-in-2d
     * @param line
     * @param rect
     */
    Intersecter.isLineIntersectRectangle = function (line, rect) {
        var a_rectangleMinX = rect.position().x;
        var a_rectangleMinY = rect.position().y;
        var a_rectangleMaxX = rect.position().x + rect.width();
        var a_rectangleMaxY = rect.position().y + rect.height();
        var a_p1x = line.getStartPoint().x;
        var a_p1y = line.getStartPoint().y;
        var a_p2x = line.getEndPoint().x;
        var a_p2y = line.getEndPoint().y;
        // Find min and max X for the segment
        var minX = a_p1x;
        var maxX = a_p2x;
        if (a_p1x > a_p2x) {
            minX = a_p2x;
            maxX = a_p1x;
        }
        // Find the intersection of the segment's and rectangle's x-projections
        if (maxX > a_rectangleMaxX) {
            maxX = a_rectangleMaxX;
        }
        if (minX < a_rectangleMinX) {
            minX = a_rectangleMinX;
        }
        // If their projections do not intersect return false
        if (minX > maxX) {
            return false;
        }
        // Find corresponding min and max Y for min and max X we found before
        var minY = a_p1y;
        var maxY = a_p2y;
        var dx = a_p2x - a_p1x;
        if (Math.abs(dx) > 0.0000001) {
            var a = (a_p2y - a_p1y) / dx;
            var b = a_p1y - a * a_p1x;
            minY = a * minX + b;
            maxY = a * maxX + b;
        }
        if (minY > maxY) {
            var tmp = maxY;
            maxY = minY;
            minY = tmp;
        }
        // Find the intersection of the segment's and rectangle's y-projections
        if (maxY > a_rectangleMaxY) {
            maxY = a_rectangleMaxY;
        }
        if (minY < a_rectangleMinY) {
            minY = a_rectangleMinY;
        }
        // If Y-projections do not intersect return false
        if (minY > maxY) {
            return false;
        }
        return true;
    };
    /**
     * Check if two geometry objects shapes intersect each other.
     */
    Intersecter.isShapeIntersectShape = function (geom1, geom2) {
        var geom1Type = geom1.type;
        var geom2Type = geom2.type;
        if (geom1Type === constants_1.default.CIRCLE && geom2Type === constants_1.default.CIRCLE) {
            var circle1 = geom1;
            var circle2 = geom2;
            return Intersecter.isCircleIntersectCircle(circle1, circle2);
        }
        if (geom1Type === constants_1.default.POLYGON && geom2Type === constants_1.default.POLYGON) {
            var poly1 = geom1;
            var poly2 = geom2;
            return Intersecter.isPolygonIntersectPolygon(poly1, poly2);
        }
        if (geom1Type === constants_1.default.RECTANGLE && geom2Type === constants_1.default.RECTANGLE) {
            var rect1 = geom1;
            var rect2 = geom2;
            return Intersecter.isRectangleIntersectRectangle(rect1, rect2);
        }
        // Circle - Polygon
        if (geom1Type === constants_1.default.CIRCLE && geom2Type === constants_1.default.POLYGON) {
            var circle = geom1;
            var poly = geom2;
            return Intersecter.isCircleIntersectPolygon(circle, poly);
        }
        if (geom1Type === constants_1.default.POLYGON && geom2Type === constants_1.default.CIRCLE) {
            var circle = geom2;
            var poly = geom1;
            return Intersecter.isCircleIntersectPolygon(circle, poly);
        }
        // Circle - Rectangle
        if (geom1Type === constants_1.default.CIRCLE && geom2Type === constants_1.default.RECTANGLE) {
            var circle = geom1;
            var rect = geom2;
            return Intersecter.isRectangleIntersectCircle(rect, circle);
        }
        if (geom1Type === constants_1.default.RECTANGLE && geom2Type === constants_1.default.CIRCLE) {
            var circle = geom2;
            var rect = geom1;
            return Intersecter.isRectangleIntersectCircle(rect, circle);
        }
        // Rectangle - Polygon
        if (geom1Type === constants_1.default.POLYGON && geom2Type === constants_1.default.RECTANGLE) {
            var poly = geom1;
            var rect = geom2;
            return Intersecter.isRectangleIntersectPolygon(rect, poly);
        }
        if (geom1Type === constants_1.default.RECTANGLE && geom2Type === constants_1.default.POLYGON) {
            var poly = geom2;
            var rect = geom1;
            return Intersecter.isRectangleIntersectPolygon(rect, poly);
        }
        return false;
    };
    /**
     * Check if a geometry shape intersect a point.
     */
    Intersecter.isShapeIntersectPoint = function (geom, point) {
        var geomType = geom.type;
        if (geomType === constants_1.default.CIRCLE) {
            var circle = geom;
            return Intersecter.isPointIntersectCircle(point, circle);
        }
        if (geomType === constants_1.default.POLYGON) {
            var polygon = geom;
            return Intersecter.isPointIntersectPolygon(point, polygon);
        }
        return false;
    };
    /**
     * Checks to see if a point is on a line.
     * Source: http://stackoverflow.com/questions/7050186/find-if-point-lays-on-line-segment
     * @param point
     * @param line
     * @returns {boolean}
     */
    Intersecter.isPointOnLine = function (point, line) {
        var epsilon = 0.01;
        var pt1 = line.getStartPoint();
        var pt2 = line.getEndPoint();
        var pt = point;
        if (pt.x - Math.max(pt1.x, pt2.x) > epsilon ||
            Math.min(pt1.x, pt2.x) - pt.x > epsilon ||
            pt.y - Math.max(pt1.y, pt2.y) > epsilon ||
            Math.min(pt1.y, pt2.y) - pt.y > epsilon) {
            return false;
        }
        if (Math.abs(pt2.x - pt1.x) < epsilon) {
            return Math.abs(pt1.x - pt.x) < epsilon || Math.abs(pt2.x - pt.x) < epsilon;
        }
        if (Math.abs(pt2.y - pt1.y) < epsilon) {
            return Math.abs(pt1.y - pt.y) < epsilon || Math.abs(pt2.y - pt.y) < epsilon;
        }
        var x = pt1.x + (pt.y - pt1.y) * (pt2.x - pt1.x) / (pt2.y - pt1.y);
        var y = pt1.y + (pt.x - pt1.x) * (pt2.y - pt1.y) / (pt2.x - pt1.x);
        return Math.abs(pt.x - x) < epsilon || Math.abs(pt.y - y) < epsilon;
    };
    /**
     * Checks to see if two polygons intersect. Uses the axis-separation test.
     *
     * Source: http://www.dyn4j.org/2010/01/sat/
     *
     * @param poly1
     * @param poly2
     */
    Intersecter.isPolygonIntersectPolygon = function (poly1, poly2) {
        //
        // Generate a list of axis (normalized vectors) from an array of points in
        // the polygon.
        //
        function generateAxis(polyPoints) {
            var axis = [];
            for (var i = 0; i < polyPoints.length - 1; i++) {
                var polyPointA = polyPoints[i];
                var polyPointB = polyPoints[i + 1 === polyPoints.length ? 0 : i + 1];
                var vector = new vector_1.default(polyPointB.x - polyPointA.x, polyPointB.y - polyPointA.y);
                vector.normalize();
                vector.perpendicular();
                axis.push(vector);
            }
            return axis;
        }
        var poly1Points = poly1.getArrayOfPoints();
        var poly2Points = poly2.getArrayOfPoints();
        var axes1 = generateAxis(poly1Points);
        var axes2 = generateAxis(poly2Points);
        for (var i = 0; i < axes1.length; i++) {
            var axis = axes1[i];
            // project both shapes onto the axis
            var p1 = new Projection(poly1, axis);
            var p2 = new Projection(poly2, axis);
            // do the projections overlap?
            if (!p1.overlap(p2)) {
                // then we can guarantee that the shapes do not overlap
                return false;
            }
        }
        for (var i = 0; i < axes2.length; i++) {
            var axis = axes2[i];
            // project both shapes onto the axis
            var p1 = new Projection(poly1, axis);
            var p2 = new Projection(poly2, axis);
            // do the projections overlap?
            if (!p1.overlap(p2)) {
                // then we can guarantee that the shapes do not overlap
                return false;
            }
        }
        // if we get here then we know that every axis had overlap on it
        // so we can guarantee an intersection
        return true;
    };
    return Intersecter;
}());
exports.Intersecter = Intersecter;
/**
 * A projection object based on projecting the points of a polygon to the
 * given axis.
 * @param polygon A convex polygon.
 * @param axis A vector.
 */
var Projection = /** @class */ (function () {
    function Projection(polygon, axis) {
        var points = polygon.getArrayOfPoints();
        this._min = axis.dot2(points[0].x, points[0].y, points[0].z);
        this._max = this._min;
        for (var ii = 1; ii < points.length; ii++) {
            // NOTE: the axis must be normalized to get accurate projections
            var p = axis.dot2(points[ii].x, points[ii].y, points[ii].z);
            if (p < this._min) {
                this._min = p;
            }
            else if (p > this._max) {
                this._max = p;
            }
        }
    }
    Projection.prototype.getMin = function () {
        return this._min;
    };
    Projection.prototype.getMax = function () {
        return this._max;
    };
    Projection.prototype.overlap = function (projection2) {
        if (this._max > projection2.getMin()) {
            return true;
        }
        if (this._min > projection2.getMax()) {
            return true;
        }
        return false;
    };
    return Projection;
}());
exports.default = Intersecter;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvaW50ZXJzZWN0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFLQSwrQkFBMEI7QUFDMUIsbUNBQThCO0FBRTlCLG1DQUFnQztBQUNoQyx5Q0FBb0M7QUFFcEM7SUFBQTtJQXdkQSxDQUFDO0lBN1ZHOzs7OztPQUtHO0lBQ1csK0JBQW1CLEdBQWpDLFVBQWtDLEtBQVcsRUFBRSxLQUFXO1FBQ3RELElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDeEQsTUFBTSxDQUFDLFdBQVcsS0FBSyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7OztPQWNHO0lBQ1csbUNBQXVCLEdBQXJDLFVBQXNDLE9BQWUsRUFBRSxPQUFlO1FBRWxFLElBQUksQ0FBQyxHQUFXLGVBQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlFLElBQUksRUFBRSxHQUFXLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQyxJQUFJLEVBQUUsR0FBVyxPQUFPLENBQUMsU0FBUyxFQUFFLENBQUM7UUFFckMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUV6QixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDVyx5Q0FBNkIsR0FBM0MsVUFBNEMsS0FBZ0IsRUFBRSxLQUFnQjtRQUMxRSxJQUFJLEVBQUUsR0FBVyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BDLElBQUksRUFBRSxHQUFXLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEMsSUFBSSxNQUFNLEdBQVcsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLElBQUksT0FBTyxHQUFXLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUVyQyxJQUFJLEVBQUUsR0FBVyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BDLElBQUksRUFBRSxHQUFXLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEMsSUFBSSxNQUFNLEdBQVcsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLElBQUksT0FBTyxHQUFXLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNyQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLEdBQUcsTUFBTSxJQUFJLEVBQUUsR0FBRyxNQUFNLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsT0FBTyxJQUFJLEVBQUUsR0FBRyxPQUFPLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDN0YsQ0FBQztJQUVEOzs7O09BSUc7SUFDVyxzQ0FBMEIsR0FBeEMsVUFBeUMsSUFBZSxFQUFFLE1BQWM7UUFFcEUsSUFBSSxlQUFlLEdBQVcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxRCxJQUFJLGVBQWUsR0FBWSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRTNELEVBQUUsQ0FBQyxDQUFDLGVBQWUsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUFDLENBQUM7UUFDaEYsRUFBRSxDQUFDLENBQUMsZUFBZSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQUMsQ0FBQztRQUVqRixFQUFFLENBQUMsQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUFDLENBQUM7UUFDM0QsRUFBRSxDQUFDLENBQUMsZUFBZSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBRTVELElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDM0UsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUVyRyxNQUFNLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFFLEdBQUcsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFFekUsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ1csa0NBQXNCLEdBQXBDLFVBQXFDLEtBQVksRUFBRSxNQUFjO1FBRTdELElBQUksTUFBTSxHQUFXLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUN4QyxJQUFJLElBQUksR0FBVyxlQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMzRSxNQUFNLENBQUMsQ0FBRSxJQUFJLElBQUksTUFBTSxDQUFFLENBQUM7SUFFOUIsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNXLG1DQUF1QixHQUFyQyxVQUFzQyxLQUFZLEVBQUUsSUFBYTtRQUU3RCxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQixJQUFJLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDZCxJQUFJLFFBQVEsR0FBb0IsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDeEQsSUFBSSxnQkFBZ0IsR0FBVyxRQUFRLENBQUMsTUFBTSxDQUFDO1FBRS9DLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGdCQUFnQixHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsZ0JBQWdCLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDbEUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM1SCxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDWCxDQUFDO1FBQ0wsQ0FBQztRQUVELE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFFYixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDVyxpQ0FBcUIsR0FBbkMsVUFBb0MsSUFBVSxFQUFFLE1BQWM7UUFFMUQsK0NBQStDO1FBQy9DLElBQUksQ0FBQyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUVsRSwyQ0FBMkM7UUFDM0MsSUFBSSxDQUFDLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO1FBRWxFLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUV6QixJQUFJLFlBQVksR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JDLEVBQUUsQ0FBQyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25CLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBRUosMEVBQTBFO1lBQzFFLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBRXZDLHdHQUF3RztZQUN4Ryw0Q0FBNEM7WUFDNUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN2QyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBRXZDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDaEIsQ0FBQztZQUVELDZGQUE2RjtZQUM3RixFQUFFLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2hCLENBQUM7WUFFRCxtQkFBbUI7WUFDbkIsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUVqQixDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDVyxvQ0FBd0IsR0FBdEMsVUFBdUMsTUFBYyxFQUFFLE9BQWdCO1FBRW5FLGlFQUFpRTtRQUNqRSxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRSxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFRCxxREFBcUQ7UUFDckQsSUFBSSxhQUFhLEdBQW9CLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ2hFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUNoRCxJQUFJLElBQUksR0FBRyxJQUFJLGNBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVELEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsRCxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2hCLENBQUM7UUFDTCxDQUFDO1FBRUQsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUVqQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNXLHVDQUEyQixHQUF6QyxVQUEwQyxJQUFlLEVBQUUsT0FBZ0I7UUFFdkUsb0VBQW9FO1FBQ3BFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pFLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVELHdEQUF3RDtRQUN4RCxJQUFJLGFBQWEsR0FBb0IsT0FBTyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDaEUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ2hELElBQUksSUFBSSxHQUFHLElBQUksY0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUQsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDaEIsQ0FBQztRQUNMLENBQUM7UUFFRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBRWpCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNXLG9DQUF3QixHQUF0QyxVQUF1QyxJQUFVLEVBQUUsSUFBZTtRQUU5RCxJQUFJLGVBQWUsR0FBVyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2hELElBQUksZUFBZSxHQUFXLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDaEQsSUFBSSxlQUFlLEdBQVcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDL0QsSUFBSSxlQUFlLEdBQVcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDaEUsSUFBSSxLQUFLLEdBQVcsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUMzQyxJQUFJLEtBQUssR0FBVyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzNDLElBQUksS0FBSyxHQUFXLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDekMsSUFBSSxLQUFLLEdBQVcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUV6QyxxQ0FBcUM7UUFDckMsSUFBSSxJQUFJLEdBQVcsS0FBSyxDQUFDO1FBQ3pCLElBQUksSUFBSSxHQUFXLEtBQUssQ0FBQztRQUV6QixFQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNoQixJQUFJLEdBQUcsS0FBSyxDQUFDO1lBQ2IsSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQsdUVBQXVFO1FBQ3ZFLEVBQUUsQ0FBQyxDQUFDLElBQUksR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksR0FBRyxlQUFlLENBQUM7UUFDM0IsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksR0FBRyxlQUFlLENBQUM7UUFDM0IsQ0FBQztRQUVELHFEQUFxRDtRQUNyRCxFQUFFLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNsQixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2IsQ0FBQztRQUVELHFFQUFxRTtRQUNyRSxJQUFJLElBQUksR0FBVyxLQUFLLENBQUM7UUFDekIsSUFBSSxJQUFJLEdBQVcsS0FBSyxDQUFDO1FBQ3pCLElBQUksRUFBRSxHQUFXLEtBQUssR0FBRyxLQUFLLENBQUM7UUFFL0IsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFVLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxHQUFXLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNyQyxJQUFJLENBQUMsR0FBVyxLQUFLLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUNsQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFDLENBQUM7WUFDcEIsSUFBSSxHQUFHLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ3hCLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBRSxJQUFJLEdBQUcsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNoQixJQUFJLEdBQUcsR0FBVyxJQUFJLENBQUM7WUFDdkIsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNaLElBQUksR0FBRyxHQUFHLENBQUM7UUFDZixDQUFDO1FBRUQsdUVBQXVFO1FBQ3ZFLEVBQUUsQ0FBQyxDQUFFLElBQUksR0FBRyxlQUFnQixDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLEdBQUcsZUFBZSxDQUFDO1FBQzNCLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxJQUFJLEdBQUcsZUFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxHQUFHLGVBQWUsQ0FBQztRQUMzQixDQUFDO1FBRUQsaURBQWlEO1FBQ2pELEVBQUUsQ0FBQyxDQUFFLElBQUksR0FBRyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQXRaRDs7T0FFRztJQUNXLGlDQUFxQixHQUFHLFVBQVUsS0FBWSxFQUFFLEtBQVk7UUFFdEUsSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUMzQixJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBRTNCLEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxtQkFBUyxDQUFDLE1BQU0sSUFBSSxTQUFTLEtBQUssbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ25FLElBQUksT0FBTyxHQUFHLEtBQWUsQ0FBQztZQUM5QixJQUFJLE9BQU8sR0FBRyxLQUFlLENBQUM7WUFDOUIsTUFBTSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDakUsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxtQkFBUyxDQUFDLE9BQU8sSUFBSSxTQUFTLEtBQUssbUJBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksS0FBSyxHQUFHLEtBQWdCLENBQUM7WUFDN0IsSUFBSSxLQUFLLEdBQUcsS0FBZ0IsQ0FBQztZQUM3QixNQUFNLENBQUMsV0FBVyxDQUFDLHlCQUF5QixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMvRCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUUsU0FBUyxLQUFLLG1CQUFTLENBQUMsU0FBUyxJQUFJLFNBQVMsS0FBSyxtQkFBUyxDQUFDLFNBQVUsQ0FBQyxDQUFDLENBQUM7WUFDM0UsSUFBSSxLQUFLLEdBQUcsS0FBa0IsQ0FBQztZQUMvQixJQUFJLEtBQUssR0FBRyxLQUFrQixDQUFDO1lBQy9CLE1BQU0sQ0FBQyxXQUFXLENBQUMsNkJBQTZCLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ25FLENBQUM7UUFFRCxtQkFBbUI7UUFDbkIsRUFBRSxDQUFDLENBQUMsU0FBUyxLQUFLLG1CQUFTLENBQUMsTUFBTSxJQUFJLFNBQVMsS0FBSyxtQkFBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDcEUsSUFBSSxNQUFNLEdBQUcsS0FBZSxDQUFDO1lBQzdCLElBQUksSUFBSSxHQUFHLEtBQWdCLENBQUM7WUFDNUIsTUFBTSxDQUFDLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUQsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxtQkFBUyxDQUFDLE9BQU8sSUFBSSxTQUFTLEtBQUssbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3BFLElBQUksTUFBTSxHQUFHLEtBQWUsQ0FBQztZQUM3QixJQUFJLElBQUksR0FBRyxLQUFnQixDQUFDO1lBQzVCLE1BQU0sQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlELENBQUM7UUFFRCxxQkFBcUI7UUFDckIsRUFBRSxDQUFDLENBQUMsU0FBUyxLQUFLLG1CQUFTLENBQUMsTUFBTSxJQUFJLFNBQVMsS0FBSyxtQkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdEUsSUFBSSxNQUFNLEdBQUcsS0FBZSxDQUFDO1lBQzdCLElBQUksSUFBSSxHQUFHLEtBQWtCLENBQUM7WUFDOUIsTUFBTSxDQUFDLFdBQVcsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDaEUsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxtQkFBUyxDQUFDLFNBQVMsSUFBSSxTQUFTLEtBQUssbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3RFLElBQUksTUFBTSxHQUFHLEtBQWUsQ0FBQztZQUM3QixJQUFJLElBQUksR0FBRyxLQUFrQixDQUFDO1lBQzlCLE1BQU0sQ0FBQyxXQUFXLENBQUMsMEJBQTBCLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ2hFLENBQUM7UUFFRCxzQkFBc0I7UUFDdEIsRUFBRSxDQUFDLENBQUMsU0FBUyxLQUFLLG1CQUFTLENBQUMsT0FBTyxJQUFJLFNBQVMsS0FBSyxtQkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkUsSUFBSSxJQUFJLEdBQUcsS0FBZ0IsQ0FBQztZQUM1QixJQUFJLElBQUksR0FBRyxLQUFrQixDQUFDO1lBQzlCLE1BQU0sQ0FBQyxXQUFXLENBQUMsMkJBQTJCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQy9ELENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxTQUFTLEtBQUssbUJBQVMsQ0FBQyxTQUFTLElBQUksU0FBUyxLQUFLLG1CQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN2RSxJQUFJLElBQUksR0FBRyxLQUFnQixDQUFDO1lBQzVCLElBQUksSUFBSSxHQUFHLEtBQWtCLENBQUM7WUFDOUIsTUFBTSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0QsQ0FBQztRQUVELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFFakIsQ0FBQyxDQUFDO0lBR0Y7O09BRUc7SUFDVyxpQ0FBcUIsR0FBRyxVQUFVLElBQVcsRUFBRSxLQUFZO1FBRXJFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFFekIsRUFBRSxDQUFDLENBQUMsUUFBUSxLQUFLLG1CQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLE1BQU0sR0FBRyxJQUFjLENBQUM7WUFDNUIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDN0QsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLFFBQVEsS0FBSyxtQkFBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxPQUFPLEdBQUcsSUFBZSxDQUFDO1lBQzlCLE1BQU0sQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQy9ELENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBRWpCLENBQUMsQ0FBQztJQUdGOzs7Ozs7T0FNRztJQUNXLHlCQUFhLEdBQUcsVUFBVSxLQUFZLEVBQUUsSUFBVTtRQUU1RCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxHQUFHLEdBQVUsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RDLElBQUksR0FBRyxHQUFVLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNwQyxJQUFJLEVBQUUsR0FBVSxLQUFLLENBQUM7UUFFdEIsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU87WUFDdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxHQUFHLE9BQU87WUFDdkMsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU87WUFDdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDMUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU8sSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQztRQUNoRixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU8sSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQztRQUNoRixDQUFDO1FBRUQsSUFBSSxDQUFDLEdBQVcsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsR0FBVyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRTNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsT0FBTyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUM7SUFFeEUsQ0FBQyxDQUFDO0lBaVNGOzs7Ozs7O09BT0c7SUFDVyxxQ0FBeUIsR0FBRyxVQUFVLEtBQWMsRUFBRSxLQUFjO1FBRTlFLEVBQUU7UUFDRiwwRUFBMEU7UUFDMUUsZUFBZTtRQUNmLEVBQUU7UUFDRixzQkFBc0IsVUFBZTtZQUNqQyxJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7WUFDZCxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzdDLElBQUksVUFBVSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDL0IsSUFBSSxVQUFVLEdBQUcsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBRXJFLElBQUksTUFBTSxHQUFHLElBQUksZ0JBQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xGLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDbkIsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDO2dCQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3RCLENBQUM7WUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFRCxJQUFJLFdBQVcsR0FBb0IsS0FBSyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDNUQsSUFBSSxXQUFXLEdBQW9CLEtBQUssQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBRTVELElBQUksS0FBSyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN0QyxJQUFJLEtBQUssR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFdEMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDcEMsSUFBSSxJQUFJLEdBQVcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLG9DQUFvQztZQUNwQyxJQUFJLEVBQUUsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDckMsSUFBSSxFQUFFLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3JDLDhCQUE4QjtZQUM5QixFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQix1REFBdUQ7Z0JBQ3ZELE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDakIsQ0FBQztRQUNMLENBQUM7UUFFRCxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUNwQyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEIsb0NBQW9DO1lBQ3BDLElBQUksRUFBRSxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNyQyxJQUFJLEVBQUUsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDckMsOEJBQThCO1lBQzlCLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLHVEQUF1RDtnQkFDdkQsTUFBTSxDQUFDLEtBQUssQ0FBQztZQUNqQixDQUFDO1FBQ0wsQ0FBQztRQUNELGdFQUFnRTtRQUNoRSxzQ0FBc0M7UUFDdEMsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDLENBQUM7SUFFTixrQkFBQztDQXhkRCxBQXdkQyxJQUFBO0FBeGRZLGtDQUFXO0FBMGR4Qjs7Ozs7R0FLRztBQUNIO0lBS0ksb0JBQVksT0FBZ0IsRUFBRSxJQUFZO1FBQ3RDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN0QixHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQztZQUN4QyxnRUFBZ0U7WUFDaEUsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVELEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDaEIsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7WUFDbEIsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1lBQ2xCLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVNLDJCQUFNLEdBQWI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRU0sMkJBQU0sR0FBYjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3JCLENBQUM7SUFFTSw0QkFBTyxHQUFkLFVBQWUsV0FBZ0I7UUFDM0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ25DLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNuQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFTCxpQkFBQztBQUFELENBdENBLEFBc0NDLElBQUE7QUFFRCxrQkFBZSxXQUFXLENBQUMiLCJmaWxlIjoibWF0aC9pbnRlcnNlY3Rlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQb2ludCBmcm9tIFwiLi9wb2ludFwiO1xyXG5pbXBvcnQgU2hhcGUgZnJvbSBcIi4vc2hhcGVcIjtcclxuaW1wb3J0IENpcmNsZSBmcm9tIFwiLi9jaXJjbGVcIjtcclxuaW1wb3J0IFBvbHlnb24gZnJvbSBcIi4vcG9seWdvblwiO1xyXG5pbXBvcnQgUmVjdGFuZ2xlIGZyb20gXCIuL3JlY3RhbmdsZVwiO1xyXG5pbXBvcnQgTGluZSBmcm9tIFwiLi9saW5lXCI7XHJcbmltcG9ydCBWZWN0b3IgZnJvbSBcIi4vdmVjdG9yXCI7XHJcbmltcG9ydCBSZWxhdGl2ZVBvaW50IGZyb20gXCIuL3JlbGF0aXZlcG9pbnRcIjtcclxuaW1wb3J0IHtDb21tb259IGZyb20gXCIuL2NvbW1vblwiO1xyXG5pbXBvcnQgQ29uc3RhbnRzIGZyb20gXCIuL2NvbnN0YW50c1wiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEludGVyc2VjdGVyIHtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIHR3byBnZW9tZXRyeSBvYmplY3RzIHNoYXBlcyBpbnRlcnNlY3QgZWFjaCBvdGhlci5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBpc1NoYXBlSW50ZXJzZWN0U2hhcGUgPSBmdW5jdGlvbiAoZ2VvbTE6IFNoYXBlLCBnZW9tMjogU2hhcGUpOiBib29sZWFuIHtcclxuXHJcbiAgICAgICAgbGV0IGdlb20xVHlwZSA9IGdlb20xLnR5cGU7XHJcbiAgICAgICAgbGV0IGdlb20yVHlwZSA9IGdlb20yLnR5cGU7XHJcblxyXG4gICAgICAgIGlmIChnZW9tMVR5cGUgPT09IENvbnN0YW50cy5DSVJDTEUgJiYgZ2VvbTJUeXBlID09PSBDb25zdGFudHMuQ0lSQ0xFKSB7XHJcbiAgICAgICAgICAgIGxldCBjaXJjbGUxID0gZ2VvbTEgYXMgQ2lyY2xlO1xyXG4gICAgICAgICAgICBsZXQgY2lyY2xlMiA9IGdlb20yIGFzIENpcmNsZTtcclxuICAgICAgICAgICAgcmV0dXJuIEludGVyc2VjdGVyLmlzQ2lyY2xlSW50ZXJzZWN0Q2lyY2xlKGNpcmNsZTEsIGNpcmNsZTIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZ2VvbTFUeXBlID09PSBDb25zdGFudHMuUE9MWUdPTiAmJiBnZW9tMlR5cGUgPT09IENvbnN0YW50cy5QT0xZR09OKSB7XHJcbiAgICAgICAgICAgIGxldCBwb2x5MSA9IGdlb20xIGFzIFBvbHlnb247XHJcbiAgICAgICAgICAgIGxldCBwb2x5MiA9IGdlb20yIGFzIFBvbHlnb247XHJcbiAgICAgICAgICAgIHJldHVybiBJbnRlcnNlY3Rlci5pc1BvbHlnb25JbnRlcnNlY3RQb2x5Z29uKHBvbHkxLCBwb2x5Mik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggZ2VvbTFUeXBlID09PSBDb25zdGFudHMuUkVDVEFOR0xFICYmIGdlb20yVHlwZSA9PT0gQ29uc3RhbnRzLlJFQ1RBTkdMRSApIHtcclxuICAgICAgICAgICAgbGV0IHJlY3QxID0gZ2VvbTEgYXMgUmVjdGFuZ2xlO1xyXG4gICAgICAgICAgICBsZXQgcmVjdDIgPSBnZW9tMiBhcyBSZWN0YW5nbGU7XHJcbiAgICAgICAgICAgIHJldHVybiBJbnRlcnNlY3Rlci5pc1JlY3RhbmdsZUludGVyc2VjdFJlY3RhbmdsZShyZWN0MSwgcmVjdDIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQ2lyY2xlIC0gUG9seWdvblxyXG4gICAgICAgIGlmIChnZW9tMVR5cGUgPT09IENvbnN0YW50cy5DSVJDTEUgJiYgZ2VvbTJUeXBlID09PSBDb25zdGFudHMuUE9MWUdPTikge1xyXG4gICAgICAgICAgICBsZXQgY2lyY2xlID0gZ2VvbTEgYXMgQ2lyY2xlO1xyXG4gICAgICAgICAgICBsZXQgcG9seSA9IGdlb20yIGFzIFBvbHlnb247XHJcbiAgICAgICAgICAgIHJldHVybiBJbnRlcnNlY3Rlci5pc0NpcmNsZUludGVyc2VjdFBvbHlnb24oY2lyY2xlLCBwb2x5KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGdlb20xVHlwZSA9PT0gQ29uc3RhbnRzLlBPTFlHT04gJiYgZ2VvbTJUeXBlID09PSBDb25zdGFudHMuQ0lSQ0xFKSB7XHJcbiAgICAgICAgICAgIGxldCBjaXJjbGUgPSBnZW9tMiBhcyBDaXJjbGU7XHJcbiAgICAgICAgICAgIGxldCBwb2x5ID0gZ2VvbTEgYXMgUG9seWdvbjtcclxuICAgICAgICAgICAgcmV0dXJuIEludGVyc2VjdGVyLmlzQ2lyY2xlSW50ZXJzZWN0UG9seWdvbihjaXJjbGUsIHBvbHkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQ2lyY2xlIC0gUmVjdGFuZ2xlXHJcbiAgICAgICAgaWYgKGdlb20xVHlwZSA9PT0gQ29uc3RhbnRzLkNJUkNMRSAmJiBnZW9tMlR5cGUgPT09IENvbnN0YW50cy5SRUNUQU5HTEUpIHtcclxuICAgICAgICAgICAgbGV0IGNpcmNsZSA9IGdlb20xIGFzIENpcmNsZTtcclxuICAgICAgICAgICAgbGV0IHJlY3QgPSBnZW9tMiBhcyBSZWN0YW5nbGU7XHJcbiAgICAgICAgICAgIHJldHVybiBJbnRlcnNlY3Rlci5pc1JlY3RhbmdsZUludGVyc2VjdENpcmNsZShyZWN0LCBjaXJjbGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZ2VvbTFUeXBlID09PSBDb25zdGFudHMuUkVDVEFOR0xFICYmIGdlb20yVHlwZSA9PT0gQ29uc3RhbnRzLkNJUkNMRSkge1xyXG4gICAgICAgICAgICBsZXQgY2lyY2xlID0gZ2VvbTIgYXMgQ2lyY2xlO1xyXG4gICAgICAgICAgICBsZXQgcmVjdCA9IGdlb20xIGFzIFJlY3RhbmdsZTtcclxuICAgICAgICAgICAgcmV0dXJuIEludGVyc2VjdGVyLmlzUmVjdGFuZ2xlSW50ZXJzZWN0Q2lyY2xlKHJlY3QsIGNpcmNsZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBSZWN0YW5nbGUgLSBQb2x5Z29uXHJcbiAgICAgICAgaWYgKGdlb20xVHlwZSA9PT0gQ29uc3RhbnRzLlBPTFlHT04gJiYgZ2VvbTJUeXBlID09PSBDb25zdGFudHMuUkVDVEFOR0xFKSB7XHJcbiAgICAgICAgICAgIGxldCBwb2x5ID0gZ2VvbTEgYXMgUG9seWdvbjtcclxuICAgICAgICAgICAgbGV0IHJlY3QgPSBnZW9tMiBhcyBSZWN0YW5nbGU7XHJcbiAgICAgICAgICAgIHJldHVybiBJbnRlcnNlY3Rlci5pc1JlY3RhbmdsZUludGVyc2VjdFBvbHlnb24ocmVjdCwgcG9seSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChnZW9tMVR5cGUgPT09IENvbnN0YW50cy5SRUNUQU5HTEUgJiYgZ2VvbTJUeXBlID09PSBDb25zdGFudHMuUE9MWUdPTikge1xyXG4gICAgICAgICAgICBsZXQgcG9seSA9IGdlb20yIGFzIFBvbHlnb247XHJcbiAgICAgICAgICAgIGxldCByZWN0ID0gZ2VvbTEgYXMgUmVjdGFuZ2xlO1xyXG4gICAgICAgICAgICByZXR1cm4gSW50ZXJzZWN0ZXIuaXNSZWN0YW5nbGVJbnRlcnNlY3RQb2x5Z29uKHJlY3QsIHBvbHkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgIH07XHJcblxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgYSBnZW9tZXRyeSBzaGFwZSBpbnRlcnNlY3QgYSBwb2ludC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBpc1NoYXBlSW50ZXJzZWN0UG9pbnQgPSBmdW5jdGlvbiAoZ2VvbTogU2hhcGUsIHBvaW50OiBQb2ludCk6IGJvb2xlYW4ge1xyXG5cclxuICAgICAgICBsZXQgZ2VvbVR5cGUgPSBnZW9tLnR5cGU7XHJcblxyXG4gICAgICAgIGlmIChnZW9tVHlwZSA9PT0gQ29uc3RhbnRzLkNJUkNMRSkge1xyXG4gICAgICAgICAgICBsZXQgY2lyY2xlID0gZ2VvbSBhcyBDaXJjbGU7XHJcbiAgICAgICAgICAgIHJldHVybiBJbnRlcnNlY3Rlci5pc1BvaW50SW50ZXJzZWN0Q2lyY2xlKHBvaW50LCBjaXJjbGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZ2VvbVR5cGUgPT09IENvbnN0YW50cy5QT0xZR09OKSB7XHJcbiAgICAgICAgICAgIGxldCBwb2x5Z29uID0gZ2VvbSBhcyBQb2x5Z29uO1xyXG4gICAgICAgICAgICByZXR1cm4gSW50ZXJzZWN0ZXIuaXNQb2ludEludGVyc2VjdFBvbHlnb24ocG9pbnQsIHBvbHlnb24pO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgfTtcclxuXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgdG8gc2VlIGlmIGEgcG9pbnQgaXMgb24gYSBsaW5lLlxyXG4gICAgICogU291cmNlOiBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzcwNTAxODYvZmluZC1pZi1wb2ludC1sYXlzLW9uLWxpbmUtc2VnbWVudFxyXG4gICAgICogQHBhcmFtIHBvaW50XHJcbiAgICAgKiBAcGFyYW0gbGluZVxyXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgaXNQb2ludE9uTGluZSA9IGZ1bmN0aW9uIChwb2ludDogUG9pbnQsIGxpbmU6IExpbmUpOiBib29sZWFuIHtcclxuXHJcbiAgICAgICAgbGV0IGVwc2lsb24gPSAwLjAxO1xyXG4gICAgICAgIGxldCBwdDE6IFBvaW50ID0gbGluZS5nZXRTdGFydFBvaW50KCk7XHJcbiAgICAgICAgbGV0IHB0MjogUG9pbnQgPSBsaW5lLmdldEVuZFBvaW50KCk7XHJcbiAgICAgICAgbGV0IHB0OiBQb2ludCA9IHBvaW50O1xyXG5cclxuICAgICAgICBpZiAocHQueCAtIE1hdGgubWF4KHB0MS54LCBwdDIueCkgPiBlcHNpbG9uIHx8XHJcbiAgICAgICAgICAgIE1hdGgubWluKHB0MS54LCBwdDIueCkgLSBwdC54ID4gZXBzaWxvbiB8fFxyXG4gICAgICAgICAgICBwdC55IC0gTWF0aC5tYXgocHQxLnksIHB0Mi55KSA+IGVwc2lsb24gfHxcclxuICAgICAgICAgICAgTWF0aC5taW4ocHQxLnksIHB0Mi55KSAtIHB0LnkgPiBlcHNpbG9uKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChNYXRoLmFicyhwdDIueCAtIHB0MS54KSA8IGVwc2lsb24pIHtcclxuICAgICAgICAgICAgcmV0dXJuIE1hdGguYWJzKHB0MS54IC0gcHQueCkgPCBlcHNpbG9uIHx8IE1hdGguYWJzKHB0Mi54IC0gcHQueCkgPCBlcHNpbG9uO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKE1hdGguYWJzKHB0Mi55IC0gcHQxLnkpIDwgZXBzaWxvbikge1xyXG4gICAgICAgICAgICByZXR1cm4gTWF0aC5hYnMocHQxLnkgLSBwdC55KSA8IGVwc2lsb24gfHwgTWF0aC5hYnMocHQyLnkgLSBwdC55KSA8IGVwc2lsb247XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgeDogbnVtYmVyID0gcHQxLnggKyAocHQueSAtIHB0MS55KSAqIChwdDIueCAtIHB0MS54KSAvIChwdDIueSAtIHB0MS55KTtcclxuICAgICAgICBsZXQgeTogbnVtYmVyID0gcHQxLnkgKyAocHQueCAtIHB0MS54KSAqIChwdDIueSAtIHB0MS55KSAvIChwdDIueCAtIHB0MS54KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIE1hdGguYWJzKHB0LnggLSB4KSA8IGVwc2lsb24gfHwgTWF0aC5hYnMocHQueSAtIHkpIDwgZXBzaWxvbjtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgMiBsaW5lIHNlZ21lbnRzIGludGVyc2VjdC5cclxuICAgICAqIFNvdXJjZTogaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvNDU0MzUwNi9hbGdvcml0aG0tZm9yLWludGVyc2VjdGlvbi1vZi0yLWxpbmVzXHJcbiAgICAgKiBAcGFyYW0gbGluZTFcclxuICAgICAqIEBwYXJhbSBsaW5lMlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGlzTGluZUludGVyc2VjdExpbmUobGluZTE6IExpbmUsIGxpbmUyOiBMaW5lKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGRldGVybWluYW50ID0gbGluZTEuYSAqIGxpbmUyLmIgLSBsaW5lMi5hICogbGluZTEuYjtcclxuICAgICAgICByZXR1cm4gZGV0ZXJtaW5hbnQgIT09IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmUgaWYgdHdvIGNpcmNsZXMgaW50ZXJzZWN0IG9yIG92ZXJsYXAuXHJcbiAgICAgKlxyXG4gICAgICogSWYgZCBpcyB0aGUgZGlzdGFuY2UgYmV0d2VlbiB0aGUgY2VudGVyIG9mIHRoZSB0d28gY2lyY2xlcywgYW5kIHIwIGlzIHRoZVxyXG4gICAgICogcmFkaXVzIG9mIGNpcmNsZSAxIGFuZCByMSBpcyB0aGUgcmFkaXVzIG9mIGNpcmNsZSAyLCB0aGVuOlxyXG4gICAgICpcclxuICAgICAqIElmIGQgPiByMCArIHIxOiBUaGUgY2lyY2xlcyBhcmUgdG9vIGZhciBhcGFydCB0byBpbnRlcnNlY3QuXHJcbiAgICAgKiBJZiBkIDwgfHIwIOKAkyByMXw6IE9uZSBjaXJjbGUgaXMgaW5zaWRlIHRoZSBvdGhlciBzbyB0aGVyZSBpcyBubyBpbnRlcnNlY3Rpb24uXHJcbiAgICAgKiBJZiBkID0gMCBhbmQgcjAgPSByMTogVGhlIGNpcmNsZXMgYXJlIHRoZSBzYW1lLlxyXG4gICAgICogSWYgZCA9IHIwICsgcjE6IFRoZSBjaXJjbGVzIHRvdWNoIGF0IGEgc2luZ2xlIHBvaW50LlxyXG4gICAgICogT3RoZXJ3aXNlOiBUaGUgY2lyY2xlcyB0b3VjaCBhdCB0d28gcG9pbnRzLlxyXG4gICAgICogKE5vdyBkID0gcjAgKyByMSBiZWNhdXNlIGlmIG5vdCB0aGUgb2JqZWN0cyBzdGVlciB3YXkgY2xlYXIgb2YgZWFjaCBvdGhlcilcclxuICAgICAqIEBwYXJhbSBjaXJjbGUxXHJcbiAgICAgKiBAcGFyYW0gY2lyY2xlMlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGlzQ2lyY2xlSW50ZXJzZWN0Q2lyY2xlKGNpcmNsZTE6IENpcmNsZSwgY2lyY2xlMjogQ2lyY2xlKSB7XHJcblxyXG4gICAgICAgIGxldCBkOiBudW1iZXIgPSBDb21tb24uZGlzdGFuY2UyZChjaXJjbGUxLngsIGNpcmNsZTEueSwgY2lyY2xlMi54LCBjaXJjbGUyLnkpO1xyXG4gICAgICAgIGxldCByMDogbnVtYmVyID0gY2lyY2xlMS5nZXRSYWRpdXMoKTtcclxuICAgICAgICBsZXQgcjE6IG51bWJlciA9IGNpcmNsZTIuZ2V0UmFkaXVzKCk7XHJcblxyXG4gICAgICAgIHJldHVybiAoZCA8IHIwICsgcjEpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIHR3byByZWN0YW5nbGVzIGludGVyc2VjdC5cclxuICAgICAqIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzEzMzkwMzMzL3R3by1yZWN0YW5nbGVzLWludGVyc2VjdGlvblxyXG4gICAgICogQHBhcmFtIHJlY3QxXHJcbiAgICAgKiBAcGFyYW0gcmVjdDJcclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBpc1JlY3RhbmdsZUludGVyc2VjdFJlY3RhbmdsZShyZWN0MTogUmVjdGFuZ2xlLCByZWN0MjogUmVjdGFuZ2xlKSB7XHJcbiAgICAgICAgbGV0IHgxOiBudW1iZXIgPSByZWN0MS5wb3NpdGlvbigpLng7XHJcbiAgICAgICAgbGV0IHkxOiBudW1iZXIgPSByZWN0MS5wb3NpdGlvbigpLnk7XHJcbiAgICAgICAgbGV0IHdpZHRoMTogbnVtYmVyID0gcmVjdDEud2lkdGgoKTtcclxuICAgICAgICBsZXQgaGVpZ2h0MTogbnVtYmVyID0gcmVjdDEuaGVpZ2h0KCk7XHJcblxyXG4gICAgICAgIGxldCB4MjogbnVtYmVyID0gcmVjdDIucG9zaXRpb24oKS54O1xyXG4gICAgICAgIGxldCB5MjogbnVtYmVyID0gcmVjdDIucG9zaXRpb24oKS55O1xyXG4gICAgICAgIGxldCB3aWR0aDI6IG51bWJlciA9IHJlY3QyLndpZHRoKCk7XHJcbiAgICAgICAgbGV0IGhlaWdodDI6IG51bWJlciA9IHJlY3QyLmhlaWdodCgpO1xyXG4gICAgICAgIHJldHVybiAhKHgxID4geDIgKyB3aWR0aDIgfHwgeDEgKyB3aWR0aDEgPCB4MiB8fCB5MSA+IHkyICsgaGVpZ2h0MiB8fCB5MSArIGhlaWdodDEgPCB5Mik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVjayBpZiBjaXJjbGUgaW50ZXJzZWN0cyByZWN0YW5nbGUuXHJcbiAgICAgKiBAcGFyYW0gcmVjdFxyXG4gICAgICogQHBhcmFtIGNpcmNsZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGlzUmVjdGFuZ2xlSW50ZXJzZWN0Q2lyY2xlKHJlY3Q6IFJlY3RhbmdsZSwgY2lyY2xlOiBDaXJjbGUpIHtcclxuXHJcbiAgICAgICAgbGV0IGNpcmNsZURpc3RhbmNlWDogbnVtYmVyID0gTWF0aC5hYnMoY2lyY2xlLnggLSByZWN0LngpO1xyXG4gICAgICAgIGxldCBjaXJjbGVEaXN0YW5jZVk6IG51bWJlciA9ICBNYXRoLmFicyhjaXJjbGUueSAtIHJlY3QueSk7XHJcblxyXG4gICAgICAgIGlmIChjaXJjbGVEaXN0YW5jZVggPiAocmVjdC53aWR0aCgpIC8gMiArIGNpcmNsZS5nZXRSYWRpdXMoKSkpIHsgcmV0dXJuIGZhbHNlOyB9XHJcbiAgICAgICAgaWYgKGNpcmNsZURpc3RhbmNlWSA+IChyZWN0LmhlaWdodCgpIC8gMiArIGNpcmNsZS5nZXRSYWRpdXMoKSkpIHsgcmV0dXJuIGZhbHNlOyB9XHJcblxyXG4gICAgICAgIGlmIChjaXJjbGVEaXN0YW5jZVggPD0gKHJlY3Qud2lkdGgoKSAvIDIpKSB7IHJldHVybiB0cnVlOyB9XHJcbiAgICAgICAgaWYgKGNpcmNsZURpc3RhbmNlWSA8PSAocmVjdC5oZWlnaHQoKSAvIDIpKSB7IHJldHVybiB0cnVlOyB9XHJcblxyXG4gICAgICAgIGxldCBjb3JuZXJEaXN0YW5jZVNxID0gKGNpcmNsZURpc3RhbmNlWCAtIHJlY3Qud2lkdGgoKSAvIDIpICogKGNpcmNsZURpc3RhbmNlWCAtIHJlY3Qud2lkdGgoKSAvIDIpICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChjaXJjbGVEaXN0YW5jZVkgLSByZWN0LmhlaWdodCgpIC8gMikgKiAoY2lyY2xlRGlzdGFuY2VZIC0gcmVjdC5oZWlnaHQoKSAvIDIpO1xyXG5cclxuICAgICAgICByZXR1cm4gKGNvcm5lckRpc3RhbmNlU3EgPD0gY2lyY2xlLmdldFJhZGl1cygpICogY2lyY2xlLmdldFJhZGl1cygpKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmVzIGlmIGEgcG9pbnQgaXMgaW5zaWRlIGEgY2lyY2xlXHJcbiAgICAgKiBAcGFyYW0gcG9pbnRcclxuICAgICAqIEBwYXJhbSBjaXJjbGVcclxuICAgICAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHRoZSBwb2ludCBpcyBpbnNpZGUgdGhlIGNpcmNsZS4gRmFsc2Ugb3RoZXJ3aXNlLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGlzUG9pbnRJbnRlcnNlY3RDaXJjbGUocG9pbnQ6IFBvaW50LCBjaXJjbGU6IENpcmNsZSk6IGJvb2xlYW4ge1xyXG5cclxuICAgICAgICBsZXQgcmFkaXVzOiBudW1iZXIgPSBjaXJjbGUuZ2V0UmFkaXVzKCk7XHJcbiAgICAgICAgbGV0IGRpc3Q6IG51bWJlciA9IENvbW1vbi5kaXN0YW5jZTJkKGNpcmNsZS54LCBjaXJjbGUueSwgcG9pbnQueCwgcG9pbnQueSk7XHJcbiAgICAgICAgcmV0dXJuICggZGlzdCA8PSByYWRpdXMgKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVjayBpZiBhIHBvaW50IGlzIGluc2lkZSBhIHBvbHlnb24uXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHBvaW50XHJcbiAgICAgKiBAcGFyYW0gcG9seVxyXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgaXNQb2ludEludGVyc2VjdFBvbHlnb24ocG9pbnQ6IFBvaW50LCBwb2x5OiBQb2x5Z29uKTogYm9vbGVhbiB7XHJcblxyXG4gICAgICAgIGxldCBpID0gMCwgaiA9IDA7XHJcbiAgICAgICAgbGV0IGMgPSBmYWxzZTtcclxuICAgICAgICBsZXQgdmVydGljZXM6IFJlbGF0aXZlUG9pbnRbXSA9IHBvbHkuZ2V0QXJyYXlPZlBvaW50cygpO1xyXG4gICAgICAgIGxldCBudW1iZXJPZlZlcnRpY2VzOiBudW1iZXIgPSB2ZXJ0aWNlcy5sZW5ndGg7XHJcblxyXG4gICAgICAgIGZvciAoaSA9IDAsIGogPSBudW1iZXJPZlZlcnRpY2VzIC0gMTsgaSA8IG51bWJlck9mVmVydGljZXM7IGogPSBpKyspIHtcclxuICAgICAgICAgICAgaWYgKCgodmVydGljZXNbaV0ueSA+IHBvaW50LnkpICE9PSAodmVydGljZXNbal0ueSA+IHBvaW50LnkpKSAmJlxyXG4gICAgICAgICAgICAgICAgKHBvaW50LnggPCAodmVydGljZXNbal0ueCAtIHZlcnRpY2VzW2ldLngpICogKHBvaW50LnkgLSB2ZXJ0aWNlc1tpXS55KSAvICh2ZXJ0aWNlc1tqXS55IC0gdmVydGljZXNbaV0ueSkgKyB2ZXJ0aWNlc1tpXS54KSkge1xyXG4gICAgICAgICAgICAgICAgYyA9ICFjO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gYztcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUZXN0IHRvIHNlZSBpZiBhIGxpbmUgaW50ZXJzZWN0cyBhIGNpcmNsZS5cclxuICAgICAqIFNvdXJjZTogaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8xMDczMzM2L2NpcmNsZS1saW5lLXNlZ21lbnQtY29sbGlzaW9uLWRldGVjdGlvbi1hbGdvcml0aG1cclxuICAgICAqIEBwYXJhbSBsaW5lXHJcbiAgICAgKiBAcGFyYW0gY2lyY2xlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgaXNMaW5lSW50ZXJzZWN0Q2lyY2xlKGxpbmU6IExpbmUsIGNpcmNsZTogQ2lyY2xlKTogYm9vbGVhbiB7XHJcblxyXG4gICAgICAgIC8vIERpcmVjdGlvbiB2ZWN0b3Igb2YgcmF5LCBmcm9tIHN0YXJ0IHRvIGVuZCApXHJcbiAgICAgICAgbGV0IGQgPSBWZWN0b3Iuc3VidHJhY3QobGluZS5nZXRTdGFydFBvaW50KCksIGxpbmUuZ2V0RW5kUG9pbnQoKSk7XHJcblxyXG4gICAgICAgIC8vIFZlY3RvciBmcm9tIGNlbnRlciBzcGhlcmUgdG8gcmF5IHN0YXJ0IClcclxuICAgICAgICBsZXQgZiA9IFZlY3Rvci5zdWJ0cmFjdChjaXJjbGUuZ2V0Q2VudGVyKCksIGxpbmUuZ2V0U3RhcnRQb2ludCgpKTtcclxuXHJcbiAgICAgICAgbGV0IHIgPSBjaXJjbGUuZ2V0UmFkaXVzKCk7XHJcbiAgICAgICAgbGV0IGEgPSBkLmRvdChkKTtcclxuICAgICAgICBsZXQgYiA9IDIgKiBmLmRvdChkKTtcclxuICAgICAgICBsZXQgYyA9IGYuZG90KGYpIC0gciAqIHI7XHJcblxyXG4gICAgICAgIGxldCBkaXNjcmltaW5hbnQgPSBiICogYiAtIDQgKiBhICogYztcclxuICAgICAgICBpZiAoZGlzY3JpbWluYW50IDwgMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgIC8vIFJheSBkaWRuXCJ0IHRvdGFsbHkgbWlzcyBzcGhlcmUsIHNvIHRoZXJlIGlzIGEgc29sdXRpb24gdG8gdGhlIGVxdWF0aW9uLlxyXG4gICAgICAgICAgICBkaXNjcmltaW5hbnQgPSBNYXRoLnNxcnQoZGlzY3JpbWluYW50KTtcclxuXHJcbiAgICAgICAgICAgIC8vIGVpdGhlciBzb2x1dGlvbiBtYXkgYmUgb24gb3Igb2ZmIHRoZSByYXkgc28gbmVlZCB0byB0ZXN0IGJvdGggdDEgaXMgYWx3YXlzIHRoZSBzbWFsbGVyIHZhbHVlLCBiZWNhdXNlXHJcbiAgICAgICAgICAgIC8vIEJPVEggZGlzY3JpbWluYW50IGFuZCBhIGFyZSBub24tbmVnYXRpdmUuXHJcbiAgICAgICAgICAgIGxldCB0MSA9ICgtYiAtIGRpc2NyaW1pbmFudCkgLyAoMiAqIGEpO1xyXG4gICAgICAgICAgICBsZXQgdDIgPSAoLWIgKyBkaXNjcmltaW5hbnQpIC8gKDIgKiBhKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0MSA+PSAwICYmIHQxIDw9IDEpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBIZXJlIHQxIGRpZG5cInQgaW50ZXJzZWN0IHNvIHdlIGFyZSBlaXRoZXIgc3RhcnRlZCBpbnNpZGUgdGhlIHNwaGVyZSBvciBjb21wbGV0ZWx5IHBhc3QgaXQuXHJcbiAgICAgICAgICAgIGlmICh0MiA+PSAwICYmIHQyIDw9IDEpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBObyBpbnRlcnNlY3Rpb24uXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGVzdCB0byBzZWUgaWYgYSBjaXJjbGUgaW50ZXJzZWN0cyBhIHBvbHlnb24uXHJcbiAgICAgKiBAcGFyYW0gY2lyY2xlXHJcbiAgICAgKiBAcGFyYW0gcG9seWdvblxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGlzQ2lyY2xlSW50ZXJzZWN0UG9seWdvbihjaXJjbGU6IENpcmNsZSwgcG9seWdvbjogUG9seWdvbik6IGJvb2xlYW4ge1xyXG5cclxuICAgICAgICAvLyBJZiBjZW50ZXIgb2YgdGhlIGNpcmNsZSBpcyBpbiB0aGUgcG9seWdvbiwgdGhlbiBpdCBpbnRlcnNlY3RzLlxyXG4gICAgICAgIGlmIChJbnRlcnNlY3Rlci5pc1BvaW50SW50ZXJzZWN0UG9seWdvbihjaXJjbGUuZ2V0Q2VudGVyKCksIHBvbHlnb24pKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gSWYgbGluZSBpbnRlcnNlY3RzIHRoZSBjaXJjbGUsIHRoZW4gaXQgaW50ZXJzZWN0cy5cclxuICAgICAgICBsZXQgYXJyYXlPZlBvaW50czogUmVsYXRpdmVQb2ludFtdID0gcG9seWdvbi5nZXRBcnJheU9mUG9pbnRzKCk7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhcnJheU9mUG9pbnRzLmxlbmd0aCAtIDE7IGkrKykge1xyXG4gICAgICAgICAgICBsZXQgbGluZSA9IG5ldyBMaW5lKGFycmF5T2ZQb2ludHNbaV0sIGFycmF5T2ZQb2ludHNbaSArIDFdKTtcclxuICAgICAgICAgICAgaWYgKEludGVyc2VjdGVyLmlzTGluZUludGVyc2VjdENpcmNsZShsaW5lLCBjaXJjbGUpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRlc3QgdG8gc2VlIGlmIGEgcmVjdGFuZ2xlIGludGVyc2VjdHMgYSBwb2x5Z29uLlxyXG4gICAgICogQHBhcmFtIHJlY3RcclxuICAgICAqIEBwYXJhbSBwb2x5Z29uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgaXNSZWN0YW5nbGVJbnRlcnNlY3RQb2x5Z29uKHJlY3Q6IFJlY3RhbmdsZSwgcG9seWdvbjogUG9seWdvbik6IGJvb2xlYW4ge1xyXG5cclxuICAgICAgICAvLyBJZiBjZW50ZXIgb2YgdGhlIHJlY3RhbmdsZSBpcyBpbiB0aGUgcG9seWdvbiwgdGhlbiBpdCBpbnRlcnNlY3RzLlxyXG4gICAgICAgIGlmIChJbnRlcnNlY3Rlci5pc1BvaW50SW50ZXJzZWN0UG9seWdvbihyZWN0LmdldENlbnRlcigpLCBwb2x5Z29uKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIElmIGxpbmUgaW50ZXJzZWN0cyB0aGUgcmVjdGFuZ2xlLCB0aGVuIGl0IGludGVyc2VjdHMuXHJcbiAgICAgICAgbGV0IGFycmF5T2ZQb2ludHM6IFJlbGF0aXZlUG9pbnRbXSA9IHBvbHlnb24uZ2V0QXJyYXlPZlBvaW50cygpO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYXJyYXlPZlBvaW50cy5sZW5ndGggLSAxOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IGxpbmUgPSBuZXcgTGluZShhcnJheU9mUG9pbnRzW2ldLCBhcnJheU9mUG9pbnRzW2kgKyAxXSk7XHJcbiAgICAgICAgICAgIGlmIChJbnRlcnNlY3Rlci5pc0xpbmVJbnRlcnNlY3RSZWN0YW5nbGUobGluZSwgcmVjdCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGVzdCB0byBzZWUgaWYgYSBsaW5lIGludGVyc2VjdHMgYSByZWN0YW5nbGUuXHJcbiAgICAgKiBTb3VyY2U6IGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzk5MzUzL2hvdy10by10ZXN0LWlmLWEtbGluZS1zZWdtZW50LWludGVyc2VjdHMtYW4tYXhpcy1hbGlnbmVkLXJlY3RhbmdlLWluLTJkXHJcbiAgICAgKiBAcGFyYW0gbGluZVxyXG4gICAgICogQHBhcmFtIHJlY3RcclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBpc0xpbmVJbnRlcnNlY3RSZWN0YW5nbGUobGluZTogTGluZSwgcmVjdDogUmVjdGFuZ2xlKTogYm9vbGVhbiB7XHJcblxyXG4gICAgICAgIGxldCBhX3JlY3RhbmdsZU1pblg6IG51bWJlciA9IHJlY3QucG9zaXRpb24oKS54O1xyXG4gICAgICAgIGxldCBhX3JlY3RhbmdsZU1pblk6IG51bWJlciA9IHJlY3QucG9zaXRpb24oKS55O1xyXG4gICAgICAgIGxldCBhX3JlY3RhbmdsZU1heFg6IG51bWJlciA9IHJlY3QucG9zaXRpb24oKS54ICsgcmVjdC53aWR0aCgpO1xyXG4gICAgICAgIGxldCBhX3JlY3RhbmdsZU1heFk6IG51bWJlciA9IHJlY3QucG9zaXRpb24oKS55ICsgcmVjdC5oZWlnaHQoKTtcclxuICAgICAgICBsZXQgYV9wMXg6IG51bWJlciA9IGxpbmUuZ2V0U3RhcnRQb2ludCgpLng7XHJcbiAgICAgICAgbGV0IGFfcDF5OiBudW1iZXIgPSBsaW5lLmdldFN0YXJ0UG9pbnQoKS55O1xyXG4gICAgICAgIGxldCBhX3AyeDogbnVtYmVyID0gbGluZS5nZXRFbmRQb2ludCgpLng7XHJcbiAgICAgICAgbGV0IGFfcDJ5OiBudW1iZXIgPSBsaW5lLmdldEVuZFBvaW50KCkueTtcclxuXHJcbiAgICAgICAgLy8gRmluZCBtaW4gYW5kIG1heCBYIGZvciB0aGUgc2VnbWVudFxyXG4gICAgICAgIGxldCBtaW5YOiBudW1iZXIgPSBhX3AxeDtcclxuICAgICAgICBsZXQgbWF4WDogbnVtYmVyID0gYV9wMng7XHJcblxyXG4gICAgICAgIGlmIChhX3AxeCA+IGFfcDJ4KSB7XHJcbiAgICAgICAgICAgIG1pblggPSBhX3AyeDtcclxuICAgICAgICAgICAgbWF4WCA9IGFfcDF4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gRmluZCB0aGUgaW50ZXJzZWN0aW9uIG9mIHRoZSBzZWdtZW50J3MgYW5kIHJlY3RhbmdsZSdzIHgtcHJvamVjdGlvbnNcclxuICAgICAgICBpZiAobWF4WCA+IGFfcmVjdGFuZ2xlTWF4WCkge1xyXG4gICAgICAgICAgICBtYXhYID0gYV9yZWN0YW5nbGVNYXhYO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAobWluWCA8IGFfcmVjdGFuZ2xlTWluWCkge1xyXG4gICAgICAgICAgICBtaW5YID0gYV9yZWN0YW5nbGVNaW5YO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gSWYgdGhlaXIgcHJvamVjdGlvbnMgZG8gbm90IGludGVyc2VjdCByZXR1cm4gZmFsc2VcclxuICAgICAgICBpZiAobWluWCA+IG1heFgpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBGaW5kIGNvcnJlc3BvbmRpbmcgbWluIGFuZCBtYXggWSBmb3IgbWluIGFuZCBtYXggWCB3ZSBmb3VuZCBiZWZvcmVcclxuICAgICAgICBsZXQgbWluWTogbnVtYmVyID0gYV9wMXk7XHJcbiAgICAgICAgbGV0IG1heFk6IG51bWJlciA9IGFfcDJ5O1xyXG4gICAgICAgIGxldCBkeDogbnVtYmVyID0gYV9wMnggLSBhX3AxeDtcclxuXHJcbiAgICAgICAgaWYgKCBNYXRoLmFicyhkeCkgPiAwLjAwMDAwMDEgKSB7XHJcbiAgICAgICAgICAgIGxldCBhOiBudW1iZXIgPSAoYV9wMnkgLSBhX3AxeSkgLyBkeDtcclxuICAgICAgICAgICAgbGV0IGI6IG51bWJlciA9IGFfcDF5IC0gYSAqIGFfcDF4O1xyXG4gICAgICAgICAgICBtaW5ZID0gYSAqIG1pblggKyBiO1xyXG4gICAgICAgICAgICBtYXhZID0gYSAqIG1heFggKyBiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCBtaW5ZID4gbWF4WSApIHtcclxuICAgICAgICAgICAgbGV0IHRtcDogbnVtYmVyID0gbWF4WTtcclxuICAgICAgICAgICAgbWF4WSA9IG1pblk7XHJcbiAgICAgICAgICAgIG1pblkgPSB0bXA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBGaW5kIHRoZSBpbnRlcnNlY3Rpb24gb2YgdGhlIHNlZ21lbnQncyBhbmQgcmVjdGFuZ2xlJ3MgeS1wcm9qZWN0aW9uc1xyXG4gICAgICAgIGlmICggbWF4WSA+IGFfcmVjdGFuZ2xlTWF4WSApIHtcclxuICAgICAgICAgICAgbWF4WSA9IGFfcmVjdGFuZ2xlTWF4WTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCBtaW5ZIDwgYV9yZWN0YW5nbGVNaW5ZICkge1xyXG4gICAgICAgICAgICBtaW5ZID0gYV9yZWN0YW5nbGVNaW5ZO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gSWYgWS1wcm9qZWN0aW9ucyBkbyBub3QgaW50ZXJzZWN0IHJldHVybiBmYWxzZVxyXG4gICAgICAgIGlmICggbWluWSA+IG1heFkgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIHRvIHNlZSBpZiB0d28gcG9seWdvbnMgaW50ZXJzZWN0LiBVc2VzIHRoZSBheGlzLXNlcGFyYXRpb24gdGVzdC5cclxuICAgICAqXHJcbiAgICAgKiBTb3VyY2U6IGh0dHA6Ly93d3cuZHluNGoub3JnLzIwMTAvMDEvc2F0L1xyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBwb2x5MVxyXG4gICAgICogQHBhcmFtIHBvbHkyXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgaXNQb2x5Z29uSW50ZXJzZWN0UG9seWdvbiA9IGZ1bmN0aW9uIChwb2x5MTogUG9seWdvbiwgcG9seTI6IFBvbHlnb24pIHtcclxuXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyBHZW5lcmF0ZSBhIGxpc3Qgb2YgYXhpcyAobm9ybWFsaXplZCB2ZWN0b3JzKSBmcm9tIGFuIGFycmF5IG9mIHBvaW50cyBpblxyXG4gICAgICAgIC8vIHRoZSBwb2x5Z29uLlxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgZnVuY3Rpb24gZ2VuZXJhdGVBeGlzKHBvbHlQb2ludHM6IGFueSkge1xyXG4gICAgICAgICAgICBsZXQgYXhpcyA9IFtdO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBvbHlQb2ludHMubGVuZ3RoIC0gMTsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9seVBvaW50QSA9IHBvbHlQb2ludHNbaV07XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9seVBvaW50QiA9IHBvbHlQb2ludHNbaSArIDEgPT09IHBvbHlQb2ludHMubGVuZ3RoID8gMCA6IGkgKyAxXTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgdmVjdG9yID0gbmV3IFZlY3Rvcihwb2x5UG9pbnRCLnggLSBwb2x5UG9pbnRBLngsIHBvbHlQb2ludEIueSAtIHBvbHlQb2ludEEueSk7XHJcbiAgICAgICAgICAgICAgICB2ZWN0b3Iubm9ybWFsaXplKCk7XHJcbiAgICAgICAgICAgICAgICB2ZWN0b3IucGVycGVuZGljdWxhcigpO1xyXG4gICAgICAgICAgICAgICAgYXhpcy5wdXNoKHZlY3Rvcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGF4aXM7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgcG9seTFQb2ludHM6IFJlbGF0aXZlUG9pbnRbXSA9IHBvbHkxLmdldEFycmF5T2ZQb2ludHMoKTtcclxuICAgICAgICBsZXQgcG9seTJQb2ludHM6IFJlbGF0aXZlUG9pbnRbXSA9IHBvbHkyLmdldEFycmF5T2ZQb2ludHMoKTtcclxuXHJcbiAgICAgICAgbGV0IGF4ZXMxID0gZ2VuZXJhdGVBeGlzKHBvbHkxUG9pbnRzKTtcclxuICAgICAgICBsZXQgYXhlczIgPSBnZW5lcmF0ZUF4aXMocG9seTJQb2ludHMpO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGF4ZXMxLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGxldCBheGlzOiBWZWN0b3IgPSBheGVzMVtpXTtcclxuICAgICAgICAgICAgLy8gcHJvamVjdCBib3RoIHNoYXBlcyBvbnRvIHRoZSBheGlzXHJcbiAgICAgICAgICAgIGxldCBwMSA9IG5ldyBQcm9qZWN0aW9uKHBvbHkxLCBheGlzKTtcclxuICAgICAgICAgICAgbGV0IHAyID0gbmV3IFByb2plY3Rpb24ocG9seTIsIGF4aXMpO1xyXG4gICAgICAgICAgICAvLyBkbyB0aGUgcHJvamVjdGlvbnMgb3ZlcmxhcD9cclxuICAgICAgICAgICAgaWYgKCFwMS5vdmVybGFwKHAyKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gdGhlbiB3ZSBjYW4gZ3VhcmFudGVlIHRoYXQgdGhlIHNoYXBlcyBkbyBub3Qgb3ZlcmxhcFxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGF4ZXMyLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGxldCBheGlzID0gYXhlczJbaV07XHJcbiAgICAgICAgICAgIC8vIHByb2plY3QgYm90aCBzaGFwZXMgb250byB0aGUgYXhpc1xyXG4gICAgICAgICAgICBsZXQgcDEgPSBuZXcgUHJvamVjdGlvbihwb2x5MSwgYXhpcyk7XHJcbiAgICAgICAgICAgIGxldCBwMiA9IG5ldyBQcm9qZWN0aW9uKHBvbHkyLCBheGlzKTtcclxuICAgICAgICAgICAgLy8gZG8gdGhlIHByb2plY3Rpb25zIG92ZXJsYXA/XHJcbiAgICAgICAgICAgIGlmICghcDEub3ZlcmxhcChwMikpIHtcclxuICAgICAgICAgICAgICAgIC8vIHRoZW4gd2UgY2FuIGd1YXJhbnRlZSB0aGF0IHRoZSBzaGFwZXMgZG8gbm90IG92ZXJsYXBcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBpZiB3ZSBnZXQgaGVyZSB0aGVuIHdlIGtub3cgdGhhdCBldmVyeSBheGlzIGhhZCBvdmVybGFwIG9uIGl0XHJcbiAgICAgICAgLy8gc28gd2UgY2FuIGd1YXJhbnRlZSBhbiBpbnRlcnNlY3Rpb25cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH07XHJcblxyXG59XHJcblxyXG4vKipcclxuICogQSBwcm9qZWN0aW9uIG9iamVjdCBiYXNlZCBvbiBwcm9qZWN0aW5nIHRoZSBwb2ludHMgb2YgYSBwb2x5Z29uIHRvIHRoZVxyXG4gKiBnaXZlbiBheGlzLlxyXG4gKiBAcGFyYW0gcG9seWdvbiBBIGNvbnZleCBwb2x5Z29uLlxyXG4gKiBAcGFyYW0gYXhpcyBBIHZlY3Rvci5cclxuICovXHJcbmNsYXNzIFByb2plY3Rpb24ge1xyXG5cclxuICAgIHByaXZhdGUgX21pbjogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfbWF4OiBudW1iZXI7XHJcblxyXG4gICAgY29uc3RydWN0b3IocG9seWdvbjogUG9seWdvbiwgYXhpczogVmVjdG9yKSB7XHJcbiAgICAgICAgbGV0IHBvaW50cyA9IHBvbHlnb24uZ2V0QXJyYXlPZlBvaW50cygpO1xyXG4gICAgICAgIHRoaXMuX21pbiA9IGF4aXMuZG90Mihwb2ludHNbMF0ueCwgcG9pbnRzWzBdLnksIHBvaW50c1swXS56KTtcclxuICAgICAgICB0aGlzLl9tYXggPSB0aGlzLl9taW47XHJcbiAgICAgICAgZm9yIChsZXQgaWkgPSAxOyBpaSA8IHBvaW50cy5sZW5ndGg7IGlpKyspIHtcclxuICAgICAgICAgICAgLy8gTk9URTogdGhlIGF4aXMgbXVzdCBiZSBub3JtYWxpemVkIHRvIGdldCBhY2N1cmF0ZSBwcm9qZWN0aW9uc1xyXG4gICAgICAgICAgICBsZXQgcCA9IGF4aXMuZG90Mihwb2ludHNbaWldLngsIHBvaW50c1tpaV0ueSwgcG9pbnRzW2lpXS56KTtcclxuICAgICAgICAgICAgaWYgKHAgPCB0aGlzLl9taW4pIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX21pbiA9IHA7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocCA+IHRoaXMuX21heCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fbWF4ID0gcDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0TWluKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21pbjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0TWF4KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21heDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb3ZlcmxhcChwcm9qZWN0aW9uMjogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuX21heCA+IHByb2plY3Rpb24yLmdldE1pbigpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fbWluID4gcHJvamVjdGlvbjIuZ2V0TWF4KCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEludGVyc2VjdGVyO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
