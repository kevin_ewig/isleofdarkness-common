export class Point {

    /**
     * Type of this point.
     */
    public static TYPE = "point";

    private _x: number;
    private _y: number;
    private _z: number;

    /**
     * Convert a JSON object to a Point.
     * @param jsonObj
     * @returns {math.Point}
     */
    public static fromJSON(jsonObj: any): Point  {

        if (!jsonObj) {
            throw new Error("Cannot convert undefined to Point");
        } else if (!jsonObj.hasOwnProperty("x")) {
            throw new Error("X property does not exist");
        } else if (!jsonObj.hasOwnProperty("y")) {
            throw new Error("Y property does not exist");
        } else if (!jsonObj.hasOwnProperty("z")) {
            throw new Error("Z property does not exist");
        } else if (!jsonObj.hasOwnProperty("type")) {
            throw new Error("Type property does not exist");
        } else if (jsonObj.type !== Point.TYPE) {
            throw new Error("Invalid type");
        }

        let tempPoint = new Point(jsonObj.x, jsonObj.y, jsonObj.z);
        return tempPoint;

    }

    constructor( pointX: number, pointY: number, pointZ= 0 ) {
        this._x = pointX === undefined ? 0 : pointX;
        this._y = pointY === undefined ? 0 : pointY;
        this._z = pointZ === undefined ? 0 : pointZ;
    }

    /**
     * Clone this point.
     */
    public clone(): Point {
        return new Point(this._x, this._y, this._z);
    }

    /**
     * Check if this point is equal to another point.
     */
    public equals( otherPoint: Point ) {
        if ( !otherPoint ) {
            return false;
        }
        return ( otherPoint.x === this.x && otherPoint.y === this.y && otherPoint.z === this.z );
    }

    /**
     * Get the X value.
     * @returns {number}
     */
    get x() { return this._x; }

    /**
     * Set the X value.
     */
    set x(value: number) {
        this._x = value;
    }

    /**
     * Get the Y value.
     * @returns {number}
     */
    get y() { return this._y; }

    /**
     * Set the Y value.
     */
    set y(value: number) {
        this._y = value;
    }

    /**
     * Get the Z value.
     * @returns {number}
     */
    get z() { return this._z; }

    /**
     * Set the Z value.
     */
    set z(value: number) {
        this._z = value;
    }

    /**
     * Get the world section related to this point.
     */
    public getSectionCoordinate(): Point {
        let sectionX: number = Math.round(this._x / 100);
        let sectionY: number = Math.round(this._y / 100);
        let sectionZ: number = Math.round(this._z / 100);
        return new Point(sectionX, sectionY, sectionZ);
    }

    /**
     * Convert this point to JSON.
     */
    public toJSON() {
        return {
            "x": this._x,
            "y": this._y,
            "z": this._z,
            "type" : Point.TYPE
        };
    }

    /**
     * Convert this point to string..
     */
    public toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    /**
     * Get the type of this object.
     */
    get type(): string  {
        return Point.TYPE;
    }

}

export default Point;
