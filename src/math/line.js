"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("./point");
var Line = /** @class */ (function () {
    function Line(pointStart, pointEnd) {
        this._pointStart = pointStart;
        this._pointEnd = pointEnd;
        this._a = pointEnd.y - pointStart.y;
        this._b = pointStart.x - pointEnd.x;
        this._c = this._a * pointStart.x + this._b * pointStart.y;
    }
    /**
     * Convert JSON object to a Line object.
     * @param jsonObj
     * @returns {math.Line}
     */
    Line.fromJSON = function (jsonObj) {
        if (!jsonObj) {
            throw new Error("Cannot convert undefined to Line");
        }
        else if (!jsonObj.hasOwnProperty("pointStart")) {
            throw new Error("pointStart property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("pointEnd")) {
            throw new Error("pointEnd property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("type")) {
            throw new Error("Type property does not exist");
        }
        else if (jsonObj.type !== Line.TYPE) {
            throw new Error("Invalid type");
        }
        var pointStart = point_1.default.fromJSON(jsonObj.pointStart);
        var pointEnd = point_1.default.fromJSON(jsonObj.pointEnd);
        return new Line(pointStart, pointEnd);
    };
    Object.defineProperty(Line.prototype, "a", {
        /**
         * Get the A value.
         * @returns {number}
         */
        get: function () { return this._a; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Line.prototype, "b", {
        /**
         * Get the B value.
         * @returns {number}
         */
        get: function () { return this._b; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Line.prototype, "c", {
        /**
         * Get the C value.
         * @returns {number}
         */
        get: function () { return this._c; },
        enumerable: true,
        configurable: true
    });
    /**
     * Clone this line.
     * @returns {math.Line}
     */
    Line.prototype.clone = function () {
        return new Line(this._pointStart, this._pointEnd);
    };
    /**
     * Returns the angle (in degrees) for this line
     * relative to the x-axis.
     * @returns {number}
     */
    Line.prototype.getAngle = function () {
        var radians = this.getRadiansAngle();
        return radians * 180 / Math.PI;
    };
    /**
     * Get the center of this line.
     */
    Line.prototype.getCenter = function () {
        var mx = (this._pointStart.x + this._pointEnd.x) / 2;
        var my = (this._pointStart.y + this._pointEnd.y) / 2;
        var mz = (this._pointStart.z + this._pointEnd.z) / 2;
        return new point_1.default(mx, my, mz);
    };
    /**
     * Get the end point of this line segment.
     * @returns {Point}
     */
    Line.prototype.getEndPoint = function () {
        return this._pointEnd;
    };
    /**
     * Returns the length of this line segment.
     * @returns {number}
     */
    Line.prototype.getLength = function () {
        var ax = this._pointStart.x;
        var ay = this._pointStart.y;
        var bx = this._pointEnd.x;
        var by = this._pointEnd.y;
        return Math.sqrt((ax - bx) * (ax - bx) + (ay - by) * (ay - by));
    };
    /**
     * Returns the angle (in radians) for this line
     * relative to the x-axis.
     * @returns {number}
     */
    Line.prototype.getRadiansAngle = function () {
        var ax = this._pointStart.x;
        var ay = this._pointStart.y;
        // Shift the end point relative to origin and normalize it.
        var dx = (this._pointEnd.x - ax);
        var dy = (this._pointEnd.y - ay);
        return Math.atan2(dy, dx);
    };
    /**
     * Get the start point of this line segment.
     * @returns {Point}
     */
    Line.prototype.getStartPoint = function () {
        return this._pointStart;
    };
    /**
     * Convert line to JSON.
     */
    Line.prototype.toJSON = function () {
        return {
            "pointStart": this._pointStart.toJSON(),
            "pointEnd": this._pointEnd.toJSON(),
            "type": Line.TYPE
        };
    };
    Object.defineProperty(Line.prototype, "type", {
        /**
         * Get the type of this object.
         */
        get: function () {
            return Line.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    Line.TYPE = "line";
    return Line;
}());
exports.Line = Line;
exports.default = Line;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvbGluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlDQUE0QjtBQUU1QjtJQW1DSSxjQUFhLFVBQWlCLEVBQUUsUUFBZTtRQUMzQyxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztRQUM5QixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsRUFBRSxHQUFHLFFBQVEsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsRUFBRSxHQUFHLFVBQVUsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsVUFBVSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQS9CRDs7OztPQUlHO0lBQ1csYUFBUSxHQUF0QixVQUF1QixPQUFZO1FBRS9CLEVBQUUsQ0FBQyxDQUFFLENBQUMsT0FBUSxDQUFDLENBQUMsQ0FBQztZQUNiLE1BQU0sSUFBSSxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQztRQUN4RCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDakQsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO1FBQzFELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUMvQyxNQUFNLElBQUksS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7UUFDeEQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLE1BQU0sSUFBSSxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUNwRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLE9BQU8sQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDdEMsTUFBTSxJQUFJLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwQyxDQUFDO1FBRUQsSUFBSSxVQUFVLEdBQUcsZUFBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEQsSUFBSSxRQUFRLEdBQUcsZUFBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDaEQsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUUxQyxDQUFDO0lBY0Qsc0JBQUksbUJBQUM7UUFKTDs7O1dBR0c7YUFDSCxjQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFNM0Isc0JBQUksbUJBQUM7UUFKTDs7O1dBR0c7YUFDSCxjQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFNM0Isc0JBQUksbUJBQUM7UUFKTDs7O1dBR0c7YUFDSCxjQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFFM0I7OztPQUdHO0lBQ0ksb0JBQUssR0FBWjtRQUNJLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLHVCQUFRLEdBQWY7UUFDSSxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDckMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBRUQ7O09BRUc7SUFDSSx3QkFBUyxHQUFoQjtRQUNJLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDckQsSUFBSSxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyRCxJQUFJLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JELE1BQU0sQ0FBQyxJQUFJLGVBQUssQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRDs7O09BR0c7SUFDSSwwQkFBVyxHQUFsQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFRDs7O09BR0c7SUFDSSx3QkFBUyxHQUFoQjtRQUNJLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzVCLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzVCLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQzFCLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFFLENBQUM7SUFDdEUsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSw4QkFBZSxHQUF0QjtRQUVJLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzVCLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBRTVCLDJEQUEyRDtRQUMzRCxJQUFJLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ2pDLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFFakMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRTlCLENBQUM7SUFFRDs7O09BR0c7SUFDSSw0QkFBYSxHQUFwQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7T0FFRztJQUNJLHFCQUFNLEdBQWI7UUFDSSxNQUFNLENBQUM7WUFDSCxZQUFZLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUU7WUFDdkMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO1lBQ25DLE1BQU0sRUFBRyxJQUFJLENBQUMsSUFBSTtTQUNyQixDQUFDO0lBQ04sQ0FBQztJQU1ELHNCQUFJLHNCQUFJO1FBSFI7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3JCLENBQUM7OztPQUFBO0lBdEphLFNBQUksR0FBRyxNQUFNLENBQUM7SUF3SmhDLFdBQUM7Q0ExSkQsQUEwSkMsSUFBQTtBQTFKWSxvQkFBSTtBQTRKakIsa0JBQWUsSUFBSSxDQUFDIiwiZmlsZSI6Im1hdGgvbGluZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQb2ludCBmcm9tIFwiLi9wb2ludFwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIExpbmUge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgVFlQRSA9IFwibGluZVwiO1xyXG5cclxuICAgIHByaXZhdGUgX2E6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX2I6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX2M6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX3BvaW50U3RhcnQ6IFBvaW50O1xyXG4gICAgcHJpdmF0ZSBfcG9pbnRFbmQ6IFBvaW50O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBKU09OIG9iamVjdCB0byBhIExpbmUgb2JqZWN0LlxyXG4gICAgICogQHBhcmFtIGpzb25PYmpcclxuICAgICAqIEByZXR1cm5zIHttYXRoLkxpbmV9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iajogYW55KTogTGluZSB7XHJcblxyXG4gICAgICAgIGlmICggIWpzb25PYmogKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbm5vdCBjb252ZXJ0IHVuZGVmaW5lZCB0byBMaW5lXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoICFqc29uT2JqLmhhc093blByb3BlcnR5KFwicG9pbnRTdGFydFwiKSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwicG9pbnRTdGFydCBwcm9wZXJ0eSBkb2VzIG5vdCBleGlzdFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCAhanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcInBvaW50RW5kXCIpICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJwb2ludEVuZCBwcm9wZXJ0eSBkb2VzIG5vdCBleGlzdFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCAhanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcInR5cGVcIikgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlR5cGUgcHJvcGVydHkgZG9lcyBub3QgZXhpc3RcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICgganNvbk9iai50eXBlICE9PSBMaW5lLlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgdHlwZVwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBwb2ludFN0YXJ0ID0gUG9pbnQuZnJvbUpTT04oanNvbk9iai5wb2ludFN0YXJ0KTtcclxuICAgICAgICBsZXQgcG9pbnRFbmQgPSBQb2ludC5mcm9tSlNPTihqc29uT2JqLnBvaW50RW5kKTtcclxuICAgICAgICByZXR1cm4gbmV3IExpbmUocG9pbnRTdGFydCwgcG9pbnRFbmQpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcG9pbnRTdGFydDogUG9pbnQsIHBvaW50RW5kOiBQb2ludCApIHtcclxuICAgICAgICB0aGlzLl9wb2ludFN0YXJ0ID0gcG9pbnRTdGFydDtcclxuICAgICAgICB0aGlzLl9wb2ludEVuZCA9IHBvaW50RW5kO1xyXG4gICAgICAgIHRoaXMuX2EgPSBwb2ludEVuZC55IC0gcG9pbnRTdGFydC55O1xyXG4gICAgICAgIHRoaXMuX2IgPSBwb2ludFN0YXJ0LnggLSBwb2ludEVuZC54O1xyXG4gICAgICAgIHRoaXMuX2MgPSB0aGlzLl9hICogcG9pbnRTdGFydC54ICsgdGhpcy5fYiAqIHBvaW50U3RhcnQueTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgQSB2YWx1ZS5cclxuICAgICAqIEByZXR1cm5zIHtudW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIGdldCBhKCkgeyByZXR1cm4gdGhpcy5fYTsgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBCIHZhbHVlLlxyXG4gICAgICogQHJldHVybnMge251bWJlcn1cclxuICAgICAqL1xyXG4gICAgZ2V0IGIoKSB7IHJldHVybiB0aGlzLl9iOyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIEMgdmFsdWUuXHJcbiAgICAgKiBAcmV0dXJucyB7bnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBnZXQgYygpIHsgcmV0dXJuIHRoaXMuX2M7IH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENsb25lIHRoaXMgbGluZS5cclxuICAgICAqIEByZXR1cm5zIHttYXRoLkxpbmV9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBjbG9uZSgpOiBMaW5lIHtcclxuICAgICAgICByZXR1cm4gbmV3IExpbmUodGhpcy5fcG9pbnRTdGFydCwgdGhpcy5fcG9pbnRFbmQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyB0aGUgYW5nbGUgKGluIGRlZ3JlZXMpIGZvciB0aGlzIGxpbmVcclxuICAgICAqIHJlbGF0aXZlIHRvIHRoZSB4LWF4aXMuXHJcbiAgICAgKiBAcmV0dXJucyB7bnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0QW5nbGUoKTogbnVtYmVyIHtcclxuICAgICAgICBsZXQgcmFkaWFucyA9IHRoaXMuZ2V0UmFkaWFuc0FuZ2xlKCk7XHJcbiAgICAgICAgcmV0dXJuIHJhZGlhbnMgKiAxODAgLyBNYXRoLlBJO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBjZW50ZXIgb2YgdGhpcyBsaW5lLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0Q2VudGVyKCk6IFBvaW50IHtcclxuICAgICAgICBsZXQgbXggPSAodGhpcy5fcG9pbnRTdGFydC54ICsgdGhpcy5fcG9pbnRFbmQueCkgLyAyO1xyXG4gICAgICAgIGxldCBteSA9ICh0aGlzLl9wb2ludFN0YXJ0LnkgKyB0aGlzLl9wb2ludEVuZC55KSAvIDI7XHJcbiAgICAgICAgbGV0IG16ID0gKHRoaXMuX3BvaW50U3RhcnQueiArIHRoaXMuX3BvaW50RW5kLnopIC8gMjtcclxuICAgICAgICByZXR1cm4gbmV3IFBvaW50KG14LCBteSwgbXopO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBlbmQgcG9pbnQgb2YgdGhpcyBsaW5lIHNlZ21lbnQuXHJcbiAgICAgKiBAcmV0dXJucyB7UG9pbnR9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRFbmRQb2ludCgpOiBQb2ludCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BvaW50RW5kO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyB0aGUgbGVuZ3RoIG9mIHRoaXMgbGluZSBzZWdtZW50LlxyXG4gICAgICogQHJldHVybnMge251bWJlcn1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldExlbmd0aCgpOiBudW1iZXIge1xyXG4gICAgICAgIGxldCBheCA9IHRoaXMuX3BvaW50U3RhcnQueDtcclxuICAgICAgICBsZXQgYXkgPSB0aGlzLl9wb2ludFN0YXJ0Lnk7XHJcbiAgICAgICAgbGV0IGJ4ID0gdGhpcy5fcG9pbnRFbmQueDtcclxuICAgICAgICBsZXQgYnkgPSB0aGlzLl9wb2ludEVuZC55O1xyXG4gICAgICAgIHJldHVybiBNYXRoLnNxcnQoIChheCAtIGJ4KSAqIChheCAtIGJ4KSArIChheSAtIGJ5KSAqIChheSAtIGJ5KSApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyB0aGUgYW5nbGUgKGluIHJhZGlhbnMpIGZvciB0aGlzIGxpbmVcclxuICAgICAqIHJlbGF0aXZlIHRvIHRoZSB4LWF4aXMuXHJcbiAgICAgKiBAcmV0dXJucyB7bnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0UmFkaWFuc0FuZ2xlKCk6IG51bWJlciB7XHJcblxyXG4gICAgICAgIGxldCBheCA9IHRoaXMuX3BvaW50U3RhcnQueDtcclxuICAgICAgICBsZXQgYXkgPSB0aGlzLl9wb2ludFN0YXJ0Lnk7XHJcblxyXG4gICAgICAgIC8vIFNoaWZ0IHRoZSBlbmQgcG9pbnQgcmVsYXRpdmUgdG8gb3JpZ2luIGFuZCBub3JtYWxpemUgaXQuXHJcbiAgICAgICAgbGV0IGR4ID0gKHRoaXMuX3BvaW50RW5kLnggLSBheCk7XHJcbiAgICAgICAgbGV0IGR5ID0gKHRoaXMuX3BvaW50RW5kLnkgLSBheSk7XHJcblxyXG4gICAgICAgIHJldHVybiBNYXRoLmF0YW4yKGR5LCBkeCk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBzdGFydCBwb2ludCBvZiB0aGlzIGxpbmUgc2VnbWVudC5cclxuICAgICAqIEByZXR1cm5zIHtQb2ludH1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFN0YXJ0UG9pbnQoKTogUG9pbnQge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wb2ludFN0YXJ0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBsaW5lIHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogYW55IHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBcInBvaW50U3RhcnRcIjogdGhpcy5fcG9pbnRTdGFydC50b0pTT04oKSxcclxuICAgICAgICAgICAgXCJwb2ludEVuZFwiOiB0aGlzLl9wb2ludEVuZC50b0pTT04oKSxcclxuICAgICAgICAgICAgXCJ0eXBlXCIgOiBMaW5lLlRZUEVcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgdHlwZSBvZiB0aGlzIG9iamVjdC5cclxuICAgICAqL1xyXG4gICAgZ2V0IHR5cGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gTGluZS5UWVBFO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgTGluZTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
