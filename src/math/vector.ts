import Point from "./point";

export class Vector {

    private _x: number;
    private _y: number;
    private _z: number;

    /**
     * Get the cross product of two vectors.
     */
    public static cross( a: Vector, b: Vector ): Vector {

        let cx: number = a.y * b.z - a.z * b.y;
        let cy: number = a.z * b.x - a.x * b.z;
        let cz: number = a.x * b.y - a.y * b.x;

        return new Vector(cx, cy, cz);

    }

    /**
     * Subtract 2 points to get the direction of the vector.
     */
    public static subtract( a: Point, b: Point ): Vector {
        return new Vector( a.x - b.x, a.y - b.y, a.z - b.z );
    }

    constructor(xValue: number, yValue: number, zValue?: number) {
        this._x = xValue;
        this._y = yValue;
        this._z = zValue ? zValue : 0;
    }

    /**
     * Clone this vector.
     */
    public clone(): Vector {
        return new Vector(this._x, this._y, this._z);
    }

    /**
     * Get the dot product of this vector with another vector.
     * @param vector2
     */
    public dot( vector2: Vector ): number {
        return this._x * vector2.x + this._y * vector2.y + this._z * vector2.z;
    }

    /**
     * Get the dot product of this vector with the x, y, z of another vector.
     */
    public dot2( x: number, y: number, z: number ): number {
        return this._x * x + this._y * y + this._z * z;
    }

    /**
     * Normalize this vector.
     */
    public normalize(): void {
        let length = Math.sqrt(this._x * this._x) + Math.sqrt(this._y * this._y) + Math.sqrt(this._z * this._z);
        this._x /= length;
        this._y /= length;
        this._z /= length;
    }

    /**
     * Flip this vector and make it perpendicular.
     */
    public perpendicular(): void {
        let a = this.y;
        let b = -this.x;
        this._x = a;
        this._y = b;
    }

    /**
     * Get the X value.
     */
    get x(): number {
        return this._x;
    }

    /**
     * Set the X value.
     */
    set x( value: number ) {
        this._x = value;
    }

    /**
     * Get the Y value.
     */
    get y(): number {
        return this._y;
    }

    /**
     * Get the Y value.
     */
    set y( value: number ) {
        this._y = value;
    }

    /**
     * Get the Z value.
     */
    get z(): number {
        return this._z;
    }

    /**
     * Get the Z value.
     */
    set z( value: number ) {
        this._z = value;
    }

}

export default Vector;
