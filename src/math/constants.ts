export class Constants {

    public static CIRCLE = "circle";
    public static POLYGON = "polygon";
    public static RECTANGLE = "rectangle";

}

export default Constants;
