import Point from "./point";
import Shape from "./shape";
import Constants from "./constants";

export class Rectangle extends Shape {

    private _position: Point;
    private _width: number;
    private _height: number;
    private _centerPoint: Point;
    private _linkPosition: any;

    /**
     * Takes in a Tiled object (no type) and converts it into a Rectangle.
     */
    public static fromTiled( x: number, y: number, width: number, height: number ): Rectangle {
        let point: Point = new Point(x, y);
        return new Rectangle( point, width, height );
    }

    /**
     * Convert JSON to math.Rectangle object.
     * @param jsonObj
     * @returns {math.Rectangle}
     */
    public static fromJSON(jsonObj: any) {

        if (!jsonObj) {
            throw new Error("Cannot convert undefined to Rectangle");
        } else if (!jsonObj.hasOwnProperty("position")) {
            throw new Error("Position property does not exist");
        } else if (!jsonObj.hasOwnProperty("width")) {
            throw new Error("Width property does not exist");
        } else if (!jsonObj.hasOwnProperty("type")) {
            throw new Error("Height property does not exist");
        } else if (jsonObj.type !== Constants.RECTANGLE) {
            throw new Error("Invalid type");
        }

        let position = Point.fromJSON(jsonObj.position);
        let width = jsonObj.width;
        let height = jsonObj.height;

        return new Rectangle(position, width, height);

    }

    /**
     * Consruct a rectangle object.
     * @param centerPoint
     * @param circleRadius
     */
    constructor(position: Point, width: number, height: number) {
        super();
        this._position = position;
        this._width = width;
        this._height = height;
        this._centerPoint = new Point( this._position.x + this._width / 2, this._position.y + this._height / 2 );
    }

    /**
     * Get adjacent shapes. Include diagonals. This is used for path-finding.
     */
    public getAdjacentShapes(): Shape[] {
        let list = [];
        let w: number = this._width;
        let h: number = this._height;
        for (let i = -w; i <= w; i += w) {
            for (let j = -h; j <= h; j += h) {
                if (i !== 0 || j !== 0) {
                    let point = new Point(this._position.x + i, this._position.y + j);
                    let rect = new Rectangle(point, w, h);
                    list.push(rect);
                }
            }
        }
        return list;
    }

    /**
     * Get a point related to the center of this shape.
     */
    public getPosition(): Point {
        return new Point( this.x + this._width / 2, this.y + this._height / 2 + this.z );
    }

    /**
     * Get the Z value relative to a given point. This is used in case
     * the rectangle represents a staircase and you need to get Z value
     * for a given X, Y.
     * In this case, the rectangle only has one z.
     */
    public getZRelativeToPoint(point: Point): number {
        let z = 0;
        if ( this._position.z > 0 ) {
            z = this._position.z;
        }
        return z;
    }

    /**
     * Get upper left position.
     */
    public position(): Point {
        return this._position;
    }

    /**
     * Get width.
     */
    public width(): number {
        return this._width;
    }

    /**
     * Get height.
     */
    public height(): number {
        return this._height;
    }

    /**
     * Clone this rectangle.
     */
    public clone(): Shape {
        return new Rectangle(this._position, this._width, this._height);
    }

    /**
     * Get the center of this rectangle.
     */
    public getCenter(): Point {
        return this._centerPoint;
    }

    /**
     * Get the type of this shape.
     */
    get type(): string {
        return Constants.RECTANGLE;
    }

    /**
     * Get the X coordinate of the center of this shape.
     */
    get x(): number {
        return this._centerPoint.x;
    }

    /**
     * Get the Y coordinate of the center of this shape.
     */
    get y(): number {
        return this._centerPoint.y;
    }

    /**
     * Get the Z coordinate of the center of this shape.
     */
    get z(): number {
        return this._centerPoint.z;
    }

    /**
     * Set the X coordinate of the center of this shape.
     */
    set x(value: number) {
        this._centerPoint.x = value;
        if (this._linkPosition) {
            this._linkPosition.x = value;
        }
    }

    /**
     * Set the Y coordinate of the center of this shape.
     */
    set y(value: number) {
        this._centerPoint.y = value;
        if (this._linkPosition) {
            this._linkPosition.y = value;
        }
    }

    /**
     * Set the Z coordinate of the center of this shape.
     */
    set z(value: number) {
        this._centerPoint.z = value;
        if (this._linkPosition) {
            this._linkPosition.z = value;
        }
    }

    /**
     * Set the center of the rectangle.
     */
    public setCenter(aPoint: Point): void {
        let deltaX = aPoint.x - this._centerPoint.x;
        let deltaY = aPoint.y - this._centerPoint.y;
        let deltaZ = aPoint.z - this._centerPoint.z;
        this._position.x += deltaX;
        this._position.y += deltaY;
        this._position.z += deltaZ;
        this._centerPoint = aPoint;
        if (this._linkPosition) {
            this._linkPosition.x = aPoint.x;
            this._linkPosition.y = aPoint.y;
            this._linkPosition.z = aPoint.z;
        }
    }

    /**
     * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
     * shape changes the object of this link position also changes.
     */
    get linkPosition(): any {
        return this._linkPosition;
    }

    /**
     * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
     * shape changes the object of this link position also changes.
     */
    set linkPosition(value: any) {
        if (!value) {
            throw new Error("Value is null");
        } else if (value.hasOwnProperty("x") && value.hasOwnProperty("y") && value.hasOwnProperty("z")) {
            this._linkPosition = value;
            this._linkPosition.x = this._centerPoint.x;
            this._linkPosition.y = this._centerPoint.y;
            this._linkPosition.z = this._centerPoint.z;
        } else {
            throw new Error("Value does not have all the required properties");
        }
    }

    /**
     * Determine if a rectangle equals this rectangle.
     */
    public equals(aRectangleObject: Rectangle): boolean {
        if (aRectangleObject.position().x === this._position.x &&
            aRectangleObject.position().y === this._position.y &&
            aRectangleObject.position().z === this._position.z &&
            aRectangleObject.width() === this._width &&
            aRectangleObject.height() === this._height ) {
            return true;
        }
        return false;
    }

    /**
     * Convert the rectangle to JSON.
     */
    public toJSON(): any {
        return {
            "position": this._position.toJSON(),
            "width": this._width,
            "height": this._height,
            "type": Constants.RECTANGLE
        };
    }

}

export default Rectangle;
