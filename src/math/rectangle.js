"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("./point");
var shape_1 = require("./shape");
var constants_1 = require("./constants");
var Rectangle = /** @class */ (function (_super) {
    __extends(Rectangle, _super);
    /**
     * Consruct a rectangle object.
     * @param centerPoint
     * @param circleRadius
     */
    function Rectangle(position, width, height) {
        var _this = _super.call(this) || this;
        _this._position = position;
        _this._width = width;
        _this._height = height;
        _this._centerPoint = new point_1.default(_this._position.x + _this._width / 2, _this._position.y + _this._height / 2);
        return _this;
    }
    /**
     * Takes in a Tiled object (no type) and converts it into a Rectangle.
     */
    Rectangle.fromTiled = function (x, y, width, height) {
        var point = new point_1.default(x, y);
        return new Rectangle(point, width, height);
    };
    /**
     * Convert JSON to math.Rectangle object.
     * @param jsonObj
     * @returns {math.Rectangle}
     */
    Rectangle.fromJSON = function (jsonObj) {
        if (!jsonObj) {
            throw new Error("Cannot convert undefined to Rectangle");
        }
        else if (!jsonObj.hasOwnProperty("position")) {
            throw new Error("Position property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("width")) {
            throw new Error("Width property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("type")) {
            throw new Error("Height property does not exist");
        }
        else if (jsonObj.type !== constants_1.default.RECTANGLE) {
            throw new Error("Invalid type");
        }
        var position = point_1.default.fromJSON(jsonObj.position);
        var width = jsonObj.width;
        var height = jsonObj.height;
        return new Rectangle(position, width, height);
    };
    /**
     * Get adjacent shapes. Include diagonals. This is used for path-finding.
     */
    Rectangle.prototype.getAdjacentShapes = function () {
        var list = [];
        var w = this._width;
        var h = this._height;
        for (var i = -w; i <= w; i += w) {
            for (var j = -h; j <= h; j += h) {
                if (i !== 0 || j !== 0) {
                    var point = new point_1.default(this._position.x + i, this._position.y + j);
                    var rect = new Rectangle(point, w, h);
                    list.push(rect);
                }
            }
        }
        return list;
    };
    /**
     * Get a point related to the center of this shape.
     */
    Rectangle.prototype.getPosition = function () {
        return new point_1.default(this.x + this._width / 2, this.y + this._height / 2 + this.z);
    };
    /**
     * Get the Z value relative to a given point. This is used in case
     * the rectangle represents a staircase and you need to get Z value
     * for a given X, Y.
     * In this case, the rectangle only has one z.
     */
    Rectangle.prototype.getZRelativeToPoint = function (point) {
        var z = 0;
        if (this._position.z > 0) {
            z = this._position.z;
        }
        return z;
    };
    /**
     * Get upper left position.
     */
    Rectangle.prototype.position = function () {
        return this._position;
    };
    /**
     * Get width.
     */
    Rectangle.prototype.width = function () {
        return this._width;
    };
    /**
     * Get height.
     */
    Rectangle.prototype.height = function () {
        return this._height;
    };
    /**
     * Clone this rectangle.
     */
    Rectangle.prototype.clone = function () {
        return new Rectangle(this._position, this._width, this._height);
    };
    /**
     * Get the center of this rectangle.
     */
    Rectangle.prototype.getCenter = function () {
        return this._centerPoint;
    };
    Object.defineProperty(Rectangle.prototype, "type", {
        /**
         * Get the type of this shape.
         */
        get: function () {
            return constants_1.default.RECTANGLE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "x", {
        /**
         * Get the X coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.x;
        },
        /**
         * Set the X coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.x = value;
            if (this._linkPosition) {
                this._linkPosition.x = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "y", {
        /**
         * Get the Y coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.y;
        },
        /**
         * Set the Y coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.y = value;
            if (this._linkPosition) {
                this._linkPosition.y = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "z", {
        /**
         * Get the Z coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.z;
        },
        /**
         * Set the Z coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.z = value;
            if (this._linkPosition) {
                this._linkPosition.z = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Set the center of the rectangle.
     */
    Rectangle.prototype.setCenter = function (aPoint) {
        var deltaX = aPoint.x - this._centerPoint.x;
        var deltaY = aPoint.y - this._centerPoint.y;
        var deltaZ = aPoint.z - this._centerPoint.z;
        this._position.x += deltaX;
        this._position.y += deltaY;
        this._position.z += deltaZ;
        this._centerPoint = aPoint;
        if (this._linkPosition) {
            this._linkPosition.x = aPoint.x;
            this._linkPosition.y = aPoint.y;
            this._linkPosition.z = aPoint.z;
        }
    };
    Object.defineProperty(Rectangle.prototype, "linkPosition", {
        /**
         * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
         * shape changes the object of this link position also changes.
         */
        get: function () {
            return this._linkPosition;
        },
        /**
         * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
         * shape changes the object of this link position also changes.
         */
        set: function (value) {
            if (!value) {
                throw new Error("Value is null");
            }
            else if (value.hasOwnProperty("x") && value.hasOwnProperty("y") && value.hasOwnProperty("z")) {
                this._linkPosition = value;
                this._linkPosition.x = this._centerPoint.x;
                this._linkPosition.y = this._centerPoint.y;
                this._linkPosition.z = this._centerPoint.z;
            }
            else {
                throw new Error("Value does not have all the required properties");
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if a rectangle equals this rectangle.
     */
    Rectangle.prototype.equals = function (aRectangleObject) {
        if (aRectangleObject.position().x === this._position.x &&
            aRectangleObject.position().y === this._position.y &&
            aRectangleObject.position().z === this._position.z &&
            aRectangleObject.width() === this._width &&
            aRectangleObject.height() === this._height) {
            return true;
        }
        return false;
    };
    /**
     * Convert the rectangle to JSON.
     */
    Rectangle.prototype.toJSON = function () {
        return {
            "position": this._position.toJSON(),
            "width": this._width,
            "height": this._height,
            "type": constants_1.default.RECTANGLE
        };
    };
    return Rectangle;
}(shape_1.default));
exports.Rectangle = Rectangle;
exports.default = Rectangle;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvcmVjdGFuZ2xlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLGlDQUE0QjtBQUM1QixpQ0FBNEI7QUFDNUIseUNBQW9DO0FBRXBDO0lBQStCLDZCQUFLO0lBMkNoQzs7OztPQUlHO0lBQ0gsbUJBQVksUUFBZSxFQUFFLEtBQWEsRUFBRSxNQUFjO1FBQTFELFlBQ0ksaUJBQU8sU0FLVjtRQUpHLEtBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1FBQzFCLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLEtBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ3RCLEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxlQUFLLENBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsS0FBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUUsQ0FBQzs7SUFDN0csQ0FBQztJQTlDRDs7T0FFRztJQUNXLG1CQUFTLEdBQXZCLFVBQXlCLENBQVMsRUFBRSxDQUFTLEVBQUUsS0FBYSxFQUFFLE1BQWM7UUFDeEUsSUFBSSxLQUFLLEdBQVUsSUFBSSxlQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxJQUFJLFNBQVMsQ0FBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBRSxDQUFDO0lBQ2pELENBQUM7SUFFRDs7OztPQUlHO0lBQ1csa0JBQVEsR0FBdEIsVUFBdUIsT0FBWTtRQUUvQixFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDWCxNQUFNLElBQUksS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7UUFDN0QsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdDLE1BQU0sSUFBSSxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQztRQUN4RCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUMsTUFBTSxJQUFJLEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBQ3JELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6QyxNQUFNLElBQUksS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7UUFDdEQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLG1CQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM5QyxNQUFNLElBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7UUFFRCxJQUFJLFFBQVEsR0FBRyxlQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNoRCxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO1FBQzFCLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFFNUIsTUFBTSxDQUFDLElBQUksU0FBUyxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFFbEQsQ0FBQztJQWVEOztPQUVHO0lBQ0kscUNBQWlCLEdBQXhCO1FBQ0ksSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLEdBQVcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM1QixJQUFJLENBQUMsR0FBVyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzdCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBQzlCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUM5QixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNyQixJQUFJLEtBQUssR0FBRyxJQUFJLGVBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ2xFLElBQUksSUFBSSxHQUFHLElBQUksU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksK0JBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsSUFBSSxlQUFLLENBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUUsQ0FBQztJQUNyRixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSSx1Q0FBbUIsR0FBMUIsVUFBMkIsS0FBWTtRQUNuQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDVixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUN6QixDQUFDO1FBQ0QsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNiLENBQUM7SUFFRDs7T0FFRztJQUNJLDRCQUFRLEdBQWY7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRUQ7O09BRUc7SUFDSSx5QkFBSyxHQUFaO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksMEJBQU0sR0FBYjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFFRDs7T0FFRztJQUNJLHlCQUFLLEdBQVo7UUFDSSxNQUFNLENBQUMsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRUQ7O09BRUc7SUFDSSw2QkFBUyxHQUFoQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFLRCxzQkFBSSwyQkFBSTtRQUhSOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsbUJBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0IsQ0FBQzs7O09BQUE7SUFLRCxzQkFBSSx3QkFBQztRQUhMOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQWdCRDs7V0FFRzthQUNILFVBQU0sS0FBYTtZQUNmLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUM1QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLENBQUM7UUFDTCxDQUFDOzs7T0F4QkE7SUFLRCxzQkFBSSx3QkFBQztRQUhMOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQW1CRDs7V0FFRzthQUNILFVBQU0sS0FBYTtZQUNmLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUM1QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLENBQUM7UUFDTCxDQUFDOzs7T0EzQkE7SUFLRCxzQkFBSSx3QkFBQztRQUhMOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQXNCRDs7V0FFRzthQUNILFVBQU0sS0FBYTtZQUNmLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUM1QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLENBQUM7UUFDTCxDQUFDOzs7T0E5QkE7SUFnQ0Q7O09BRUc7SUFDSSw2QkFBUyxHQUFoQixVQUFpQixNQUFhO1FBQzFCLElBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDNUMsSUFBSSxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUM1QyxJQUFJLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQztRQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUM7UUFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDO1FBQzNCLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDO1FBQzNCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7SUFDTCxDQUFDO0lBTUQsc0JBQUksbUNBQVk7UUFKaEI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM5QixDQUFDO1FBRUQ7OztXQUdHO2FBQ0gsVUFBaUIsS0FBVTtZQUN2QixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsTUFBTSxJQUFJLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNyQyxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUMzQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDL0MsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztZQUN2RSxDQUFDO1FBQ0wsQ0FBQzs7O09BakJBO0lBbUJEOztPQUVHO0lBQ0ksMEJBQU0sR0FBYixVQUFjLGdCQUEyQjtRQUNyQyxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2xELGdCQUFnQixDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbEQsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNsRCxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsS0FBSyxJQUFJLENBQUMsTUFBTTtZQUN4QyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxJQUFJLENBQUMsT0FBUSxDQUFDLENBQUMsQ0FBQztZQUM5QyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRDs7T0FFRztJQUNJLDBCQUFNLEdBQWI7UUFDSSxNQUFNLENBQUM7WUFDSCxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7WUFDbkMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ3BCLFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTztZQUN0QixNQUFNLEVBQUUsbUJBQVMsQ0FBQyxTQUFTO1NBQzlCLENBQUM7SUFDTixDQUFDO0lBRUwsZ0JBQUM7QUFBRCxDQWxRQSxBQWtRQyxDQWxROEIsZUFBSyxHQWtRbkM7QUFsUVksOEJBQVM7QUFvUXRCLGtCQUFlLFNBQVMsQ0FBQyIsImZpbGUiOiJtYXRoL3JlY3RhbmdsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQb2ludCBmcm9tIFwiLi9wb2ludFwiO1xyXG5pbXBvcnQgU2hhcGUgZnJvbSBcIi4vc2hhcGVcIjtcclxuaW1wb3J0IENvbnN0YW50cyBmcm9tIFwiLi9jb25zdGFudHNcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBSZWN0YW5nbGUgZXh0ZW5kcyBTaGFwZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBfcG9zaXRpb246IFBvaW50O1xyXG4gICAgcHJpdmF0ZSBfd2lkdGg6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX2hlaWdodDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfY2VudGVyUG9pbnQ6IFBvaW50O1xyXG4gICAgcHJpdmF0ZSBfbGlua1Bvc2l0aW9uOiBhbnk7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUYWtlcyBpbiBhIFRpbGVkIG9iamVjdCAobm8gdHlwZSkgYW5kIGNvbnZlcnRzIGl0IGludG8gYSBSZWN0YW5nbGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbVRpbGVkKCB4OiBudW1iZXIsIHk6IG51bWJlciwgd2lkdGg6IG51bWJlciwgaGVpZ2h0OiBudW1iZXIgKTogUmVjdGFuZ2xlIHtcclxuICAgICAgICBsZXQgcG9pbnQ6IFBvaW50ID0gbmV3IFBvaW50KHgsIHkpO1xyXG4gICAgICAgIHJldHVybiBuZXcgUmVjdGFuZ2xlKCBwb2ludCwgd2lkdGgsIGhlaWdodCApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBKU09OIHRvIG1hdGguUmVjdGFuZ2xlIG9iamVjdC5cclxuICAgICAqIEBwYXJhbSBqc29uT2JqXHJcbiAgICAgKiBAcmV0dXJucyB7bWF0aC5SZWN0YW5nbGV9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iajogYW55KSB7XHJcblxyXG4gICAgICAgIGlmICghanNvbk9iaikge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgY29udmVydCB1bmRlZmluZWQgdG8gUmVjdGFuZ2xlXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIWpzb25PYmouaGFzT3duUHJvcGVydHkoXCJwb3NpdGlvblwiKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJQb3NpdGlvbiBwcm9wZXJ0eSBkb2VzIG5vdCBleGlzdFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCFqc29uT2JqLmhhc093blByb3BlcnR5KFwid2lkdGhcIikpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiV2lkdGggcHJvcGVydHkgZG9lcyBub3QgZXhpc3RcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICghanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcInR5cGVcIikpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiSGVpZ2h0IHByb3BlcnR5IGRvZXMgbm90IGV4aXN0XCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoanNvbk9iai50eXBlICE9PSBDb25zdGFudHMuUkVDVEFOR0xFKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgdHlwZVwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBwb3NpdGlvbiA9IFBvaW50LmZyb21KU09OKGpzb25PYmoucG9zaXRpb24pO1xyXG4gICAgICAgIGxldCB3aWR0aCA9IGpzb25PYmoud2lkdGg7XHJcbiAgICAgICAgbGV0IGhlaWdodCA9IGpzb25PYmouaGVpZ2h0O1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IFJlY3RhbmdsZShwb3NpdGlvbiwgd2lkdGgsIGhlaWdodCk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3J1Y3QgYSByZWN0YW5nbGUgb2JqZWN0LlxyXG4gICAgICogQHBhcmFtIGNlbnRlclBvaW50XHJcbiAgICAgKiBAcGFyYW0gY2lyY2xlUmFkaXVzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHBvc2l0aW9uOiBQb2ludCwgd2lkdGg6IG51bWJlciwgaGVpZ2h0OiBudW1iZXIpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMuX3Bvc2l0aW9uID0gcG9zaXRpb247XHJcbiAgICAgICAgdGhpcy5fd2lkdGggPSB3aWR0aDtcclxuICAgICAgICB0aGlzLl9oZWlnaHQgPSBoZWlnaHQ7XHJcbiAgICAgICAgdGhpcy5fY2VudGVyUG9pbnQgPSBuZXcgUG9pbnQoIHRoaXMuX3Bvc2l0aW9uLnggKyB0aGlzLl93aWR0aCAvIDIsIHRoaXMuX3Bvc2l0aW9uLnkgKyB0aGlzLl9oZWlnaHQgLyAyICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYWRqYWNlbnQgc2hhcGVzLiBJbmNsdWRlIGRpYWdvbmFscy4gVGhpcyBpcyB1c2VkIGZvciBwYXRoLWZpbmRpbmcuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRBZGphY2VudFNoYXBlcygpOiBTaGFwZVtdIHtcclxuICAgICAgICBsZXQgbGlzdCA9IFtdO1xyXG4gICAgICAgIGxldCB3OiBudW1iZXIgPSB0aGlzLl93aWR0aDtcclxuICAgICAgICBsZXQgaDogbnVtYmVyID0gdGhpcy5faGVpZ2h0O1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAtdzsgaSA8PSB3OyBpICs9IHcpIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgaiA9IC1oOyBqIDw9IGg7IGogKz0gaCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGkgIT09IDAgfHwgaiAhPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBwb2ludCA9IG5ldyBQb2ludCh0aGlzLl9wb3NpdGlvbi54ICsgaSwgdGhpcy5fcG9zaXRpb24ueSArIGopO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCByZWN0ID0gbmV3IFJlY3RhbmdsZShwb2ludCwgdywgaCk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGlzdC5wdXNoKHJlY3QpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBsaXN0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGEgcG9pbnQgcmVsYXRlZCB0byB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRQb3NpdGlvbigpOiBQb2ludCB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQb2ludCggdGhpcy54ICsgdGhpcy5fd2lkdGggLyAyLCB0aGlzLnkgKyB0aGlzLl9oZWlnaHQgLyAyICsgdGhpcy56ICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFogdmFsdWUgcmVsYXRpdmUgdG8gYSBnaXZlbiBwb2ludC4gVGhpcyBpcyB1c2VkIGluIGNhc2VcclxuICAgICAqIHRoZSByZWN0YW5nbGUgcmVwcmVzZW50cyBhIHN0YWlyY2FzZSBhbmQgeW91IG5lZWQgdG8gZ2V0IFogdmFsdWVcclxuICAgICAqIGZvciBhIGdpdmVuIFgsIFkuXHJcbiAgICAgKiBJbiB0aGlzIGNhc2UsIHRoZSByZWN0YW5nbGUgb25seSBoYXMgb25lIHouXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRaUmVsYXRpdmVUb1BvaW50KHBvaW50OiBQb2ludCk6IG51bWJlciB7XHJcbiAgICAgICAgbGV0IHogPSAwO1xyXG4gICAgICAgIGlmICggdGhpcy5fcG9zaXRpb24ueiA+IDAgKSB7XHJcbiAgICAgICAgICAgIHogPSB0aGlzLl9wb3NpdGlvbi56O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gejtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB1cHBlciBsZWZ0IHBvc2l0aW9uLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcG9zaXRpb24oKTogUG9pbnQge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wb3NpdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB3aWR0aC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHdpZHRoKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3dpZHRoO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGhlaWdodC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGhlaWdodCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9oZWlnaHQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDbG9uZSB0aGlzIHJlY3RhbmdsZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGNsb25lKCk6IFNoYXBlIHtcclxuICAgICAgICByZXR1cm4gbmV3IFJlY3RhbmdsZSh0aGlzLl9wb3NpdGlvbiwgdGhpcy5fd2lkdGgsIHRoaXMuX2hlaWdodCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIGNlbnRlciBvZiB0aGlzIHJlY3RhbmdsZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldENlbnRlcigpOiBQb2ludCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSB0eXBlIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIGdldCB0eXBlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIENvbnN0YW50cy5SRUNUQU5HTEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFggY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIGdldCB4KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50Lng7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFkgY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIGdldCB5KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50Lnk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFogY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIGdldCB6KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50Lno7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIFggY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIHNldCB4KHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9jZW50ZXJQb2ludC54ID0gdmFsdWU7XHJcbiAgICAgICAgaWYgKHRoaXMuX2xpbmtQb3NpdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLl9saW5rUG9zaXRpb24ueCA9IHZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCB0aGUgWSBjb29yZGluYXRlIG9mIHRoZSBjZW50ZXIgb2YgdGhpcyBzaGFwZS5cclxuICAgICAqL1xyXG4gICAgc2V0IHkodmFsdWU6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2NlbnRlclBvaW50LnkgPSB2YWx1ZTtcclxuICAgICAgICBpZiAodGhpcy5fbGlua1Bvc2l0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi55ID0gdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSBaIGNvb3JkaW5hdGUgb2YgdGhlIGNlbnRlciBvZiB0aGlzIHNoYXBlLlxyXG4gICAgICovXHJcbiAgICBzZXQgeih2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fY2VudGVyUG9pbnQueiA9IHZhbHVlO1xyXG4gICAgICAgIGlmICh0aGlzLl9saW5rUG9zaXRpb24pIHtcclxuICAgICAgICAgICAgdGhpcy5fbGlua1Bvc2l0aW9uLnogPSB2YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIGNlbnRlciBvZiB0aGUgcmVjdGFuZ2xlLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc2V0Q2VudGVyKGFQb2ludDogUG9pbnQpOiB2b2lkIHtcclxuICAgICAgICBsZXQgZGVsdGFYID0gYVBvaW50LnggLSB0aGlzLl9jZW50ZXJQb2ludC54O1xyXG4gICAgICAgIGxldCBkZWx0YVkgPSBhUG9pbnQueSAtIHRoaXMuX2NlbnRlclBvaW50Lnk7XHJcbiAgICAgICAgbGV0IGRlbHRhWiA9IGFQb2ludC56IC0gdGhpcy5fY2VudGVyUG9pbnQuejtcclxuICAgICAgICB0aGlzLl9wb3NpdGlvbi54ICs9IGRlbHRhWDtcclxuICAgICAgICB0aGlzLl9wb3NpdGlvbi55ICs9IGRlbHRhWTtcclxuICAgICAgICB0aGlzLl9wb3NpdGlvbi56ICs9IGRlbHRhWjtcclxuICAgICAgICB0aGlzLl9jZW50ZXJQb2ludCA9IGFQb2ludDtcclxuICAgICAgICBpZiAodGhpcy5fbGlua1Bvc2l0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi54ID0gYVBvaW50Lng7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi55ID0gYVBvaW50Lnk7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi56ID0gYVBvaW50Lno7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTGluayB0aGUgeCwgeSwgeiBwb3NpdGlvbiBvZiB0aGUgZ2l2ZW4gb2JqZWN0IHRvIHRoaXMgc2hhcGUncyBwb3NpdGlvbi4gRXZlcnkgdGltZSB0aGUgeCwgeSBvciB6IHZhbHVlIG9mIHRoaXNcclxuICAgICAqIHNoYXBlIGNoYW5nZXMgdGhlIG9iamVjdCBvZiB0aGlzIGxpbmsgcG9zaXRpb24gYWxzbyBjaGFuZ2VzLlxyXG4gICAgICovXHJcbiAgICBnZXQgbGlua1Bvc2l0aW9uKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xpbmtQb3NpdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExpbmsgdGhlIHgsIHksIHogcG9zaXRpb24gb2YgdGhlIGdpdmVuIG9iamVjdCB0byB0aGlzIHNoYXBlJ3MgcG9zaXRpb24uIEV2ZXJ5IHRpbWUgdGhlIHgsIHkgb3IgeiB2YWx1ZSBvZiB0aGlzXHJcbiAgICAgKiBzaGFwZSBjaGFuZ2VzIHRoZSBvYmplY3Qgb2YgdGhpcyBsaW5rIHBvc2l0aW9uIGFsc28gY2hhbmdlcy5cclxuICAgICAqL1xyXG4gICAgc2V0IGxpbmtQb3NpdGlvbih2YWx1ZTogYW55KSB7XHJcbiAgICAgICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJWYWx1ZSBpcyBudWxsXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodmFsdWUuaGFzT3duUHJvcGVydHkoXCJ4XCIpICYmIHZhbHVlLmhhc093blByb3BlcnR5KFwieVwiKSAmJiB2YWx1ZS5oYXNPd25Qcm9wZXJ0eShcInpcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5fbGlua1Bvc2l0aW9uID0gdmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi54ID0gdGhpcy5fY2VudGVyUG9pbnQueDtcclxuICAgICAgICAgICAgdGhpcy5fbGlua1Bvc2l0aW9uLnkgPSB0aGlzLl9jZW50ZXJQb2ludC55O1xyXG4gICAgICAgICAgICB0aGlzLl9saW5rUG9zaXRpb24ueiA9IHRoaXMuX2NlbnRlclBvaW50Lno7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiVmFsdWUgZG9lcyBub3QgaGF2ZSBhbGwgdGhlIHJlcXVpcmVkIHByb3BlcnRpZXNcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGV0ZXJtaW5lIGlmIGEgcmVjdGFuZ2xlIGVxdWFscyB0aGlzIHJlY3RhbmdsZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGVxdWFscyhhUmVjdGFuZ2xlT2JqZWN0OiBSZWN0YW5nbGUpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoYVJlY3RhbmdsZU9iamVjdC5wb3NpdGlvbigpLnggPT09IHRoaXMuX3Bvc2l0aW9uLnggJiZcclxuICAgICAgICAgICAgYVJlY3RhbmdsZU9iamVjdC5wb3NpdGlvbigpLnkgPT09IHRoaXMuX3Bvc2l0aW9uLnkgJiZcclxuICAgICAgICAgICAgYVJlY3RhbmdsZU9iamVjdC5wb3NpdGlvbigpLnogPT09IHRoaXMuX3Bvc2l0aW9uLnogJiZcclxuICAgICAgICAgICAgYVJlY3RhbmdsZU9iamVjdC53aWR0aCgpID09PSB0aGlzLl93aWR0aCAmJlxyXG4gICAgICAgICAgICBhUmVjdGFuZ2xlT2JqZWN0LmhlaWdodCgpID09PSB0aGlzLl9oZWlnaHQgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoZSByZWN0YW5nbGUgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIFwicG9zaXRpb25cIjogdGhpcy5fcG9zaXRpb24udG9KU09OKCksXHJcbiAgICAgICAgICAgIFwid2lkdGhcIjogdGhpcy5fd2lkdGgsXHJcbiAgICAgICAgICAgIFwiaGVpZ2h0XCI6IHRoaXMuX2hlaWdodCxcclxuICAgICAgICAgICAgXCJ0eXBlXCI6IENvbnN0YW50cy5SRUNUQU5HTEVcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUmVjdGFuZ2xlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
