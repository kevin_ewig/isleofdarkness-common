"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("./point");
var relativepoint_1 = require("./relativepoint");
var plane_1 = require("./plane");
var constants_1 = require("./constants");
var Polygon = /** @class */ (function () {
    function Polygon(arrayOfPoints) {
        // Polygon must have 3 or more points.
        if (arrayOfPoints.length < 3) {
            throw new Error("Polygon must have 3 or more points");
        }
        // Create an array of relative points.
        this._arrayOfPoints = new Array();
        // Compute the min and max x and y values.
        var minXPoint = arrayOfPoints[0];
        var maxXPoint = arrayOfPoints[0];
        var minYPoint = arrayOfPoints[0];
        var maxYPoint = arrayOfPoints[0];
        for (var i = 0; i < arrayOfPoints.length; i++) {
            var point = arrayOfPoints[i];
            if (point.x > maxXPoint.x) {
                maxXPoint = point;
            }
            if (point.x < minXPoint.x) {
                minXPoint = point;
            }
            if (point.y > maxYPoint.y) {
                maxYPoint = point;
            }
            if (point.y < minYPoint.y) {
                minYPoint = point;
            }
        }
        // Compute the center point.
        var averageX = (maxXPoint.x + minXPoint.x) / 2;
        var averageY = (maxYPoint.y + minYPoint.y) / 2;
        this._centerPoint = new point_1.default(averageX, averageY, 0);
        for (var i = 0; i < arrayOfPoints.length; i++) {
            var tempPoint = arrayOfPoints[i];
            var x = tempPoint.x ? tempPoint.x : 0;
            var y = tempPoint.y ? tempPoint.y : 0;
            var z = tempPoint.z ? tempPoint.z : 0;
            var tempX = x - this._centerPoint.x;
            var tempY = y - this._centerPoint.y;
            var tempZ = z - this._centerPoint.z;
            var tempRelativePoint = new relativepoint_1.default(tempX, tempY, tempZ);
            tempRelativePoint.setRelativePoint(this._centerPoint);
            this._arrayOfPoints.push(tempRelativePoint);
        }
        // Compute the plane.
        this._plane = plane_1.default.from3Points(this._arrayOfPoints[0], this._arrayOfPoints[1], this._arrayOfPoints[2]);
    }
    /**
     * Takes in a Tiled object (Polyline) and converts it into a Polygon.
     *
     * @param startPoint The starting x,y point of the polyline.
     * @param arrayOfPoints An array of math.Point objects.
     * @param angle The angle in degrees
     * @param heights (Optional) An array of numbers which represents the height of each point in the array.
     */
    Polygon.fromTiled = function (startPoint, arrayOfPoints, angle, heights) {
        var radian = angle * Math.PI / 180;
        var polygonArray = [];
        var firstPoint = arrayOfPoints[0];
        var heightOfPoints = heights;
        if (heightOfPoints) {
            if (heightOfPoints.length !== arrayOfPoints.length) {
                throw new Error("Number of points in the height does not match the number of points in the array");
            }
        }
        else {
            heightOfPoints = new Array(arrayOfPoints.length);
            for (var i = 0; i < heightOfPoints.length; i++) {
                heightOfPoints[i] = 0;
            }
        }
        for (var i = 0; i < arrayOfPoints.length; i++) {
            var point = arrayOfPoints[i];
            var tempX = firstPoint.x + (point.x - firstPoint.x) * Math.cos(radian) - (point.y - firstPoint.y) * Math.sin(radian);
            var tempY = firstPoint.y + (point.x - firstPoint.x) * Math.sin(radian) + (point.y - firstPoint.y) * Math.cos(radian);
            var tempZ = heightOfPoints[i];
            var point2 = new relativepoint_1.default(tempX + startPoint.x, tempY + startPoint.y, tempZ);
            polygonArray.push(point2);
        }
        var brandNewPolygon = new Polygon(polygonArray);
        return brandNewPolygon;
    };
    /**
     * Convert JSON object to point.
     * @param jsonObj
     * @returns {math.Polygon}
     */
    Polygon.fromJSON = function (jsonObj) {
        if (!jsonObj) {
            throw new Error("Cannot convert undefined to polygon");
        }
        if (!jsonObj.hasOwnProperty("arrayOfPoints")) {
            throw new Error("No arrayOfPoints property");
        }
        if (jsonObj.type !== constants_1.default.POLYGON) {
            throw new Error("Incorrect type");
        }
        var listOfPoints = [];
        for (var i = 0; i < jsonObj.arrayOfPoints.length; i++) {
            var pointJson = jsonObj.arrayOfPoints[i];
            var point = relativepoint_1.default.fromJSON(pointJson);
            listOfPoints.push(point);
        }
        return new Polygon(listOfPoints);
    };
    /**
     * Clone this Polygon.
     */
    Polygon.prototype.clone = function () {
        var tempArrayOfPoints = new Array();
        // Convert the relative points back to regular points.
        for (var i = 0; i < this._arrayOfPoints.length; i++) {
            var tempRelativePoint = this._arrayOfPoints[i];
            var tempPoint = new point_1.default(tempRelativePoint.x, tempRelativePoint.y, tempRelativePoint.z);
            tempArrayOfPoints.push(tempPoint);
        }
        return new Polygon(tempArrayOfPoints);
    };
    /**
     * Test if the given polygon is equal to this one.
     */
    Polygon.prototype.equals = function (otherObj) {
        if (!otherObj) {
            return false;
        }
        var otherArrayOfPoints = otherObj.getArrayOfPoints();
        if (otherArrayOfPoints.length !== this._arrayOfPoints.length) {
            return false;
        }
        for (var i = 0; i < otherArrayOfPoints.length; i++) {
            if (!otherArrayOfPoints[i].equals(this._arrayOfPoints[i])) {
                return false;
            }
        }
        return true;
    };
    /**
     * Get adjacent shapes. Include diagonals. This is used for path-finding.
     */
    Polygon.prototype.getAdjacentShapes = function () {
        var list = [];
        var arrayOfPoints = this.getArrayOfPoints();
        var rangeX = this.getRangeX();
        var rangeY = this.getRangeY();
        for (var i = -1; i <= 1; i++) {
            for (var j = -1; j <= 1; j++) {
                if (i !== 0 || j !== 0) {
                    var listOfPoints = [];
                    for (var n = 0; n < arrayOfPoints.length; n++) {
                        var p = arrayOfPoints[n];
                        var cloneOfP = p.clone();
                        cloneOfP.x = cloneOfP.x + i * rangeX;
                        cloneOfP.y = cloneOfP.y + j * rangeY;
                        listOfPoints.push(cloneOfP);
                    }
                    var poly = new Polygon(listOfPoints);
                    var shape = poly;
                    list.push(shape);
                }
            }
        }
        return list;
    };
    /**
     * Get an array of points that make up this polygon.
     */
    Polygon.prototype.getArrayOfPoints = function () {
        return this._arrayOfPoints;
    };
    /**
     * Get a point related to the center of this shape.
     */
    Polygon.prototype.getPosition = function () {
        return new point_1.default(this.x, this.y, this.z);
    };
    /*
     * Get the Z value relative to a given point. This is used in case
     * the rectangle represents a staircase and you need to get Z value
     * for a given X, Y.
     */
    Polygon.prototype.getZRelativeToPoint = function (point) {
        return this._plane.getZ(point.x, point.y);
    };
    Object.defineProperty(Polygon.prototype, "type", {
        /**
         * Get the type of this polygon.
         */
        get: function () {
            return constants_1.default.POLYGON;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Polygon.prototype, "x", {
        /**
         * Get the X coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.x;
        },
        /**
         * Set the X coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.x = value;
            if (this._linkPosition) {
                this._linkPosition.x = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Polygon.prototype, "y", {
        /**
         * Get the Y coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.y;
        },
        /**
         * Set the Y coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.y = value;
            if (this._linkPosition) {
                this._linkPosition.y = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Polygon.prototype, "z", {
        /**
         * Get the Z coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.z;
        },
        /**
         * Set the Z coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.z = value;
            if (this._linkPosition) {
                this._linkPosition.z = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Polygon.prototype, "linkPosition", {
        /**
         * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
         * shape changes the object of this link position also changes.
         */
        get: function () {
            return this._linkPosition;
        },
        /**
         * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
         * shape changes the object of this link position also changes.
         */
        set: function (value) {
            if (!value) {
                throw new Error("Value is null");
            }
            else if (value.hasOwnProperty("x") && value.hasOwnProperty("y") && value.hasOwnProperty("z")) {
                this._linkPosition = value;
                this._linkPosition.x = this._centerPoint.x;
                this._linkPosition.y = this._centerPoint.y;
                this._linkPosition.z = this._centerPoint.z;
            }
            else {
                throw new Error("Value does not have all the required properties");
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert this polygon to JSON.
     * @returns {{arrayOfPoints: Array}}
     */
    Polygon.prototype.toJSON = function () {
        var arrayOfPoints = this.getArrayOfPoints();
        var arr = [];
        for (var i = 0; i < arrayOfPoints.length; i++) {
            arr.push(this._arrayOfPoints[i].toJSON());
        }
        return {
            "arrayOfPoints": arr,
            "type": constants_1.default.POLYGON
        };
    };
    Polygon.prototype.toString = function () {
        var arrayOfPoints = this.getArrayOfPoints();
        var output = "";
        for (var i = 0; i < arrayOfPoints.length; i++) {
            output += this._arrayOfPoints[i].toString() + ";";
        }
        return "[" + output + "]";
    };
    /**
     * Get the range (max-min) in X axis.
     */
    Polygon.prototype.getRangeX = function () {
        var arrayOfPoints = this.getArrayOfPoints();
        var min = arrayOfPoints[0].x;
        var max = min;
        for (var i = 0; i < this._arrayOfPoints.length; i++) {
            var x = arrayOfPoints[i].x;
            if (x > max) {
                max = x;
            }
            if (x < min) {
                min = x;
            }
        }
        return max - min;
    };
    /**
     * Get the range (max-min) in Y axis.
     */
    Polygon.prototype.getRangeY = function () {
        var arrayOfPoints = this.getArrayOfPoints();
        var min = arrayOfPoints[0].y;
        var max = min;
        for (var i = 0; i < this._arrayOfPoints.length; i++) {
            var y = this._arrayOfPoints[i].y;
            if (y > max) {
                max = y;
            }
            if (y < min) {
                min = y;
            }
        }
        return max - min;
    };
    return Polygon;
}());
exports.Polygon = Polygon;
exports.default = Polygon;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvcG9seWdvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlDQUE0QjtBQUU1QixpREFBNEM7QUFDNUMsaUNBQTRCO0FBQzVCLHlDQUFvQztBQUVwQztJQTBFSSxpQkFBWSxhQUFzQjtRQUU5QixzQ0FBc0M7UUFDdEMsRUFBRSxDQUFDLENBQUUsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdCLE1BQU0sSUFBSSxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQztRQUMxRCxDQUFDO1FBRUQsc0NBQXNDO1FBQ3RDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxLQUFLLEVBQWlCLENBQUM7UUFFakQsMENBQTBDO1FBQzFDLElBQUksU0FBUyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUFDLElBQUksU0FBUyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLFNBQVMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFBQyxJQUFJLFNBQVMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkUsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDOUMsSUFBSSxLQUFLLEdBQVUsYUFBYSxDQUFDLENBQUMsQ0FBVSxDQUFDO1lBQzdDLEVBQUUsQ0FBQyxDQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdEIsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdEIsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdEIsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdEIsQ0FBQztRQUNMLENBQUM7UUFFRCw0QkFBNEI7UUFDNUIsSUFBSSxRQUFRLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0MsSUFBSSxRQUFRLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLGVBQUssQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRXJELEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1lBQzlDLElBQUksU0FBUyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLEtBQUssR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDcEMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLElBQUksS0FBSyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNwQyxJQUFJLGlCQUFpQixHQUFHLElBQUksdUJBQWEsQ0FBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBRSxDQUFDO1lBQ2pFLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2hELENBQUM7UUFFRCxxQkFBcUI7UUFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxlQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFNUcsQ0FBQztJQXJIRDs7Ozs7OztPQU9HO0lBQ1csaUJBQVMsR0FBdkIsVUFBeUIsVUFBaUIsRUFBRSxhQUFzQixFQUFFLEtBQWEsRUFBRSxPQUFrQjtRQUVqRyxJQUFJLE1BQU0sR0FBVyxLQUFLLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUM7UUFDM0MsSUFBSSxZQUFZLEdBQW9CLEVBQUUsQ0FBQztRQUN2QyxJQUFJLFVBQVUsR0FBVSxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekMsSUFBSSxjQUFjLEdBQUcsT0FBTyxDQUFDO1FBRTdCLEVBQUUsQ0FBQyxDQUFFLGNBQWUsQ0FBQyxDQUFDLENBQUM7WUFDbkIsRUFBRSxDQUFDLENBQUUsY0FBYyxDQUFDLE1BQU0sS0FBSyxhQUFhLENBQUMsTUFBTyxDQUFDLENBQUMsQ0FBQztnQkFDbkQsTUFBTSxJQUFJLEtBQUssQ0FBQyxpRkFBaUYsQ0FBQyxDQUFDO1lBQ3ZHLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixjQUFjLEdBQUcsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2pELEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO2dCQUMvQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLENBQUM7UUFDTCxDQUFDO1FBRUQsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDOUMsSUFBSSxLQUFLLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNySCxJQUFJLEtBQUssR0FBRyxVQUFVLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDckgsSUFBSSxLQUFLLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksTUFBTSxHQUFHLElBQUksdUJBQWEsQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLENBQUMsRUFBRSxLQUFLLEdBQUcsVUFBVSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsRixZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFRCxJQUFJLGVBQWUsR0FBRyxJQUFJLE9BQU8sQ0FBRSxZQUFZLENBQUUsQ0FBQztRQUNsRCxNQUFNLENBQUMsZUFBZSxDQUFDO0lBQzNCLENBQUM7SUFFRDs7OztPQUlHO0lBQ1csZ0JBQVEsR0FBdEIsVUFBdUIsT0FBWTtRQUUvQixFQUFFLENBQUMsQ0FBRSxDQUFDLE9BQVEsQ0FBQyxDQUFDLENBQUM7WUFDYixNQUFNLElBQUksS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7UUFDM0QsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFFLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0MsTUFBTSxJQUFJLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxPQUFPLENBQUMsSUFBSSxLQUFLLG1CQUFTLENBQUMsT0FBUSxDQUFDLENBQUMsQ0FBQztZQUN2QyxNQUFNLElBQUksS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDdEMsQ0FBQztRQUVELElBQUksWUFBWSxHQUFvQixFQUFFLENBQUM7UUFDdkMsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1lBQ3RELElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxLQUFLLEdBQUcsdUJBQWEsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBRXJDLENBQUM7SUFzREQ7O09BRUc7SUFDSSx1QkFBSyxHQUFaO1FBRUksSUFBSSxpQkFBaUIsR0FBRyxJQUFJLEtBQUssRUFBUyxDQUFDO1FBRTNDLHNEQUFzRDtRQUN0RCxHQUFHLENBQUMsQ0FBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDcEQsSUFBSSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9DLElBQUksU0FBUyxHQUFHLElBQUksZUFBSyxDQUFFLGlCQUFpQixDQUFDLENBQUMsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsaUJBQWlCLENBQUMsQ0FBQyxDQUFFLENBQUM7WUFDM0YsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3RDLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUUxQyxDQUFDO0lBRUQ7O09BRUc7SUFDSSx3QkFBTSxHQUFiLFVBQWUsUUFBaUI7UUFFNUIsRUFBRSxDQUFDLENBQUUsQ0FBQyxRQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2QsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQsSUFBSSxrQkFBa0IsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUNyRCxFQUFFLENBQUMsQ0FBRSxrQkFBa0IsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzdELE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQUVELEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsa0JBQWtCLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDbkQsRUFBRSxDQUFDLENBQUUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDNUQsTUFBTSxDQUFDLEtBQUssQ0FBQztZQUNqQixDQUFDO1FBQ0wsQ0FBQztRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFFaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksbUNBQWlCLEdBQXhCO1FBRUksSUFBSSxJQUFJLEdBQVksRUFBRSxDQUFDO1FBRXZCLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUM5QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFFOUIsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1lBQzdCLEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUcsQ0FBQztnQkFDN0IsRUFBRSxDQUFDLENBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBRSxDQUFDLENBQUMsQ0FBQztvQkFFdkIsSUFBSSxZQUFZLEdBQW9CLEVBQUUsQ0FBQztvQkFDdkMsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFHLENBQUM7d0JBQzlDLElBQUksQ0FBQyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDekIsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUN6QixRQUFRLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQzt3QkFDckMsUUFBUSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUM7d0JBQ3JDLFlBQVksQ0FBQyxJQUFJLENBQUUsUUFBUSxDQUFFLENBQUM7b0JBQ2xDLENBQUM7b0JBRUQsSUFBSSxJQUFJLEdBQUcsSUFBSSxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3JDLElBQUksS0FBSyxHQUFVLElBQWEsQ0FBQztvQkFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFckIsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUVoQixDQUFDO0lBRUQ7O09BRUc7SUFDSSxrQ0FBZ0IsR0FBdkI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQixDQUFDO0lBRUQ7O09BRUc7SUFDSSw2QkFBVyxHQUFsQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLGVBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRDs7OztPQUlHO0lBQ0kscUNBQW1CLEdBQTFCLFVBQTJCLEtBQVk7UUFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFLRCxzQkFBSSx5QkFBSTtRQUhSOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsbUJBQVMsQ0FBQyxPQUFPLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7SUFLRCxzQkFBSSxzQkFBQztRQUhMOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQUVEOztXQUVHO2FBQ0gsVUFBTSxLQUFhO1lBQ2YsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxhQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDakMsQ0FBQztRQUNMLENBQUM7OztPQVZBO0lBZUQsc0JBQUksc0JBQUM7UUFITDs7V0FFRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1FBQy9CLENBQUM7UUFFRDs7V0FFRzthQUNILFVBQU0sS0FBYTtZQUNmLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUM1QixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsYUFBYyxDQUFDLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLENBQUM7UUFDTCxDQUFDOzs7T0FWQTtJQWVELHNCQUFJLHNCQUFDO1FBSEw7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUMvQixDQUFDO1FBRUQ7O1dBRUc7YUFDSCxVQUFNLEtBQWE7WUFDZixJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDNUIsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLGFBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUNqQyxDQUFDO1FBQ0wsQ0FBQzs7O09BVkE7SUFnQkQsc0JBQUksaUNBQVk7UUFKaEI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM5QixDQUFDO1FBRUQ7OztXQUdHO2FBQ0gsVUFBaUIsS0FBVTtZQUN2QixFQUFFLENBQUMsQ0FBRSxDQUFDLEtBQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsTUFBTSxJQUFJLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNyQyxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLEtBQUssQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBRSxDQUFDLENBQUMsQ0FBQztnQkFDL0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUMzQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDL0MsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztZQUN2RSxDQUFDO1FBQ0wsQ0FBQzs7O09BakJBO0lBbUJEOzs7T0FHRztJQUNJLHdCQUFNLEdBQWI7UUFDSSxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM1QyxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDYixHQUFHLENBQUMsQ0FBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUcsQ0FBQztZQUM5QyxHQUFHLENBQUMsSUFBSSxDQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUUsQ0FBQztRQUNoRCxDQUFDO1FBQ0QsTUFBTSxDQUFDO1lBQ0gsZUFBZSxFQUFFLEdBQUc7WUFDcEIsTUFBTSxFQUFFLG1CQUFTLENBQUMsT0FBTztTQUM1QixDQUFDO0lBQ04sQ0FBQztJQUVNLDBCQUFRLEdBQWY7UUFDSSxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM1QyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDaEIsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDOUMsTUFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRyxDQUFDO1FBQ3RELENBQUM7UUFDRCxNQUFNLENBQUMsR0FBRyxHQUFHLE1BQU0sR0FBRyxHQUFHLENBQUM7SUFDOUIsQ0FBQztJQUVEOztPQUVHO0lBQ0ssMkJBQVMsR0FBakI7UUFDSSxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM1QyxJQUFJLEdBQUcsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdCLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNkLEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUcsQ0FBQztZQUNwRCxJQUFJLENBQUMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNCLEVBQUUsQ0FBQyxDQUFFLENBQUMsR0FBRyxHQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNaLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDWixDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUUsQ0FBQyxHQUFHLEdBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ1osR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNaLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7SUFDckIsQ0FBQztJQUVEOztPQUVHO0lBQ0ssMkJBQVMsR0FBakI7UUFDSSxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM1QyxJQUFJLEdBQUcsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdCLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNkLEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUcsQ0FBQztZQUNwRCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxFQUFFLENBQUMsQ0FBRSxDQUFDLEdBQUcsR0FBSSxDQUFDLENBQUMsQ0FBQztnQkFDWixHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ1osQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFFLENBQUMsR0FBRyxHQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNaLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDWixDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO0lBQ3JCLENBQUM7SUFFTCxjQUFDO0FBQUQsQ0FyWEEsQUFxWEMsSUFBQTtBQXJYWSwwQkFBTztBQXVYcEIsa0JBQWUsT0FBTyxDQUFDIiwiZmlsZSI6Im1hdGgvcG9seWdvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQb2ludCBmcm9tIFwiLi9wb2ludFwiO1xyXG5pbXBvcnQgU2hhcGUgZnJvbSBcIi4vc2hhcGVcIjtcclxuaW1wb3J0IFJlbGF0aXZlUG9pbnQgZnJvbSBcIi4vcmVsYXRpdmVwb2ludFwiO1xyXG5pbXBvcnQgUGxhbmUgZnJvbSBcIi4vcGxhbmVcIjtcclxuaW1wb3J0IENvbnN0YW50cyBmcm9tIFwiLi9jb25zdGFudHNcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBQb2x5Z29uIGltcGxlbWVudHMgU2hhcGUge1xyXG5cclxuICAgIHByaXZhdGUgX2FycmF5T2ZQb2ludHM6IFJlbGF0aXZlUG9pbnRbXTtcclxuICAgIHByaXZhdGUgX2NlbnRlclBvaW50OiBQb2ludDtcclxuICAgIHByaXZhdGUgX3BsYW5lOiBQbGFuZTtcclxuICAgIHByaXZhdGUgX2xpbmtQb3NpdGlvbjogYW55O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGFrZXMgaW4gYSBUaWxlZCBvYmplY3QgKFBvbHlsaW5lKSBhbmQgY29udmVydHMgaXQgaW50byBhIFBvbHlnb24uXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHN0YXJ0UG9pbnQgVGhlIHN0YXJ0aW5nIHgseSBwb2ludCBvZiB0aGUgcG9seWxpbmUuXHJcbiAgICAgKiBAcGFyYW0gYXJyYXlPZlBvaW50cyBBbiBhcnJheSBvZiBtYXRoLlBvaW50IG9iamVjdHMuXHJcbiAgICAgKiBAcGFyYW0gYW5nbGUgVGhlIGFuZ2xlIGluIGRlZ3JlZXNcclxuICAgICAqIEBwYXJhbSBoZWlnaHRzIChPcHRpb25hbCkgQW4gYXJyYXkgb2YgbnVtYmVycyB3aGljaCByZXByZXNlbnRzIHRoZSBoZWlnaHQgb2YgZWFjaCBwb2ludCBpbiB0aGUgYXJyYXkuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbVRpbGVkKCBzdGFydFBvaW50OiBQb2ludCwgYXJyYXlPZlBvaW50czogUG9pbnRbXSwgYW5nbGU6IG51bWJlciwgaGVpZ2h0cz86IG51bWJlcltdICkge1xyXG5cclxuICAgICAgICBsZXQgcmFkaWFuOiBudW1iZXIgPSBhbmdsZSAqIE1hdGguUEkgLyAxODA7XHJcbiAgICAgICAgbGV0IHBvbHlnb25BcnJheTogUmVsYXRpdmVQb2ludFtdID0gW107XHJcbiAgICAgICAgbGV0IGZpcnN0UG9pbnQ6IFBvaW50ID0gYXJyYXlPZlBvaW50c1swXTtcclxuICAgICAgICBsZXQgaGVpZ2h0T2ZQb2ludHMgPSBoZWlnaHRzO1xyXG5cclxuICAgICAgICBpZiAoIGhlaWdodE9mUG9pbnRzICkge1xyXG4gICAgICAgICAgICBpZiAoIGhlaWdodE9mUG9pbnRzLmxlbmd0aCAhPT0gYXJyYXlPZlBvaW50cy5sZW5ndGggKSB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJOdW1iZXIgb2YgcG9pbnRzIGluIHRoZSBoZWlnaHQgZG9lcyBub3QgbWF0Y2ggdGhlIG51bWJlciBvZiBwb2ludHMgaW4gdGhlIGFycmF5XCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaGVpZ2h0T2ZQb2ludHMgPSBuZXcgQXJyYXkoYXJyYXlPZlBvaW50cy5sZW5ndGgpO1xyXG4gICAgICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBoZWlnaHRPZlBvaW50cy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgICAgIGhlaWdodE9mUG9pbnRzW2ldID0gMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZm9yICggbGV0IGkgPSAwOyBpIDwgYXJyYXlPZlBvaW50cy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgbGV0IHBvaW50ID0gYXJyYXlPZlBvaW50c1tpXTtcclxuICAgICAgICAgICAgbGV0IHRlbXBYID0gZmlyc3RQb2ludC54ICsgKHBvaW50LnggLSBmaXJzdFBvaW50LngpICogTWF0aC5jb3MocmFkaWFuKSAtIChwb2ludC55IC0gZmlyc3RQb2ludC55KSAqIE1hdGguc2luKHJhZGlhbik7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wWSA9IGZpcnN0UG9pbnQueSArIChwb2ludC54IC0gZmlyc3RQb2ludC54KSAqIE1hdGguc2luKHJhZGlhbikgKyAocG9pbnQueSAtIGZpcnN0UG9pbnQueSkgKiBNYXRoLmNvcyhyYWRpYW4pO1xyXG4gICAgICAgICAgICBsZXQgdGVtcFogPSBoZWlnaHRPZlBvaW50c1tpXTtcclxuICAgICAgICAgICAgbGV0IHBvaW50MiA9IG5ldyBSZWxhdGl2ZVBvaW50KHRlbXBYICsgc3RhcnRQb2ludC54LCB0ZW1wWSArIHN0YXJ0UG9pbnQueSwgdGVtcFopO1xyXG4gICAgICAgICAgICBwb2x5Z29uQXJyYXkucHVzaChwb2ludDIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGJyYW5kTmV3UG9seWdvbiA9IG5ldyBQb2x5Z29uKCBwb2x5Z29uQXJyYXkgKTtcclxuICAgICAgICByZXR1cm4gYnJhbmROZXdQb2x5Z29uO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBKU09OIG9iamVjdCB0byBwb2ludC5cclxuICAgICAqIEBwYXJhbSBqc29uT2JqXHJcbiAgICAgKiBAcmV0dXJucyB7bWF0aC5Qb2x5Z29ufVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmo6IGFueSk6IFBvbHlnb24ge1xyXG5cclxuICAgICAgICBpZiAoICFqc29uT2JqICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgY29udmVydCB1bmRlZmluZWQgdG8gcG9seWdvblwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCAhanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcImFycmF5T2ZQb2ludHNcIikgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIk5vIGFycmF5T2ZQb2ludHMgcHJvcGVydHlcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICgganNvbk9iai50eXBlICE9PSBDb25zdGFudHMuUE9MWUdPTiApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiSW5jb3JyZWN0IHR5cGVcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgbGlzdE9mUG9pbnRzOiBSZWxhdGl2ZVBvaW50W10gPSBbXTtcclxuICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBqc29uT2JqLmFycmF5T2ZQb2ludHMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgICAgIGxldCBwb2ludEpzb24gPSBqc29uT2JqLmFycmF5T2ZQb2ludHNbaV07XHJcbiAgICAgICAgICAgIGxldCBwb2ludCA9IFJlbGF0aXZlUG9pbnQuZnJvbUpTT04ocG9pbnRKc29uKTtcclxuICAgICAgICAgICAgbGlzdE9mUG9pbnRzLnB1c2gocG9pbnQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQb2x5Z29uKGxpc3RPZlBvaW50cyk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGFycmF5T2ZQb2ludHM6IFBvaW50W10pIHtcclxuXHJcbiAgICAgICAgLy8gUG9seWdvbiBtdXN0IGhhdmUgMyBvciBtb3JlIHBvaW50cy5cclxuICAgICAgICBpZiAoIGFycmF5T2ZQb2ludHMubGVuZ3RoIDwgMyApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiUG9seWdvbiBtdXN0IGhhdmUgMyBvciBtb3JlIHBvaW50c1wiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIENyZWF0ZSBhbiBhcnJheSBvZiByZWxhdGl2ZSBwb2ludHMuXHJcbiAgICAgICAgdGhpcy5fYXJyYXlPZlBvaW50cyA9IG5ldyBBcnJheTxSZWxhdGl2ZVBvaW50PigpO1xyXG5cclxuICAgICAgICAvLyBDb21wdXRlIHRoZSBtaW4gYW5kIG1heCB4IGFuZCB5IHZhbHVlcy5cclxuICAgICAgICBsZXQgbWluWFBvaW50ID0gYXJyYXlPZlBvaW50c1swXTsgbGV0IG1heFhQb2ludCA9IGFycmF5T2ZQb2ludHNbMF07XHJcbiAgICAgICAgbGV0IG1pbllQb2ludCA9IGFycmF5T2ZQb2ludHNbMF07IGxldCBtYXhZUG9pbnQgPSBhcnJheU9mUG9pbnRzWzBdO1xyXG4gICAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IGFycmF5T2ZQb2ludHMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgICAgIGxldCBwb2ludDogUG9pbnQgPSBhcnJheU9mUG9pbnRzW2ldIGFzIFBvaW50O1xyXG4gICAgICAgICAgICBpZiAoIHBvaW50LnggPiBtYXhYUG9pbnQueCApIHtcclxuICAgICAgICAgICAgICAgIG1heFhQb2ludCA9IHBvaW50O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICggcG9pbnQueCA8IG1pblhQb2ludC54ICkge1xyXG4gICAgICAgICAgICAgICAgbWluWFBvaW50ID0gcG9pbnQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKCBwb2ludC55ID4gbWF4WVBvaW50LnkgKSB7XHJcbiAgICAgICAgICAgICAgICBtYXhZUG9pbnQgPSBwb2ludDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIHBvaW50LnkgPCBtaW5ZUG9pbnQueSApIHtcclxuICAgICAgICAgICAgICAgIG1pbllQb2ludCA9IHBvaW50O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBDb21wdXRlIHRoZSBjZW50ZXIgcG9pbnQuXHJcbiAgICAgICAgbGV0IGF2ZXJhZ2VYID0gKG1heFhQb2ludC54ICsgbWluWFBvaW50LngpIC8gMjtcclxuICAgICAgICBsZXQgYXZlcmFnZVkgPSAobWF4WVBvaW50LnkgKyBtaW5ZUG9pbnQueSkgLyAyO1xyXG4gICAgICAgIHRoaXMuX2NlbnRlclBvaW50ID0gbmV3IFBvaW50KGF2ZXJhZ2VYLCBhdmVyYWdlWSwgMCk7XHJcblxyXG4gICAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IGFycmF5T2ZQb2ludHMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wUG9pbnQgPSBhcnJheU9mUG9pbnRzW2ldO1xyXG4gICAgICAgICAgICBsZXQgeCA9IHRlbXBQb2ludC54ID8gdGVtcFBvaW50LnggOiAwO1xyXG4gICAgICAgICAgICBsZXQgeSA9IHRlbXBQb2ludC55ID8gdGVtcFBvaW50LnkgOiAwO1xyXG4gICAgICAgICAgICBsZXQgeiA9IHRlbXBQb2ludC56ID8gdGVtcFBvaW50LnogOiAwO1xyXG4gICAgICAgICAgICBsZXQgdGVtcFggPSB4IC0gdGhpcy5fY2VudGVyUG9pbnQueDtcclxuICAgICAgICAgICAgbGV0IHRlbXBZID0geSAtIHRoaXMuX2NlbnRlclBvaW50Lnk7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wWiA9IHogLSB0aGlzLl9jZW50ZXJQb2ludC56O1xyXG4gICAgICAgICAgICBsZXQgdGVtcFJlbGF0aXZlUG9pbnQgPSBuZXcgUmVsYXRpdmVQb2ludCggdGVtcFgsIHRlbXBZLCB0ZW1wWiApO1xyXG4gICAgICAgICAgICB0ZW1wUmVsYXRpdmVQb2ludC5zZXRSZWxhdGl2ZVBvaW50KHRoaXMuX2NlbnRlclBvaW50KTtcclxuICAgICAgICAgICAgdGhpcy5fYXJyYXlPZlBvaW50cy5wdXNoKHRlbXBSZWxhdGl2ZVBvaW50KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIENvbXB1dGUgdGhlIHBsYW5lLlxyXG4gICAgICAgIHRoaXMuX3BsYW5lID0gUGxhbmUuZnJvbTNQb2ludHModGhpcy5fYXJyYXlPZlBvaW50c1swXSwgdGhpcy5fYXJyYXlPZlBvaW50c1sxXSwgdGhpcy5fYXJyYXlPZlBvaW50c1syXSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2xvbmUgdGhpcyBQb2x5Z29uLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgY2xvbmUoKTogUG9seWdvbiB7XHJcblxyXG4gICAgICAgIGxldCB0ZW1wQXJyYXlPZlBvaW50cyA9IG5ldyBBcnJheTxQb2ludD4oKTtcclxuXHJcbiAgICAgICAgLy8gQ29udmVydCB0aGUgcmVsYXRpdmUgcG9pbnRzIGJhY2sgdG8gcmVndWxhciBwb2ludHMuXHJcbiAgICAgICAgZm9yICggbGV0IGkgPSAwOyBpIDwgdGhpcy5fYXJyYXlPZlBvaW50cy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgbGV0IHRlbXBSZWxhdGl2ZVBvaW50ID0gdGhpcy5fYXJyYXlPZlBvaW50c1tpXTtcclxuICAgICAgICAgICAgbGV0IHRlbXBQb2ludCA9IG5ldyBQb2ludCggdGVtcFJlbGF0aXZlUG9pbnQueCwgdGVtcFJlbGF0aXZlUG9pbnQueSwgdGVtcFJlbGF0aXZlUG9pbnQueiApO1xyXG4gICAgICAgICAgICB0ZW1wQXJyYXlPZlBvaW50cy5wdXNoKHRlbXBQb2ludCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbmV3IFBvbHlnb24odGVtcEFycmF5T2ZQb2ludHMpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRlc3QgaWYgdGhlIGdpdmVuIHBvbHlnb24gaXMgZXF1YWwgdG8gdGhpcyBvbmUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBlcXVhbHMoIG90aGVyT2JqOiBQb2x5Z29uICkge1xyXG5cclxuICAgICAgICBpZiAoICFvdGhlck9iaiApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IG90aGVyQXJyYXlPZlBvaW50cyA9IG90aGVyT2JqLmdldEFycmF5T2ZQb2ludHMoKTtcclxuICAgICAgICBpZiAoIG90aGVyQXJyYXlPZlBvaW50cy5sZW5ndGggIT09IHRoaXMuX2FycmF5T2ZQb2ludHMubGVuZ3RoICkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBvdGhlckFycmF5T2ZQb2ludHMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgICAgIGlmICggIW90aGVyQXJyYXlPZlBvaW50c1tpXS5lcXVhbHMoIHRoaXMuX2FycmF5T2ZQb2ludHNbaV0gKSApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGFkamFjZW50IHNoYXBlcy4gSW5jbHVkZSBkaWFnb25hbHMuIFRoaXMgaXMgdXNlZCBmb3IgcGF0aC1maW5kaW5nLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0QWRqYWNlbnRTaGFwZXMoKTogU2hhcGVbXSB7XHJcblxyXG4gICAgICAgIGxldCBsaXN0OiBTaGFwZVtdID0gW107XHJcblxyXG4gICAgICAgIGxldCBhcnJheU9mUG9pbnRzID0gdGhpcy5nZXRBcnJheU9mUG9pbnRzKCk7XHJcbiAgICAgICAgbGV0IHJhbmdlWCA9IHRoaXMuZ2V0UmFuZ2VYKCk7XHJcbiAgICAgICAgbGV0IHJhbmdlWSA9IHRoaXMuZ2V0UmFuZ2VZKCk7XHJcblxyXG4gICAgICAgIGZvciAoIGxldCBpID0gLTE7IGkgPD0gMTsgaSsrICkge1xyXG4gICAgICAgICAgICBmb3IgKCBsZXQgaiA9IC0xOyBqIDw9IDE7IGorKyApIHtcclxuICAgICAgICAgICAgICAgIGlmICggaSAhPT0gMCB8fCBqICE9PSAwICkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBsZXQgbGlzdE9mUG9pbnRzOiBSZWxhdGl2ZVBvaW50W10gPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKCBsZXQgbiA9IDA7IG4gPCBhcnJheU9mUG9pbnRzLmxlbmd0aDsgbisrICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgcCA9IGFycmF5T2ZQb2ludHNbbl07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBjbG9uZU9mUCA9IHAuY2xvbmUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xvbmVPZlAueCA9IGNsb25lT2ZQLnggKyBpICogcmFuZ2VYO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbG9uZU9mUC55ID0gY2xvbmVPZlAueSArIGogKiByYW5nZVk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxpc3RPZlBvaW50cy5wdXNoKCBjbG9uZU9mUCApO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHBvbHkgPSBuZXcgUG9seWdvbihsaXN0T2ZQb2ludHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBzaGFwZTogU2hhcGUgPSBwb2x5IGFzIFNoYXBlO1xyXG4gICAgICAgICAgICAgICAgICAgIGxpc3QucHVzaChzaGFwZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbGlzdDtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYW4gYXJyYXkgb2YgcG9pbnRzIHRoYXQgbWFrZSB1cCB0aGlzIHBvbHlnb24uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRBcnJheU9mUG9pbnRzKCk6IFJlbGF0aXZlUG9pbnRbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FycmF5T2ZQb2ludHM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYSBwb2ludCByZWxhdGVkIHRvIHRoZSBjZW50ZXIgb2YgdGhpcyBzaGFwZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFBvc2l0aW9uKCk6IFBvaW50IHtcclxuICAgICAgICByZXR1cm4gbmV3IFBvaW50KHRoaXMueCwgdGhpcy55LCB0aGlzLnopO1xyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBHZXQgdGhlIFogdmFsdWUgcmVsYXRpdmUgdG8gYSBnaXZlbiBwb2ludC4gVGhpcyBpcyB1c2VkIGluIGNhc2VcclxuICAgICAqIHRoZSByZWN0YW5nbGUgcmVwcmVzZW50cyBhIHN0YWlyY2FzZSBhbmQgeW91IG5lZWQgdG8gZ2V0IFogdmFsdWVcclxuICAgICAqIGZvciBhIGdpdmVuIFgsIFkuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRaUmVsYXRpdmVUb1BvaW50KHBvaW50OiBQb2ludCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BsYW5lLmdldFoocG9pbnQueCwgcG9pbnQueSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIHR5cGUgb2YgdGhpcyBwb2x5Z29uLlxyXG4gICAgICovXHJcbiAgICBnZXQgdHlwZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBDb25zdGFudHMuUE9MWUdPTjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgWCBjb29yZGluYXRlIG9mIHRoZSBjZW50ZXIgb2YgdGhpcyBzaGFwZS5cclxuICAgICAqL1xyXG4gICAgZ2V0IHgoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY2VudGVyUG9pbnQueDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCB0aGUgWCBjb29yZGluYXRlIG9mIHRoZSBjZW50ZXIgb2YgdGhpcyBzaGFwZS5cclxuICAgICAqL1xyXG4gICAgc2V0IHgodmFsdWU6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2NlbnRlclBvaW50LnggPSB2YWx1ZTtcclxuICAgICAgICBpZiAoIHRoaXMuX2xpbmtQb3NpdGlvbiApIHtcclxuICAgICAgICAgICAgdGhpcy5fbGlua1Bvc2l0aW9uLnggPSB2YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFkgY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIGdldCB5KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50Lnk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIFkgY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIHNldCB5KHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9jZW50ZXJQb2ludC55ID0gdmFsdWU7XHJcbiAgICAgICAgaWYgKCB0aGlzLl9saW5rUG9zaXRpb24gKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi55ID0gdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBaIGNvb3JkaW5hdGUgb2YgdGhlIGNlbnRlciBvZiB0aGlzIHNoYXBlLlxyXG4gICAgICovXHJcbiAgICBnZXQgeigpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jZW50ZXJQb2ludC56O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSBaIGNvb3JkaW5hdGUgb2YgdGhlIGNlbnRlciBvZiB0aGlzIHNoYXBlLlxyXG4gICAgICovXHJcbiAgICBzZXQgeih2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fY2VudGVyUG9pbnQueiA9IHZhbHVlO1xyXG4gICAgICAgIGlmICggdGhpcy5fbGlua1Bvc2l0aW9uICkge1xyXG4gICAgICAgICAgICB0aGlzLl9saW5rUG9zaXRpb24ueiA9IHZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExpbmsgdGhlIHgsIHksIHogcG9zaXRpb24gb2YgdGhlIGdpdmVuIG9iamVjdCB0byB0aGlzIHNoYXBlJ3MgcG9zaXRpb24uIEV2ZXJ5IHRpbWUgdGhlIHgsIHkgb3IgeiB2YWx1ZSBvZiB0aGlzXHJcbiAgICAgKiBzaGFwZSBjaGFuZ2VzIHRoZSBvYmplY3Qgb2YgdGhpcyBsaW5rIHBvc2l0aW9uIGFsc28gY2hhbmdlcy5cclxuICAgICAqL1xyXG4gICAgZ2V0IGxpbmtQb3NpdGlvbigpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9saW5rUG9zaXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMaW5rIHRoZSB4LCB5LCB6IHBvc2l0aW9uIG9mIHRoZSBnaXZlbiBvYmplY3QgdG8gdGhpcyBzaGFwZSdzIHBvc2l0aW9uLiBFdmVyeSB0aW1lIHRoZSB4LCB5IG9yIHogdmFsdWUgb2YgdGhpc1xyXG4gICAgICogc2hhcGUgY2hhbmdlcyB0aGUgb2JqZWN0IG9mIHRoaXMgbGluayBwb3NpdGlvbiBhbHNvIGNoYW5nZXMuXHJcbiAgICAgKi9cclxuICAgIHNldCBsaW5rUG9zaXRpb24odmFsdWU6IGFueSkge1xyXG4gICAgICAgIGlmICggIXZhbHVlICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJWYWx1ZSBpcyBudWxsXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIHZhbHVlLmhhc093blByb3BlcnR5KFwieFwiKSAmJiB2YWx1ZS5oYXNPd25Qcm9wZXJ0eShcInlcIikgJiYgdmFsdWUuaGFzT3duUHJvcGVydHkoXCJ6XCIpICkge1xyXG4gICAgICAgICAgICB0aGlzLl9saW5rUG9zaXRpb24gPSB2YWx1ZTtcclxuICAgICAgICAgICAgdGhpcy5fbGlua1Bvc2l0aW9uLnggPSB0aGlzLl9jZW50ZXJQb2ludC54O1xyXG4gICAgICAgICAgICB0aGlzLl9saW5rUG9zaXRpb24ueSA9IHRoaXMuX2NlbnRlclBvaW50Lnk7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi56ID0gdGhpcy5fY2VudGVyUG9pbnQuejtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJWYWx1ZSBkb2VzIG5vdCBoYXZlIGFsbCB0aGUgcmVxdWlyZWQgcHJvcGVydGllc1wiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgcG9seWdvbiB0byBKU09OLlxyXG4gICAgICogQHJldHVybnMge3thcnJheU9mUG9pbnRzOiBBcnJheX19XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogYW55IHtcclxuICAgICAgICBsZXQgYXJyYXlPZlBvaW50cyA9IHRoaXMuZ2V0QXJyYXlPZlBvaW50cygpO1xyXG4gICAgICAgIGxldCBhcnIgPSBbXTtcclxuICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBhcnJheU9mUG9pbnRzLmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgICAgICBhcnIucHVzaCggdGhpcy5fYXJyYXlPZlBvaW50c1tpXS50b0pTT04oKSApO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBcImFycmF5T2ZQb2ludHNcIjogYXJyLFxyXG4gICAgICAgICAgICBcInR5cGVcIjogQ29uc3RhbnRzLlBPTFlHT05cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b1N0cmluZygpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCBhcnJheU9mUG9pbnRzID0gdGhpcy5nZXRBcnJheU9mUG9pbnRzKCk7XHJcbiAgICAgICAgbGV0IG91dHB1dCA9IFwiXCI7XHJcbiAgICAgICAgZm9yICggbGV0IGkgPSAwOyBpIDwgYXJyYXlPZlBvaW50cy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgb3V0cHV0ICs9IHRoaXMuX2FycmF5T2ZQb2ludHNbaV0udG9TdHJpbmcoKSArIFwiO1wiO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gXCJbXCIgKyBvdXRwdXQgKyBcIl1cIjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgcmFuZ2UgKG1heC1taW4pIGluIFggYXhpcy5cclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBnZXRSYW5nZVgoKTogbnVtYmVyIHtcclxuICAgICAgICBsZXQgYXJyYXlPZlBvaW50cyA9IHRoaXMuZ2V0QXJyYXlPZlBvaW50cygpO1xyXG4gICAgICAgIGxldCBtaW4gPSBhcnJheU9mUG9pbnRzWzBdLng7XHJcbiAgICAgICAgbGV0IG1heCA9IG1pbjtcclxuICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCB0aGlzLl9hcnJheU9mUG9pbnRzLmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgICAgICBsZXQgeCA9IGFycmF5T2ZQb2ludHNbaV0ueDtcclxuICAgICAgICAgICAgaWYgKCB4ID4gbWF4ICkge1xyXG4gICAgICAgICAgICAgICAgbWF4ID0geDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIHggPCBtaW4gKSB7XHJcbiAgICAgICAgICAgICAgICBtaW4gPSB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBtYXggLSBtaW47XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIHJhbmdlIChtYXgtbWluKSBpbiBZIGF4aXMuXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgZ2V0UmFuZ2VZKCk6IG51bWJlciB7XHJcbiAgICAgICAgbGV0IGFycmF5T2ZQb2ludHMgPSB0aGlzLmdldEFycmF5T2ZQb2ludHMoKTtcclxuICAgICAgICBsZXQgbWluID0gYXJyYXlPZlBvaW50c1swXS55O1xyXG4gICAgICAgIGxldCBtYXggPSBtaW47XHJcbiAgICAgICAgZm9yICggbGV0IGkgPSAwOyBpIDwgdGhpcy5fYXJyYXlPZlBvaW50cy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgbGV0IHkgPSB0aGlzLl9hcnJheU9mUG9pbnRzW2ldLnk7XHJcbiAgICAgICAgICAgIGlmICggeSA+IG1heCApIHtcclxuICAgICAgICAgICAgICAgIG1heCA9IHk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKCB5IDwgbWluICkge1xyXG4gICAgICAgICAgICAgICAgbWluID0geTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbWF4IC0gbWluO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUG9seWdvbjtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
