"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("./point");
var shape_1 = require("./shape");
var constants_1 = require("./constants");
var Circle = /** @class */ (function (_super) {
    __extends(Circle, _super);
    /**
     * Consruct a circle object.
     * @param centerPoint
     * @param circleRadius
     */
    function Circle(centerPoint, circleRadius) {
        var _this = _super.call(this) || this;
        _this._centerPoint = centerPoint;
        _this._circleRadius = circleRadius;
        return _this;
    }
    /**
     * Convert JSON to math.Circle object.
     * @param jsonObj
     * @returns {math.Circle}
     */
    Circle.fromJSON = function (jsonObj) {
        if (!jsonObj) {
            throw new Error("Cannot convert undefined to Circle");
        }
        else if (!jsonObj.hasOwnProperty("center")) {
            throw new Error("Center property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("radius")) {
            throw new Error("Radius property does not exist");
        }
        else if (!jsonObj.hasOwnProperty("type")) {
            throw new Error("Type property does not exist");
        }
        else if (jsonObj.type !== constants_1.default.CIRCLE) {
            throw new Error("Invalid type");
        }
        var centerPoint = point_1.default.fromJSON(jsonObj.center);
        var radius = jsonObj.radius;
        return new Circle(centerPoint, radius);
    };
    /**
     * Get adjacent shapes. Include diagonals. This is used for path-finding.
     */
    Circle.prototype.getAdjacentShapes = function () {
        var list = [];
        var doubleRadius = this._circleRadius * 2;
        for (var i = -1; i <= 1; i++) {
            for (var j = -1; j <= 1; j++) {
                if (i !== 0 || j !== 0) {
                    var point = new point_1.default(this._centerPoint.x + i * doubleRadius, this._centerPoint.y + j * doubleRadius, this._centerPoint.z);
                    var circle = new Circle(point, this._circleRadius);
                    list.push(circle);
                }
            }
        }
        return list;
    };
    /**
     * Clone this circle.
     */
    Circle.prototype.clone = function () {
        return new Circle(this._centerPoint, this._circleRadius);
    };
    /**
     * Get the center of this circle.
     */
    Circle.prototype.getCenter = function () {
        return this._centerPoint;
    };
    /**
     * Get a point related to the center of this shape.
     */
    Circle.prototype.getPosition = function () {
        return this._centerPoint;
    };
    /**
     * Get the radius of this circle.
     */
    Circle.prototype.getRadius = function () {
        return this._circleRadius;
    };
    /**
     * Get the Z value relative to a given point. In the case of a circle
     * the z is always the circle's z value.
     */
    Circle.prototype.getZRelativeToPoint = function (point) {
        return this.z;
    };
    Object.defineProperty(Circle.prototype, "type", {
        /**
         * Get the type of this polygon.
         */
        get: function () {
            return constants_1.default.CIRCLE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Circle.prototype, "x", {
        /**
         * Get the X coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.x;
        },
        /**
         * Set the X coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.x = value;
            if (this._linkPosition) {
                this._linkPosition.x = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Circle.prototype, "y", {
        /**
         * Get the Y coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.y;
        },
        /**
         * Set the Y coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.y = value;
            if (this._linkPosition) {
                this._linkPosition.y = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Circle.prototype, "z", {
        /**
         * Get the Z coordinate of the center of this shape.
         */
        get: function () {
            return this._centerPoint.z;
        },
        /**
         * Set the Z coordinate of the center of this shape.
         */
        set: function (value) {
            this._centerPoint.z = value;
            if (this._linkPosition) {
                this._linkPosition.z = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Set the center of the circle.
     */
    Circle.prototype.setCenter = function (aPoint) {
        this._centerPoint = aPoint;
        if (this._linkPosition) {
            this._linkPosition.x = aPoint.x;
            this._linkPosition.y = aPoint.y;
            this._linkPosition.z = aPoint.z;
        }
    };
    /**
     * Set the radius of the circle.
     */
    Circle.prototype.setRadius = function (aRadius) {
        this._circleRadius = aRadius;
    };
    Object.defineProperty(Circle.prototype, "linkPosition", {
        /**
         * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
         * shape changes the object of this link position also changes.
         */
        get: function () {
            return this._linkPosition;
        },
        /**
         * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
         * shape changes the object of this link position also changes.
         */
        set: function (value) {
            if (!value) {
                throw new Error("Value is null");
            }
            else if (value.hasOwnProperty("x") && value.hasOwnProperty("y") && value.hasOwnProperty("z")) {
                this._linkPosition = value;
                this._linkPosition.x = this._centerPoint.x;
                this._linkPosition.y = this._centerPoint.y;
                this._linkPosition.z = this._centerPoint.z;
            }
            else {
                throw new Error("Value does not have all the required properties");
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if a circle equals this circle.
     */
    Circle.prototype.equals = function (aCircleObj) {
        if (aCircleObj.getCenter().equals(this.getCenter()) &&
            aCircleObj.getRadius() === this.getRadius()) {
            return true;
        }
        return false;
    };
    /**
     * Convert the circle to JSON.
     */
    Circle.prototype.toJSON = function () {
        return {
            "center": this._centerPoint.toJSON(),
            "radius": this._circleRadius,
            "type": constants_1.default.CIRCLE
        };
    };
    return Circle;
}(shape_1.default));
exports.Circle = Circle;
exports.default = Circle;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGgvY2lyY2xlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLGlDQUE0QjtBQUM1QixpQ0FBNEI7QUFDNUIseUNBQW9DO0FBRXBDO0lBQTRCLDBCQUFLO0lBZ0M3Qjs7OztPQUlHO0lBQ0gsZ0JBQVksV0FBa0IsRUFBRSxZQUFvQjtRQUFwRCxZQUNJLGlCQUFPLFNBR1Y7UUFGRyxLQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQztRQUNoQyxLQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQzs7SUFDdEMsQ0FBQztJQW5DRDs7OztPQUlHO0lBQ1csZUFBUSxHQUF0QixVQUF1QixPQUFZO1FBRS9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNYLE1BQU0sSUFBSSxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQztRQUMxRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0MsTUFBTSxJQUFJLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO1FBQ3RELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQyxNQUFNLElBQUksS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7UUFDdEQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLE1BQU0sSUFBSSxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUNwRCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEtBQUssbUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzNDLE1BQU0sSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUVELElBQUksV0FBVyxHQUFHLGVBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pELElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFFNUIsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUUzQyxDQUFDO0lBYUQ7O09BRUc7SUFDSSxrQ0FBaUIsR0FBeEI7UUFDSSxJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7UUFDZCxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUMxQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDM0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUMzQixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNyQixJQUFJLEtBQUssR0FBRyxJQUFJLGVBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxZQUFZLEVBQ2hHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLElBQUksTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ25ELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3RCLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksc0JBQUssR0FBWjtRQUNJLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRUQ7O09BRUc7SUFDSSwwQkFBUyxHQUFoQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFFRDs7T0FFRztJQUNJLDRCQUFXLEdBQWxCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQztJQUVEOztPQUVHO0lBQ0ksMEJBQVMsR0FBaEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksb0NBQW1CLEdBQTFCLFVBQTJCLEtBQVk7UUFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDbEIsQ0FBQztJQUtELHNCQUFJLHdCQUFJO1FBSFI7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxtQkFBUyxDQUFDLE1BQU0sQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQUtELHNCQUFJLHFCQUFDO1FBSEw7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUMvQixDQUFDO1FBZ0JEOztXQUVHO2FBQ0gsVUFBTSxLQUFhO1lBQ2YsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDakMsQ0FBQztRQUNMLENBQUM7OztPQXhCQTtJQUtELHNCQUFJLHFCQUFDO1FBSEw7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUMvQixDQUFDO1FBbUJEOztXQUVHO2FBQ0gsVUFBTSxLQUFhO1lBQ2YsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDakMsQ0FBQztRQUNMLENBQUM7OztPQTNCQTtJQUtELHNCQUFJLHFCQUFDO1FBSEw7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUMvQixDQUFDO1FBc0JEOztXQUVHO2FBQ0gsVUFBTSxLQUFhO1lBQ2YsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDakMsQ0FBQztRQUNMLENBQUM7OztPQTlCQTtJQWdDRDs7T0FFRztJQUNJLDBCQUFTLEdBQWhCLFVBQWlCLE1BQWE7UUFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUM7UUFDM0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDcEMsQ0FBQztJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNJLDBCQUFTLEdBQWhCLFVBQWlCLE9BQWU7UUFDNUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7SUFDakMsQ0FBQztJQU1ELHNCQUFJLGdDQUFZO1FBSmhCOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDOUIsQ0FBQztRQUVEOzs7V0FHRzthQUNILFVBQWlCLEtBQVU7WUFDdkIsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNULE1BQU0sSUFBSSxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDckMsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdGLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQy9DLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixNQUFNLElBQUksS0FBSyxDQUFDLGlEQUFpRCxDQUFDLENBQUM7WUFDdkUsQ0FBQztRQUNMLENBQUM7OztPQWpCQTtJQW1CRDs7T0FFRztJQUNJLHVCQUFNLEdBQWIsVUFBYyxVQUFrQjtRQUM1QixFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUMvQyxVQUFVLENBQUMsU0FBUyxFQUFFLEtBQUssSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM5QyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRDs7T0FFRztJQUNJLHVCQUFNLEdBQWI7UUFDSSxNQUFNLENBQUM7WUFDSCxRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDcEMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhO1lBQzVCLE1BQU0sRUFBRSxtQkFBUyxDQUFDLE1BQU07U0FDM0IsQ0FBQztJQUNOLENBQUM7SUFFTCxhQUFDO0FBQUQsQ0E5TkEsQUE4TkMsQ0E5TjJCLGVBQUssR0E4TmhDO0FBOU5ZLHdCQUFNO0FBZ09uQixrQkFBZSxNQUFNLENBQUMiLCJmaWxlIjoibWF0aC9jaXJjbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUG9pbnQgZnJvbSBcIi4vcG9pbnRcIjtcclxuaW1wb3J0IFNoYXBlIGZyb20gXCIuL3NoYXBlXCI7XHJcbmltcG9ydCBDb25zdGFudHMgZnJvbSBcIi4vY29uc3RhbnRzXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2lyY2xlIGV4dGVuZHMgU2hhcGUge1xyXG5cclxuICAgIHByaXZhdGUgX2NlbnRlclBvaW50OiBQb2ludDtcclxuICAgIHByaXZhdGUgX2NpcmNsZVJhZGl1czogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfbGlua1Bvc2l0aW9uOiBhbnk7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IEpTT04gdG8gbWF0aC5DaXJjbGUgb2JqZWN0LlxyXG4gICAgICogQHBhcmFtIGpzb25PYmpcclxuICAgICAqIEByZXR1cm5zIHttYXRoLkNpcmNsZX1cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqOiBhbnkpIHtcclxuXHJcbiAgICAgICAgaWYgKCFqc29uT2JqKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbm5vdCBjb252ZXJ0IHVuZGVmaW5lZCB0byBDaXJjbGVcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICghanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcImNlbnRlclwiKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDZW50ZXIgcHJvcGVydHkgZG9lcyBub3QgZXhpc3RcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICghanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcInJhZGl1c1wiKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJSYWRpdXMgcHJvcGVydHkgZG9lcyBub3QgZXhpc3RcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmICghanNvbk9iai5oYXNPd25Qcm9wZXJ0eShcInR5cGVcIikpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiVHlwZSBwcm9wZXJ0eSBkb2VzIG5vdCBleGlzdFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGpzb25PYmoudHlwZSAhPT0gQ29uc3RhbnRzLkNJUkNMRSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIHR5cGVcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgY2VudGVyUG9pbnQgPSBQb2ludC5mcm9tSlNPTihqc29uT2JqLmNlbnRlcik7XHJcbiAgICAgICAgbGV0IHJhZGl1cyA9IGpzb25PYmoucmFkaXVzO1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IENpcmNsZShjZW50ZXJQb2ludCwgcmFkaXVzKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zcnVjdCBhIGNpcmNsZSBvYmplY3QuXHJcbiAgICAgKiBAcGFyYW0gY2VudGVyUG9pbnRcclxuICAgICAqIEBwYXJhbSBjaXJjbGVSYWRpdXNcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoY2VudGVyUG9pbnQ6IFBvaW50LCBjaXJjbGVSYWRpdXM6IG51bWJlcikge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5fY2VudGVyUG9pbnQgPSBjZW50ZXJQb2ludDtcclxuICAgICAgICB0aGlzLl9jaXJjbGVSYWRpdXMgPSBjaXJjbGVSYWRpdXM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYWRqYWNlbnQgc2hhcGVzLiBJbmNsdWRlIGRpYWdvbmFscy4gVGhpcyBpcyB1c2VkIGZvciBwYXRoLWZpbmRpbmcuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRBZGphY2VudFNoYXBlcygpOiBTaGFwZVtdIHtcclxuICAgICAgICBsZXQgbGlzdCA9IFtdO1xyXG4gICAgICAgIGxldCBkb3VibGVSYWRpdXMgPSB0aGlzLl9jaXJjbGVSYWRpdXMgKiAyO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAtMTsgaSA8PSAxOyBpKyspIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgaiA9IC0xOyBqIDw9IDE7IGorKykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGkgIT09IDAgfHwgaiAhPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBwb2ludCA9IG5ldyBQb2ludCh0aGlzLl9jZW50ZXJQb2ludC54ICsgaSAqIGRvdWJsZVJhZGl1cywgdGhpcy5fY2VudGVyUG9pbnQueSArIGogKiBkb3VibGVSYWRpdXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX2NlbnRlclBvaW50LnopO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBjaXJjbGUgPSBuZXcgQ2lyY2xlKHBvaW50LCB0aGlzLl9jaXJjbGVSYWRpdXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxpc3QucHVzaChjaXJjbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBsaXN0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2xvbmUgdGhpcyBjaXJjbGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBjbG9uZSgpOiBTaGFwZSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBDaXJjbGUodGhpcy5fY2VudGVyUG9pbnQsIHRoaXMuX2NpcmNsZVJhZGl1cyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIGNlbnRlciBvZiB0aGlzIGNpcmNsZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldENlbnRlcigpOiBQb2ludCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGEgcG9pbnQgcmVsYXRlZCB0byB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRQb3NpdGlvbigpOiBQb2ludCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSByYWRpdXMgb2YgdGhpcyBjaXJjbGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRSYWRpdXMoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY2lyY2xlUmFkaXVzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBaIHZhbHVlIHJlbGF0aXZlIHRvIGEgZ2l2ZW4gcG9pbnQuIEluIHRoZSBjYXNlIG9mIGEgY2lyY2xlXHJcbiAgICAgKiB0aGUgeiBpcyBhbHdheXMgdGhlIGNpcmNsZSdzIHogdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRaUmVsYXRpdmVUb1BvaW50KHBvaW50OiBQb2ludCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuejtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgdHlwZSBvZiB0aGlzIHBvbHlnb24uXHJcbiAgICAgKi9cclxuICAgIGdldCB0eXBlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIENvbnN0YW50cy5DSVJDTEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFggY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIGdldCB4KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50Lng7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFkgY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIGdldCB5KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50Lnk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFogY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIGdldCB6KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NlbnRlclBvaW50Lno7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIFggY29vcmRpbmF0ZSBvZiB0aGUgY2VudGVyIG9mIHRoaXMgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIHNldCB4KHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9jZW50ZXJQb2ludC54ID0gdmFsdWU7XHJcbiAgICAgICAgaWYgKHRoaXMuX2xpbmtQb3NpdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLl9saW5rUG9zaXRpb24ueCA9IHZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCB0aGUgWSBjb29yZGluYXRlIG9mIHRoZSBjZW50ZXIgb2YgdGhpcyBzaGFwZS5cclxuICAgICAqL1xyXG4gICAgc2V0IHkodmFsdWU6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2NlbnRlclBvaW50LnkgPSB2YWx1ZTtcclxuICAgICAgICBpZiAodGhpcy5fbGlua1Bvc2l0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi55ID0gdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSBaIGNvb3JkaW5hdGUgb2YgdGhlIGNlbnRlciBvZiB0aGlzIHNoYXBlLlxyXG4gICAgICovXHJcbiAgICBzZXQgeih2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fY2VudGVyUG9pbnQueiA9IHZhbHVlO1xyXG4gICAgICAgIGlmICh0aGlzLl9saW5rUG9zaXRpb24pIHtcclxuICAgICAgICAgICAgdGhpcy5fbGlua1Bvc2l0aW9uLnogPSB2YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIGNlbnRlciBvZiB0aGUgY2lyY2xlLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc2V0Q2VudGVyKGFQb2ludDogUG9pbnQpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9jZW50ZXJQb2ludCA9IGFQb2ludDtcclxuICAgICAgICBpZiAodGhpcy5fbGlua1Bvc2l0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi54ID0gYVBvaW50Lng7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi55ID0gYVBvaW50Lnk7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi56ID0gYVBvaW50Lno7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSByYWRpdXMgb2YgdGhlIGNpcmNsZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldFJhZGl1cyhhUmFkaXVzOiBudW1iZXIpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9jaXJjbGVSYWRpdXMgPSBhUmFkaXVzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTGluayB0aGUgeCwgeSwgeiBwb3NpdGlvbiBvZiB0aGUgZ2l2ZW4gb2JqZWN0IHRvIHRoaXMgc2hhcGUncyBwb3NpdGlvbi4gRXZlcnkgdGltZSB0aGUgeCwgeSBvciB6IHZhbHVlIG9mIHRoaXNcclxuICAgICAqIHNoYXBlIGNoYW5nZXMgdGhlIG9iamVjdCBvZiB0aGlzIGxpbmsgcG9zaXRpb24gYWxzbyBjaGFuZ2VzLlxyXG4gICAgICovXHJcbiAgICBnZXQgbGlua1Bvc2l0aW9uKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xpbmtQb3NpdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExpbmsgdGhlIHgsIHksIHogcG9zaXRpb24gb2YgdGhlIGdpdmVuIG9iamVjdCB0byB0aGlzIHNoYXBlJ3MgcG9zaXRpb24uIEV2ZXJ5IHRpbWUgdGhlIHgsIHkgb3IgeiB2YWx1ZSBvZiB0aGlzXHJcbiAgICAgKiBzaGFwZSBjaGFuZ2VzIHRoZSBvYmplY3Qgb2YgdGhpcyBsaW5rIHBvc2l0aW9uIGFsc28gY2hhbmdlcy5cclxuICAgICAqL1xyXG4gICAgc2V0IGxpbmtQb3NpdGlvbih2YWx1ZTogYW55KSB7XHJcbiAgICAgICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJWYWx1ZSBpcyBudWxsXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodmFsdWUuaGFzT3duUHJvcGVydHkoXCJ4XCIpICYmIHZhbHVlLmhhc093blByb3BlcnR5KFwieVwiKSAmJiB2YWx1ZS5oYXNPd25Qcm9wZXJ0eShcInpcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5fbGlua1Bvc2l0aW9uID0gdmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMuX2xpbmtQb3NpdGlvbi54ID0gdGhpcy5fY2VudGVyUG9pbnQueDtcclxuICAgICAgICAgICAgdGhpcy5fbGlua1Bvc2l0aW9uLnkgPSB0aGlzLl9jZW50ZXJQb2ludC55O1xyXG4gICAgICAgICAgICB0aGlzLl9saW5rUG9zaXRpb24ueiA9IHRoaXMuX2NlbnRlclBvaW50Lno7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiVmFsdWUgZG9lcyBub3QgaGF2ZSBhbGwgdGhlIHJlcXVpcmVkIHByb3BlcnRpZXNcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGV0ZXJtaW5lIGlmIGEgY2lyY2xlIGVxdWFscyB0aGlzIGNpcmNsZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGVxdWFscyhhQ2lyY2xlT2JqOiBDaXJjbGUpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoYUNpcmNsZU9iai5nZXRDZW50ZXIoKS5lcXVhbHModGhpcy5nZXRDZW50ZXIoKSkgJiZcclxuICAgICAgICAgICAgYUNpcmNsZU9iai5nZXRSYWRpdXMoKSA9PT0gdGhpcy5nZXRSYWRpdXMoKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB0aGUgY2lyY2xlIHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogYW55IHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBcImNlbnRlclwiOiB0aGlzLl9jZW50ZXJQb2ludC50b0pTT04oKSxcclxuICAgICAgICAgICAgXCJyYWRpdXNcIjogdGhpcy5fY2lyY2xlUmFkaXVzLFxyXG4gICAgICAgICAgICBcInR5cGVcIjogQ29uc3RhbnRzLkNJUkNMRVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBDaXJjbGU7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
