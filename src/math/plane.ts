import Point from "./point";
import Vector from "./vector";

export class Plane {

    private _a: number;
    private _b: number;
    private _c: number;
    private _d: number;

    public static from3Points(point1: Point, point2: Point, point3: Point): Plane {

        let v1: Vector = Vector.subtract(point1, point2);
        let v2: Vector = Vector.subtract(point3, point2);
        let crossVector: Vector = Vector.cross(v1, v2);

        let a: number = crossVector.x;
        let b: number = crossVector.y;
        let c: number = crossVector.z;
        let d: number = a * point1.x + b * point1.y + c * point1.z;
        return new Plane(a, b, c, d);

    }

    constructor(a: number, b: number, c: number, d: number) {
        this._a = a;
        this._b = b;
        this._c = c;
        this._d = d;
    }

    public getZ(x: number, y: number) {
        return (this._d - this._a * x - this._b * y) / this._c;
    }

}

export default Plane;
