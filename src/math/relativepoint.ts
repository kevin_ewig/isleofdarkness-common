import Point from "./point";

export class RelativePoint extends Point {

    /**
     * Type of this point.
     */
    public static TYPE = "relativepoint";

    private _nX: number;
    private _nY: number;
    private _nZ: number;
    private _relative: Point;

    /**
     * Convert a JSON object to a RelativePoint.
     * @param jsonObj
     * @returns {math.Point}
     */
    public static fromJSON( jsonObj: any ): RelativePoint  {

        if ( !jsonObj ) {
            throw new Error("Cannot convert undefined");
        } else if ( !jsonObj.hasOwnProperty("x") ) {
            throw new Error("X property does not exist");
        } else if ( !jsonObj.hasOwnProperty("y") ) {
            throw new Error("Y property does not exist");
        } else if ( !jsonObj.hasOwnProperty("z") ) {
            throw new Error("Z property does not exist");
        } else if ( !jsonObj.hasOwnProperty("type") ) {
            throw new Error("Type property does not exist");
        } else if ( jsonObj.type !==  RelativePoint.TYPE ) {
            throw new Error("Invalid type");
        }

        let tempPoint = new RelativePoint(jsonObj.x, jsonObj.y, jsonObj.z);
        return tempPoint;

    }

    constructor(pointX: number, pointY: number, pointZ: number ) {
        let tempX = pointX === undefined ? 0 : pointX;
        let tempY = pointY === undefined ? 0 : pointY;
        let tempZ = pointZ === undefined ? 0 : pointZ;

        super( tempX, tempY, tempZ );
        this._nX = tempX;
        this._nY = tempY;
        this._nZ = tempZ;
        this._relative = new Point(0, 0, 0);
    }

    /**
     * Clone this point. The cloned point will be relative to the same point object.s
     */
    public clone(): RelativePoint {
        let clonePoint = new RelativePoint(this._nX, this._nY, this._nZ);
        clonePoint.setRelativePoint(this._relative);
        return clonePoint;
    }

    /**
     * This is the point that this relative point is relative to.
     * @param point
     */
    public setRelativePoint(point: Point) {
        this._relative = point;
    }

    /**
     * Get the X value.
     * @returns {number}
     */
    get x() { return this._nX + this._relative.x; }

    /**
     * Set the X value.
     */
    set x(value: number) {
        this._nX = value - this._relative.x;
    }

    /**
     * Get the Y value.
     * @returns {number}
     */
    get y() { return this._nY + this._relative.y; }

    /**
     * Set the Y value.
     */
    set y(value: number) {
        this._nY = value - this._relative.y;
    }

    /**
     * Get the Z value.
     * @returns {number}
     */
    get z() { return this._nZ + this._relative.z; }

    /**
     * Set the Z value.
     */
    set z(value: number) {
        this._nZ = value - this._relative.z;
    }

    /**
     * Convert this point to JSON.
     */
    public toJSON() {
        return {
            "x": this.x,
            "y": this.y,
            "z": this.z,
            "type" : RelativePoint.TYPE
        };
    }

}

export default RelativePoint;
