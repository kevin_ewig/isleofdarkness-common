import Point from "./point";
import Shape from "./shape";
import RelativePoint from "./relativepoint";
import Plane from "./plane";
import Constants from "./constants";

export class Polygon implements Shape {

    private _arrayOfPoints: RelativePoint[];
    private _centerPoint: Point;
    private _plane: Plane;
    private _linkPosition: any;

    /**
     * Takes in a Tiled object (Polyline) and converts it into a Polygon.
     *
     * @param startPoint The starting x,y point of the polyline.
     * @param arrayOfPoints An array of math.Point objects.
     * @param angle The angle in degrees
     * @param heights (Optional) An array of numbers which represents the height of each point in the array.
     */
    public static fromTiled( startPoint: Point, arrayOfPoints: Point[], angle: number, heights?: number[] ) {

        let radian: number = angle * Math.PI / 180;
        let polygonArray: RelativePoint[] = [];
        let firstPoint: Point = arrayOfPoints[0];
        let heightOfPoints = heights;

        if ( heightOfPoints ) {
            if ( heightOfPoints.length !== arrayOfPoints.length ) {
                throw new Error("Number of points in the height does not match the number of points in the array");
            }
        } else {
            heightOfPoints = new Array(arrayOfPoints.length);
            for ( let i = 0; i < heightOfPoints.length; i++ ) {
                heightOfPoints[i] = 0;
            }
        }

        for ( let i = 0; i < arrayOfPoints.length; i++ ) {
            let point = arrayOfPoints[i];
            let tempX = firstPoint.x + (point.x - firstPoint.x) * Math.cos(radian) - (point.y - firstPoint.y) * Math.sin(radian);
            let tempY = firstPoint.y + (point.x - firstPoint.x) * Math.sin(radian) + (point.y - firstPoint.y) * Math.cos(radian);
            let tempZ = heightOfPoints[i];
            let point2 = new RelativePoint(tempX + startPoint.x, tempY + startPoint.y, tempZ);
            polygonArray.push(point2);
        }

        let brandNewPolygon = new Polygon( polygonArray );
        return brandNewPolygon;
    }

    /**
     * Convert JSON object to point.
     * @param jsonObj
     * @returns {math.Polygon}
     */
    public static fromJSON(jsonObj: any): Polygon {

        if ( !jsonObj ) {
            throw new Error("Cannot convert undefined to polygon");
        }
        if ( !jsonObj.hasOwnProperty("arrayOfPoints") ) {
            throw new Error("No arrayOfPoints property");
        }
        if ( jsonObj.type !== Constants.POLYGON ) {
            throw new Error("Incorrect type");
        }

        let listOfPoints: RelativePoint[] = [];
        for ( let i = 0; i < jsonObj.arrayOfPoints.length; i++ ) {
            let pointJson = jsonObj.arrayOfPoints[i];
            let point = RelativePoint.fromJSON(pointJson);
            listOfPoints.push(point);
        }

        return new Polygon(listOfPoints);

    }

    constructor(arrayOfPoints: Point[]) {

        // Polygon must have 3 or more points.
        if ( arrayOfPoints.length < 3 ) {
            throw new Error("Polygon must have 3 or more points");
        }

        // Create an array of relative points.
        this._arrayOfPoints = new Array<RelativePoint>();

        // Compute the min and max x and y values.
        let minXPoint = arrayOfPoints[0]; let maxXPoint = arrayOfPoints[0];
        let minYPoint = arrayOfPoints[0]; let maxYPoint = arrayOfPoints[0];
        for ( let i = 0; i < arrayOfPoints.length; i++ ) {
            let point: Point = arrayOfPoints[i] as Point;
            if ( point.x > maxXPoint.x ) {
                maxXPoint = point;
            }
            if ( point.x < minXPoint.x ) {
                minXPoint = point;
            }
            if ( point.y > maxYPoint.y ) {
                maxYPoint = point;
            }
            if ( point.y < minYPoint.y ) {
                minYPoint = point;
            }
        }

        // Compute the center point.
        let averageX = (maxXPoint.x + minXPoint.x) / 2;
        let averageY = (maxYPoint.y + minYPoint.y) / 2;
        this._centerPoint = new Point(averageX, averageY, 0);

        for ( let i = 0; i < arrayOfPoints.length; i++ ) {
            let tempPoint = arrayOfPoints[i];
            let x = tempPoint.x ? tempPoint.x : 0;
            let y = tempPoint.y ? tempPoint.y : 0;
            let z = tempPoint.z ? tempPoint.z : 0;
            let tempX = x - this._centerPoint.x;
            let tempY = y - this._centerPoint.y;
            let tempZ = z - this._centerPoint.z;
            let tempRelativePoint = new RelativePoint( tempX, tempY, tempZ );
            tempRelativePoint.setRelativePoint(this._centerPoint);
            this._arrayOfPoints.push(tempRelativePoint);
        }

        // Compute the plane.
        this._plane = Plane.from3Points(this._arrayOfPoints[0], this._arrayOfPoints[1], this._arrayOfPoints[2]);

    }

    /**
     * Clone this Polygon.
     */
    public clone(): Polygon {

        let tempArrayOfPoints = new Array<Point>();

        // Convert the relative points back to regular points.
        for ( let i = 0; i < this._arrayOfPoints.length; i++ ) {
            let tempRelativePoint = this._arrayOfPoints[i];
            let tempPoint = new Point( tempRelativePoint.x, tempRelativePoint.y, tempRelativePoint.z );
            tempArrayOfPoints.push(tempPoint);
        }

        return new Polygon(tempArrayOfPoints);

    }

    /**
     * Test if the given polygon is equal to this one.
     */
    public equals( otherObj: Polygon ) {

        if ( !otherObj ) {
            return false;
        }

        let otherArrayOfPoints = otherObj.getArrayOfPoints();
        if ( otherArrayOfPoints.length !== this._arrayOfPoints.length ) {
            return false;
        }

        for ( let i = 0; i < otherArrayOfPoints.length; i++ ) {
            if ( !otherArrayOfPoints[i].equals( this._arrayOfPoints[i] ) ) {
                return false;
            }
        }

        return true;

    }

    /**
     * Get adjacent shapes. Include diagonals. This is used for path-finding.
     */
    public getAdjacentShapes(): Shape[] {

        let list: Shape[] = [];

        let arrayOfPoints = this.getArrayOfPoints();
        let rangeX = this.getRangeX();
        let rangeY = this.getRangeY();

        for ( let i = -1; i <= 1; i++ ) {
            for ( let j = -1; j <= 1; j++ ) {
                if ( i !== 0 || j !== 0 ) {

                    let listOfPoints: RelativePoint[] = [];
                    for ( let n = 0; n < arrayOfPoints.length; n++ ) {
                        let p = arrayOfPoints[n];
                        let cloneOfP = p.clone();
                        cloneOfP.x = cloneOfP.x + i * rangeX;
                        cloneOfP.y = cloneOfP.y + j * rangeY;
                        listOfPoints.push( cloneOfP );
                    }

                    let poly = new Polygon(listOfPoints);
                    let shape: Shape = poly as Shape;
                    list.push(shape);

                }
            }
        }

        return list;

    }

    /**
     * Get an array of points that make up this polygon.
     */
    public getArrayOfPoints(): RelativePoint[] {
        return this._arrayOfPoints;
    }

    /**
     * Get a point related to the center of this shape.
     */
    public getPosition(): Point {
        return new Point(this.x, this.y, this.z);
    }

    /*
     * Get the Z value relative to a given point. This is used in case
     * the rectangle represents a staircase and you need to get Z value
     * for a given X, Y.
     */
    public getZRelativeToPoint(point: Point): number {
        return this._plane.getZ(point.x, point.y);
    }

    /**
     * Get the type of this polygon.
     */
    get type(): string {
        return Constants.POLYGON;
    }

    /**
     * Get the X coordinate of the center of this shape.
     */
    get x(): number {
        return this._centerPoint.x;
    }

    /**
     * Set the X coordinate of the center of this shape.
     */
    set x(value: number) {
        this._centerPoint.x = value;
        if ( this._linkPosition ) {
            this._linkPosition.x = value;
        }
    }

    /**
     * Get the Y coordinate of the center of this shape.
     */
    get y(): number {
        return this._centerPoint.y;
    }

    /**
     * Set the Y coordinate of the center of this shape.
     */
    set y(value: number) {
        this._centerPoint.y = value;
        if ( this._linkPosition ) {
            this._linkPosition.y = value;
        }
    }

    /**
     * Get the Z coordinate of the center of this shape.
     */
    get z(): number {
        return this._centerPoint.z;
    }

    /**
     * Set the Z coordinate of the center of this shape.
     */
    set z(value: number) {
        this._centerPoint.z = value;
        if ( this._linkPosition ) {
            this._linkPosition.z = value;
        }
    }

    /**
     * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
     * shape changes the object of this link position also changes.
     */
    get linkPosition(): any {
        return this._linkPosition;
    }

    /**
     * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
     * shape changes the object of this link position also changes.
     */
    set linkPosition(value: any) {
        if ( !value ) {
            throw new Error("Value is null");
        } else if ( value.hasOwnProperty("x") && value.hasOwnProperty("y") && value.hasOwnProperty("z") ) {
            this._linkPosition = value;
            this._linkPosition.x = this._centerPoint.x;
            this._linkPosition.y = this._centerPoint.y;
            this._linkPosition.z = this._centerPoint.z;
        } else {
            throw new Error("Value does not have all the required properties");
        }
    }

    /**
     * Convert this polygon to JSON.
     * @returns {{arrayOfPoints: Array}}
     */
    public toJSON(): any {
        let arrayOfPoints = this.getArrayOfPoints();
        let arr = [];
        for ( let i = 0; i < arrayOfPoints.length; i++ ) {
            arr.push( this._arrayOfPoints[i].toJSON() );
        }
        return {
            "arrayOfPoints": arr,
            "type": Constants.POLYGON
        };
    }

    public toString(): string {
        let arrayOfPoints = this.getArrayOfPoints();
        let output = "";
        for ( let i = 0; i < arrayOfPoints.length; i++ ) {
            output += this._arrayOfPoints[i].toString() + ";";
        }
        return "[" + output + "]";
    }

    /**
     * Get the range (max-min) in X axis.
     */
    private getRangeX(): number {
        let arrayOfPoints = this.getArrayOfPoints();
        let min = arrayOfPoints[0].x;
        let max = min;
        for ( let i = 0; i < this._arrayOfPoints.length; i++ ) {
            let x = arrayOfPoints[i].x;
            if ( x > max ) {
                max = x;
            }
            if ( x < min ) {
                min = x;
            }
        }
        return max - min;
    }

    /**
     * Get the range (max-min) in Y axis.
     */
    private getRangeY(): number {
        let arrayOfPoints = this.getArrayOfPoints();
        let min = arrayOfPoints[0].y;
        let max = min;
        for ( let i = 0; i < this._arrayOfPoints.length; i++ ) {
            let y = this._arrayOfPoints[i].y;
            if ( y > max ) {
                max = y;
            }
            if ( y < min ) {
                min = y;
            }
        }
        return max - min;
    }

}

export default Polygon;
