import Polygon from "./polygon";
import Circle from "./circle";
import Constants from "./constants";
import Shape from "./shape";
import Rectangle from "./rectangle";

export class Common {

    /**
     * Gets the distance between two points.
     */
    public static distance2d = function(aX1: number, aY1: number, aX2: number, aY2: number) {
        return Math.sqrt( (aX2 - aX1) * (aX2 - aX1) + (aY2 - aY1) * (aY2 - aY1) );
    };


    /**
     * Convert a JSON object to a Shape.
     * @param jsonObject
     */
    public static convertShape(jsonObject: any): Shape {
        if (!jsonObject) {
            throw new Error("Cannot convert undefined to Shape");
        }
        if (!jsonObject.hasOwnProperty("type")) {
            throw new Error("Cannot convert. No type.");
        }
        if (jsonObject.type === Constants.POLYGON) {
            return Polygon.fromJSON(jsonObject);
        } else if (jsonObject.type === Constants.CIRCLE) {
            return Circle.fromJSON(jsonObject);
        } else if (jsonObject.type === Constants.RECTANGLE) {
            return Rectangle.fromJSON(jsonObject);
        } else {
            throw new Error("Cannot convert. Shape has incorrect type.");
        }
    }

}
