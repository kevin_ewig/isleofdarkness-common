import {Common} from "./common";
import {Point} from "./point";

export abstract class Shape {

    /**
     * Clone this shape.
     */
    public abstract clone(): Shape;

    /**
     * Determine if this shape equals the other shape.
     * @param shape
     */
    public abstract equals(shape: Shape): boolean;

    /**
     * Get adjacent shapes. Include diagonals. This is used for path-finding.
     */
    public abstract getAdjacentShapes(): Shape[];

    /**
     * Get the Z value relative to a given point.
     */
    public abstract getZRelativeToPoint(point: Point): number;

    /**
     * Get the X coordinate of the center of this shape.
     */
    abstract get x(): number;

    /**
     * Get the Y coordinate of the center of this shape.
     */
    abstract get y(): number;

    /**
     * Get the Z coordinate of the center of this shape.
     */
    abstract get z(): number;

    /**
     * Set the X coordinate of the center of this shape.
     */
    abstract set x(value: number);

    /**
     * Set the Y coordinate of the center of this shape.
     */
    abstract set y(value: number);

    /**
     * Set the Z coordinate of the center of this shape.
     */
    abstract set z(value: number);

    /**
     * Get a point related to the center of this shape.
     */
    public abstract getPosition(): Point;

    /**
     * Get the type of this shape.
     */
    abstract get type(): string;

    /**
     * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
     * shape changes the object of this link position also changes.
     */
    abstract get linkPosition(): any;

    /**
     * Link the x, y, z position of the given object to this shape's position. Every time the x, y or z value of this
     * shape changes the object of this link position also changes.
     */
    abstract set linkPosition(value: any);

    /**
     * Convert this shape to JSON.
     */
    public abstract toJSON(): string;

}

export default Shape;
