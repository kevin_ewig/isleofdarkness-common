import User from "../../model/user";
import Ticket from "../../model/ticket";
import Response from "./response";
import UserMetaData from "../../model/usermetadata";
import AuthenticatedResponse from "./authenticatedresponse";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";
import { InvalidJSONError } from "../../error/invalidjsonerror";

export class RegisterUserResponse extends AuthenticatedResponse {

    public static TYPE = "registeruserresponse";

    private _result: boolean;

    public static fromJSON(jsonObject: any): RegisterUserResponse {
        if ( jsonObject.type !== RegisterUserResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let result: boolean = jsonObject.result;
            let response = new RegisterUserResponse(userMetaData, ticket, result);
            Response.populateJSON(response, jsonObject);
            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, result: boolean) {
        super(userMetaData, ticket);
        this._result = result;
    }

    /**
     * Get result of the registration.
     * @param user
     */
    get result(): boolean {
        return this._result;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return RegisterUserResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return false;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.type = RegisterUserResponse.TYPE;
        result.result = this._result;
        return result;
    }

}

export default RegisterUserResponse;
