"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ticket_1 = require("../../model/ticket");
var response_1 = require("./response");
var usermetadata_1 = require("../../model/usermetadata");
var authenticatedresponse_1 = require("./authenticatedresponse");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var OpenConnectionResponse = /** @class */ (function (_super) {
    __extends(OpenConnectionResponse, _super);
    function OpenConnectionResponse(userMetaData, ticket, result) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        _this._result = result;
        return _this;
    }
    OpenConnectionResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== OpenConnectionResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.InvalidJSONForResponseError();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var response = new OpenConnectionResponse(userMetaData, ticket, jsonObject.result === true);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.InvalidJSONError();
        }
    };
    Object.defineProperty(OpenConnectionResponse.prototype, "result", {
        /**
         * Return the result.
         */
        get: function () {
            return this._result;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OpenConnectionResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return OpenConnectionResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    OpenConnectionResponse.prototype.isBroadcast = function () {
        return false;
    };
    /**
     * Convert this response object to JSON.
     */
    OpenConnectionResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.type = OpenConnectionResponse.TYPE;
        result.result = this._result;
        return result;
    };
    OpenConnectionResponse.TYPE = "openconnectionresponse";
    return OpenConnectionResponse;
}(authenticatedresponse_1.default));
exports.OpenConnectionResponse = OpenConnectionResponse;
exports.default = OpenConnectionResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL29wZW5jb25uZWN0aW9ucmVzcG9uc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQ0EsNkNBQXdDO0FBQ3hDLHVDQUFrQztBQUNsQyx5REFBb0Q7QUFDcEQsaUVBQTREO0FBQzVELHVGQUFzRjtBQUN0RixpRUFBZ0U7QUFFaEU7SUFBNEMsMENBQXFCO0lBc0I3RCxnQ0FBWSxZQUEwQixFQUFFLE1BQWMsRUFBRSxNQUFlO1FBQXZFLFlBQ0ksa0JBQU0sWUFBWSxFQUFFLE1BQU0sQ0FBQyxTQUU5QjtRQURHLEtBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDOztJQUMxQixDQUFDO0lBbkJhLCtCQUFRLEdBQXRCLFVBQXVCLFVBQWU7UUFDbEMsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLElBQUksS0FBSyxzQkFBc0IsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3BELE1BQU0sSUFBSSx5REFBMkIsRUFBRSxDQUFDO1FBQzVDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLFlBQVksSUFBSSxVQUFVLENBQUMsTUFBTyxDQUFDLENBQUMsQ0FBQztZQUV4RCxJQUFJLFlBQVksR0FBRyxzQkFBWSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEUsSUFBSSxNQUFNLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELElBQUksUUFBUSxHQUFHLElBQUksc0JBQXNCLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxVQUFVLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBRSxDQUFDO1lBQzdGLGtCQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUM1QyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBRXBCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxtQ0FBZ0IsRUFBRSxDQUFDO1FBQ2pDLENBQUM7SUFDTCxDQUFDO0lBVUQsc0JBQUksMENBQU07UUFIVjs7V0FFRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDeEIsQ0FBQzs7O09BQUE7SUFNRCxzQkFBSSx3Q0FBSTtRQUpSOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQztRQUN2QyxDQUFDOzs7T0FBQTtJQUVEOzs7T0FHRztJQUNJLDRDQUFXLEdBQWxCO1FBQ0ksTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQ7O09BRUc7SUFDSSx1Q0FBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ3JELE1BQU0sQ0FBQyxJQUFJLEdBQUcsc0JBQXNCLENBQUMsSUFBSSxDQUFDO1FBQzFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM3QixNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUF4RGEsMkJBQUksR0FBRyx3QkFBd0IsQ0FBQztJQTBEbEQsNkJBQUM7Q0E1REQsQUE0REMsQ0E1RDJDLCtCQUFxQixHQTREaEU7QUE1RFksd0RBQXNCO0FBOERuQyxrQkFBZSxzQkFBc0IsQ0FBQyIsImZpbGUiOiJwcm90b2NvbC9yZXNwb25zZS9vcGVuY29ubmVjdGlvbnJlc3BvbnNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVzZXIgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJcIjtcclxuaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZVwiO1xyXG5pbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuLi8uLi9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IEF1dGhlbnRpY2F0ZWRSZXNwb25zZSBmcm9tIFwiLi9hdXRoZW50aWNhdGVkcmVzcG9uc2VcIjtcclxuaW1wb3J0IHsgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yIH0gZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVzcG9uc2VlcnJvclwiO1xyXG5pbXBvcnQgeyBJbnZhbGlkSlNPTkVycm9yIH0gZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZXJyb3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBPcGVuQ29ubmVjdGlvblJlc3BvbnNlIGV4dGVuZHMgQXV0aGVudGljYXRlZFJlc3BvbnNlIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcIm9wZW5jb25uZWN0aW9ucmVzcG9uc2VcIjtcclxuXHJcbiAgICBwcml2YXRlIF9yZXN1bHQ6IGJvb2xlYW47XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqZWN0OiBhbnkpOiBPcGVuQ29ubmVjdGlvblJlc3BvbnNlIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gT3BlbkNvbm5lY3Rpb25SZXNwb25zZS5UWVBFICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdC51c2VyTWV0YURhdGEgJiYganNvbk9iamVjdC50aWNrZXQgKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgdXNlck1ldGFEYXRhID0gVXNlck1ldGFEYXRhLmZyb21KU09OKGpzb25PYmplY3QudXNlck1ldGFEYXRhKTtcclxuICAgICAgICAgICAgbGV0IHRpY2tldCA9IFRpY2tldC5mcm9tSlNPTihqc29uT2JqZWN0LnRpY2tldCk7XHJcbiAgICAgICAgICAgIGxldCByZXNwb25zZSA9IG5ldyBPcGVuQ29ubmVjdGlvblJlc3BvbnNlKHVzZXJNZXRhRGF0YSwgdGlja2V0LCBqc29uT2JqZWN0LnJlc3VsdCA9PT0gdHJ1ZSApO1xyXG4gICAgICAgICAgICBSZXNwb25zZS5wb3B1bGF0ZUpTT04ocmVzcG9uc2UsIGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgcmVzdWx0OiBib29sZWFuKSB7XHJcbiAgICAgICAgc3VwZXIodXNlck1ldGFEYXRhLCB0aWNrZXQpO1xyXG4gICAgICAgIHRoaXMuX3Jlc3VsdCA9IHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiB0aGUgcmVzdWx0LlxyXG4gICAgICovXHJcbiAgICBnZXQgcmVzdWx0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9yZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUeXBlIG9mIHJlc3BvbnNlLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgZ2V0IHR5cGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gT3BlbkNvbm5lY3Rpb25SZXNwb25zZS5UWVBFO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGV0ZXJtaW5lIGlmIHRoaXMgcmVxdWVzdCBpcyB0byBiZSBicm9hZGNhc3QvcHJvY2Vzc2VkIHRvIGV2ZXJ5b25lXHJcbiAgICAgKiBjb25uZWN0ZWQgdG8gdGhlIHNlcnZlciAoYW5kIGFsbCBvdGhlciBzZXJ2ZXJzIGluIHRoZSBmYXJtKS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGlzQnJvYWRjYXN0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyByZXNwb25zZSBvYmplY3QgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICByZXN1bHQudHlwZSA9IE9wZW5Db25uZWN0aW9uUmVzcG9uc2UuVFlQRTtcclxuICAgICAgICByZXN1bHQucmVzdWx0ID0gdGhpcy5fcmVzdWx0O1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBPcGVuQ29ubmVjdGlvblJlc3BvbnNlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
