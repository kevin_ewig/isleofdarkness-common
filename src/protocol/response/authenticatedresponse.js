"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var response_1 = require("./response");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
/**
 * A response that comes from an already-authenticated user.
 * In this case, no ticket information is needed.
 */
var AuthenticatedResponse = /** @class */ (function (_super) {
    __extends(AuthenticatedResponse, _super);
    function AuthenticatedResponse(userMetaData, ticket) {
        var _this = _super.call(this) || this;
        if (!userMetaData || !ticket) {
            throw new invalidjsonerror_1.default();
        }
        else {
            _this._userMetaData = userMetaData;
            _this._ticket = ticket;
        }
        return _this;
    }
    /**
     * Get some information about the user making this request.
     */
    AuthenticatedResponse.prototype.getUserMetaData = function () {
        return this._userMetaData;
    };
    /**
     * Get ticket authenticating this response.
     */
    AuthenticatedResponse.prototype.getTicket = function () {
        return this._ticket;
    };
    /**
     * Convert this response object to JSON.
     */
    AuthenticatedResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        if (this.getUserMetaData()) {
            result.userMetaData = this.getUserMetaData().toJSON();
        }
        if (this.getTicket()) {
            result.ticket = this.getTicket().toJSON();
        }
        return result;
    };
    AuthenticatedResponse.TYPE = "authenticatedresponse";
    return AuthenticatedResponse;
}(response_1.default));
exports.AuthenticatedResponse = AuthenticatedResponse;
exports.default = AuthenticatedResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL2F1dGhlbnRpY2F0ZWRyZXNwb25zZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQSx1Q0FBa0M7QUFFbEMsaUVBQTREO0FBSTVEOzs7R0FHRztBQUNIO0lBQW9ELHlDQUFRO0lBT3hELCtCQUFZLFlBQTBCLEVBQUUsTUFBYztRQUF0RCxZQUNJLGlCQUFPLFNBT1Y7UUFORyxFQUFFLENBQUMsQ0FBRSxDQUFDLFlBQVksSUFBSSxDQUFDLE1BQU8sQ0FBQyxDQUFDLENBQUM7WUFDN0IsTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osS0FBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUM7WUFDbEMsS0FBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDMUIsQ0FBQzs7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSwrQ0FBZSxHQUF0QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRDs7T0FFRztJQUNJLHlDQUFTLEdBQWhCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksc0NBQU0sR0FBYjtRQUNJLElBQUksTUFBTSxHQUE0QixpQkFBTSxNQUFNLFdBQUUsQ0FBQztRQUNyRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsZUFBZSxFQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE1BQU0sQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzFELENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsU0FBUyxFQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzlDLENBQUM7UUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUF6Q2EsMEJBQUksR0FBRyx1QkFBdUIsQ0FBQztJQTJDakQsNEJBQUM7Q0E3Q0QsQUE2Q0MsQ0E3Q21ELGtCQUFRLEdBNkMzRDtBQTdDcUIsc0RBQXFCO0FBK0MzQyxrQkFBZSxxQkFBcUIsQ0FBQyIsImZpbGUiOiJwcm90b2NvbC9yZXNwb25zZS9hdXRoZW50aWNhdGVkcmVzcG9uc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuLi8uLi9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IFJlc3BvbnNlIGZyb20gXCIuL3Jlc3BvbnNlXCI7XHJcbmltcG9ydCBUaWNrZXQgZnJvbSBcIi4uLy4uL21vZGVsL3RpY2tldFwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvbWlzc2luZ2NvbnN0cnVjdG9yYXJndW1lbnRlcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmZvcnJlc3BvbnNlZXJyb3JcIjtcclxuXHJcbi8qKlxyXG4gKiBBIHJlc3BvbnNlIHRoYXQgY29tZXMgZnJvbSBhbiBhbHJlYWR5LWF1dGhlbnRpY2F0ZWQgdXNlci5cclxuICogSW4gdGhpcyBjYXNlLCBubyB0aWNrZXQgaW5mb3JtYXRpb24gaXMgbmVlZGVkLlxyXG4gKi9cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEF1dGhlbnRpY2F0ZWRSZXNwb25zZSBleHRlbmRzIFJlc3BvbnNlIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcImF1dGhlbnRpY2F0ZWRyZXNwb25zZVwiO1xyXG5cclxuICAgIHByaXZhdGUgX3VzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhO1xyXG4gICAgcHJpdmF0ZSBfdGlja2V0OiBUaWNrZXQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IodXNlck1ldGFEYXRhOiBVc2VyTWV0YURhdGEsIHRpY2tldDogVGlja2V0KSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBpZiAoICF1c2VyTWV0YURhdGEgfHwgIXRpY2tldCApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORXJyb3IoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl91c2VyTWV0YURhdGEgPSB1c2VyTWV0YURhdGE7XHJcbiAgICAgICAgICAgIHRoaXMuX3RpY2tldCA9IHRpY2tldDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgc29tZSBpbmZvcm1hdGlvbiBhYm91dCB0aGUgdXNlciBtYWtpbmcgdGhpcyByZXF1ZXN0LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0VXNlck1ldGFEYXRhKCk6IFVzZXJNZXRhRGF0YSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJNZXRhRGF0YTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aWNrZXQgYXV0aGVudGljYXRpbmcgdGhpcyByZXNwb25zZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFRpY2tldCgpOiBUaWNrZXQge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl90aWNrZXQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgcmVzcG9uc2Ugb2JqZWN0IHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IHsgW2tleTogc3RyaW5nXTogYW55OyB9ID0gc3VwZXIudG9KU09OKCk7XHJcbiAgICAgICAgaWYgKCB0aGlzLmdldFVzZXJNZXRhRGF0YSgpICkge1xyXG4gICAgICAgICAgICByZXN1bHQudXNlck1ldGFEYXRhID0gdGhpcy5nZXRVc2VyTWV0YURhdGEoKS50b0pTT04oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCB0aGlzLmdldFRpY2tldCgpICkge1xyXG4gICAgICAgICAgICByZXN1bHQudGlja2V0ID0gdGhpcy5nZXRUaWNrZXQoKS50b0pTT04oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEF1dGhlbnRpY2F0ZWRSZXNwb25zZTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
