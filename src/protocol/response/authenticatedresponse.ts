import UserMetaData from "../../model/usermetadata";
import Response from "./response";
import Ticket from "../../model/ticket";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

/**
 * A response that comes from an already-authenticated user.
 * In this case, no ticket information is needed.
 */
export abstract class AuthenticatedResponse extends Response {

    public static TYPE = "authenticatedresponse";

    private _userMetaData: UserMetaData;
    private _ticket: Ticket;

    constructor(userMetaData: UserMetaData, ticket: Ticket) {
        super();
        if ( !userMetaData || !ticket ) {
            throw new InvalidJSONError();
        } else {
            this._userMetaData = userMetaData;
            this._ticket = ticket;
        }
    }

    /**
     * Get some information about the user making this request.
     */
    public getUserMetaData(): UserMetaData {
        return this._userMetaData;
    }

    /**
     * Get ticket authenticating this response.
     */
    public getTicket(): Ticket {
        return this._ticket;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        if ( this.getUserMetaData() ) {
            result.userMetaData = this.getUserMetaData().toJSON();
        }
        if ( this.getTicket() ) {
            result.ticket = this.getTicket().toJSON();
        }
        return result;
    }

}

export default AuthenticatedResponse;
