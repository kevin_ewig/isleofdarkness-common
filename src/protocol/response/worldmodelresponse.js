"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ticket_1 = require("../../model/ticket");
var world_1 = require("../../model/world");
var response_1 = require("./response");
var authenticatedresponse_1 = require("./authenticatedresponse");
var usermetadata_1 = require("../../model/usermetadata");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
/**
 * Get the contents of the world.
 */
var WorldModelResponse = /** @class */ (function (_super) {
    __extends(WorldModelResponse, _super);
    function WorldModelResponse(userMetaData, ticket, world) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        if (world) {
            _this._world = world;
        }
        else {
            throw new missingconstructorargumenterror_1.default();
        }
        return _this;
    }
    /**
     * Convert a JSON object to a World Model response object.
     * @param jsonObject
     * @returns {WorldModelResponse}
     */
    WorldModelResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== WorldModelResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket) {
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var world = world_1.default.fromJSON(jsonObject.world);
            var response = new WorldModelResponse(userMetaData, ticket, world);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.default();
        }
    };
    Object.defineProperty(WorldModelResponse.prototype, "world", {
        /**
         * World returned in this response.
         * @returns {World}
         */
        get: function () {
            return this._world;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WorldModelResponse.prototype, "type", {
        /**
         * The type of response.
         * @returns {string}
         */
        get: function () {
            return WorldModelResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    WorldModelResponse.prototype.isBroadcast = function () {
        return false;
    };
    /**
     * Convert this response object to JSON.
     */
    WorldModelResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.type = WorldModelResponse.TYPE;
        if (this._world) {
            result.world = this._world.toJSON();
        }
        return result;
    };
    WorldModelResponse.TYPE = "worldmodelresponse";
    return WorldModelResponse;
}(authenticatedresponse_1.default));
exports.WorldModelResponse = WorldModelResponse;
exports.default = WorldModelResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL3dvcmxkbW9kZWxyZXNwb25zZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSw2Q0FBd0M7QUFDeEMsMkNBQXNDO0FBQ3RDLHVDQUFrQztBQUNsQyxpRUFBNEQ7QUFDNUQseURBQW9EO0FBQ3BELGlFQUE0RDtBQUM1RCwrRkFBMEY7QUFDMUYsdUZBQWtGO0FBRWxGOztHQUVHO0FBQ0g7SUFBd0Msc0NBQXFCO0lBOEJ6RCw0QkFBWSxZQUEwQixFQUFFLE1BQWMsRUFBRSxLQUFZO1FBQXBFLFlBQ0ksa0JBQU0sWUFBWSxFQUFFLE1BQU0sQ0FBQyxTQU05QjtRQUxHLEVBQUUsQ0FBQyxDQUFFLEtBQU0sQ0FBQyxDQUFDLENBQUM7WUFDVixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN4QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUkseUNBQStCLEVBQUUsQ0FBQztRQUNoRCxDQUFDOztJQUNMLENBQUM7SUEvQkQ7Ozs7T0FJRztJQUNXLDJCQUFRLEdBQXRCLFVBQXVCLFVBQWU7UUFDbEMsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLElBQUksS0FBSyxrQkFBa0IsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2hELE1BQU0sSUFBSSxxQ0FBMkIsRUFBRSxDQUFDO1FBQzVDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLFlBQVksSUFBSSxVQUFVLENBQUMsTUFBTyxDQUFDLENBQUMsQ0FBQztZQUV4RCxJQUFJLE1BQU0sR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDaEQsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xFLElBQUksS0FBSyxHQUFHLGVBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRTdDLElBQUksUUFBUSxHQUF1QixJQUFJLGtCQUFrQixDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdkYsa0JBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBRTVDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFFcEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztJQUNMLENBQUM7SUFlRCxzQkFBSSxxQ0FBSztRQUpUOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQzs7O09BQUE7SUFNRCxzQkFBSSxvQ0FBSTtRQUpSOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQztRQUNuQyxDQUFDOzs7T0FBQTtJQUVEOzs7T0FHRztJQUNJLHdDQUFXLEdBQWxCO1FBQ0ksTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQ7O09BRUc7SUFDSSxtQ0FBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ3JELE1BQU0sQ0FBQyxJQUFJLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxNQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN4QyxDQUFDO1FBQ0QsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBdkVhLHVCQUFJLEdBQUcsb0JBQW9CLENBQUM7SUF5RTlDLHlCQUFDO0NBM0VELEFBMkVDLENBM0V1QywrQkFBcUIsR0EyRTVEO0FBM0VZLGdEQUFrQjtBQTZFL0Isa0JBQWUsa0JBQWtCLENBQUMiLCJmaWxlIjoicHJvdG9jb2wvcmVzcG9uc2Uvd29ybGRtb2RlbHJlc3BvbnNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBXb3JsZCBmcm9tIFwiLi4vLi4vbW9kZWwvd29ybGRcIjtcclxuaW1wb3J0IFJlc3BvbnNlIGZyb20gXCIuL3Jlc3BvbnNlXCI7XHJcbmltcG9ydCBBdXRoZW50aWNhdGVkUmVzcG9uc2UgZnJvbSBcIi4vYXV0aGVudGljYXRlZHJlc3BvbnNlXCI7XHJcbmltcG9ydCBVc2VyTWV0YURhdGEgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJtZXRhZGF0YVwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvbWlzc2luZ2NvbnN0cnVjdG9yYXJndW1lbnRlcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmZvcnJlc3BvbnNlZXJyb3JcIjtcclxuXHJcbi8qKlxyXG4gKiBHZXQgdGhlIGNvbnRlbnRzIG9mIHRoZSB3b3JsZC5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBXb3JsZE1vZGVsUmVzcG9uc2UgZXh0ZW5kcyBBdXRoZW50aWNhdGVkUmVzcG9uc2Uge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgVFlQRSA9IFwid29ybGRtb2RlbHJlc3BvbnNlXCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfd29ybGQ6IFdvcmxkO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBhIEpTT04gb2JqZWN0IHRvIGEgV29ybGQgTW9kZWwgcmVzcG9uc2Ugb2JqZWN0LlxyXG4gICAgICogQHBhcmFtIGpzb25PYmplY3RcclxuICAgICAqIEByZXR1cm5zIHtXb3JsZE1vZGVsUmVzcG9uc2V9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iamVjdDogYW55KTogV29ybGRNb2RlbFJlc3BvbnNlIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gV29ybGRNb2RlbFJlc3BvbnNlLlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkZvclJlc3BvbnNlRXJyb3IoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCBqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSAmJiBqc29uT2JqZWN0LnRpY2tldCApIHtcclxuXHJcbiAgICAgICAgICAgIGxldCB0aWNrZXQgPSBUaWNrZXQuZnJvbUpTT04oanNvbk9iamVjdC50aWNrZXQpO1xyXG4gICAgICAgICAgICBsZXQgdXNlck1ldGFEYXRhID0gVXNlck1ldGFEYXRhLmZyb21KU09OKGpzb25PYmplY3QudXNlck1ldGFEYXRhKTtcclxuICAgICAgICAgICAgbGV0IHdvcmxkID0gV29ybGQuZnJvbUpTT04oanNvbk9iamVjdC53b3JsZCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgcmVzcG9uc2U6IFdvcmxkTW9kZWxSZXNwb25zZSA9IG5ldyBXb3JsZE1vZGVsUmVzcG9uc2UodXNlck1ldGFEYXRhLCB0aWNrZXQsIHdvcmxkKTtcclxuICAgICAgICAgICAgUmVzcG9uc2UucG9wdWxhdGVKU09OKHJlc3BvbnNlLCBqc29uT2JqZWN0KTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IodXNlck1ldGFEYXRhOiBVc2VyTWV0YURhdGEsIHRpY2tldDogVGlja2V0LCB3b3JsZDogV29ybGQpIHtcclxuICAgICAgICBzdXBlcih1c2VyTWV0YURhdGEsIHRpY2tldCk7XHJcbiAgICAgICAgaWYgKCB3b3JsZCApIHtcclxuICAgICAgICAgICAgdGhpcy5fd29ybGQgPSB3b3JsZDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvcigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFdvcmxkIHJldHVybmVkIGluIHRoaXMgcmVzcG9uc2UuXHJcbiAgICAgKiBAcmV0dXJucyB7V29ybGR9XHJcbiAgICAgKi9cclxuICAgIGdldCB3b3JsZCgpOiBXb3JsZCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3dvcmxkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhlIHR5cGUgb2YgcmVzcG9uc2UuXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBnZXQgdHlwZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBXb3JsZE1vZGVsUmVzcG9uc2UuVFlQRTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERldGVybWluZSBpZiB0aGlzIHJlcXVlc3QgaXMgdG8gYmUgYnJvYWRjYXN0L3Byb2Nlc3NlZCB0byBldmVyeW9uZVxyXG4gICAgICogY29ubmVjdGVkIHRvIHRoZSBzZXJ2ZXIgKGFuZCBhbGwgb3RoZXIgc2VydmVycyBpbiB0aGUgZmFybSkuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBpc0Jyb2FkY2FzdCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgcmVzcG9uc2Ugb2JqZWN0IHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IHsgW2tleTogc3RyaW5nXTogYW55OyB9ID0gc3VwZXIudG9KU09OKCk7XHJcbiAgICAgICAgcmVzdWx0LnR5cGUgPSBXb3JsZE1vZGVsUmVzcG9uc2UuVFlQRTtcclxuICAgICAgICBpZiAoIHRoaXMuX3dvcmxkICkge1xyXG4gICAgICAgICAgICByZXN1bHQud29ybGQgPSB0aGlzLl93b3JsZC50b0pTT04oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFdvcmxkTW9kZWxSZXNwb25zZTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
