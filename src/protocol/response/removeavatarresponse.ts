import UserMetaData from "../../model/usermetadata";
import Ticket from "../../model/ticket";
import Response from "./response";
import AuthenticatedResponse from "./authenticatedresponse";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";
import { MissingConstructorArgumentError } from "../../error/missingconstructorargumenterror";

export class RemoveAvatarResponse extends AuthenticatedResponse {

    public static TYPE = "removeavatarresponse";

    private _result: boolean;
    private _objectModelId: string;

    public static fromJSON(jsonObject: any): RemoveAvatarResponse {
        if ( jsonObject.type !== RemoveAvatarResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if (jsonObject.userMetaData && jsonObject.ticket && jsonObject.objectModelId) {

            let result = !!jsonObject.result;
            let usermetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let objectModelId: string = jsonObject.objectModelId;
            let response = new RemoveAvatarResponse(usermetaData, ticket, objectModelId, result);
            Response.populateJSON(response, jsonObject);
            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, objectModelId: string, result: boolean) {
        super(userMetaData, ticket);
        if ( objectModelId ) {
            this._result = !!result;
            this._objectModelId = objectModelId;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Flag to determine whether avatar was removed.
     */
    get result(): boolean {
        return this._result;
    }

    /**
     * Return the object model id of the avatar to be removed.
     */
    get objectModelId(): string {
        return this._objectModelId;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return RemoveAvatarResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return true;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.result = this._result;
        result.objectModelId = this._objectModelId;
        result.type = RemoveAvatarResponse.TYPE;
        return result;
    }

}

export default RemoveAvatarResponse;
