import UserMetaData from "../../model/usermetadata";
import Ticket from "../../model/ticket";
import Response from "./response";
import AuthenticatedResponse from "./authenticatedresponse";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

export class AddObjectModelResponse extends AuthenticatedResponse {

    public static TYPE = "addobjectmodelresponse";

    private _result: boolean;

    public static fromJSON(jsonObject: any): AddObjectModelResponse {
        if ( jsonObject.type !== AddObjectModelResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if (jsonObject.result && jsonObject.userMetaData && jsonObject.ticket) {

            let result = jsonObject.result;
            let usermetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let response = new AddObjectModelResponse(usermetaData, ticket, result);
            Response.populateJSON(response, jsonObject);
            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, result: boolean) {
        super(userMetaData, ticket);
        this._result = result;
    }

    /**
     * Result of adding an object to the world.
     */
    get result(): boolean {
        return this._result;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return AddObjectModelResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return true;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.result = this._result;
        result.type = AddObjectModelResponse.TYPE;
        return result;
    }

}

export default AddObjectModelResponse;
