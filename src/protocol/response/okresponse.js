"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var response_1 = require("./response");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var OkResponse = /** @class */ (function (_super) {
    __extends(OkResponse, _super);
    function OkResponse() {
        return _super.call(this) || this;
    }
    OkResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== OkResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else {
            var response = new OkResponse();
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
    };
    Object.defineProperty(OkResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return OkResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    OkResponse.prototype.isBroadcast = function () {
        return false;
    };
    /**
     * Convert this response object to JSON.
     */
    OkResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.type = OkResponse.TYPE;
        return result;
    };
    OkResponse.TYPE = "okresponse";
    return OkResponse;
}(response_1.default));
exports.OkResponse = OkResponse;
exports.default = OkResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL29rcmVzcG9uc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQWtDO0FBR2xDLHVGQUFrRjtBQUVsRjtJQUFnQyw4QkFBUTtJQWtCcEM7ZUFDSSxpQkFBTztJQUNYLENBQUM7SUFkYSxtQkFBUSxHQUF0QixVQUF1QixVQUFlO1FBQ2xDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxJQUFJLEtBQUssVUFBVSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDeEMsTUFBTSxJQUFJLHFDQUEyQixFQUFFLENBQUM7UUFDNUMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBRUosSUFBSSxRQUFRLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztZQUNoQyxrQkFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDNUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUVwQixDQUFDO0lBQ0wsQ0FBQztJQVVELHNCQUFJLDRCQUFJO1FBSlI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztRQUMzQixDQUFDOzs7T0FBQTtJQUVEOzs7T0FHRztJQUNJLGdDQUFXLEdBQWxCO1FBQ0ksTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQ7O09BRUc7SUFDSSwyQkFBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ3JELE1BQU0sQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQztRQUM5QixNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUEzQ2EsZUFBSSxHQUFHLFlBQVksQ0FBQztJQTZDdEMsaUJBQUM7Q0EvQ0QsQUErQ0MsQ0EvQytCLGtCQUFRLEdBK0N2QztBQS9DWSxnQ0FBVTtBQWlEdkIsa0JBQWUsVUFBVSxDQUFDIiwiZmlsZSI6InByb3RvY29sL3Jlc3BvbnNlL29rcmVzcG9uc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVzcG9uc2UgZnJvbSBcIi4vcmVzcG9uc2VcIjtcclxuaW1wb3J0IEludmFsaWRKU09ORXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZXJyb3JcIjtcclxuaW1wb3J0IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL21pc3Npbmdjb25zdHJ1Y3RvcmFyZ3VtZW50ZXJyb3JcIjtcclxuaW1wb3J0IEludmFsaWRKU09ORm9yUmVzcG9uc2VFcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25mb3JyZXNwb25zZWVycm9yXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgT2tSZXNwb25zZSBleHRlbmRzIFJlc3BvbnNlIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcIm9rcmVzcG9uc2VcIjtcclxuXHJcbiAgICBwcml2YXRlIF9lcnJvck1lc3NhZ2U6IHN0cmluZztcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSk6IE9rUmVzcG9uc2Uge1xyXG4gICAgICAgIGlmICgganNvbk9iamVjdC50eXBlICE9PSBPa1Jlc3BvbnNlLlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkZvclJlc3BvbnNlRXJyb3IoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgbGV0IHJlc3BvbnNlID0gbmV3IE9rUmVzcG9uc2UoKTtcclxuICAgICAgICAgICAgUmVzcG9uc2UucG9wdWxhdGVKU09OKHJlc3BvbnNlLCBqc29uT2JqZWN0KTtcclxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFR5cGUgb2YgcmVzcG9uc2UuXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBnZXQgdHlwZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBPa1Jlc3BvbnNlLlRZUEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmUgaWYgdGhpcyByZXF1ZXN0IGlzIHRvIGJlIGJyb2FkY2FzdC9wcm9jZXNzZWQgdG8gZXZlcnlvbmVcclxuICAgICAqIGNvbm5lY3RlZCB0byB0aGUgc2VydmVyIChhbmQgYWxsIG90aGVyIHNlcnZlcnMgaW4gdGhlIGZhcm0pLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgaXNCcm9hZGNhc3QoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB0aGlzIHJlc3BvbnNlIG9iamVjdCB0byBKU09OLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHsgW2tleTogc3RyaW5nXTogYW55OyB9IHtcclxuICAgICAgICBsZXQgcmVzdWx0OiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIHJlc3VsdC50eXBlID0gT2tSZXNwb25zZS5UWVBFO1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBPa1Jlc3BvbnNlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
