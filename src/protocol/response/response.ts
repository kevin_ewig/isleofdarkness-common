import Ticket from "../../model/ticket";
import Uuid from "../../uuid";

export abstract class Response {

    private _requestId: string;
    private _responseId: string;
    private _originServerName: string;

    /**
     * Populate response object with fields from JSON object.
     * @param response
     * @param jsonObject
     */
    public static populateJSON(response: Response, jsonObject: any): void {
        response.responseId = jsonObject.responseId;
        response.requestId = jsonObject.requestId;
        response.originServerName = jsonObject.originServerName;
    }

    constructor() {
        this._responseId = Uuid.getUuid();
    }

    /**
     * Type of response.
     */
    public abstract get type(): string;

    /**
     * Get the server name that this request came from.
     */
    get originServerName(): string {
        return this._originServerName;
    }

    /**
     * Set the server name that this request came from.
     * @param requestId
     */
    set originServerName(originServerName: string) {
        this._originServerName = originServerName;
    }

    /**
     * Get request id.
     */
    public get requestId(): string {
        return this._requestId;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public abstract isBroadcast(): boolean;

    /**
     * Set request id.
     */
    public set requestId(requestId: string) {
        this._requestId = requestId;
    }

    /**
     * Get response id.
     */
    public get responseId(): string {
        return this._responseId;
    }

    /**
     * Set response id.
     */
    public set responseId(responseId: string) {
        this._responseId = responseId;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = {};
        if ( this.type ) {
            result.type = this.type;
        }
        if ( this._responseId ) {
            result.responseId = this._responseId;
        }
        if ( this._requestId ) {
            result.requestId = this._requestId;
        }
        if ( this._originServerName ) {
            result.originServerName = this._originServerName;
        }

        return result;
    }

}

export default Response;
