"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usermetadata_1 = require("../../model/usermetadata");
var ticket_1 = require("../../model/ticket");
var response_1 = require("./response");
var point_1 = require("../../math/point");
var authenticatedresponse_1 = require("./authenticatedresponse");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var AddAvatarResponse = /** @class */ (function (_super) {
    __extends(AddAvatarResponse, _super);
    function AddAvatarResponse(userMetaData, ticket, result) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        if (result) {
            _this._result = result;
        }
        else {
            throw new missingconstructorargumenterror_1.default();
        }
        return _this;
    }
    AddAvatarResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== AddAvatarResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.result && jsonObject.userMetaData && jsonObject.ticket) {
            var result = point_1.default.fromJSON(jsonObject.result);
            var usermetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var response = new AddAvatarResponse(usermetaData, ticket, result);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.default();
        }
    };
    Object.defineProperty(AddAvatarResponse.prototype, "result", {
        /**
         * Location of where avatar is added in the world.
         */
        get: function () {
            return this._result;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AddAvatarResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return AddAvatarResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    AddAvatarResponse.prototype.isBroadcast = function () {
        return true;
    };
    /**
     * Convert this response object to JSON.
     */
    AddAvatarResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.result = this._result.toJSON();
        result.type = AddAvatarResponse.TYPE;
        return result;
    };
    AddAvatarResponse.TYPE = "addavatarresponse";
    return AddAvatarResponse;
}(authenticatedresponse_1.default));
exports.AddAvatarResponse = AddAvatarResponse;
exports.default = AddAvatarResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL2FkZGF2YXRhcnJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHlEQUFvRDtBQUNwRCw2Q0FBd0M7QUFDeEMsdUNBQWtDO0FBQ2xDLDBDQUFxQztBQUNyQyxpRUFBNEQ7QUFDNUQsaUVBQTREO0FBQzVELCtGQUEwRjtBQUMxRix1RkFBa0Y7QUFFbEY7SUFBdUMscUNBQXFCO0lBdUJ4RCwyQkFBWSxZQUEwQixFQUFFLE1BQWMsRUFBRSxNQUFhO1FBQXJFLFlBQ0ksa0JBQU0sWUFBWSxFQUFFLE1BQU0sQ0FBQyxTQU85QjtRQU5HLEVBQUUsQ0FBQyxDQUFFLE1BQU8sQ0FBQyxDQUFDLENBQUM7WUFDWCxLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUMxQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUkseUNBQStCLEVBQUUsQ0FBQztRQUNoRCxDQUFDOztJQUVMLENBQUM7SUF6QmEsMEJBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUNsQyxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsSUFBSSxLQUFLLGlCQUFpQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDL0MsTUFBTSxJQUFJLHFDQUEyQixFQUFFLENBQUM7UUFDNUMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLFVBQVUsQ0FBQyxZQUFZLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFFM0UsSUFBSSxNQUFNLEdBQUcsZUFBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDL0MsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xFLElBQUksTUFBTSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxJQUFJLFFBQVEsR0FBRyxJQUFJLGlCQUFpQixDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDbkUsa0JBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFFcEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztJQUNMLENBQUM7SUFlRCxzQkFBSSxxQ0FBTTtRQUhWOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLG1DQUFJO1FBSlI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBRUQ7OztPQUdHO0lBQ0ksdUNBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNJLGtDQUFNLEdBQWI7UUFDSSxJQUFJLE1BQU0sR0FBNEIsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDckQsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3RDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1FBQ3JDLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQTlEYSxzQkFBSSxHQUFHLG1CQUFtQixDQUFDO0lBZ0U3Qyx3QkFBQztDQWxFRCxBQWtFQyxDQWxFc0MsK0JBQXFCLEdBa0UzRDtBQWxFWSw4Q0FBaUI7QUFvRTlCLGtCQUFlLGlCQUFpQixDQUFDIiwiZmlsZSI6InByb3RvY29sL3Jlc3BvbnNlL2FkZGF2YXRhcnJlc3BvbnNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVzZXJNZXRhRGF0YSBmcm9tIFwiLi4vLi4vbW9kZWwvdXNlcm1ldGFkYXRhXCI7XHJcbmltcG9ydCBUaWNrZXQgZnJvbSBcIi4uLy4uL21vZGVsL3RpY2tldFwiO1xyXG5pbXBvcnQgUmVzcG9uc2UgZnJvbSBcIi4vcmVzcG9uc2VcIjtcclxuaW1wb3J0IFBvaW50IGZyb20gXCIuLi8uLi9tYXRoL3BvaW50XCI7XHJcbmltcG9ydCBBdXRoZW50aWNhdGVkUmVzcG9uc2UgZnJvbSBcIi4vYXV0aGVudGljYXRlZHJlc3BvbnNlXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmVycm9yXCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkZvclJlc3BvbnNlRXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVzcG9uc2VlcnJvclwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEFkZEF2YXRhclJlc3BvbnNlIGV4dGVuZHMgQXV0aGVudGljYXRlZFJlc3BvbnNlIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcImFkZGF2YXRhcnJlc3BvbnNlXCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVzdWx0OiBQb2ludDtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSk6IEFkZEF2YXRhclJlc3BvbnNlIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gQWRkQXZhdGFyUmVzcG9uc2UuVFlQRSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORm9yUmVzcG9uc2VFcnJvcigpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoanNvbk9iamVjdC5yZXN1bHQgJiYganNvbk9iamVjdC51c2VyTWV0YURhdGEgJiYganNvbk9iamVjdC50aWNrZXQpIHtcclxuXHJcbiAgICAgICAgICAgIGxldCByZXN1bHQgPSBQb2ludC5mcm9tSlNPTihqc29uT2JqZWN0LnJlc3VsdCk7XHJcbiAgICAgICAgICAgIGxldCB1c2VybWV0YURhdGEgPSBVc2VyTWV0YURhdGEuZnJvbUpTT04oanNvbk9iamVjdC51c2VyTWV0YURhdGEpO1xyXG4gICAgICAgICAgICBsZXQgdGlja2V0ID0gVGlja2V0LmZyb21KU09OKGpzb25PYmplY3QudGlja2V0KTtcclxuICAgICAgICAgICAgbGV0IHJlc3BvbnNlID0gbmV3IEFkZEF2YXRhclJlc3BvbnNlKHVzZXJtZXRhRGF0YSwgdGlja2V0LCByZXN1bHQpO1xyXG4gICAgICAgICAgICBSZXNwb25zZS5wb3B1bGF0ZUpTT04ocmVzcG9uc2UsIGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgcmVzdWx0OiBQb2ludCkge1xyXG4gICAgICAgIHN1cGVyKHVzZXJNZXRhRGF0YSwgdGlja2V0KTtcclxuICAgICAgICBpZiAoIHJlc3VsdCApIHtcclxuICAgICAgICAgICAgdGhpcy5fcmVzdWx0ID0gcmVzdWx0O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvY2F0aW9uIG9mIHdoZXJlIGF2YXRhciBpcyBhZGRlZCBpbiB0aGUgd29ybGQuXHJcbiAgICAgKi9cclxuICAgIGdldCByZXN1bHQoKTogUG9pbnQge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9yZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUeXBlIG9mIHJlc3BvbnNlLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgZ2V0IHR5cGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gQWRkQXZhdGFyUmVzcG9uc2UuVFlQRTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERldGVybWluZSBpZiB0aGlzIHJlcXVlc3QgaXMgdG8gYmUgYnJvYWRjYXN0L3Byb2Nlc3NlZCB0byBldmVyeW9uZVxyXG4gICAgICogY29ubmVjdGVkIHRvIHRoZSBzZXJ2ZXIgKGFuZCBhbGwgb3RoZXIgc2VydmVycyBpbiB0aGUgZmFybSkuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBpc0Jyb2FkY2FzdCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyByZXNwb25zZSBvYmplY3QgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICByZXN1bHQucmVzdWx0ID0gdGhpcy5fcmVzdWx0LnRvSlNPTigpO1xyXG4gICAgICAgIHJlc3VsdC50eXBlID0gQWRkQXZhdGFyUmVzcG9uc2UuVFlQRTtcclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQWRkQXZhdGFyUmVzcG9uc2U7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
