import User from "../../model/user";
import Ticket from "../../model/ticket";
import Response from "./response";
import UserMetaData from "../../model/usermetadata";
import AuthenticatedResponse from "./authenticatedresponse";
import { InvalidJSONForResponseError } from "../../error/invalidjsonforresponseerror";
import { InvalidJSONError } from "../../error/invalidjsonerror";

export class OpenConnectionResponse extends AuthenticatedResponse {

    public static TYPE = "openconnectionresponse";

    private _result: boolean;

    public static fromJSON(jsonObject: any): OpenConnectionResponse {
        if ( jsonObject.type !== OpenConnectionResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let response = new OpenConnectionResponse(userMetaData, ticket, jsonObject.result === true );
            Response.populateJSON(response, jsonObject);
            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, result: boolean) {
        super(userMetaData, ticket);
        this._result = result;
    }

    /**
     * Return the result.
     */
    get result(): boolean {
        return this._result;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return OpenConnectionResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return false;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.type = OpenConnectionResponse.TYPE;
        result.result = this._result;
        return result;
    }

}

export default OpenConnectionResponse;
