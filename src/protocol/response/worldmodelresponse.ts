import Ticket from "../../model/ticket";
import World from "../../model/world";
import Response from "./response";
import AuthenticatedResponse from "./authenticatedresponse";
import UserMetaData from "../../model/usermetadata";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

/**
 * Get the contents of the world.
 */
export class WorldModelResponse extends AuthenticatedResponse {

    public static TYPE = "worldmodelresponse";

    private _world: World;

    /**
     * Convert a JSON object to a World Model response object.
     * @param jsonObject
     * @returns {WorldModelResponse}
     */
    public static fromJSON(jsonObject: any): WorldModelResponse {
        if ( jsonObject.type !== WorldModelResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket ) {

            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let world = World.fromJSON(jsonObject.world);

            let response: WorldModelResponse = new WorldModelResponse(userMetaData, ticket, world);
            Response.populateJSON(response, jsonObject);

            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, world: World) {
        super(userMetaData, ticket);
        if ( world ) {
            this._world = world;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * World returned in this response.
     * @returns {World}
     */
    get world(): World {
        return this._world;
    }

    /**
     * The type of response.
     * @returns {string}
     */
    get type(): string {
        return WorldModelResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return false;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.type = WorldModelResponse.TYPE;
        if ( this._world ) {
            result.world = this._world.toJSON();
        }
        return result;
    }

}

export default WorldModelResponse;
