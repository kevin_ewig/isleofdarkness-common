import UserMetaData from "../../model/usermetadata";
import Ticket from "../../model/ticket";
import Response from "./response";
import Point from "../../math/point";
import AuthenticatedResponse from "./authenticatedresponse";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

export class AddAvatarResponse extends AuthenticatedResponse {

    public static TYPE = "addavatarresponse";

    private _result: Point;

    public static fromJSON(jsonObject: any): AddAvatarResponse {
        if ( jsonObject.type !== AddAvatarResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if (jsonObject.result && jsonObject.userMetaData && jsonObject.ticket) {

            let result = Point.fromJSON(jsonObject.result);
            let usermetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let response = new AddAvatarResponse(usermetaData, ticket, result);
            Response.populateJSON(response, jsonObject);
            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, result: Point) {
        super(userMetaData, ticket);
        if ( result ) {
            this._result = result;
        } else {
            throw new MissingConstructorArgumentError();
        }

    }

    /**
     * Location of where avatar is added in the world.
     */
    get result(): Point {
        return this._result;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return AddAvatarResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return true;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.result = this._result.toJSON();
        result.type = AddAvatarResponse.TYPE;
        return result;
    }

}

export default AddAvatarResponse;
