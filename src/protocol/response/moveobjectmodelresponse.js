"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ticket_1 = require("../../model/ticket");
var response_1 = require("./response");
var usermetadata_1 = require("../../model/usermetadata");
var authenticatedresponse_1 = require("./authenticatedresponse");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var point_1 = require("../../math/point");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
var MoveObjectModelResponse = /** @class */ (function (_super) {
    __extends(MoveObjectModelResponse, _super);
    function MoveObjectModelResponse(userMetaData, ticket, endPoint, result) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        if (endPoint) {
            _this._endPoint = endPoint;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError();
        }
        _this._result = !!result;
        return _this;
    }
    MoveObjectModelResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== MoveObjectModelResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var result = jsonObject.result;
            var objectModelId = jsonObject.objectModelId;
            if (jsonObject.endPoint) {
                var endPoint = point_1.default.fromJSON(jsonObject.endPoint);
                var response = new MoveObjectModelResponse(userMetaData, ticket, endPoint, result);
                response_1.default.populateJSON(response, jsonObject);
                response.objectModelId = objectModelId;
                return response;
            }
            else {
                throw new invalidjsonerror_1.default();
            }
        }
        else {
            throw new invalidjsonerror_1.default();
        }
    };
    Object.defineProperty(MoveObjectModelResponse.prototype, "endPoint", {
        /**
         * Get the object model id of the object model that is moving.
         */
        get: function () {
            return this._endPoint;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoveObjectModelResponse.prototype, "result", {
        /**
         * Get result of moving the object.
         * @param user
         */
        get: function () {
            return this._result;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoveObjectModelResponse.prototype, "objectModelId", {
        /**
         * Get the object model id of the object model that is moving.
         */
        get: function () {
            return this._objectModelId;
        },
        /**
         * Set the object model id of the object model that is moving.
         */
        set: function (value) {
            this._objectModelId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoveObjectModelResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return MoveObjectModelResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    MoveObjectModelResponse.prototype.isBroadcast = function () {
        return true;
    };
    /**
     * Convert this response object to JSON.
     */
    MoveObjectModelResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.type = MoveObjectModelResponse.TYPE;
        result.result = this._result;
        if (this._endPoint) {
            result.endPoint = this._endPoint.toJSON();
        }
        if (this._objectModelId) {
            result.objectModelId = this._objectModelId;
        }
        return result;
    };
    MoveObjectModelResponse.TYPE = "moveobjectmodelresponse";
    return MoveObjectModelResponse;
}(authenticatedresponse_1.default));
exports.MoveObjectModelResponse = MoveObjectModelResponse;
exports.default = MoveObjectModelResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL21vdmVvYmplY3Rtb2RlbHJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUNBLDZDQUF3QztBQUN4Qyx1Q0FBa0M7QUFDbEMseURBQW9EO0FBQ3BELGlFQUE0RDtBQUM1RCx1RkFBa0Y7QUFDbEYsaUVBQTREO0FBQzVELDBDQUFxQztBQUNyQywrRkFBOEY7QUFFOUY7SUFBNkMsMkNBQXFCO0lBc0M5RCxpQ0FBWSxZQUEwQixFQUFFLE1BQWMsRUFBRSxRQUFlLEVBQUUsTUFBZTtRQUF4RixZQUNJLGtCQUFNLFlBQVksRUFBRSxNQUFNLENBQUMsU0FPOUI7UUFORyxFQUFFLENBQUMsQ0FBRSxRQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2IsS0FBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDOUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLGlFQUErQixFQUFFLENBQUM7UUFDaEQsQ0FBQztRQUNELEtBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQzs7SUFDNUIsQ0FBQztJQXRDYSxnQ0FBUSxHQUF0QixVQUF1QixVQUFlO1FBQ2xDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxJQUFJLEtBQUssdUJBQXVCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNyRCxNQUFNLElBQUkscUNBQTJCLEVBQUUsQ0FBQztRQUM1QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxZQUFZLElBQUksVUFBVSxDQUFDLE1BQU8sQ0FBQyxDQUFDLENBQUM7WUFFeEQsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBRWxFLElBQUksTUFBTSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxJQUFJLE1BQU0sR0FBWSxVQUFVLENBQUMsTUFBTSxDQUFDO1lBQ3hDLElBQUksYUFBYSxHQUFXLFVBQVUsQ0FBQyxhQUFhLENBQUM7WUFFckQsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLFFBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBRXhCLElBQUksUUFBUSxHQUFVLGVBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUUxRCxJQUFJLFFBQVEsR0FBRyxJQUFJLHVCQUF1QixDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNuRixrQkFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBRTVDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO2dCQUN2QyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBRXBCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixNQUFNLElBQUksMEJBQWdCLEVBQUUsQ0FBQztZQUNqQyxDQUFDO1FBRUwsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztJQUNMLENBQUM7SUFlRCxzQkFBSSw2Q0FBUTtRQUhaOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLDJDQUFNO1FBSlY7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQUtELHNCQUFJLGtEQUFhO1FBSGpCOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUMvQixDQUFDO1FBRUQ7O1dBRUc7YUFDSCxVQUFrQixLQUFhO1lBQzNCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLENBQUM7OztPQVBBO0lBYUQsc0JBQUkseUNBQUk7UUFKUjs7O1dBR0c7YUFDSDtZQUNJLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUM7UUFDeEMsQ0FBQzs7O09BQUE7SUFFRDs7O09BR0c7SUFDSSw2Q0FBVyxHQUFsQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksd0NBQU0sR0FBYjtRQUNJLElBQUksTUFBTSxHQUE0QixpQkFBTSxNQUFNLFdBQUUsQ0FBQztRQUNyRCxNQUFNLENBQUMsSUFBSSxHQUFHLHVCQUF1QixDQUFDLElBQUksQ0FBQztRQUMzQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDN0IsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFNBQVUsQ0FBQyxDQUFDLENBQUM7WUFDbkIsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzlDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsY0FBZSxDQUFDLENBQUMsQ0FBQztZQUN4QixNQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDL0MsQ0FBQztRQUNELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQXpHYSw0QkFBSSxHQUFHLHlCQUF5QixDQUFDO0lBMkduRCw4QkFBQztDQTdHRCxBQTZHQyxDQTdHNEMsK0JBQXFCLEdBNkdqRTtBQTdHWSwwREFBdUI7QUErR3BDLGtCQUFlLHVCQUF1QixDQUFDIiwiZmlsZSI6InByb3RvY29sL3Jlc3BvbnNlL21vdmVvYmplY3Rtb2RlbHJlc3BvbnNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVzZXIgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJcIjtcclxuaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZVwiO1xyXG5pbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuLi8uLi9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IEF1dGhlbnRpY2F0ZWRSZXNwb25zZSBmcm9tIFwiLi9hdXRoZW50aWNhdGVkcmVzcG9uc2VcIjtcclxuaW1wb3J0IEludmFsaWRKU09ORm9yUmVzcG9uc2VFcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25mb3JyZXNwb25zZWVycm9yXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmVycm9yXCI7XHJcbmltcG9ydCBQb2ludCBmcm9tIFwiLi4vLi4vbWF0aC9wb2ludFwiO1xyXG5pbXBvcnQgeyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIH0gZnJvbSBcIi4uLy4uL2Vycm9yL21pc3Npbmdjb25zdHJ1Y3RvcmFyZ3VtZW50ZXJyb3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBNb3ZlT2JqZWN0TW9kZWxSZXNwb25zZSBleHRlbmRzIEF1dGhlbnRpY2F0ZWRSZXNwb25zZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJtb3Zlb2JqZWN0bW9kZWxyZXNwb25zZVwiO1xyXG5cclxuICAgIHByaXZhdGUgX3Jlc3VsdDogYm9vbGVhbjtcclxuICAgIHByaXZhdGUgX29iamVjdE1vZGVsSWQ6IHN0cmluZztcclxuICAgIHByaXZhdGUgX2VuZFBvaW50OiBQb2ludDtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSk6IE1vdmVPYmplY3RNb2RlbFJlc3BvbnNlIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gTW92ZU9iamVjdE1vZGVsUmVzcG9uc2UuVFlQRSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORm9yUmVzcG9uc2VFcnJvcigpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3QudXNlck1ldGFEYXRhICYmIGpzb25PYmplY3QudGlja2V0ICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHVzZXJNZXRhRGF0YSA9IFVzZXJNZXRhRGF0YS5mcm9tSlNPTihqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSk7XHJcblxyXG4gICAgICAgICAgICBsZXQgdGlja2V0ID0gVGlja2V0LmZyb21KU09OKGpzb25PYmplY3QudGlja2V0KTtcclxuICAgICAgICAgICAgbGV0IHJlc3VsdDogYm9vbGVhbiA9IGpzb25PYmplY3QucmVzdWx0O1xyXG4gICAgICAgICAgICBsZXQgb2JqZWN0TW9kZWxJZDogc3RyaW5nID0ganNvbk9iamVjdC5vYmplY3RNb2RlbElkO1xyXG5cclxuICAgICAgICAgICAgaWYgKCBqc29uT2JqZWN0LmVuZFBvaW50ICkge1xyXG5cclxuICAgICAgICAgICAgICAgIGxldCBlbmRQb2ludDogUG9pbnQgPSBQb2ludC5mcm9tSlNPTihqc29uT2JqZWN0LmVuZFBvaW50KTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgcmVzcG9uc2UgPSBuZXcgTW92ZU9iamVjdE1vZGVsUmVzcG9uc2UodXNlck1ldGFEYXRhLCB0aWNrZXQsIGVuZFBvaW50LCByZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgUmVzcG9uc2UucG9wdWxhdGVKU09OKHJlc3BvbnNlLCBqc29uT2JqZWN0KTtcclxuXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZS5vYmplY3RNb2RlbElkID0gb2JqZWN0TW9kZWxJZDtcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEpTT05FcnJvcigpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgZW5kUG9pbnQ6IFBvaW50LCByZXN1bHQ6IGJvb2xlYW4pIHtcclxuICAgICAgICBzdXBlcih1c2VyTWV0YURhdGEsIHRpY2tldCk7XHJcbiAgICAgICAgaWYgKCBlbmRQb2ludCApIHtcclxuICAgICAgICAgICAgdGhpcy5fZW5kUG9pbnQgPSBlbmRQb2ludDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvcigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9yZXN1bHQgPSAhIXJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgb2JqZWN0IG1vZGVsIGlkIG9mIHRoZSBvYmplY3QgbW9kZWwgdGhhdCBpcyBtb3ZpbmcuXHJcbiAgICAgKi9cclxuICAgIGdldCBlbmRQb2ludCgpOiBQb2ludCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2VuZFBvaW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHJlc3VsdCBvZiBtb3ZpbmcgdGhlIG9iamVjdC5cclxuICAgICAqIEBwYXJhbSB1c2VyXHJcbiAgICAgKi9cclxuICAgIGdldCByZXN1bHQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3Jlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgb2JqZWN0IG1vZGVsIGlkIG9mIHRoZSBvYmplY3QgbW9kZWwgdGhhdCBpcyBtb3ZpbmcuXHJcbiAgICAgKi9cclxuICAgIGdldCBvYmplY3RNb2RlbElkKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX29iamVjdE1vZGVsSWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIG9iamVjdCBtb2RlbCBpZCBvZiB0aGUgb2JqZWN0IG1vZGVsIHRoYXQgaXMgbW92aW5nLlxyXG4gICAgICovXHJcbiAgICBzZXQgb2JqZWN0TW9kZWxJZCh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fb2JqZWN0TW9kZWxJZCA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHlwZSBvZiByZXNwb25zZS5cclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICAgKi9cclxuICAgIGdldCB0eXBlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIE1vdmVPYmplY3RNb2RlbFJlc3BvbnNlLlRZUEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmUgaWYgdGhpcyByZXF1ZXN0IGlzIHRvIGJlIGJyb2FkY2FzdC9wcm9jZXNzZWQgdG8gZXZlcnlvbmVcclxuICAgICAqIGNvbm5lY3RlZCB0byB0aGUgc2VydmVyIChhbmQgYWxsIG90aGVyIHNlcnZlcnMgaW4gdGhlIGZhcm0pLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgaXNCcm9hZGNhc3QoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgcmVzcG9uc2Ugb2JqZWN0IHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IHsgW2tleTogc3RyaW5nXTogYW55OyB9ID0gc3VwZXIudG9KU09OKCk7XHJcbiAgICAgICAgcmVzdWx0LnR5cGUgPSBNb3ZlT2JqZWN0TW9kZWxSZXNwb25zZS5UWVBFO1xyXG4gICAgICAgIHJlc3VsdC5yZXN1bHQgPSB0aGlzLl9yZXN1bHQ7XHJcbiAgICAgICAgaWYgKCB0aGlzLl9lbmRQb2ludCApIHtcclxuICAgICAgICAgICAgcmVzdWx0LmVuZFBvaW50ID0gdGhpcy5fZW5kUG9pbnQudG9KU09OKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggdGhpcy5fb2JqZWN0TW9kZWxJZCApIHtcclxuICAgICAgICAgICAgcmVzdWx0Lm9iamVjdE1vZGVsSWQgPSB0aGlzLl9vYmplY3RNb2RlbElkO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgTW92ZU9iamVjdE1vZGVsUmVzcG9uc2U7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
