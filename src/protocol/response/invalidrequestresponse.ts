import User from "../../model/user";
import Ticket from "../../model/ticket";
import Response from "./response";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

export class InvalidRequestResponse extends Response {

    public static TYPE = "invalidrequestresponse";

    public static fromJSON(jsonObject: any): InvalidRequestResponse {
        if ( jsonObject.type !== InvalidRequestResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else {
            let response = new InvalidRequestResponse();
            Response.populateJSON(response, jsonObject);
            return response;
        }
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return InvalidRequestResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return false;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.type = InvalidRequestResponse.TYPE;
        return result;
    }

}

export default InvalidRequestResponse;
