"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var response_1 = require("./response");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var InvalidRequestResponse = /** @class */ (function (_super) {
    __extends(InvalidRequestResponse, _super);
    function InvalidRequestResponse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    InvalidRequestResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== InvalidRequestResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else {
            var response = new InvalidRequestResponse();
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
    };
    Object.defineProperty(InvalidRequestResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return InvalidRequestResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    InvalidRequestResponse.prototype.isBroadcast = function () {
        return false;
    };
    /**
     * Convert this response object to JSON.
     */
    InvalidRequestResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.type = InvalidRequestResponse.TYPE;
        return result;
    };
    InvalidRequestResponse.TYPE = "invalidrequestresponse";
    return InvalidRequestResponse;
}(response_1.default));
exports.InvalidRequestResponse = InvalidRequestResponse;
exports.default = InvalidRequestResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL2ludmFsaWRyZXF1ZXN0cmVzcG9uc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBRUEsdUNBQWtDO0FBR2xDLHVGQUFrRjtBQUVsRjtJQUE0QywwQ0FBUTtJQUFwRDs7SUF1Q0EsQ0FBQztJQW5DaUIsK0JBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUNsQyxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsSUFBSSxLQUFLLHNCQUFzQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDcEQsTUFBTSxJQUFJLHFDQUEyQixFQUFFLENBQUM7UUFDNUMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxRQUFRLEdBQUcsSUFBSSxzQkFBc0IsRUFBRSxDQUFDO1lBQzVDLGtCQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUM1QyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ3BCLENBQUM7SUFDTCxDQUFDO0lBTUQsc0JBQUksd0NBQUk7UUFKUjs7O1dBR0c7YUFDSDtZQUNJLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7SUFFRDs7O09BR0c7SUFDSSw0Q0FBVyxHQUFsQjtRQUNJLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksdUNBQU0sR0FBYjtRQUNJLElBQUksTUFBTSxHQUE0QixpQkFBTSxNQUFNLFdBQUUsQ0FBQztRQUNyRCxNQUFNLENBQUMsSUFBSSxHQUFHLHNCQUFzQixDQUFDLElBQUksQ0FBQztRQUMxQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFuQ2EsMkJBQUksR0FBRyx3QkFBd0IsQ0FBQztJQXFDbEQsNkJBQUM7Q0F2Q0QsQUF1Q0MsQ0F2QzJDLGtCQUFRLEdBdUNuRDtBQXZDWSx3REFBc0I7QUF5Q25DLGtCQUFlLHNCQUFzQixDQUFDIiwiZmlsZSI6InByb3RvY29sL3Jlc3BvbnNlL2ludmFsaWRyZXF1ZXN0cmVzcG9uc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVXNlciBmcm9tIFwiLi4vLi4vbW9kZWwvdXNlclwiO1xyXG5pbXBvcnQgVGlja2V0IGZyb20gXCIuLi8uLi9tb2RlbC90aWNrZXRcIjtcclxuaW1wb3J0IFJlc3BvbnNlIGZyb20gXCIuL3Jlc3BvbnNlXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmVycm9yXCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkZvclJlc3BvbnNlRXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVzcG9uc2VlcnJvclwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEludmFsaWRSZXF1ZXN0UmVzcG9uc2UgZXh0ZW5kcyBSZXNwb25zZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJpbnZhbGlkcmVxdWVzdHJlc3BvbnNlXCI7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqZWN0OiBhbnkpOiBJbnZhbGlkUmVxdWVzdFJlc3BvbnNlIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gSW52YWxpZFJlcXVlc3RSZXNwb25zZS5UWVBFICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGV0IHJlc3BvbnNlID0gbmV3IEludmFsaWRSZXF1ZXN0UmVzcG9uc2UoKTtcclxuICAgICAgICAgICAgUmVzcG9uc2UucG9wdWxhdGVKU09OKHJlc3BvbnNlLCBqc29uT2JqZWN0KTtcclxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFR5cGUgb2YgcmVzcG9uc2UuXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBnZXQgdHlwZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBJbnZhbGlkUmVxdWVzdFJlc3BvbnNlLlRZUEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmUgaWYgdGhpcyByZXF1ZXN0IGlzIHRvIGJlIGJyb2FkY2FzdC9wcm9jZXNzZWQgdG8gZXZlcnlvbmVcclxuICAgICAqIGNvbm5lY3RlZCB0byB0aGUgc2VydmVyIChhbmQgYWxsIG90aGVyIHNlcnZlcnMgaW4gdGhlIGZhcm0pLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgaXNCcm9hZGNhc3QoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB0aGlzIHJlc3BvbnNlIG9iamVjdCB0byBKU09OLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHsgW2tleTogc3RyaW5nXTogYW55OyB9IHtcclxuICAgICAgICBsZXQgcmVzdWx0OiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIHJlc3VsdC50eXBlID0gSW52YWxpZFJlcXVlc3RSZXNwb25zZS5UWVBFO1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBJbnZhbGlkUmVxdWVzdFJlc3BvbnNlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
