"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var response_1 = require("./response");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var ErrorResponse = /** @class */ (function (_super) {
    __extends(ErrorResponse, _super);
    function ErrorResponse(errorMessage) {
        var _this = _super.call(this) || this;
        _this._errorMessage = errorMessage;
        return _this;
    }
    ErrorResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== ErrorResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.errorMessage) {
            var response = new ErrorResponse(jsonObject.errorMessage);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.default();
        }
    };
    Object.defineProperty(ErrorResponse.prototype, "errorMessage", {
        /**
         * Return the result.
         */
        get: function () {
            return this._errorMessage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ErrorResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return ErrorResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    ErrorResponse.prototype.isBroadcast = function () {
        return false;
    };
    /**
     * Convert this response object to JSON.
     */
    ErrorResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.type = ErrorResponse.TYPE;
        result.errorMessage = this._errorMessage;
        return result;
    };
    ErrorResponse.TYPE = "errorresponse";
    return ErrorResponse;
}(response_1.default));
exports.ErrorResponse = ErrorResponse;
exports.default = ErrorResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL2Vycm9ycmVzcG9uc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQWtDO0FBQ2xDLGlFQUE0RDtBQUU1RCx1RkFBa0Y7QUFFbEY7SUFBbUMsaUNBQVE7SUFvQnZDLHVCQUFZLFlBQW9CO1FBQWhDLFlBQ0ksaUJBQU8sU0FFVjtRQURHLEtBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDOztJQUN0QyxDQUFDO0lBakJhLHNCQUFRLEdBQXRCLFVBQXVCLFVBQWU7UUFDbEMsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUMzQyxNQUFNLElBQUkscUNBQTJCLEVBQUUsQ0FBQztRQUM1QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxZQUFhLENBQUMsQ0FBQyxDQUFDO1lBRW5DLElBQUksUUFBUSxHQUFHLElBQUksYUFBYSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUMxRCxrQkFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDNUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUVwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksMEJBQWdCLEVBQUUsQ0FBQztRQUNqQyxDQUFDO0lBQ0wsQ0FBQztJQVVELHNCQUFJLHVDQUFZO1FBSGhCOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLCtCQUFJO1FBSlI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQUVEOzs7T0FHRztJQUNJLG1DQUFXLEdBQWxCO1FBQ0ksTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQ7O09BRUc7SUFDSSw4QkFBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ3JELE1BQU0sQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQztRQUNqQyxNQUFNLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDekMsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBdERhLGtCQUFJLEdBQUcsZUFBZSxDQUFDO0lBd0R6QyxvQkFBQztDQTFERCxBQTBEQyxDQTFEa0Msa0JBQVEsR0EwRDFDO0FBMURZLHNDQUFhO0FBNEQxQixrQkFBZSxhQUFhLENBQUMiLCJmaWxlIjoicHJvdG9jb2wvcmVzcG9uc2UvZXJyb3JyZXNwb25zZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZVwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvbWlzc2luZ2NvbnN0cnVjdG9yYXJndW1lbnRlcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmZvcnJlc3BvbnNlZXJyb3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBFcnJvclJlc3BvbnNlIGV4dGVuZHMgUmVzcG9uc2Uge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgVFlQRSA9IFwiZXJyb3JyZXNwb25zZVwiO1xyXG5cclxuICAgIHByaXZhdGUgX2Vycm9yTWVzc2FnZTogc3RyaW5nO1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iamVjdDogYW55KTogRXJyb3JSZXNwb25zZSB7XHJcbiAgICAgICAgaWYgKCBqc29uT2JqZWN0LnR5cGUgIT09IEVycm9yUmVzcG9uc2UuVFlQRSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORm9yUmVzcG9uc2VFcnJvcigpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3QuZXJyb3JNZXNzYWdlICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHJlc3BvbnNlID0gbmV3IEVycm9yUmVzcG9uc2UoanNvbk9iamVjdC5lcnJvck1lc3NhZ2UpO1xyXG4gICAgICAgICAgICBSZXNwb25zZS5wb3B1bGF0ZUpTT04ocmVzcG9uc2UsIGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGVycm9yTWVzc2FnZTogc3RyaW5nKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICB0aGlzLl9lcnJvck1lc3NhZ2UgPSBlcnJvck1lc3NhZ2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gdGhlIHJlc3VsdC5cclxuICAgICAqL1xyXG4gICAgZ2V0IGVycm9yTWVzc2FnZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9lcnJvck1lc3NhZ2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUeXBlIG9mIHJlc3BvbnNlLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgZ2V0IHR5cGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gRXJyb3JSZXNwb25zZS5UWVBFO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGV0ZXJtaW5lIGlmIHRoaXMgcmVxdWVzdCBpcyB0byBiZSBicm9hZGNhc3QvcHJvY2Vzc2VkIHRvIGV2ZXJ5b25lXHJcbiAgICAgKiBjb25uZWN0ZWQgdG8gdGhlIHNlcnZlciAoYW5kIGFsbCBvdGhlciBzZXJ2ZXJzIGluIHRoZSBmYXJtKS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGlzQnJvYWRjYXN0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyByZXNwb25zZSBvYmplY3QgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICByZXN1bHQudHlwZSA9IEVycm9yUmVzcG9uc2UuVFlQRTtcclxuICAgICAgICByZXN1bHQuZXJyb3JNZXNzYWdlID0gdGhpcy5fZXJyb3JNZXNzYWdlO1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBFcnJvclJlc3BvbnNlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
