"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usermetadata_1 = require("../../model/usermetadata");
var ticket_1 = require("../../model/ticket");
var response_1 = require("./response");
var authenticatedresponse_1 = require("./authenticatedresponse");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
var RemoveAvatarResponse = /** @class */ (function (_super) {
    __extends(RemoveAvatarResponse, _super);
    function RemoveAvatarResponse(userMetaData, ticket, objectModelId, result) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        if (objectModelId) {
            _this._result = !!result;
            _this._objectModelId = objectModelId;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError();
        }
        return _this;
    }
    RemoveAvatarResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== RemoveAvatarResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket && jsonObject.objectModelId) {
            var result = !!jsonObject.result;
            var usermetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var objectModelId = jsonObject.objectModelId;
            var response = new RemoveAvatarResponse(usermetaData, ticket, objectModelId, result);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.default();
        }
    };
    Object.defineProperty(RemoveAvatarResponse.prototype, "result", {
        /**
         * Flag to determine whether avatar was removed.
         */
        get: function () {
            return this._result;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RemoveAvatarResponse.prototype, "objectModelId", {
        /**
         * Return the object model id of the avatar to be removed.
         */
        get: function () {
            return this._objectModelId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RemoveAvatarResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return RemoveAvatarResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    RemoveAvatarResponse.prototype.isBroadcast = function () {
        return true;
    };
    /**
     * Convert this response object to JSON.
     */
    RemoveAvatarResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.result = this._result;
        result.objectModelId = this._objectModelId;
        result.type = RemoveAvatarResponse.TYPE;
        return result;
    };
    RemoveAvatarResponse.TYPE = "removeavatarresponse";
    return RemoveAvatarResponse;
}(authenticatedresponse_1.default));
exports.RemoveAvatarResponse = RemoveAvatarResponse;
exports.default = RemoveAvatarResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL3JlbW92ZWF2YXRhcnJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHlEQUFvRDtBQUNwRCw2Q0FBd0M7QUFDeEMsdUNBQWtDO0FBQ2xDLGlFQUE0RDtBQUM1RCxpRUFBNEQ7QUFDNUQsdUZBQWtGO0FBQ2xGLCtGQUE4RjtBQUU5RjtJQUEwQyx3Q0FBcUI7SUF5QjNELDhCQUFZLFlBQTBCLEVBQUUsTUFBYyxFQUFFLGFBQXFCLEVBQUUsTUFBZTtRQUE5RixZQUNJLGtCQUFNLFlBQVksRUFBRSxNQUFNLENBQUMsU0FPOUI7UUFORyxFQUFFLENBQUMsQ0FBRSxhQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ2xCLEtBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUN4QixLQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQztRQUN4QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksaUVBQStCLEVBQUUsQ0FBQztRQUNoRCxDQUFDOztJQUNMLENBQUM7SUExQmEsNkJBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUNsQyxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsSUFBSSxLQUFLLG9CQUFvQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDbEQsTUFBTSxJQUFJLHFDQUEyQixFQUFFLENBQUM7UUFDNUMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsWUFBWSxJQUFJLFVBQVUsQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFFbEYsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUM7WUFDakMsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xFLElBQUksTUFBTSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxJQUFJLGFBQWEsR0FBVyxVQUFVLENBQUMsYUFBYSxDQUFDO1lBQ3JELElBQUksUUFBUSxHQUFHLElBQUksb0JBQW9CLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDckYsa0JBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFFcEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztJQUNMLENBQUM7SUFlRCxzQkFBSSx3Q0FBTTtRQUhWOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQUtELHNCQUFJLCtDQUFhO1FBSGpCOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUMvQixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLHNDQUFJO1FBSlI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO1FBQ3JDLENBQUM7OztPQUFBO0lBRUQ7OztPQUdHO0lBQ0ksMENBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNJLHFDQUFNLEdBQWI7UUFDSSxJQUFJLE1BQU0sR0FBNEIsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDckQsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUMzQyxNQUFNLENBQUMsSUFBSSxHQUFHLG9CQUFvQixDQUFDLElBQUksQ0FBQztRQUN4QyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUF4RWEseUJBQUksR0FBRyxzQkFBc0IsQ0FBQztJQTBFaEQsMkJBQUM7Q0E1RUQsQUE0RUMsQ0E1RXlDLCtCQUFxQixHQTRFOUQ7QUE1RVksb0RBQW9CO0FBOEVqQyxrQkFBZSxvQkFBb0IsQ0FBQyIsImZpbGUiOiJwcm90b2NvbC9yZXNwb25zZS9yZW1vdmVhdmF0YXJyZXNwb25zZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBVc2VyTWV0YURhdGEgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJtZXRhZGF0YVwiO1xyXG5pbXBvcnQgVGlja2V0IGZyb20gXCIuLi8uLi9tb2RlbC90aWNrZXRcIjtcclxuaW1wb3J0IFJlc3BvbnNlIGZyb20gXCIuL3Jlc3BvbnNlXCI7XHJcbmltcG9ydCBBdXRoZW50aWNhdGVkUmVzcG9uc2UgZnJvbSBcIi4vYXV0aGVudGljYXRlZHJlc3BvbnNlXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmVycm9yXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkZvclJlc3BvbnNlRXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVzcG9uc2VlcnJvclwiO1xyXG5pbXBvcnQgeyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIH0gZnJvbSBcIi4uLy4uL2Vycm9yL21pc3Npbmdjb25zdHJ1Y3RvcmFyZ3VtZW50ZXJyb3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBSZW1vdmVBdmF0YXJSZXNwb25zZSBleHRlbmRzIEF1dGhlbnRpY2F0ZWRSZXNwb25zZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJyZW1vdmVhdmF0YXJyZXNwb25zZVwiO1xyXG5cclxuICAgIHByaXZhdGUgX3Jlc3VsdDogYm9vbGVhbjtcclxuICAgIHByaXZhdGUgX29iamVjdE1vZGVsSWQ6IHN0cmluZztcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSk6IFJlbW92ZUF2YXRhclJlc3BvbnNlIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gUmVtb3ZlQXZhdGFyUmVzcG9uc2UuVFlQRSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORm9yUmVzcG9uc2VFcnJvcigpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoanNvbk9iamVjdC51c2VyTWV0YURhdGEgJiYganNvbk9iamVjdC50aWNrZXQgJiYganNvbk9iamVjdC5vYmplY3RNb2RlbElkKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgcmVzdWx0ID0gISFqc29uT2JqZWN0LnJlc3VsdDtcclxuICAgICAgICAgICAgbGV0IHVzZXJtZXRhRGF0YSA9IFVzZXJNZXRhRGF0YS5mcm9tSlNPTihqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSk7XHJcbiAgICAgICAgICAgIGxldCB0aWNrZXQgPSBUaWNrZXQuZnJvbUpTT04oanNvbk9iamVjdC50aWNrZXQpO1xyXG4gICAgICAgICAgICBsZXQgb2JqZWN0TW9kZWxJZDogc3RyaW5nID0ganNvbk9iamVjdC5vYmplY3RNb2RlbElkO1xyXG4gICAgICAgICAgICBsZXQgcmVzcG9uc2UgPSBuZXcgUmVtb3ZlQXZhdGFyUmVzcG9uc2UodXNlcm1ldGFEYXRhLCB0aWNrZXQsIG9iamVjdE1vZGVsSWQsIHJlc3VsdCk7XHJcbiAgICAgICAgICAgIFJlc3BvbnNlLnBvcHVsYXRlSlNPTihyZXNwb25zZSwganNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IodXNlck1ldGFEYXRhOiBVc2VyTWV0YURhdGEsIHRpY2tldDogVGlja2V0LCBvYmplY3RNb2RlbElkOiBzdHJpbmcsIHJlc3VsdDogYm9vbGVhbikge1xyXG4gICAgICAgIHN1cGVyKHVzZXJNZXRhRGF0YSwgdGlja2V0KTtcclxuICAgICAgICBpZiAoIG9iamVjdE1vZGVsSWQgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jlc3VsdCA9ICEhcmVzdWx0O1xyXG4gICAgICAgICAgICB0aGlzLl9vYmplY3RNb2RlbElkID0gb2JqZWN0TW9kZWxJZDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvcigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZsYWcgdG8gZGV0ZXJtaW5lIHdoZXRoZXIgYXZhdGFyIHdhcyByZW1vdmVkLlxyXG4gICAgICovXHJcbiAgICBnZXQgcmVzdWx0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9yZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gdGhlIG9iamVjdCBtb2RlbCBpZCBvZiB0aGUgYXZhdGFyIHRvIGJlIHJlbW92ZWQuXHJcbiAgICAgKi9cclxuICAgIGdldCBvYmplY3RNb2RlbElkKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX29iamVjdE1vZGVsSWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUeXBlIG9mIHJlc3BvbnNlLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgZ2V0IHR5cGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gUmVtb3ZlQXZhdGFyUmVzcG9uc2UuVFlQRTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERldGVybWluZSBpZiB0aGlzIHJlcXVlc3QgaXMgdG8gYmUgYnJvYWRjYXN0L3Byb2Nlc3NlZCB0byBldmVyeW9uZVxyXG4gICAgICogY29ubmVjdGVkIHRvIHRoZSBzZXJ2ZXIgKGFuZCBhbGwgb3RoZXIgc2VydmVycyBpbiB0aGUgZmFybSkuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBpc0Jyb2FkY2FzdCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyByZXNwb25zZSBvYmplY3QgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICByZXN1bHQucmVzdWx0ID0gdGhpcy5fcmVzdWx0O1xyXG4gICAgICAgIHJlc3VsdC5vYmplY3RNb2RlbElkID0gdGhpcy5fb2JqZWN0TW9kZWxJZDtcclxuICAgICAgICByZXN1bHQudHlwZSA9IFJlbW92ZUF2YXRhclJlc3BvbnNlLlRZUEU7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFJlbW92ZUF2YXRhclJlc3BvbnNlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
