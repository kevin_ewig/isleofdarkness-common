"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ticket_1 = require("../../model/ticket");
var response_1 = require("./response");
var usermetadata_1 = require("../../model/usermetadata");
var authenticatedresponse_1 = require("./authenticatedresponse");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var RegisterUserResponse = /** @class */ (function (_super) {
    __extends(RegisterUserResponse, _super);
    function RegisterUserResponse(userMetaData, ticket, result) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        _this._result = result;
        return _this;
    }
    RegisterUserResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== RegisterUserResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var result = jsonObject.result;
            var response = new RegisterUserResponse(userMetaData, ticket, result);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.InvalidJSONError();
        }
    };
    Object.defineProperty(RegisterUserResponse.prototype, "result", {
        /**
         * Get result of the registration.
         * @param user
         */
        get: function () {
            return this._result;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RegisterUserResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return RegisterUserResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    RegisterUserResponse.prototype.isBroadcast = function () {
        return false;
    };
    /**
     * Convert this response object to JSON.
     */
    RegisterUserResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.type = RegisterUserResponse.TYPE;
        result.result = this._result;
        return result;
    };
    RegisterUserResponse.TYPE = "registeruserresponse";
    return RegisterUserResponse;
}(authenticatedresponse_1.default));
exports.RegisterUserResponse = RegisterUserResponse;
exports.default = RegisterUserResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL3JlZ2lzdGVydXNlcnJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUNBLDZDQUF3QztBQUN4Qyx1Q0FBa0M7QUFDbEMseURBQW9EO0FBQ3BELGlFQUE0RDtBQUM1RCx1RkFBa0Y7QUFDbEYsaUVBQWdFO0FBRWhFO0lBQTBDLHdDQUFxQjtJQXVCM0QsOEJBQVksWUFBMEIsRUFBRSxNQUFjLEVBQUUsTUFBZTtRQUF2RSxZQUNJLGtCQUFNLFlBQVksRUFBRSxNQUFNLENBQUMsU0FFOUI7UUFERyxLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzs7SUFDMUIsQ0FBQztJQXBCYSw2QkFBUSxHQUF0QixVQUF1QixVQUFlO1FBQ2xDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxJQUFJLEtBQUssb0JBQW9CLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNsRCxNQUFNLElBQUkscUNBQTJCLEVBQUUsQ0FBQztRQUM1QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxZQUFZLElBQUksVUFBVSxDQUFDLE1BQU8sQ0FBQyxDQUFDLENBQUM7WUFFeEQsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xFLElBQUksTUFBTSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxJQUFJLE1BQU0sR0FBWSxVQUFVLENBQUMsTUFBTSxDQUFDO1lBQ3hDLElBQUksUUFBUSxHQUFHLElBQUksb0JBQW9CLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUN0RSxrQkFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDNUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUVwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksbUNBQWdCLEVBQUUsQ0FBQztRQUNqQyxDQUFDO0lBQ0wsQ0FBQztJQVdELHNCQUFJLHdDQUFNO1FBSlY7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLHNDQUFJO1FBSlI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO1FBQ3JDLENBQUM7OztPQUFBO0lBRUQ7OztPQUdHO0lBQ0ksMENBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRDs7T0FFRztJQUNJLHFDQUFNLEdBQWI7UUFDSSxJQUFJLE1BQU0sR0FBNEIsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDckQsTUFBTSxDQUFDLElBQUksR0FBRyxvQkFBb0IsQ0FBQyxJQUFJLENBQUM7UUFDeEMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQTFEYSx5QkFBSSxHQUFHLHNCQUFzQixDQUFDO0lBNERoRCwyQkFBQztDQTlERCxBQThEQyxDQTlEeUMsK0JBQXFCLEdBOEQ5RDtBQTlEWSxvREFBb0I7QUFnRWpDLGtCQUFlLG9CQUFvQixDQUFDIiwiZmlsZSI6InByb3RvY29sL3Jlc3BvbnNlL3JlZ2lzdGVydXNlcnJlc3BvbnNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVzZXIgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJcIjtcclxuaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZVwiO1xyXG5pbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuLi8uLi9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IEF1dGhlbnRpY2F0ZWRSZXNwb25zZSBmcm9tIFwiLi9hdXRoZW50aWNhdGVkcmVzcG9uc2VcIjtcclxuaW1wb3J0IEludmFsaWRKU09ORm9yUmVzcG9uc2VFcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25mb3JyZXNwb25zZWVycm9yXCI7XHJcbmltcG9ydCB7IEludmFsaWRKU09ORXJyb3IgfSBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIFJlZ2lzdGVyVXNlclJlc3BvbnNlIGV4dGVuZHMgQXV0aGVudGljYXRlZFJlc3BvbnNlIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcInJlZ2lzdGVydXNlcnJlc3BvbnNlXCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVzdWx0OiBib29sZWFuO1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iamVjdDogYW55KTogUmVnaXN0ZXJVc2VyUmVzcG9uc2Uge1xyXG4gICAgICAgIGlmICgganNvbk9iamVjdC50eXBlICE9PSBSZWdpc3RlclVzZXJSZXNwb25zZS5UWVBFICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdC51c2VyTWV0YURhdGEgJiYganNvbk9iamVjdC50aWNrZXQgKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgdXNlck1ldGFEYXRhID0gVXNlck1ldGFEYXRhLmZyb21KU09OKGpzb25PYmplY3QudXNlck1ldGFEYXRhKTtcclxuICAgICAgICAgICAgbGV0IHRpY2tldCA9IFRpY2tldC5mcm9tSlNPTihqc29uT2JqZWN0LnRpY2tldCk7XHJcbiAgICAgICAgICAgIGxldCByZXN1bHQ6IGJvb2xlYW4gPSBqc29uT2JqZWN0LnJlc3VsdDtcclxuICAgICAgICAgICAgbGV0IHJlc3BvbnNlID0gbmV3IFJlZ2lzdGVyVXNlclJlc3BvbnNlKHVzZXJNZXRhRGF0YSwgdGlja2V0LCByZXN1bHQpO1xyXG4gICAgICAgICAgICBSZXNwb25zZS5wb3B1bGF0ZUpTT04ocmVzcG9uc2UsIGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgcmVzdWx0OiBib29sZWFuKSB7XHJcbiAgICAgICAgc3VwZXIodXNlck1ldGFEYXRhLCB0aWNrZXQpO1xyXG4gICAgICAgIHRoaXMuX3Jlc3VsdCA9IHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCByZXN1bHQgb2YgdGhlIHJlZ2lzdHJhdGlvbi5cclxuICAgICAqIEBwYXJhbSB1c2VyXHJcbiAgICAgKi9cclxuICAgIGdldCByZXN1bHQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3Jlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFR5cGUgb2YgcmVzcG9uc2UuXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBnZXQgdHlwZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBSZWdpc3RlclVzZXJSZXNwb25zZS5UWVBFO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGV0ZXJtaW5lIGlmIHRoaXMgcmVxdWVzdCBpcyB0byBiZSBicm9hZGNhc3QvcHJvY2Vzc2VkIHRvIGV2ZXJ5b25lXHJcbiAgICAgKiBjb25uZWN0ZWQgdG8gdGhlIHNlcnZlciAoYW5kIGFsbCBvdGhlciBzZXJ2ZXJzIGluIHRoZSBmYXJtKS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGlzQnJvYWRjYXN0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyByZXNwb25zZSBvYmplY3QgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICByZXN1bHQudHlwZSA9IFJlZ2lzdGVyVXNlclJlc3BvbnNlLlRZUEU7XHJcbiAgICAgICAgcmVzdWx0LnJlc3VsdCA9IHRoaXMuX3Jlc3VsdDtcclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUmVnaXN0ZXJVc2VyUmVzcG9uc2U7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
