import Response from "./response";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

export class ErrorResponse extends Response {

    public static TYPE = "errorresponse";

    private _errorMessage: string;

    public static fromJSON(jsonObject: any): ErrorResponse {
        if ( jsonObject.type !== ErrorResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if ( jsonObject.errorMessage ) {

            let response = new ErrorResponse(jsonObject.errorMessage);
            Response.populateJSON(response, jsonObject);
            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(errorMessage: string) {
        super();
        this._errorMessage = errorMessage;
    }

    /**
     * Return the result.
     */
    get errorMessage(): string {
        return this._errorMessage;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return ErrorResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return false;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.type = ErrorResponse.TYPE;
        result.errorMessage = this._errorMessage;
        return result;
    }

}

export default ErrorResponse;
