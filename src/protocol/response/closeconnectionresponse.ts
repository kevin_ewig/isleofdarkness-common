import User from "../../model/user";
import Ticket from "../../model/ticket";
import Response from "./response";
import UserMetaData from "../../model/usermetadata";
import AuthenticatedResponse from "./authenticatedresponse";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

export class CloseConnectionResponse extends AuthenticatedResponse {

    public static TYPE = "closeconnectionresponse";

    private _result: boolean;

    public static fromJSON(jsonObject: any): CloseConnectionResponse {
        if ( jsonObject.type !== CloseConnectionResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let result: boolean = jsonObject.result;
            let response = new CloseConnectionResponse(userMetaData, ticket, result);
            Response.populateJSON(response, jsonObject);
            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, result: boolean) {
        super(userMetaData, ticket);
        this._result = result;
    }

    /**
     * Get result of the logout.
     * @param user
     */
    get result(): boolean {
        return this._result;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return CloseConnectionResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return false;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.type = CloseConnectionResponse.TYPE;
        result.result = this._result;
        return result;
    }

}

export default CloseConnectionResponse;
