"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usermetadata_1 = require("../../model/usermetadata");
var ticket_1 = require("../../model/ticket");
var response_1 = require("./response");
var authenticatedresponse_1 = require("./authenticatedresponse");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var AddObjectModelResponse = /** @class */ (function (_super) {
    __extends(AddObjectModelResponse, _super);
    function AddObjectModelResponse(userMetaData, ticket, result) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        _this._result = result;
        return _this;
    }
    AddObjectModelResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== AddObjectModelResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.result && jsonObject.userMetaData && jsonObject.ticket) {
            var result = jsonObject.result;
            var usermetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var response = new AddObjectModelResponse(usermetaData, ticket, result);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.default();
        }
    };
    Object.defineProperty(AddObjectModelResponse.prototype, "result", {
        /**
         * Result of adding an object to the world.
         */
        get: function () {
            return this._result;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AddObjectModelResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return AddObjectModelResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    AddObjectModelResponse.prototype.isBroadcast = function () {
        return true;
    };
    /**
     * Convert this response object to JSON.
     */
    AddObjectModelResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.result = this._result;
        result.type = AddObjectModelResponse.TYPE;
        return result;
    };
    AddObjectModelResponse.TYPE = "addobjectmodelresponse";
    return AddObjectModelResponse;
}(authenticatedresponse_1.default));
exports.AddObjectModelResponse = AddObjectModelResponse;
exports.default = AddObjectModelResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL2FkZG9iamVjdG1vZGVscmVzcG9uc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEseURBQW9EO0FBQ3BELDZDQUF3QztBQUN4Qyx1Q0FBa0M7QUFDbEMsaUVBQTREO0FBQzVELGlFQUE0RDtBQUU1RCx1RkFBa0Y7QUFFbEY7SUFBNEMsMENBQXFCO0lBdUI3RCxnQ0FBWSxZQUEwQixFQUFFLE1BQWMsRUFBRSxNQUFlO1FBQXZFLFlBQ0ksa0JBQU0sWUFBWSxFQUFFLE1BQU0sQ0FBQyxTQUU5QjtRQURHLEtBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDOztJQUMxQixDQUFDO0lBcEJhLCtCQUFRLEdBQXRCLFVBQXVCLFVBQWU7UUFDbEMsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLElBQUksS0FBSyxzQkFBc0IsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3BELE1BQU0sSUFBSSxxQ0FBMkIsRUFBRSxDQUFDO1FBQzVDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLE1BQU0sSUFBSSxVQUFVLENBQUMsWUFBWSxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBRTNFLElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7WUFDL0IsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xFLElBQUksTUFBTSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxJQUFJLFFBQVEsR0FBRyxJQUFJLHNCQUFzQixDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDeEUsa0JBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFFcEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztJQUNMLENBQUM7SUFVRCxzQkFBSSwwQ0FBTTtRQUhWOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLHdDQUFJO1FBSlI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDO1FBQ3ZDLENBQUM7OztPQUFBO0lBRUQ7OztPQUdHO0lBQ0ksNENBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNJLHVDQUFNLEdBQWI7UUFDSSxJQUFJLE1BQU0sR0FBNEIsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDckQsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsc0JBQXNCLENBQUMsSUFBSSxDQUFDO1FBQzFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQXpEYSwyQkFBSSxHQUFHLHdCQUF3QixDQUFDO0lBMkRsRCw2QkFBQztDQTdERCxBQTZEQyxDQTdEMkMsK0JBQXFCLEdBNkRoRTtBQTdEWSx3REFBc0I7QUErRG5DLGtCQUFlLHNCQUFzQixDQUFDIiwiZmlsZSI6InByb3RvY29sL3Jlc3BvbnNlL2FkZG9iamVjdG1vZGVscmVzcG9uc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuLi8uLi9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZVwiO1xyXG5pbXBvcnQgQXV0aGVudGljYXRlZFJlc3BvbnNlIGZyb20gXCIuL2F1dGhlbnRpY2F0ZWRyZXNwb25zZVwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvbWlzc2luZ2NvbnN0cnVjdG9yYXJndW1lbnRlcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmZvcnJlc3BvbnNlZXJyb3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBBZGRPYmplY3RNb2RlbFJlc3BvbnNlIGV4dGVuZHMgQXV0aGVudGljYXRlZFJlc3BvbnNlIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcImFkZG9iamVjdG1vZGVscmVzcG9uc2VcIjtcclxuXHJcbiAgICBwcml2YXRlIF9yZXN1bHQ6IGJvb2xlYW47XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqZWN0OiBhbnkpOiBBZGRPYmplY3RNb2RlbFJlc3BvbnNlIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gQWRkT2JqZWN0TW9kZWxSZXNwb25zZS5UWVBFICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChqc29uT2JqZWN0LnJlc3VsdCAmJiBqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSAmJiBqc29uT2JqZWN0LnRpY2tldCkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHJlc3VsdCA9IGpzb25PYmplY3QucmVzdWx0O1xyXG4gICAgICAgICAgICBsZXQgdXNlcm1ldGFEYXRhID0gVXNlck1ldGFEYXRhLmZyb21KU09OKGpzb25PYmplY3QudXNlck1ldGFEYXRhKTtcclxuICAgICAgICAgICAgbGV0IHRpY2tldCA9IFRpY2tldC5mcm9tSlNPTihqc29uT2JqZWN0LnRpY2tldCk7XHJcbiAgICAgICAgICAgIGxldCByZXNwb25zZSA9IG5ldyBBZGRPYmplY3RNb2RlbFJlc3BvbnNlKHVzZXJtZXRhRGF0YSwgdGlja2V0LCByZXN1bHQpO1xyXG4gICAgICAgICAgICBSZXNwb25zZS5wb3B1bGF0ZUpTT04ocmVzcG9uc2UsIGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgcmVzdWx0OiBib29sZWFuKSB7XHJcbiAgICAgICAgc3VwZXIodXNlck1ldGFEYXRhLCB0aWNrZXQpO1xyXG4gICAgICAgIHRoaXMuX3Jlc3VsdCA9IHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlc3VsdCBvZiBhZGRpbmcgYW4gb2JqZWN0IHRvIHRoZSB3b3JsZC5cclxuICAgICAqL1xyXG4gICAgZ2V0IHJlc3VsdCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHlwZSBvZiByZXNwb25zZS5cclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICAgKi9cclxuICAgIGdldCB0eXBlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEFkZE9iamVjdE1vZGVsUmVzcG9uc2UuVFlQRTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERldGVybWluZSBpZiB0aGlzIHJlcXVlc3QgaXMgdG8gYmUgYnJvYWRjYXN0L3Byb2Nlc3NlZCB0byBldmVyeW9uZVxyXG4gICAgICogY29ubmVjdGVkIHRvIHRoZSBzZXJ2ZXIgKGFuZCBhbGwgb3RoZXIgc2VydmVycyBpbiB0aGUgZmFybSkuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBpc0Jyb2FkY2FzdCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyByZXNwb25zZSBvYmplY3QgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICByZXN1bHQucmVzdWx0ID0gdGhpcy5fcmVzdWx0O1xyXG4gICAgICAgIHJlc3VsdC50eXBlID0gQWRkT2JqZWN0TW9kZWxSZXNwb25zZS5UWVBFO1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBBZGRPYmplY3RNb2RlbFJlc3BvbnNlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
