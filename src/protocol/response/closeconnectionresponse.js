"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ticket_1 = require("../../model/ticket");
var response_1 = require("./response");
var usermetadata_1 = require("../../model/usermetadata");
var authenticatedresponse_1 = require("./authenticatedresponse");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var CloseConnectionResponse = /** @class */ (function (_super) {
    __extends(CloseConnectionResponse, _super);
    function CloseConnectionResponse(userMetaData, ticket, result) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        _this._result = result;
        return _this;
    }
    CloseConnectionResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== CloseConnectionResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var result = jsonObject.result;
            var response = new CloseConnectionResponse(userMetaData, ticket, result);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.default();
        }
    };
    Object.defineProperty(CloseConnectionResponse.prototype, "result", {
        /**
         * Get result of the logout.
         * @param user
         */
        get: function () {
            return this._result;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CloseConnectionResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return CloseConnectionResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    CloseConnectionResponse.prototype.isBroadcast = function () {
        return false;
    };
    /**
     * Convert this response object to JSON.
     */
    CloseConnectionResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.type = CloseConnectionResponse.TYPE;
        result.result = this._result;
        return result;
    };
    CloseConnectionResponse.TYPE = "closeconnectionresponse";
    return CloseConnectionResponse;
}(authenticatedresponse_1.default));
exports.CloseConnectionResponse = CloseConnectionResponse;
exports.default = CloseConnectionResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL2Nsb3NlY29ubmVjdGlvbnJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUNBLDZDQUF3QztBQUN4Qyx1Q0FBa0M7QUFDbEMseURBQW9EO0FBQ3BELGlFQUE0RDtBQUM1RCxpRUFBNEQ7QUFFNUQsdUZBQWtGO0FBRWxGO0lBQTZDLDJDQUFxQjtJQXVCOUQsaUNBQVksWUFBMEIsRUFBRSxNQUFjLEVBQUUsTUFBZTtRQUF2RSxZQUNJLGtCQUFNLFlBQVksRUFBRSxNQUFNLENBQUMsU0FFOUI7UUFERyxLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzs7SUFDMUIsQ0FBQztJQXBCYSxnQ0FBUSxHQUF0QixVQUF1QixVQUFlO1FBQ2xDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxJQUFJLEtBQUssdUJBQXVCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNyRCxNQUFNLElBQUkscUNBQTJCLEVBQUUsQ0FBQztRQUM1QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxZQUFZLElBQUksVUFBVSxDQUFDLE1BQU8sQ0FBQyxDQUFDLENBQUM7WUFFeEQsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xFLElBQUksTUFBTSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxJQUFJLE1BQU0sR0FBWSxVQUFVLENBQUMsTUFBTSxDQUFDO1lBQ3hDLElBQUksUUFBUSxHQUFHLElBQUksdUJBQXVCLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUN6RSxrQkFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDNUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUVwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksMEJBQWdCLEVBQUUsQ0FBQztRQUNqQyxDQUFDO0lBQ0wsQ0FBQztJQVdELHNCQUFJLDJDQUFNO1FBSlY7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLHlDQUFJO1FBSlI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDO1FBQ3hDLENBQUM7OztPQUFBO0lBRUQ7OztPQUdHO0lBQ0ksNkNBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRDs7T0FFRztJQUNJLHdDQUFNLEdBQWI7UUFDSSxJQUFJLE1BQU0sR0FBNEIsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDckQsTUFBTSxDQUFDLElBQUksR0FBRyx1QkFBdUIsQ0FBQyxJQUFJLENBQUM7UUFDM0MsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQTFEYSw0QkFBSSxHQUFHLHlCQUF5QixDQUFDO0lBNERuRCw4QkFBQztDQTlERCxBQThEQyxDQTlENEMsK0JBQXFCLEdBOERqRTtBQTlEWSwwREFBdUI7QUFnRXBDLGtCQUFlLHVCQUF1QixDQUFDIiwiZmlsZSI6InByb3RvY29sL3Jlc3BvbnNlL2Nsb3NlY29ubmVjdGlvbnJlc3BvbnNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVzZXIgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJcIjtcclxuaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZVwiO1xyXG5pbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuLi8uLi9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IEF1dGhlbnRpY2F0ZWRSZXNwb25zZSBmcm9tIFwiLi9hdXRoZW50aWNhdGVkcmVzcG9uc2VcIjtcclxuaW1wb3J0IEludmFsaWRKU09ORXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZXJyb3JcIjtcclxuaW1wb3J0IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL21pc3Npbmdjb25zdHJ1Y3RvcmFyZ3VtZW50ZXJyb3JcIjtcclxuaW1wb3J0IEludmFsaWRKU09ORm9yUmVzcG9uc2VFcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25mb3JyZXNwb25zZWVycm9yXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2xvc2VDb25uZWN0aW9uUmVzcG9uc2UgZXh0ZW5kcyBBdXRoZW50aWNhdGVkUmVzcG9uc2Uge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgVFlQRSA9IFwiY2xvc2Vjb25uZWN0aW9ucmVzcG9uc2VcIjtcclxuXHJcbiAgICBwcml2YXRlIF9yZXN1bHQ6IGJvb2xlYW47XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqZWN0OiBhbnkpOiBDbG9zZUNvbm5lY3Rpb25SZXNwb25zZSB7XHJcbiAgICAgICAgaWYgKCBqc29uT2JqZWN0LnR5cGUgIT09IENsb3NlQ29ubmVjdGlvblJlc3BvbnNlLlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkZvclJlc3BvbnNlRXJyb3IoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCBqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSAmJiBqc29uT2JqZWN0LnRpY2tldCApIHtcclxuXHJcbiAgICAgICAgICAgIGxldCB1c2VyTWV0YURhdGEgPSBVc2VyTWV0YURhdGEuZnJvbUpTT04oanNvbk9iamVjdC51c2VyTWV0YURhdGEpO1xyXG4gICAgICAgICAgICBsZXQgdGlja2V0ID0gVGlja2V0LmZyb21KU09OKGpzb25PYmplY3QudGlja2V0KTtcclxuICAgICAgICAgICAgbGV0IHJlc3VsdDogYm9vbGVhbiA9IGpzb25PYmplY3QucmVzdWx0O1xyXG4gICAgICAgICAgICBsZXQgcmVzcG9uc2UgPSBuZXcgQ2xvc2VDb25uZWN0aW9uUmVzcG9uc2UodXNlck1ldGFEYXRhLCB0aWNrZXQsIHJlc3VsdCk7XHJcbiAgICAgICAgICAgIFJlc3BvbnNlLnBvcHVsYXRlSlNPTihyZXNwb25zZSwganNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IodXNlck1ldGFEYXRhOiBVc2VyTWV0YURhdGEsIHRpY2tldDogVGlja2V0LCByZXN1bHQ6IGJvb2xlYW4pIHtcclxuICAgICAgICBzdXBlcih1c2VyTWV0YURhdGEsIHRpY2tldCk7XHJcbiAgICAgICAgdGhpcy5fcmVzdWx0ID0gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHJlc3VsdCBvZiB0aGUgbG9nb3V0LlxyXG4gICAgICogQHBhcmFtIHVzZXJcclxuICAgICAqL1xyXG4gICAgZ2V0IHJlc3VsdCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHlwZSBvZiByZXNwb25zZS5cclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICAgKi9cclxuICAgIGdldCB0eXBlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIENsb3NlQ29ubmVjdGlvblJlc3BvbnNlLlRZUEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmUgaWYgdGhpcyByZXF1ZXN0IGlzIHRvIGJlIGJyb2FkY2FzdC9wcm9jZXNzZWQgdG8gZXZlcnlvbmVcclxuICAgICAqIGNvbm5lY3RlZCB0byB0aGUgc2VydmVyIChhbmQgYWxsIG90aGVyIHNlcnZlcnMgaW4gdGhlIGZhcm0pLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgaXNCcm9hZGNhc3QoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB0aGlzIHJlc3BvbnNlIG9iamVjdCB0byBKU09OLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHsgW2tleTogc3RyaW5nXTogYW55OyB9IHtcclxuICAgICAgICBsZXQgcmVzdWx0OiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIHJlc3VsdC50eXBlID0gQ2xvc2VDb25uZWN0aW9uUmVzcG9uc2UuVFlQRTtcclxuICAgICAgICByZXN1bHQucmVzdWx0ID0gdGhpcy5fcmVzdWx0O1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBDbG9zZUNvbm5lY3Rpb25SZXNwb25zZTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
