"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var authenticatedresponse_1 = require("./authenticatedresponse");
var usermetadata_1 = require("../../model/usermetadata");
var ticket_1 = require("../../model/ticket");
var response_1 = require("./response");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforresponseerror_1 = require("../../error/invalidjsonforresponseerror");
var BroadcastMessageResponse = /** @class */ (function (_super) {
    __extends(BroadcastMessageResponse, _super);
    function BroadcastMessageResponse(userMetaData, ticket, message) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        _this._message = message;
        return _this;
    }
    /**
     * Convert a JSON object to a response object.
     * @param jsonObject
     */
    BroadcastMessageResponse.fromJSON = function (jsonObject) {
        if (jsonObject.type !== BroadcastMessageResponse.TYPE) {
            throw new invalidjsonforresponseerror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket && jsonObject.message) {
            var message = jsonObject.message;
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var response = new BroadcastMessageResponse(userMetaData, ticket, message);
            response_1.default.populateJSON(response, jsonObject);
            return response;
        }
        else {
            throw new invalidjsonerror_1.default();
        }
    };
    Object.defineProperty(BroadcastMessageResponse.prototype, "message", {
        /**
         * Get message to be broadcast.
         * @returns {string}
         */
        get: function () {
            return this._message;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BroadcastMessageResponse.prototype, "type", {
        /**
         * Type of response.
         * @returns {string}
         */
        get: function () {
            return BroadcastMessageResponse.TYPE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    BroadcastMessageResponse.prototype.isBroadcast = function () {
        return true;
    };
    /**
     * Convert this response object to JSON.
     */
    BroadcastMessageResponse.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        result.message = this._message;
        result.type = BroadcastMessageResponse.TYPE;
        return result;
    };
    BroadcastMessageResponse.TYPE = "broadcastmessageresponse";
    return BroadcastMessageResponse;
}(authenticatedresponse_1.default));
exports.BroadcastMessageResponse = BroadcastMessageResponse;
exports.default = BroadcastMessageResponse;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL2Jyb2FkY2FzdG1lc3NhZ2VyZXNwb25zZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxpRUFBNEQ7QUFDNUQseURBQW9EO0FBQ3BELDZDQUF3QztBQUN4Qyx1Q0FBa0M7QUFDbEMsaUVBQTREO0FBRTVELHVGQUFrRjtBQUVsRjtJQUE4Qyw0Q0FBcUI7SUEyQi9ELGtDQUFZLFlBQTBCLEVBQUUsTUFBYyxFQUFFLE9BQWU7UUFBdkUsWUFDSSxrQkFBTSxZQUFZLEVBQUUsTUFBTSxDQUFDLFNBRTlCO1FBREcsS0FBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7O0lBQzVCLENBQUM7SUF4QkQ7OztPQUdHO0lBQ1csaUNBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUNsQyxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsSUFBSSxLQUFLLHdCQUF3QixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDdEQsTUFBTSxJQUFJLHFDQUEyQixFQUFFLENBQUM7UUFDNUMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsWUFBWSxJQUFJLFVBQVUsQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLE9BQVEsQ0FBQyxDQUFDLENBQUM7WUFFOUUsSUFBSSxPQUFPLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQztZQUNqQyxJQUFJLFlBQVksR0FBRyxzQkFBWSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEUsSUFBSSxNQUFNLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELElBQUksUUFBUSxHQUFHLElBQUksd0JBQXdCLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUMzRSxrQkFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDNUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUVwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksMEJBQWdCLEVBQUUsQ0FBQztRQUNqQyxDQUFDO0lBQ0wsQ0FBQztJQVdELHNCQUFJLDZDQUFPO1FBSlg7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6QixDQUFDOzs7T0FBQTtJQU1ELHNCQUFJLDBDQUFJO1FBSlI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDO1FBQ3pDLENBQUM7OztPQUFBO0lBRUQ7OztPQUdHO0lBQ0ksOENBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNJLHlDQUFNLEdBQWI7UUFDSSxJQUFJLE1BQU0sR0FBNEIsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDckQsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQy9CLE1BQU0sQ0FBQyxJQUFJLEdBQUcsd0JBQXdCLENBQUMsSUFBSSxDQUFDO1FBQzVDLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQTlEYSw2QkFBSSxHQUFHLDBCQUEwQixDQUFDO0lBZ0VwRCwrQkFBQztDQWxFRCxBQWtFQyxDQWxFNkMsK0JBQXFCLEdBa0VsRTtBQWxFWSw0REFBd0I7QUFvRXJDLGtCQUFlLHdCQUF3QixDQUFDIiwiZmlsZSI6InByb3RvY29sL3Jlc3BvbnNlL2Jyb2FkY2FzdG1lc3NhZ2VyZXNwb25zZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBBdXRoZW50aWNhdGVkUmVzcG9uc2UgZnJvbSBcIi4vYXV0aGVudGljYXRlZHJlc3BvbnNlXCI7XHJcbmltcG9ydCBVc2VyTWV0YURhdGEgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJtZXRhZGF0YVwiO1xyXG5pbXBvcnQgVGlja2V0IGZyb20gXCIuLi8uLi9tb2RlbC90aWNrZXRcIjtcclxuaW1wb3J0IFJlc3BvbnNlIGZyb20gXCIuL3Jlc3BvbnNlXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9pbnZhbGlkanNvbmVycm9yXCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcbmltcG9ydCBJbnZhbGlkSlNPTkZvclJlc3BvbnNlRXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVzcG9uc2VlcnJvclwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEJyb2FkY2FzdE1lc3NhZ2VSZXNwb25zZSBleHRlbmRzIEF1dGhlbnRpY2F0ZWRSZXNwb25zZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJicm9hZGNhc3RtZXNzYWdlcmVzcG9uc2VcIjtcclxuXHJcbiAgICBwcml2YXRlIF9tZXNzYWdlOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IGEgSlNPTiBvYmplY3QgdG8gYSByZXNwb25zZSBvYmplY3QuXHJcbiAgICAgKiBAcGFyYW0ganNvbk9iamVjdFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSk6IEJyb2FkY2FzdE1lc3NhZ2VSZXNwb25zZSB7XHJcbiAgICAgICAgaWYgKCBqc29uT2JqZWN0LnR5cGUgIT09IEJyb2FkY2FzdE1lc3NhZ2VSZXNwb25zZS5UWVBFICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdC51c2VyTWV0YURhdGEgJiYganNvbk9iamVjdC50aWNrZXQgJiYganNvbk9iamVjdC5tZXNzYWdlICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IG1lc3NhZ2UgPSBqc29uT2JqZWN0Lm1lc3NhZ2U7XHJcbiAgICAgICAgICAgIGxldCB1c2VyTWV0YURhdGEgPSBVc2VyTWV0YURhdGEuZnJvbUpTT04oanNvbk9iamVjdC51c2VyTWV0YURhdGEpO1xyXG4gICAgICAgICAgICBsZXQgdGlja2V0ID0gVGlja2V0LmZyb21KU09OKGpzb25PYmplY3QudGlja2V0KTtcclxuICAgICAgICAgICAgbGV0IHJlc3BvbnNlID0gbmV3IEJyb2FkY2FzdE1lc3NhZ2VSZXNwb25zZSh1c2VyTWV0YURhdGEsIHRpY2tldCwgbWVzc2FnZSk7XHJcbiAgICAgICAgICAgIFJlc3BvbnNlLnBvcHVsYXRlSlNPTihyZXNwb25zZSwganNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IodXNlck1ldGFEYXRhOiBVc2VyTWV0YURhdGEsIHRpY2tldDogVGlja2V0LCBtZXNzYWdlOiBzdHJpbmcpIHtcclxuICAgICAgICBzdXBlcih1c2VyTWV0YURhdGEsIHRpY2tldCk7XHJcbiAgICAgICAgdGhpcy5fbWVzc2FnZSA9IG1lc3NhZ2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgbWVzc2FnZSB0byBiZSBicm9hZGNhc3QuXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBnZXQgbWVzc2FnZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9tZXNzYWdlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHlwZSBvZiByZXNwb25zZS5cclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICAgKi9cclxuICAgIGdldCB0eXBlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEJyb2FkY2FzdE1lc3NhZ2VSZXNwb25zZS5UWVBFO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGV0ZXJtaW5lIGlmIHRoaXMgcmVxdWVzdCBpcyB0byBiZSBicm9hZGNhc3QvcHJvY2Vzc2VkIHRvIGV2ZXJ5b25lXHJcbiAgICAgKiBjb25uZWN0ZWQgdG8gdGhlIHNlcnZlciAoYW5kIGFsbCBvdGhlciBzZXJ2ZXJzIGluIHRoZSBmYXJtKS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGlzQnJvYWRjYXN0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB0aGlzIHJlc3BvbnNlIG9iamVjdCB0byBKU09OLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHsgW2tleTogc3RyaW5nXTogYW55OyB9IHtcclxuICAgICAgICBsZXQgcmVzdWx0OiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIHJlc3VsdC5tZXNzYWdlID0gdGhpcy5fbWVzc2FnZTtcclxuICAgICAgICByZXN1bHQudHlwZSA9IEJyb2FkY2FzdE1lc3NhZ2VSZXNwb25zZS5UWVBFO1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBCcm9hZGNhc3RNZXNzYWdlUmVzcG9uc2U7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
