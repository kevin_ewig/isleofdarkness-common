import User from "../../model/user";
import Ticket from "../../model/ticket";
import Response from "./response";
import UserMetaData from "../../model/usermetadata";
import AuthenticatedResponse from "./authenticatedresponse";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";
import InvalidJSONError from "../../error/invalidjsonerror";
import Point from "../../math/point";
import { MissingConstructorArgumentError } from "../../error/missingconstructorargumenterror";

export class MoveObjectModelResponse extends AuthenticatedResponse {

    public static TYPE = "moveobjectmodelresponse";

    private _result: boolean;
    private _objectModelId: string;
    private _endPoint: Point;

    public static fromJSON(jsonObject: any): MoveObjectModelResponse {
        if ( jsonObject.type !== MoveObjectModelResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);

            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let result: boolean = jsonObject.result;
            let objectModelId: string = jsonObject.objectModelId;

            if ( jsonObject.endPoint ) {

                let endPoint: Point = Point.fromJSON(jsonObject.endPoint);

                let response = new MoveObjectModelResponse(userMetaData, ticket, endPoint, result);
                Response.populateJSON(response, jsonObject);

                response.objectModelId = objectModelId;
                return response;

            } else {
                throw new InvalidJSONError();
            }

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, endPoint: Point, result: boolean) {
        super(userMetaData, ticket);
        if ( endPoint ) {
            this._endPoint = endPoint;
        } else {
            throw new MissingConstructorArgumentError();
        }
        this._result = !!result;
    }

    /**
     * Get the object model id of the object model that is moving.
     */
    get endPoint(): Point {
        return this._endPoint;
    }

    /**
     * Get result of moving the object.
     * @param user
     */
    get result(): boolean {
        return this._result;
    }

    /**
     * Get the object model id of the object model that is moving.
     */
    get objectModelId(): string {
        return this._objectModelId;
    }

    /**
     * Set the object model id of the object model that is moving.
     */
    set objectModelId(value: string) {
        this._objectModelId = value;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return MoveObjectModelResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return true;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.type = MoveObjectModelResponse.TYPE;
        result.result = this._result;
        if ( this._endPoint ) {
            result.endPoint = this._endPoint.toJSON();
        }
        if ( this._objectModelId ) {
            result.objectModelId = this._objectModelId;
        }
        return result;
    }

}

export default MoveObjectModelResponse;
