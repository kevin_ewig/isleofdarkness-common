import Response from "./response";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

export class OkResponse extends Response {

    public static TYPE = "okresponse";

    private _errorMessage: string;

    public static fromJSON(jsonObject: any): OkResponse {
        if ( jsonObject.type !== OkResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else {

            let response = new OkResponse();
            Response.populateJSON(response, jsonObject);
            return response;

        }
    }

    constructor() {
        super();
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return OkResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return false;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.type = OkResponse.TYPE;
        return result;
    }

}

export default OkResponse;
