"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("../../uuid");
var Response = /** @class */ (function () {
    function Response() {
        this._responseId = uuid_1.default.getUuid();
    }
    /**
     * Populate response object with fields from JSON object.
     * @param response
     * @param jsonObject
     */
    Response.populateJSON = function (response, jsonObject) {
        response.responseId = jsonObject.responseId;
        response.requestId = jsonObject.requestId;
        response.originServerName = jsonObject.originServerName;
    };
    Object.defineProperty(Response.prototype, "originServerName", {
        /**
         * Get the server name that this request came from.
         */
        get: function () {
            return this._originServerName;
        },
        /**
         * Set the server name that this request came from.
         * @param requestId
         */
        set: function (originServerName) {
            this._originServerName = originServerName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Response.prototype, "requestId", {
        /**
         * Get request id.
         */
        get: function () {
            return this._requestId;
        },
        /**
         * Set request id.
         */
        set: function (requestId) {
            this._requestId = requestId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Response.prototype, "responseId", {
        /**
         * Get response id.
         */
        get: function () {
            return this._responseId;
        },
        /**
         * Set response id.
         */
        set: function (responseId) {
            this._responseId = responseId;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert this response object to JSON.
     */
    Response.prototype.toJSON = function () {
        var result = {};
        if (this.type) {
            result.type = this.type;
        }
        if (this._responseId) {
            result.responseId = this._responseId;
        }
        if (this._requestId) {
            result.requestId = this._requestId;
        }
        if (this._originServerName) {
            result.originServerName = this._originServerName;
        }
        return result;
    };
    return Response;
}());
exports.Response = Response;
exports.default = Response;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3Jlc3BvbnNlL3Jlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsbUNBQThCO0FBRTlCO0lBaUJJO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxjQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQWJEOzs7O09BSUc7SUFDVyxxQkFBWSxHQUExQixVQUEyQixRQUFrQixFQUFFLFVBQWU7UUFDMUQsUUFBUSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsVUFBVSxDQUFDO1FBQzVDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQztRQUMxQyxRQUFRLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLGdCQUFnQixDQUFDO0lBQzVELENBQUM7SUFjRCxzQkFBSSxzQ0FBZ0I7UUFIcEI7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDbEMsQ0FBQztRQUVEOzs7V0FHRzthQUNILFVBQXFCLGdCQUF3QjtZQUN6QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsZ0JBQWdCLENBQUM7UUFDOUMsQ0FBQzs7O09BUkE7SUFhRCxzQkFBVywrQkFBUztRQUhwQjs7V0FFRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDM0IsQ0FBQztRQVFEOztXQUVHO2FBQ0gsVUFBcUIsU0FBaUI7WUFDbEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDaEMsQ0FBQzs7O09BYkE7SUFrQkQsc0JBQVcsZ0NBQVU7UUFIckI7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVCLENBQUM7UUFFRDs7V0FFRzthQUNILFVBQXNCLFVBQWtCO1lBQ3BDLElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDO1FBQ2xDLENBQUM7OztPQVBBO0lBU0Q7O09BRUc7SUFDSSx5QkFBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLEVBQUUsQ0FBQztRQUN6QyxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNkLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUM1QixDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFdBQVksQ0FBQyxDQUFDLENBQUM7WUFDckIsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ3pDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsVUFBVyxDQUFDLENBQUMsQ0FBQztZQUNwQixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDdkMsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxpQkFBa0IsQ0FBQyxDQUFDLENBQUM7WUFDM0IsTUFBTSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNyRCxDQUFDO1FBRUQsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUwsZUFBQztBQUFELENBaEdBLEFBZ0dDLElBQUE7QUFoR3FCLDRCQUFRO0FBa0c5QixrQkFBZSxRQUFRLENBQUMiLCJmaWxlIjoicHJvdG9jb2wvcmVzcG9uc2UvcmVzcG9uc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVGlja2V0IGZyb20gXCIuLi8uLi9tb2RlbC90aWNrZXRcIjtcclxuaW1wb3J0IFV1aWQgZnJvbSBcIi4uLy4uL3V1aWRcIjtcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBSZXNwb25zZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVxdWVzdElkOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9yZXNwb25zZUlkOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9vcmlnaW5TZXJ2ZXJOYW1lOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQb3B1bGF0ZSByZXNwb25zZSBvYmplY3Qgd2l0aCBmaWVsZHMgZnJvbSBKU09OIG9iamVjdC5cclxuICAgICAqIEBwYXJhbSByZXNwb25zZVxyXG4gICAgICogQHBhcmFtIGpzb25PYmplY3RcclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBwb3B1bGF0ZUpTT04ocmVzcG9uc2U6IFJlc3BvbnNlLCBqc29uT2JqZWN0OiBhbnkpOiB2b2lkIHtcclxuICAgICAgICByZXNwb25zZS5yZXNwb25zZUlkID0ganNvbk9iamVjdC5yZXNwb25zZUlkO1xyXG4gICAgICAgIHJlc3BvbnNlLnJlcXVlc3RJZCA9IGpzb25PYmplY3QucmVxdWVzdElkO1xyXG4gICAgICAgIHJlc3BvbnNlLm9yaWdpblNlcnZlck5hbWUgPSBqc29uT2JqZWN0Lm9yaWdpblNlcnZlck5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5fcmVzcG9uc2VJZCA9IFV1aWQuZ2V0VXVpZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHlwZSBvZiByZXNwb25zZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGFic3RyYWN0IGdldCB0eXBlKCk6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgc2VydmVyIG5hbWUgdGhhdCB0aGlzIHJlcXVlc3QgY2FtZSBmcm9tLlxyXG4gICAgICovXHJcbiAgICBnZXQgb3JpZ2luU2VydmVyTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9vcmlnaW5TZXJ2ZXJOYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSBzZXJ2ZXIgbmFtZSB0aGF0IHRoaXMgcmVxdWVzdCBjYW1lIGZyb20uXHJcbiAgICAgKiBAcGFyYW0gcmVxdWVzdElkXHJcbiAgICAgKi9cclxuICAgIHNldCBvcmlnaW5TZXJ2ZXJOYW1lKG9yaWdpblNlcnZlck5hbWU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX29yaWdpblNlcnZlck5hbWUgPSBvcmlnaW5TZXJ2ZXJOYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHJlcXVlc3QgaWQuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXQgcmVxdWVzdElkKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3JlcXVlc3RJZDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERldGVybWluZSBpZiB0aGlzIHJlcXVlc3QgaXMgdG8gYmUgYnJvYWRjYXN0L3Byb2Nlc3NlZCB0byBldmVyeW9uZVxyXG4gICAgICogY29ubmVjdGVkIHRvIHRoZSBzZXJ2ZXIgKGFuZCBhbGwgb3RoZXIgc2VydmVycyBpbiB0aGUgZmFybSkuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhYnN0cmFjdCBpc0Jyb2FkY2FzdCgpOiBib29sZWFuO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHJlcXVlc3QgaWQuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzZXQgcmVxdWVzdElkKHJlcXVlc3RJZDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fcmVxdWVzdElkID0gcmVxdWVzdElkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHJlc3BvbnNlIGlkLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0IHJlc3BvbnNlSWQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcmVzcG9uc2VJZDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCByZXNwb25zZSBpZC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldCByZXNwb25zZUlkKHJlc3BvbnNlSWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX3Jlc3BvbnNlSWQgPSByZXNwb25zZUlkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB0aGlzIHJlc3BvbnNlIG9iamVjdCB0byBKU09OLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHsgW2tleTogc3RyaW5nXTogYW55OyB9IHtcclxuICAgICAgICBsZXQgcmVzdWx0OiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHt9O1xyXG4gICAgICAgIGlmICggdGhpcy50eXBlICkge1xyXG4gICAgICAgICAgICByZXN1bHQudHlwZSA9IHRoaXMudHlwZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCB0aGlzLl9yZXNwb25zZUlkICkge1xyXG4gICAgICAgICAgICByZXN1bHQucmVzcG9uc2VJZCA9IHRoaXMuX3Jlc3BvbnNlSWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggdGhpcy5fcmVxdWVzdElkICkge1xyXG4gICAgICAgICAgICByZXN1bHQucmVxdWVzdElkID0gdGhpcy5fcmVxdWVzdElkO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIHRoaXMuX29yaWdpblNlcnZlck5hbWUgKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdC5vcmlnaW5TZXJ2ZXJOYW1lID0gdGhpcy5fb3JpZ2luU2VydmVyTmFtZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBSZXNwb25zZTtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
