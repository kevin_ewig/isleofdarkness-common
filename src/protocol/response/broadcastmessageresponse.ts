import AuthenticatedResponse from "./authenticatedresponse";
import UserMetaData from "../../model/usermetadata";
import Ticket from "../../model/ticket";
import Response from "./response";
import InvalidJSONError from "../../error/invalidjsonerror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";
import InvalidJSONForResponseError from "../../error/invalidjsonforresponseerror";

export class BroadcastMessageResponse extends AuthenticatedResponse {

    public static TYPE = "broadcastmessageresponse";

    private _message: string;

    /**
     * Convert a JSON object to a response object.
     * @param jsonObject
     */
    public static fromJSON(jsonObject: any): BroadcastMessageResponse {
        if ( jsonObject.type !== BroadcastMessageResponse.TYPE ) {
            throw new InvalidJSONForResponseError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket && jsonObject.message ) {

            let message = jsonObject.message;
            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let response = new BroadcastMessageResponse(userMetaData, ticket, message);
            Response.populateJSON(response, jsonObject);
            return response;

        } else {
            throw new InvalidJSONError();
        }
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, message: string) {
        super(userMetaData, ticket);
        this._message = message;
    }

    /**
     * Get message to be broadcast.
     * @returns {string}
     */
    get message(): string {
        return this._message;
    }

    /**
     * Type of response.
     * @returns {string}
     */
    get type(): string {
        return BroadcastMessageResponse.TYPE;
    }

    /**
     * Determine if this request is to be broadcast/processed to everyone
     * connected to the server (and all other servers in the farm).
     */
    public isBroadcast(): boolean {
        return true;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        result.message = this._message;
        result.type = BroadcastMessageResponse.TYPE;
        return result;
    }

}

export default BroadcastMessageResponse;
