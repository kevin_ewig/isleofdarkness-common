"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var broadcastmessagerequest_1 = require("./request/broadcastmessagerequest");
var openconnectionrequest_1 = require("./request/openconnectionrequest");
var closeconnectionrequest_1 = require("./request/closeconnectionrequest");
var worldmodelrequest_1 = require("./request/worldmodelrequest");
var addobjectmodelrequest_1 = require("./request/addobjectmodelrequest");
var addavatarrequest_1 = require("./request/addavatarrequest");
var moveobjectmodelrequest_1 = require("./request/moveobjectmodelrequest");
var removeavatarrequest_1 = require("./request/removeavatarrequest");
var broadcastmessageresponse_1 = require("./response/broadcastmessageresponse");
var openconnectionresponse_1 = require("./response/openconnectionresponse");
var worldmodelresponse_1 = require("./response/worldmodelresponse");
var invalidrequestresponse_1 = require("./response/invalidrequestresponse");
var addobjectmodelresponse_1 = require("./response/addobjectmodelresponse");
var moveobjectmodelresponse_1 = require("./response/moveobjectmodelresponse");
var closeconnectionresponse_1 = require("./response/closeconnectionresponse");
var addavatarresponse_1 = require("./response/addavatarresponse");
var errorresponse_1 = require("./response/errorresponse");
var okresponse_1 = require("./response/okresponse");
var removeavatarresponse_1 = require("./response/removeavatarresponse");
var Converter = /** @class */ (function () {
    function Converter() {
    }
    /**
     * Convert a string to a Response object.
     * @param value
     * @constructor
     */
    Converter.ConvertResponse = function (value) {
        var responseObject = null;
        try {
            var jsonObject = JSON.parse(value);
            var jsonObjectType = jsonObject.type;
            if (jsonObjectType === addavatarresponse_1.default.TYPE) {
                responseObject = addavatarresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === addobjectmodelresponse_1.default.TYPE) {
                responseObject = addobjectmodelresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === broadcastmessageresponse_1.default.TYPE) {
                responseObject = broadcastmessageresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === openconnectionresponse_1.default.TYPE) {
                responseObject = openconnectionresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === worldmodelresponse_1.default.TYPE) {
                responseObject = worldmodelresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === invalidrequestresponse_1.default.TYPE) {
                responseObject = invalidrequestresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === closeconnectionresponse_1.default.TYPE) {
                responseObject = closeconnectionresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === moveobjectmodelresponse_1.default.TYPE) {
                responseObject = moveobjectmodelresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === errorresponse_1.default.TYPE) {
                responseObject = errorresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === okresponse_1.default.TYPE) {
                responseObject = okresponse_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === removeavatarresponse_1.default.TYPE) {
                responseObject = removeavatarresponse_1.default.fromJSON(jsonObject);
            }
        }
        catch (error) {
            throw new Error("Problem converting " + value + " to Response");
        }
        return responseObject;
    };
    /**
     * Convert a string to a Request object.
     * @param value
     * @constructor
     */
    Converter.ConvertRequest = function (value) {
        var requestObject = null;
        try {
            var jsonObject = JSON.parse(value);
            var jsonObjectType = jsonObject.type;
            if (jsonObjectType === addavatarrequest_1.default.TYPE) {
                requestObject = addavatarrequest_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === broadcastmessagerequest_1.default.TYPE) {
                requestObject = broadcastmessagerequest_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === openconnectionrequest_1.default.TYPE) {
                requestObject = openconnectionrequest_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === closeconnectionrequest_1.default.TYPE) {
                requestObject = closeconnectionrequest_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === worldmodelrequest_1.default.TYPE) {
                requestObject = worldmodelrequest_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === addobjectmodelrequest_1.default.TYPE) {
                requestObject = addobjectmodelrequest_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === moveobjectmodelrequest_1.default.TYPE) {
                requestObject = moveobjectmodelrequest_1.default.fromJSON(jsonObject);
            }
            else if (jsonObjectType === removeavatarrequest_1.default.TYPE) {
                requestObject = removeavatarrequest_1.default.fromJSON(jsonObject);
            }
        }
        catch (error) {
            throw new Error("Problem converting " + value + " to Request. [" + error.message + "]");
        }
        return requestObject;
    };
    return Converter;
}());
exports.Converter = Converter;
exports.default = Converter;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL2NvbnZlcnRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDZFQUF3RTtBQUN4RSx5RUFBb0U7QUFDcEUsMkVBQXNFO0FBQ3RFLGlFQUE0RDtBQUM1RCx5RUFBb0U7QUFDcEUsK0RBQTBEO0FBQzFELDJFQUFzRTtBQUN0RSxxRUFBZ0U7QUFFaEUsZ0ZBQTJFO0FBQzNFLDRFQUF1RTtBQUN2RSxvRUFBK0Q7QUFDL0QsNEVBQXVFO0FBQ3ZFLDRFQUF1RTtBQUN2RSw4RUFBeUU7QUFDekUsOEVBQXlFO0FBQ3pFLGtFQUE2RDtBQUM3RCwwREFBcUQ7QUFDckQsb0RBQStDO0FBQy9DLHdFQUFtRTtBQUtuRTtJQUFBO0lBZ0ZBLENBQUM7SUE5RUc7Ozs7T0FJRztJQUNXLHlCQUFlLEdBQTdCLFVBQThCLEtBQWE7UUFDdkMsSUFBSSxjQUFjLEdBQWtCLElBQUksQ0FBQztRQUN6QyxJQUFJLENBQUM7WUFFRCxJQUFJLFVBQVUsR0FBUSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hDLElBQUksY0FBYyxHQUFXLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFFN0MsRUFBRSxDQUFDLENBQUUsY0FBYyxLQUFLLDJCQUFpQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLGNBQWMsR0FBRywyQkFBaUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDNUQsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxjQUFjLEtBQUssZ0NBQXNCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztnQkFDMUQsY0FBYyxHQUFHLGdDQUFzQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNqRSxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLGNBQWMsS0FBSyxrQ0FBd0IsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUM1RCxjQUFjLEdBQUcsa0NBQXdCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ25FLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsY0FBYyxLQUFLLGdDQUFzQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzFELGNBQWMsR0FBRyxnQ0FBc0IsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakUsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxjQUFjLEtBQUssNEJBQWtCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztnQkFDdEQsY0FBYyxHQUFHLDRCQUFrQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM3RCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLGNBQWMsS0FBSyxnQ0FBc0IsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUMxRCxjQUFjLEdBQUcsZ0NBQXNCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2pFLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsY0FBYyxLQUFLLGlDQUF1QixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzNELGNBQWMsR0FBRyxpQ0FBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDbEUsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxjQUFjLEtBQUssaUNBQXVCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztnQkFDM0QsY0FBYyxHQUFHLGlDQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsRSxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLGNBQWMsS0FBSyx1QkFBYSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2pELGNBQWMsR0FBRyx1QkFBYSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN4RCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLGNBQWMsS0FBSyxvQkFBVSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLGNBQWMsR0FBRyxvQkFBVSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNyRCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLGNBQWMsS0FBSyw4QkFBb0IsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxjQUFjLEdBQUcsOEJBQW9CLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQy9ELENBQUM7UUFFTCxDQUFDO1FBQUMsS0FBSyxDQUFDLENBQUUsS0FBTSxDQUFDLENBQUMsQ0FBQztZQUNmLE1BQU0sSUFBSSxLQUFLLENBQUMscUJBQXFCLEdBQUcsS0FBSyxHQUFHLGNBQWMsQ0FBQyxDQUFDO1FBQ3BFLENBQUM7UUFDRCxNQUFNLENBQUMsY0FBYyxDQUFDO0lBQzFCLENBQUM7SUFFRDs7OztPQUlHO0lBQ1csd0JBQWMsR0FBNUIsVUFBNkIsS0FBYTtRQUN0QyxJQUFJLGFBQWEsR0FBaUIsSUFBSSxDQUFDO1FBQ3ZDLElBQUksQ0FBQztZQUVELElBQUksVUFBVSxHQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEMsSUFBSSxjQUFjLEdBQVcsVUFBVSxDQUFDLElBQUksQ0FBQztZQUU3QyxFQUFFLENBQUMsQ0FBRSxjQUFjLEtBQUssMEJBQWdCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsYUFBYSxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMxRCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLGNBQWMsS0FBSyxpQ0FBdUIsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUMzRCxhQUFhLEdBQUcsaUNBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2pFLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsY0FBYyxLQUFLLCtCQUFxQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELGFBQWEsR0FBRywrQkFBcUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDL0QsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxjQUFjLEtBQUssZ0NBQXNCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztnQkFDMUQsYUFBYSxHQUFHLGdDQUFzQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNoRSxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLGNBQWMsS0FBSywyQkFBaUIsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNyRCxhQUFhLEdBQUcsMkJBQWlCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzNELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsY0FBYyxLQUFLLCtCQUFxQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELGFBQWEsR0FBRywrQkFBcUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDL0QsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxjQUFjLEtBQUssZ0NBQXNCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztnQkFDMUQsYUFBYSxHQUFHLGdDQUFzQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNoRSxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLGNBQWMsS0FBSyw2QkFBbUIsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUN2RCxhQUFhLEdBQUcsNkJBQW1CLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzdELENBQUM7UUFFTCxDQUFDO1FBQUMsS0FBSyxDQUFDLENBQUUsS0FBTSxDQUFDLENBQUMsQ0FBQztZQUNmLE1BQU0sSUFBSSxLQUFLLENBQUMscUJBQXFCLEdBQUcsS0FBSyxHQUFHLGdCQUFnQixHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDNUYsQ0FBQztRQUNELE1BQU0sQ0FBQyxhQUFhLENBQUM7SUFDekIsQ0FBQztJQUVMLGdCQUFDO0FBQUQsQ0FoRkEsQUFnRkMsSUFBQTtBQWhGWSw4QkFBUztBQWtGdEIsa0JBQWUsU0FBUyxDQUFDIiwiZmlsZSI6InByb3RvY29sL2NvbnZlcnRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBCcm9hZGNhc3RNZXNzYWdlUmVxdWVzdCBmcm9tIFwiLi9yZXF1ZXN0L2Jyb2FkY2FzdG1lc3NhZ2VyZXF1ZXN0XCI7XHJcbmltcG9ydCBPcGVuQ29ubmVjdGlvblJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdC9vcGVuY29ubmVjdGlvbnJlcXVlc3RcIjtcclxuaW1wb3J0IENsb3NlQ29ubmVjdGlvblJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdC9jbG9zZWNvbm5lY3Rpb25yZXF1ZXN0XCI7XHJcbmltcG9ydCBXb3JsZE1vZGVsUmVxdWVzdCBmcm9tIFwiLi9yZXF1ZXN0L3dvcmxkbW9kZWxyZXF1ZXN0XCI7XHJcbmltcG9ydCBBZGRPYmplY3RNb2RlbFJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdC9hZGRvYmplY3Rtb2RlbHJlcXVlc3RcIjtcclxuaW1wb3J0IEFkZEF2YXRhclJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdC9hZGRhdmF0YXJyZXF1ZXN0XCI7XHJcbmltcG9ydCBNb3ZlT2JqZWN0TW9kZWxSZXF1ZXN0IGZyb20gXCIuL3JlcXVlc3QvbW92ZW9iamVjdG1vZGVscmVxdWVzdFwiO1xyXG5pbXBvcnQgUmVtb3ZlQXZhdGFyUmVxdWVzdCBmcm9tIFwiLi9yZXF1ZXN0L3JlbW92ZWF2YXRhcnJlcXVlc3RcIjtcclxuXHJcbmltcG9ydCBCcm9hZGNhc3RNZXNzYWdlUmVzcG9uc2UgZnJvbSBcIi4vcmVzcG9uc2UvYnJvYWRjYXN0bWVzc2FnZXJlc3BvbnNlXCI7XHJcbmltcG9ydCBPcGVuQ29ubmVjdGlvblJlc3BvbnNlIGZyb20gXCIuL3Jlc3BvbnNlL29wZW5jb25uZWN0aW9ucmVzcG9uc2VcIjtcclxuaW1wb3J0IFdvcmxkTW9kZWxSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZS93b3JsZG1vZGVscmVzcG9uc2VcIjtcclxuaW1wb3J0IEludmFsaWRSZXF1ZXN0UmVzcG9uc2UgZnJvbSBcIi4vcmVzcG9uc2UvaW52YWxpZHJlcXVlc3RyZXNwb25zZVwiO1xyXG5pbXBvcnQgQWRkT2JqZWN0TW9kZWxSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZS9hZGRvYmplY3Rtb2RlbHJlc3BvbnNlXCI7XHJcbmltcG9ydCBNb3ZlT2JqZWN0TW9kZWxSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZS9tb3Zlb2JqZWN0bW9kZWxyZXNwb25zZVwiO1xyXG5pbXBvcnQgQ2xvc2VDb25uZWN0aW9uUmVzcG9uc2UgZnJvbSBcIi4vcmVzcG9uc2UvY2xvc2Vjb25uZWN0aW9ucmVzcG9uc2VcIjtcclxuaW1wb3J0IEFkZEF2YXRhclJlc3BvbnNlIGZyb20gXCIuL3Jlc3BvbnNlL2FkZGF2YXRhcnJlc3BvbnNlXCI7XHJcbmltcG9ydCBFcnJvclJlc3BvbnNlIGZyb20gXCIuL3Jlc3BvbnNlL2Vycm9ycmVzcG9uc2VcIjtcclxuaW1wb3J0IE9rUmVzcG9uc2UgZnJvbSBcIi4vcmVzcG9uc2Uvb2tyZXNwb25zZVwiO1xyXG5pbXBvcnQgUmVtb3ZlQXZhdGFyUmVzcG9uc2UgZnJvbSBcIi4vcmVzcG9uc2UvcmVtb3ZlYXZhdGFycmVzcG9uc2VcIjtcclxuXHJcbmltcG9ydCBSZXNwb25zZSBmcm9tIFwiLi9yZXNwb25zZS9yZXNwb25zZVwiO1xyXG5pbXBvcnQgUmVxdWVzdCBmcm9tIFwiLi9yZXF1ZXN0L3JlcXVlc3RcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBDb252ZXJ0ZXIge1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBhIHN0cmluZyB0byBhIFJlc3BvbnNlIG9iamVjdC5cclxuICAgICAqIEBwYXJhbSB2YWx1ZVxyXG4gICAgICogQGNvbnN0cnVjdG9yXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgQ29udmVydFJlc3BvbnNlKHZhbHVlOiBzdHJpbmcpOiBSZXNwb25zZXxudWxsIHtcclxuICAgICAgICBsZXQgcmVzcG9uc2VPYmplY3Q6IFJlc3BvbnNlfG51bGwgPSBudWxsO1xyXG4gICAgICAgIHRyeSB7XHJcblxyXG4gICAgICAgICAgICBsZXQganNvbk9iamVjdDogYW55ID0gSlNPTi5wYXJzZSh2YWx1ZSk7XHJcbiAgICAgICAgICAgIGxldCBqc29uT2JqZWN0VHlwZTogc3RyaW5nID0ganNvbk9iamVjdC50eXBlO1xyXG5cclxuICAgICAgICAgICAgaWYgKCBqc29uT2JqZWN0VHlwZSA9PT0gQWRkQXZhdGFyUmVzcG9uc2UuVFlQRSApIHtcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlT2JqZWN0ID0gQWRkQXZhdGFyUmVzcG9uc2UuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBBZGRPYmplY3RNb2RlbFJlc3BvbnNlLlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZU9iamVjdCA9IEFkZE9iamVjdE1vZGVsUmVzcG9uc2UuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBCcm9hZGNhc3RNZXNzYWdlUmVzcG9uc2UuVFlQRSApIHtcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlT2JqZWN0ID0gQnJvYWRjYXN0TWVzc2FnZVJlc3BvbnNlLmZyb21KU09OKGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCBqc29uT2JqZWN0VHlwZSA9PT0gT3BlbkNvbm5lY3Rpb25SZXNwb25zZS5UWVBFICkge1xyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2VPYmplY3QgPSBPcGVuQ29ubmVjdGlvblJlc3BvbnNlLmZyb21KU09OKGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCBqc29uT2JqZWN0VHlwZSA9PT0gV29ybGRNb2RlbFJlc3BvbnNlLlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZU9iamVjdCA9IFdvcmxkTW9kZWxSZXNwb25zZS5mcm9tSlNPTihqc29uT2JqZWN0KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdFR5cGUgPT09IEludmFsaWRSZXF1ZXN0UmVzcG9uc2UuVFlQRSApIHtcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlT2JqZWN0ID0gSW52YWxpZFJlcXVlc3RSZXNwb25zZS5mcm9tSlNPTihqc29uT2JqZWN0KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdFR5cGUgPT09IENsb3NlQ29ubmVjdGlvblJlc3BvbnNlLlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZU9iamVjdCA9IENsb3NlQ29ubmVjdGlvblJlc3BvbnNlLmZyb21KU09OKGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCBqc29uT2JqZWN0VHlwZSA9PT0gTW92ZU9iamVjdE1vZGVsUmVzcG9uc2UuVFlQRSApIHtcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlT2JqZWN0ID0gTW92ZU9iamVjdE1vZGVsUmVzcG9uc2UuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBFcnJvclJlc3BvbnNlLlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZU9iamVjdCA9IEVycm9yUmVzcG9uc2UuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBPa1Jlc3BvbnNlLlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZU9iamVjdCA9IE9rUmVzcG9uc2UuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBSZW1vdmVBdmF0YXJSZXNwb25zZS5UWVBFICkge1xyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2VPYmplY3QgPSBSZW1vdmVBdmF0YXJSZXNwb25zZS5mcm9tSlNPTihqc29uT2JqZWN0KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9IGNhdGNoICggZXJyb3IgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIlByb2JsZW0gY29udmVydGluZyBcIiArIHZhbHVlICsgXCIgdG8gUmVzcG9uc2VcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXNwb25zZU9iamVjdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgYSBzdHJpbmcgdG8gYSBSZXF1ZXN0IG9iamVjdC5cclxuICAgICAqIEBwYXJhbSB2YWx1ZVxyXG4gICAgICogQGNvbnN0cnVjdG9yXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgQ29udmVydFJlcXVlc3QodmFsdWU6IHN0cmluZyk6IFJlcXVlc3R8bnVsbCB7XHJcbiAgICAgICAgbGV0IHJlcXVlc3RPYmplY3Q6IFJlcXVlc3R8bnVsbCA9IG51bGw7XHJcbiAgICAgICAgdHJ5IHtcclxuXHJcbiAgICAgICAgICAgIGxldCBqc29uT2JqZWN0OiBhbnkgPSBKU09OLnBhcnNlKHZhbHVlKTtcclxuICAgICAgICAgICAgbGV0IGpzb25PYmplY3RUeXBlOiBzdHJpbmcgPSBqc29uT2JqZWN0LnR5cGU7XHJcblxyXG4gICAgICAgICAgICBpZiAoIGpzb25PYmplY3RUeXBlID09PSBBZGRBdmF0YXJSZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0T2JqZWN0ID0gQWRkQXZhdGFyUmVxdWVzdC5mcm9tSlNPTihqc29uT2JqZWN0KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdFR5cGUgPT09IEJyb2FkY2FzdE1lc3NhZ2VSZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0T2JqZWN0ID0gQnJvYWRjYXN0TWVzc2FnZVJlcXVlc3QuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBPcGVuQ29ubmVjdGlvblJlcXVlc3QuVFlQRSApIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3RPYmplY3QgPSBPcGVuQ29ubmVjdGlvblJlcXVlc3QuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBDbG9zZUNvbm5lY3Rpb25SZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0T2JqZWN0ID0gQ2xvc2VDb25uZWN0aW9uUmVxdWVzdC5mcm9tSlNPTihqc29uT2JqZWN0KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdFR5cGUgPT09IFdvcmxkTW9kZWxSZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0T2JqZWN0ID0gV29ybGRNb2RlbFJlcXVlc3QuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBBZGRPYmplY3RNb2RlbFJlcXVlc3QuVFlQRSApIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3RPYmplY3QgPSBBZGRPYmplY3RNb2RlbFJlcXVlc3QuZnJvbUpTT04oanNvbk9iamVjdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3RUeXBlID09PSBNb3ZlT2JqZWN0TW9kZWxSZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0T2JqZWN0ID0gTW92ZU9iamVjdE1vZGVsUmVxdWVzdC5mcm9tSlNPTihqc29uT2JqZWN0KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdFR5cGUgPT09IFJlbW92ZUF2YXRhclJlcXVlc3QuVFlQRSApIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3RPYmplY3QgPSBSZW1vdmVBdmF0YXJSZXF1ZXN0LmZyb21KU09OKGpzb25PYmplY3QpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0gY2F0Y2ggKCBlcnJvciApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiUHJvYmxlbSBjb252ZXJ0aW5nIFwiICsgdmFsdWUgKyBcIiB0byBSZXF1ZXN0LiBbXCIgKyBlcnJvci5tZXNzYWdlICsgXCJdXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVxdWVzdE9iamVjdDtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IENvbnZlcnRlcjtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
