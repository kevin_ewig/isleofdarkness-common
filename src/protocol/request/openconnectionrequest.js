"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var authenticatedrequest_1 = require("./authenticatedrequest");
var ticket_1 = require("../../model/ticket");
var usermetadata_1 = require("../../model/usermetadata");
var request_1 = require("./request");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforrequesterror_1 = require("../../error/invalidjsonforrequesterror");
var OpenConnectionRequest = /** @class */ (function (_super) {
    __extends(OpenConnectionRequest, _super);
    function OpenConnectionRequest(userMetaData, ticket) {
        return _super.call(this, userMetaData, ticket) || this;
    }
    /**
     * Convert a JSON object to OpenConnectionRequest.
     * @param jsonObject
     */
    OpenConnectionRequest.fromJSON = function (jsonObject) {
        var request;
        if (jsonObject.type !== OpenConnectionRequest.TYPE) {
            throw new invalidjsonforrequesterror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket && jsonObject.requestId) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            request = new OpenConnectionRequest(userMetaData, ticket);
            request_1.default.populateJSON(request, jsonObject);
        }
        else {
            throw new invalidjsonerror_1.default();
        }
        return request;
    };
    OpenConnectionRequest.prototype.getType = function () {
        return OpenConnectionRequest.TYPE;
    };
    /**
     * Convert this response object to JSON.
     */
    OpenConnectionRequest.prototype.toJSON = function () {
        var json = _super.prototype.toJSON.call(this);
        return json;
    };
    OpenConnectionRequest.TYPE = "openconnectionrequest";
    return OpenConnectionRequest;
}(authenticatedrequest_1.default));
exports.OpenConnectionRequest = OpenConnectionRequest;
exports.default = OpenConnectionRequest;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3Qvb3BlbmNvbm5lY3Rpb25yZXF1ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLCtEQUEwRDtBQUMxRCw2Q0FBd0M7QUFDeEMseURBQW9EO0FBQ3BELHFDQUFnQztBQUNoQyxpRUFBNEQ7QUFDNUQscUZBQWdGO0FBR2hGO0lBQTJDLHlDQUFvQjtJQTZCM0QsK0JBQVksWUFBMEIsRUFBRSxNQUFjO2VBQ2xELGtCQUFNLFlBQVksRUFBRSxNQUFNLENBQUM7SUFDL0IsQ0FBQztJQTNCRDs7O09BR0c7SUFDVyw4QkFBUSxHQUF0QixVQUF1QixVQUFlO1FBRWxDLElBQUksT0FBOEIsQ0FBQztRQUVuQyxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsSUFBSSxLQUFLLHFCQUFxQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkQsTUFBTSxJQUFJLG9DQUEwQixFQUFFLENBQUM7UUFDM0MsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsWUFBWSxJQUFJLFVBQVUsQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLFNBQVUsQ0FBQyxDQUFDLENBQUM7WUFFaEYsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xFLElBQUksTUFBTSxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxPQUFPLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDMUQsaUJBQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBRTlDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSwwQkFBZ0IsRUFBRSxDQUFDO1FBQ2pDLENBQUM7UUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFNTSx1Q0FBTyxHQUFkO1FBQ0ksTUFBTSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQztJQUN0QyxDQUFDO0lBR0Q7O09BRUc7SUFDSSxzQ0FBTSxHQUFiO1FBQ0ksSUFBSSxJQUFJLEdBQTRCLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ25ELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQTFDYSwwQkFBSSxHQUFHLHVCQUF1QixDQUFDO0lBNENqRCw0QkFBQztDQTlDRCxBQThDQyxDQTlDMEMsOEJBQW9CLEdBOEM5RDtBQTlDWSxzREFBcUI7QUFnRGxDLGtCQUFlLHFCQUFxQixDQUFDIiwiZmlsZSI6InByb3RvY29sL3JlcXVlc3Qvb3BlbmNvbm5lY3Rpb25yZXF1ZXN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEF1dGhlbnRpY2F0ZWRSZXF1ZXN0IGZyb20gXCIuL2F1dGhlbnRpY2F0ZWRyZXF1ZXN0XCI7XHJcbmltcG9ydCBUaWNrZXQgZnJvbSBcIi4uLy4uL21vZGVsL3RpY2tldFwiO1xyXG5pbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuLi8uLi9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IFJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdFwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXF1ZXN0RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVxdWVzdGVycm9yXCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgT3BlbkNvbm5lY3Rpb25SZXF1ZXN0IGV4dGVuZHMgQXV0aGVudGljYXRlZFJlcXVlc3Qge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgVFlQRSA9IFwib3BlbmNvbm5lY3Rpb25yZXF1ZXN0XCI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IGEgSlNPTiBvYmplY3QgdG8gT3BlbkNvbm5lY3Rpb25SZXF1ZXN0LlxyXG4gICAgICogQHBhcmFtIGpzb25PYmplY3RcclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqZWN0OiBhbnkpOiBPcGVuQ29ubmVjdGlvblJlcXVlc3Qge1xyXG5cclxuICAgICAgICBsZXQgcmVxdWVzdDogT3BlbkNvbm5lY3Rpb25SZXF1ZXN0O1xyXG5cclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gT3BlbkNvbm5lY3Rpb25SZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkZvclJlcXVlc3RFcnJvcigpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3QudXNlck1ldGFEYXRhICYmIGpzb25PYmplY3QudGlja2V0ICYmIGpzb25PYmplY3QucmVxdWVzdElkICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHVzZXJNZXRhRGF0YSA9IFVzZXJNZXRhRGF0YS5mcm9tSlNPTihqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSk7XHJcbiAgICAgICAgICAgIGxldCB0aWNrZXQgPSBUaWNrZXQuZnJvbUpTT04oanNvbk9iamVjdC50aWNrZXQpO1xyXG4gICAgICAgICAgICByZXF1ZXN0ID0gbmV3IE9wZW5Db25uZWN0aW9uUmVxdWVzdCh1c2VyTWV0YURhdGEsIHRpY2tldCk7XHJcbiAgICAgICAgICAgIFJlcXVlc3QucG9wdWxhdGVKU09OKHJlcXVlc3QsIGpzb25PYmplY3QpO1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEpTT05FcnJvcigpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlcXVlc3Q7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCkge1xyXG4gICAgICAgIHN1cGVyKHVzZXJNZXRhRGF0YSwgdGlja2V0KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0VHlwZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBPcGVuQ29ubmVjdGlvblJlcXVlc3QuVFlQRTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgcmVzcG9uc2Ugb2JqZWN0IHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCBqc29uOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIHJldHVybiBqc29uO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgT3BlbkNvbm5lY3Rpb25SZXF1ZXN0O1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
