"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var request_1 = require("./request");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
/**
 * A request that comes from an already-authenticated user.
 */
var AuthenticatedRequest = /** @class */ (function (_super) {
    __extends(AuthenticatedRequest, _super);
    function AuthenticatedRequest(userMetaData, ticket) {
        var _this = _super.call(this) || this;
        if (!userMetaData || !ticket) {
            throw new missingconstructorargumenterror_1.default();
        }
        else {
            _this._userMetaData = userMetaData;
            _this._ticket = ticket;
        }
        return _this;
    }
    /**
     * Get some information about the user making this request.
     */
    AuthenticatedRequest.prototype.getUserMetaData = function () {
        return this._userMetaData;
    };
    /**
     * Get authentication ticket of this authenticated request.
     */
    AuthenticatedRequest.prototype.getTicket = function () {
        return this._ticket;
    };
    /**
     * Get JSON object that represents this request.
     */
    AuthenticatedRequest.prototype.toJSON = function () {
        var result = _super.prototype.toJSON.call(this);
        if (this.getUserMetaData()) {
            result.userMetaData = this.getUserMetaData().toJSON();
        }
        if (this.getTicket()) {
            result.ticket = this.getTicket().toJSON();
        }
        return result;
    };
    AuthenticatedRequest.TYPE = "authenticatedrequest";
    return AuthenticatedRequest;
}(request_1.default));
exports.AuthenticatedRequest = AuthenticatedRequest;
exports.default = AuthenticatedRequest;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3QvYXV0aGVudGljYXRlZHJlcXVlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBRUEscUNBQWdDO0FBQ2hDLCtGQUEwRjtBQUUxRjs7R0FFRztBQUNIO0lBQW1ELHdDQUFPO0lBT3RELDhCQUFZLFlBQTBCLEVBQUUsTUFBYztRQUF0RCxZQUNJLGlCQUFPLFNBT1Y7UUFORyxFQUFFLENBQUMsQ0FBRSxDQUFDLFlBQVksSUFBSSxDQUFDLE1BQU8sQ0FBQyxDQUFDLENBQUM7WUFDN0IsTUFBTSxJQUFJLHlDQUErQixFQUFFLENBQUM7UUFDaEQsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osS0FBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUM7WUFDbEMsS0FBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDMUIsQ0FBQzs7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSw4Q0FBZSxHQUF0QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRDs7T0FFRztJQUNJLHdDQUFTLEdBQWhCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQUVEOztPQUVHO0lBQ0kscUNBQU0sR0FBYjtRQUNJLElBQUksTUFBTSxHQUE0QixpQkFBTSxNQUFNLFdBQUUsQ0FBQztRQUNyRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsZUFBZSxFQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE1BQU0sQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzFELENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsU0FBUyxFQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzlDLENBQUM7UUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUF6Q2EseUJBQUksR0FBRyxzQkFBc0IsQ0FBQztJQTRDaEQsMkJBQUM7Q0E5Q0QsQUE4Q0MsQ0E5Q2tELGlCQUFPLEdBOEN6RDtBQTlDcUIsb0RBQW9CO0FBZ0QxQyxrQkFBZSxvQkFBb0IsQ0FBQyIsImZpbGUiOiJwcm90b2NvbC9yZXF1ZXN0L2F1dGhlbnRpY2F0ZWRyZXF1ZXN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVzZXJNZXRhRGF0YSBmcm9tIFwiLi4vLi4vbW9kZWwvdXNlcm1ldGFkYXRhXCI7XHJcbmltcG9ydCBUaWNrZXQgZnJvbSBcIi4uLy4uL21vZGVsL3RpY2tldFwiO1xyXG5pbXBvcnQgUmVxdWVzdCBmcm9tIFwiLi9yZXF1ZXN0XCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcblxyXG4vKipcclxuICogQSByZXF1ZXN0IHRoYXQgY29tZXMgZnJvbSBhbiBhbHJlYWR5LWF1dGhlbnRpY2F0ZWQgdXNlci5cclxuICovXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBdXRoZW50aWNhdGVkUmVxdWVzdCBleHRlbmRzIFJlcXVlc3Qge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgVFlQRSA9IFwiYXV0aGVudGljYXRlZHJlcXVlc3RcIjtcclxuXHJcbiAgICBwcml2YXRlIF91c2VyTWV0YURhdGE6IFVzZXJNZXRhRGF0YTtcclxuICAgIHByaXZhdGUgX3RpY2tldDogVGlja2V0O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgaWYgKCAhdXNlck1ldGFEYXRhIHx8ICF0aWNrZXQgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5fdXNlck1ldGFEYXRhID0gdXNlck1ldGFEYXRhO1xyXG4gICAgICAgICAgICB0aGlzLl90aWNrZXQgPSB0aWNrZXQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHNvbWUgaW5mb3JtYXRpb24gYWJvdXQgdGhlIHVzZXIgbWFraW5nIHRoaXMgcmVxdWVzdC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFVzZXJNZXRhRGF0YSgpOiBVc2VyTWV0YURhdGEge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl91c2VyTWV0YURhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYXV0aGVudGljYXRpb24gdGlja2V0IG9mIHRoaXMgYXV0aGVudGljYXRlZCByZXF1ZXN0LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0VGlja2V0KCk6IFRpY2tldCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RpY2tldDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCBKU09OIG9iamVjdCB0aGF0IHJlcHJlc2VudHMgdGhpcyByZXF1ZXN0LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHsgW2tleTogc3RyaW5nXTogYW55OyB9IHtcclxuICAgICAgICBsZXQgcmVzdWx0OiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIGlmICggdGhpcy5nZXRVc2VyTWV0YURhdGEoKSApIHtcclxuICAgICAgICAgICAgcmVzdWx0LnVzZXJNZXRhRGF0YSA9IHRoaXMuZ2V0VXNlck1ldGFEYXRhKCkudG9KU09OKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggdGhpcy5nZXRUaWNrZXQoKSApIHtcclxuICAgICAgICAgICAgcmVzdWx0LnRpY2tldCA9IHRoaXMuZ2V0VGlja2V0KCkudG9KU09OKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQXV0aGVudGljYXRlZFJlcXVlc3Q7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
