"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usermetadata_1 = require("../../model/usermetadata");
var request_1 = require("./request");
var ticket_1 = require("../../model/ticket");
var authenticatedrequest_1 = require("./authenticatedrequest");
var point_1 = require("../../math/point");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforrequesterror_1 = require("../../error/invalidjsonforrequesterror");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
/**
 * Move a oject model in the world.
 */
var MoveObjectModelRequest = /** @class */ (function (_super) {
    __extends(MoveObjectModelRequest, _super);
    function MoveObjectModelRequest(userMetaData, ticket, objectModelId, endPoint) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        if (objectModelId && endPoint) {
            _this._objectModelId = objectModelId;
            _this._endPoint = endPoint;
        }
        else {
            throw new missingconstructorargumenterror_1.default();
        }
        return _this;
    }
    /**
     * Convert a JSON object to a request.
     */
    MoveObjectModelRequest.fromJSON = function (jsonObject) {
        var request;
        if (jsonObject.type !== MoveObjectModelRequest.TYPE) {
            throw new invalidjsonforrequesterror_1.default();
        }
        if (jsonObject.ticket && jsonObject.userMetaData &&
            jsonObject.objectModelId && jsonObject.endPoint) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var objectModelId = jsonObject.objectModelId;
            var endPoint = point_1.default.fromJSON(jsonObject.endPoint);
            request = new MoveObjectModelRequest(userMetaData, ticket, objectModelId, endPoint);
            request_1.default.populateJSON(request, jsonObject);
        }
        else {
            throw new invalidjsonerror_1.default();
        }
        return request;
    };
    /**
     * Get request type.
     */
    MoveObjectModelRequest.prototype.getType = function () {
        return MoveObjectModelRequest.TYPE;
    };
    Object.defineProperty(MoveObjectModelRequest.prototype, "objectModelId", {
        /**
         * Get the object model id to move.
         */
        get: function () {
            return this._objectModelId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MoveObjectModelRequest.prototype, "endPoint", {
        /**
         * Get the location to move the object model to.
         */
        get: function () {
            return this._endPoint;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Get JSON object that represents this request.
     */
    MoveObjectModelRequest.prototype.toJSON = function () {
        var json = _super.prototype.toJSON.call(this);
        json.objectModelId = this._objectModelId;
        json.endPoint = this._endPoint.toJSON();
        return json;
    };
    MoveObjectModelRequest.TYPE = "moveobjectmodelrequest";
    return MoveObjectModelRequest;
}(authenticatedrequest_1.default));
exports.MoveObjectModelRequest = MoveObjectModelRequest;
exports.default = MoveObjectModelRequest;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3QvbW92ZW9iamVjdG1vZGVscmVxdWVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQSx5REFBb0Q7QUFFcEQscUNBQWdDO0FBQ2hDLDZDQUF3QztBQUN4QywrREFBMEQ7QUFDMUQsMENBQXFDO0FBQ3JDLGlFQUE0RDtBQUM1RCxxRkFBZ0Y7QUFDaEYsK0ZBQTBGO0FBRTFGOztHQUVHO0FBQ0g7SUFBNEMsMENBQW9CO0lBK0I1RCxnQ0FBYSxZQUEwQixFQUFFLE1BQWMsRUFBRSxhQUFxQixFQUFFLFFBQWU7UUFBL0YsWUFDSSxrQkFBTSxZQUFZLEVBQUUsTUFBTSxDQUFDLFNBTzlCO1FBTkcsRUFBRSxDQUFDLENBQUUsYUFBYSxJQUFJLFFBQVMsQ0FBQyxDQUFDLENBQUM7WUFDOUIsS0FBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUM7WUFDcEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDOUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLHlDQUErQixFQUFFLENBQUM7UUFDaEQsQ0FBQzs7SUFDTCxDQUFDO0lBaENEOztPQUVHO0lBQ1csK0JBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUNsQyxJQUFJLE9BQU8sQ0FBQztRQUNaLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxJQUFJLEtBQUssc0JBQXNCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNwRCxNQUFNLElBQUksb0NBQTBCLEVBQUUsQ0FBQztRQUMzQyxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLE1BQU0sSUFBSSxVQUFVLENBQUMsWUFBWTtZQUM1QyxVQUFVLENBQUMsYUFBYSxJQUFJLFVBQVUsQ0FBQyxRQUFTLENBQUMsQ0FBQyxDQUFDO1lBRXBELElBQUksWUFBWSxHQUFHLHNCQUFZLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNsRSxJQUFJLE1BQU0sR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDaEQsSUFBSSxhQUFhLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQztZQUM3QyxJQUFJLFFBQVEsR0FBRyxlQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNuRCxPQUFPLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxZQUFZLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUNwRixpQkFBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFFOUMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztRQUNELE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQVlEOztPQUVHO0lBQ0ksd0NBQU8sR0FBZDtRQUNJLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7SUFDdkMsQ0FBQztJQUtELHNCQUFXLGlEQUFhO1FBSHhCOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUMvQixDQUFDOzs7T0FBQTtJQUtELHNCQUFXLDRDQUFRO1FBSG5COztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDOzs7T0FBQTtJQUVEOztPQUVHO0lBQ0ksdUNBQU0sR0FBYjtRQUNJLElBQUksSUFBSSxHQUEyQixpQkFBTSxNQUFNLFdBQUUsQ0FBQztRQUNsRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDekMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQXBFYSwyQkFBSSxHQUFHLHdCQUF3QixDQUFDO0lBc0VsRCw2QkFBQztDQXhFRCxBQXdFQyxDQXhFMkMsOEJBQW9CLEdBd0UvRDtBQXhFWSx3REFBc0I7QUEwRW5DLGtCQUFlLHNCQUFzQixDQUFDIiwiZmlsZSI6InByb3RvY29sL3JlcXVlc3QvbW92ZW9iamVjdG1vZGVscmVxdWVzdC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBXb3JsZCBmcm9tIFwiLi4vLi4vbW9kZWwvd29ybGRcIjtcclxuaW1wb3J0IFVzZXJNZXRhRGF0YSBmcm9tIFwiLi4vLi4vbW9kZWwvdXNlcm1ldGFkYXRhXCI7XHJcbmltcG9ydCBPYmplY3RNb2RlbCBmcm9tIFwiLi4vLi4vbW9kZWwvb2JqZWN0bW9kZWxcIjtcclxuaW1wb3J0IFJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdFwiO1xyXG5pbXBvcnQgVGlja2V0IGZyb20gXCIuLi8uLi9tb2RlbC90aWNrZXRcIjtcclxuaW1wb3J0IEF1dGhlbnRpY2F0ZWRSZXF1ZXN0IGZyb20gXCIuL2F1dGhlbnRpY2F0ZWRyZXF1ZXN0XCI7XHJcbmltcG9ydCBQb2ludCBmcm9tIFwiLi4vLi4vbWF0aC9wb2ludFwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXF1ZXN0RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVxdWVzdGVycm9yXCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcblxyXG4vKipcclxuICogTW92ZSBhIG9qZWN0IG1vZGVsIGluIHRoZSB3b3JsZC5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBNb3ZlT2JqZWN0TW9kZWxSZXF1ZXN0IGV4dGVuZHMgQXV0aGVudGljYXRlZFJlcXVlc3Qge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgVFlQRSA9IFwibW92ZW9iamVjdG1vZGVscmVxdWVzdFwiO1xyXG5cclxuICAgIHByaXZhdGUgX29iamVjdE1vZGVsSWQ6IHN0cmluZztcclxuICAgIHByaXZhdGUgX2VuZFBvaW50OiBQb2ludDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgYSBKU09OIG9iamVjdCB0byBhIHJlcXVlc3QuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iamVjdDogYW55KTogTW92ZU9iamVjdE1vZGVsUmVxdWVzdCB7XHJcbiAgICAgICAgbGV0IHJlcXVlc3Q7XHJcbiAgICAgICAgaWYgKCBqc29uT2JqZWN0LnR5cGUgIT09IE1vdmVPYmplY3RNb2RlbFJlcXVlc3QuVFlQRSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORm9yUmVxdWVzdEVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICgganNvbk9iamVjdC50aWNrZXQgJiYganNvbk9iamVjdC51c2VyTWV0YURhdGEgJiZcclxuICAgICAgICAgICAgIGpzb25PYmplY3Qub2JqZWN0TW9kZWxJZCAmJiBqc29uT2JqZWN0LmVuZFBvaW50ICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHVzZXJNZXRhRGF0YSA9IFVzZXJNZXRhRGF0YS5mcm9tSlNPTihqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSk7XHJcbiAgICAgICAgICAgIGxldCB0aWNrZXQgPSBUaWNrZXQuZnJvbUpTT04oanNvbk9iamVjdC50aWNrZXQpO1xyXG4gICAgICAgICAgICBsZXQgb2JqZWN0TW9kZWxJZCA9IGpzb25PYmplY3Qub2JqZWN0TW9kZWxJZDtcclxuICAgICAgICAgICAgbGV0IGVuZFBvaW50ID0gUG9pbnQuZnJvbUpTT04oanNvbk9iamVjdC5lbmRQb2ludCk7XHJcbiAgICAgICAgICAgIHJlcXVlc3QgPSBuZXcgTW92ZU9iamVjdE1vZGVsUmVxdWVzdCh1c2VyTWV0YURhdGEsIHRpY2tldCwgb2JqZWN0TW9kZWxJZCwgZW5kUG9pbnQpO1xyXG4gICAgICAgICAgICBSZXF1ZXN0LnBvcHVsYXRlSlNPTihyZXF1ZXN0LCBqc29uT2JqZWN0KTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlcXVlc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgb2JqZWN0TW9kZWxJZDogc3RyaW5nLCBlbmRQb2ludDogUG9pbnQgKSB7XHJcbiAgICAgICAgc3VwZXIodXNlck1ldGFEYXRhLCB0aWNrZXQpO1xyXG4gICAgICAgIGlmICggb2JqZWN0TW9kZWxJZCAmJiBlbmRQb2ludCApIHtcclxuICAgICAgICAgICAgdGhpcy5fb2JqZWN0TW9kZWxJZCA9IG9iamVjdE1vZGVsSWQ7XHJcbiAgICAgICAgICAgIHRoaXMuX2VuZFBvaW50ID0gZW5kUG9pbnQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgcmVxdWVzdCB0eXBlLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0VHlwZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBNb3ZlT2JqZWN0TW9kZWxSZXF1ZXN0LlRZUEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIG9iamVjdCBtb2RlbCBpZCB0byBtb3ZlLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0IG9iamVjdE1vZGVsSWQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fb2JqZWN0TW9kZWxJZDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgbG9jYXRpb24gdG8gbW92ZSB0aGUgb2JqZWN0IG1vZGVsIHRvLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0IGVuZFBvaW50KCk6IFBvaW50IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZW5kUG9pbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgSlNPTiBvYmplY3QgdGhhdCByZXByZXNlbnRzIHRoaXMgcmVxdWVzdC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IGpzb246IHsgW2tleTogc3RyaW5nXTogYW55IH0gPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICBqc29uLm9iamVjdE1vZGVsSWQgPSB0aGlzLl9vYmplY3RNb2RlbElkO1xyXG4gICAgICAgIGpzb24uZW5kUG9pbnQgPSB0aGlzLl9lbmRQb2ludC50b0pTT04oKTtcclxuICAgICAgICByZXR1cm4ganNvbjtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1vdmVPYmplY3RNb2RlbFJlcXVlc3Q7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
