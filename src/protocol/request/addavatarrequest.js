"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usermetadata_1 = require("../../model/usermetadata");
var request_1 = require("./request");
var ticket_1 = require("../../model/ticket");
var authenticatedrequest_1 = require("./authenticatedrequest");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforrequesterror_1 = require("../../error/invalidjsonforrequesterror");
/**
 * Adds an avatar to the world.
 */
var AddAvatarRequest = /** @class */ (function (_super) {
    __extends(AddAvatarRequest, _super);
    function AddAvatarRequest(userMetaData, ticket) {
        return _super.call(this, userMetaData, ticket) || this;
    }
    /**
     * Convert a JSON object to a request.
     * @param jsonObject
     * @returns {AddObjectModelCommand}
     */
    AddAvatarRequest.fromJSON = function (jsonObject) {
        var request;
        if (jsonObject.type !== AddAvatarRequest.TYPE) {
            throw new invalidjsonforrequesterror_1.default();
        }
        if (jsonObject.ticket && jsonObject.userMetaData) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            request = new AddAvatarRequest(userMetaData, ticket);
            request_1.default.populateJSON(request, jsonObject);
        }
        else {
            throw new invalidjsonerror_1.default();
        }
        return request;
    };
    /**
     * Get command type.
     */
    AddAvatarRequest.prototype.getType = function () {
        return AddAvatarRequest.TYPE;
    };
    /**
     * Get JSON object that represents this command.
     */
    AddAvatarRequest.prototype.toJSON = function () {
        var json = _super.prototype.toJSON.call(this);
        return json;
    };
    AddAvatarRequest.TYPE = "addavatarrequest";
    return AddAvatarRequest;
}(authenticatedrequest_1.default));
exports.AddAvatarRequest = AddAvatarRequest;
exports.default = AddAvatarRequest;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3QvYWRkYXZhdGFycmVxdWVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQSx5REFBb0Q7QUFFcEQscUNBQWdDO0FBQ2hDLDZDQUF3QztBQUN4QywrREFBMEQ7QUFDMUQsaUVBQTREO0FBQzVELHFGQUFnRjtBQUVoRjs7R0FFRztBQUNIO0lBQXNDLG9DQUFvQjtJQThCdEQsMEJBQWEsWUFBMEIsRUFBRSxNQUFjO2VBQ25ELGtCQUFNLFlBQVksRUFBRSxNQUFNLENBQUM7SUFDL0IsQ0FBQztJQTFCRDs7OztPQUlHO0lBQ1cseUJBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUNsQyxJQUFJLE9BQU8sQ0FBQztRQUNaLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUM5QyxNQUFNLElBQUksb0NBQTBCLEVBQUUsQ0FBQztRQUMzQyxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLE1BQU0sSUFBSSxVQUFVLENBQUMsWUFBYSxDQUFDLENBQUMsQ0FBQztZQUVqRCxJQUFJLFlBQVksR0FBRyxzQkFBWSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEUsSUFBSSxNQUFNLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRWhELE9BQU8sR0FBRyxJQUFJLGdCQUFnQixDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsQ0FBQztZQUNyRCxpQkFBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFFOUMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztRQUNELE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQU1EOztPQUVHO0lBQ0ksa0NBQU8sR0FBZDtRQUNJLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQUVEOztPQUVHO0lBQ0ksaUNBQU0sR0FBYjtRQUNJLElBQUksSUFBSSxHQUE0QixpQkFBTSxNQUFNLFdBQUUsQ0FBQztRQUNuRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUE3Q2EscUJBQUksR0FBRyxrQkFBa0IsQ0FBQztJQStDNUMsdUJBQUM7Q0FqREQsQUFpREMsQ0FqRHFDLDhCQUFvQixHQWlEekQ7QUFqRFksNENBQWdCO0FBbUQ3QixrQkFBZSxnQkFBZ0IsQ0FBQyIsImZpbGUiOiJwcm90b2NvbC9yZXF1ZXN0L2FkZGF2YXRhcnJlcXVlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgV29ybGQgZnJvbSBcIi4uLy4uL21vZGVsL3dvcmxkXCI7XHJcbmltcG9ydCBVc2VyTWV0YURhdGEgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJtZXRhZGF0YVwiO1xyXG5pbXBvcnQgT2JqZWN0TW9kZWwgZnJvbSBcIi4uLy4uL21vZGVsL29iamVjdG1vZGVsXCI7XHJcbmltcG9ydCBSZXF1ZXN0IGZyb20gXCIuL3JlcXVlc3RcIjtcclxuaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBBdXRoZW50aWNhdGVkUmVxdWVzdCBmcm9tIFwiLi9hdXRoZW50aWNhdGVkcmVxdWVzdFwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXF1ZXN0RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVxdWVzdGVycm9yXCI7XHJcblxyXG4vKipcclxuICogQWRkcyBhbiBhdmF0YXIgdG8gdGhlIHdvcmxkLlxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIEFkZEF2YXRhclJlcXVlc3QgZXh0ZW5kcyBBdXRoZW50aWNhdGVkUmVxdWVzdCB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJhZGRhdmF0YXJyZXF1ZXN0XCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfb2JqZWN0TW9kZWw6IE9iamVjdE1vZGVsO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBhIEpTT04gb2JqZWN0IHRvIGEgcmVxdWVzdC5cclxuICAgICAqIEBwYXJhbSBqc29uT2JqZWN0XHJcbiAgICAgKiBAcmV0dXJucyB7QWRkT2JqZWN0TW9kZWxDb21tYW5kfVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSk6IEFkZEF2YXRhclJlcXVlc3Qge1xyXG4gICAgICAgIGxldCByZXF1ZXN0O1xyXG4gICAgICAgIGlmICgganNvbk9iamVjdC50eXBlICE9PSBBZGRBdmF0YXJSZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkZvclJlcXVlc3RFcnJvcigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudGlja2V0ICYmIGpzb25PYmplY3QudXNlck1ldGFEYXRhICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHVzZXJNZXRhRGF0YSA9IFVzZXJNZXRhRGF0YS5mcm9tSlNPTihqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSk7XHJcbiAgICAgICAgICAgIGxldCB0aWNrZXQgPSBUaWNrZXQuZnJvbUpTT04oanNvbk9iamVjdC50aWNrZXQpO1xyXG5cclxuICAgICAgICAgICAgcmVxdWVzdCA9IG5ldyBBZGRBdmF0YXJSZXF1ZXN0KHVzZXJNZXRhRGF0YSwgdGlja2V0KTtcclxuICAgICAgICAgICAgUmVxdWVzdC5wb3B1bGF0ZUpTT04ocmVxdWVzdCwganNvbk9iamVjdCk7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXF1ZXN0O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKCB1c2VyTWV0YURhdGE6IFVzZXJNZXRhRGF0YSwgdGlja2V0OiBUaWNrZXQgKSB7XHJcbiAgICAgICAgc3VwZXIodXNlck1ldGFEYXRhLCB0aWNrZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGNvbW1hbmQgdHlwZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFR5cGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gQWRkQXZhdGFyUmVxdWVzdC5UWVBFO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IEpTT04gb2JqZWN0IHRoYXQgcmVwcmVzZW50cyB0aGlzIGNvbW1hbmQuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCBqc29uOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIHJldHVybiBqc29uO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQWRkQXZhdGFyUmVxdWVzdDtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
