import Uuid from "../../uuid";

export abstract class Request {

    private _requestId: string;

    /**
     * Populate request object with fields from JSON object.
     * @param response
     * @param jsonObject
     */
    public static populateJSON(request: Request, jsonObject: any): Request {
        request.requestId = jsonObject.requestId;
        return request;
    }

    constructor() {
        this._requestId = Uuid.getUuid();
    }

    /**
     * Get the request id for this request.
     */
    get requestId(): string {
        return this._requestId;
    }

    /**
     * Set the request id for this request.
     * @param requestId
     */
    set requestId(requestId: string) {
        this._requestId = requestId;
    }

    /**
     * Get the request type.
     */
    public abstract getType(): string;

    /**
     * Convert JSON to the specific Request object.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = {};
        result.type = this.getType();
        if ( this._requestId ) {
            result.requestId = this._requestId;
        }
        return result;
    }

}

export default Request;
