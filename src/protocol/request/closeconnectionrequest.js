"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ticket_1 = require("../../model/ticket");
var usermetadata_1 = require("../../model/usermetadata");
var authenticatedrequest_1 = require("./authenticatedrequest");
var request_1 = require("./request");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforrequesterror_1 = require("../../error/invalidjsonforrequesterror");
var CloseConnectionRequest = /** @class */ (function (_super) {
    __extends(CloseConnectionRequest, _super);
    function CloseConnectionRequest(userMetaData, ticket, result) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        _this._result = result;
        return _this;
    }
    /**
     * Convert JSON object to CloseConnectionRequest.
     * @param jsonObject
     */
    CloseConnectionRequest.fromJSON = function (jsonObject) {
        var request;
        if (jsonObject.type !== CloseConnectionRequest.TYPE) {
            throw new invalidjsonforrequesterror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket && jsonObject.requestId) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var result = jsonObject.result;
            request = new CloseConnectionRequest(userMetaData, ticket, result);
            request_1.default.populateJSON(request, jsonObject);
        }
        else {
            throw new invalidjsonerror_1.default();
        }
        return request;
    };
    CloseConnectionRequest.prototype.getType = function () {
        return CloseConnectionRequest.TYPE;
    };
    /**
     * Convert this response object to JSON.
     */
    CloseConnectionRequest.prototype.toJSON = function () {
        var json = _super.prototype.toJSON.call(this);
        json.result = this._result;
        return json;
    };
    CloseConnectionRequest.TYPE = "closeconnectionrequest";
    return CloseConnectionRequest;
}(authenticatedrequest_1.default));
exports.CloseConnectionRequest = CloseConnectionRequest;
exports.default = CloseConnectionRequest;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3QvY2xvc2Vjb25uZWN0aW9ucmVxdWVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSw2Q0FBd0M7QUFDeEMseURBQW9EO0FBQ3BELCtEQUEwRDtBQUMxRCxxQ0FBZ0M7QUFDaEMsaUVBQTREO0FBQzVELHFGQUFnRjtBQUVoRjtJQUE0QywwQ0FBb0I7SUE0QjVELGdDQUFZLFlBQTBCLEVBQUUsTUFBYyxFQUFFLE1BQWU7UUFBdkUsWUFDSSxrQkFBTSxZQUFZLEVBQUUsTUFBTSxDQUFDLFNBRTlCO1FBREcsS0FBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7O0lBQzFCLENBQUM7SUF6QkQ7OztPQUdHO0lBQ1csK0JBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUNsQyxJQUFJLE9BQStCLENBQUM7UUFDcEMsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLElBQUksS0FBSyxzQkFBc0IsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3BELE1BQU0sSUFBSSxvQ0FBMEIsRUFBRSxDQUFDO1FBQzNDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLFlBQVksSUFBSSxVQUFVLENBQUMsTUFBTSxJQUFJLFVBQVUsQ0FBQyxTQUFVLENBQUMsQ0FBQyxDQUFDO1lBRWhGLElBQUksWUFBWSxHQUFHLHNCQUFZLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNsRSxJQUFJLE1BQU0sR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDaEQsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQztZQUMvQixPQUFPLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxZQUFZLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ25FLGlCQUFPLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztRQUU5QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksMEJBQWdCLEVBQUUsQ0FBQztRQUNqQyxDQUFDO1FBQ0QsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBT00sd0NBQU8sR0FBZDtRQUNJLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7SUFDdkMsQ0FBQztJQUVEOztPQUVHO0lBQ0ksdUNBQU0sR0FBYjtRQUNJLElBQUksSUFBSSxHQUE0QixpQkFBTSxNQUFNLFdBQUUsQ0FBQztRQUNuRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBMUNhLDJCQUFJLEdBQUcsd0JBQXdCLENBQUM7SUE0Q2xELDZCQUFDO0NBOUNELEFBOENDLENBOUMyQyw4QkFBb0IsR0E4Qy9EO0FBOUNZLHdEQUFzQjtBQWdEbkMsa0JBQWUsc0JBQXNCLENBQUMiLCJmaWxlIjoicHJvdG9jb2wvcmVxdWVzdC9jbG9zZWNvbm5lY3Rpb25yZXF1ZXN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBVc2VyTWV0YURhdGEgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJtZXRhZGF0YVwiO1xyXG5pbXBvcnQgQXV0aGVudGljYXRlZFJlcXVlc3QgZnJvbSBcIi4vYXV0aGVudGljYXRlZHJlcXVlc3RcIjtcclxuaW1wb3J0IFJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdFwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXF1ZXN0RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVxdWVzdGVycm9yXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2xvc2VDb25uZWN0aW9uUmVxdWVzdCBleHRlbmRzIEF1dGhlbnRpY2F0ZWRSZXF1ZXN0IHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcImNsb3NlY29ubmVjdGlvbnJlcXVlc3RcIjtcclxuXHJcbiAgICBwcml2YXRlIF9yZXN1bHQ6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IEpTT04gb2JqZWN0IHRvIENsb3NlQ29ubmVjdGlvblJlcXVlc3QuXHJcbiAgICAgKiBAcGFyYW0ganNvbk9iamVjdFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSk6IENsb3NlQ29ubmVjdGlvblJlcXVlc3Qge1xyXG4gICAgICAgIGxldCByZXF1ZXN0OiBDbG9zZUNvbm5lY3Rpb25SZXF1ZXN0O1xyXG4gICAgICAgIGlmICgganNvbk9iamVjdC50eXBlICE9PSBDbG9zZUNvbm5lY3Rpb25SZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkZvclJlcXVlc3RFcnJvcigpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3QudXNlck1ldGFEYXRhICYmIGpzb25PYmplY3QudGlja2V0ICYmIGpzb25PYmplY3QucmVxdWVzdElkICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHVzZXJNZXRhRGF0YSA9IFVzZXJNZXRhRGF0YS5mcm9tSlNPTihqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSk7XHJcbiAgICAgICAgICAgIGxldCB0aWNrZXQgPSBUaWNrZXQuZnJvbUpTT04oanNvbk9iamVjdC50aWNrZXQpO1xyXG4gICAgICAgICAgICBsZXQgcmVzdWx0ID0ganNvbk9iamVjdC5yZXN1bHQ7XHJcbiAgICAgICAgICAgIHJlcXVlc3QgPSBuZXcgQ2xvc2VDb25uZWN0aW9uUmVxdWVzdCh1c2VyTWV0YURhdGEsIHRpY2tldCwgcmVzdWx0KTtcclxuICAgICAgICAgICAgUmVxdWVzdC5wb3B1bGF0ZUpTT04ocmVxdWVzdCwganNvbk9iamVjdCk7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkVycm9yKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXF1ZXN0O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgcmVzdWx0OiBib29sZWFuKSB7XHJcbiAgICAgICAgc3VwZXIodXNlck1ldGFEYXRhLCB0aWNrZXQpO1xyXG4gICAgICAgIHRoaXMuX3Jlc3VsdCA9IHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0VHlwZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBDbG9zZUNvbm5lY3Rpb25SZXF1ZXN0LlRZUEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgcmVzcG9uc2Ugb2JqZWN0IHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCBqc29uOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIGpzb24ucmVzdWx0ID0gdGhpcy5fcmVzdWx0O1xyXG4gICAgICAgIHJldHVybiBqc29uO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQ2xvc2VDb25uZWN0aW9uUmVxdWVzdDtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
