import AuthenticatedRequest from "./authenticatedrequest";
import Ticket from "../../model/ticket";
import UserMetaData from "../../model/usermetadata";
import Request from "./request";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForRequestError from "../../error/invalidjsonforrequesterror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";

export class BroadcastMessageRequest extends AuthenticatedRequest {

    public static TYPE = "broadcastmessagerequest";

    private _message: string;

    /**
     * Convert a JSON object to a BroadcastMessageRequest.
     * @param jsonObject
     */
    public static fromJSON(jsonObject: any): BroadcastMessageRequest {

        let request: BroadcastMessageRequest;

        if ( jsonObject.type !== BroadcastMessageRequest.TYPE ) {
            throw new InvalidJSONForRequestError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket && jsonObject.requestId ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            request = new BroadcastMessageRequest(userMetaData, ticket, jsonObject.message);
            Request.populateJSON(request, jsonObject);

        } else {
            throw new InvalidJSONError();
        }

        return request;
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, message: string) {
        super(userMetaData, ticket);
        if ( message ) {
            this._message = message;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Get message from the client that is sent to the server to broadcast.
     * @param message
     */
    get message(): string {
        return this._message;
    }

    /**
     * Get request type.
     */
    public getType(): string {
        return BroadcastMessageRequest.TYPE;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = super.toJSON();
        json.message = this._message;
        return json;
    }

}

export default BroadcastMessageRequest;
