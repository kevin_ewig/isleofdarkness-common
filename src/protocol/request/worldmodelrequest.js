"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var authenticatedrequest_1 = require("./authenticatedrequest");
var usermetadata_1 = require("../../model/usermetadata");
var ticket_1 = require("../../model/ticket");
var common_1 = require("../../math/common");
var request_1 = require("./request");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforrequesterror_1 = require("../../error/invalidjsonforrequesterror");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
/**
 * Requests a subset of the world model whose object models intersect
 * the given shape.
 */
var WorldModelRequest = /** @class */ (function (_super) {
    __extends(WorldModelRequest, _super);
    function WorldModelRequest(userMetaData, ticket, shape) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        if (shape) {
            _this._shape = shape;
        }
        else {
            throw new missingconstructorargumenterror_1.default();
        }
        return _this;
    }
    /**
     * Convert a JSON object to a WorldModelRequest.
     *
     * @param jsonObject
     * @returns {WorldModelRequest}
     */
    WorldModelRequest.fromJSON = function (jsonObject) {
        var request;
        if (jsonObject.type !== WorldModelRequest.TYPE) {
            throw new invalidjsonforrequesterror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket && jsonObject.shape) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            var shape = common_1.Common.convertShape(jsonObject.shape);
            request = new WorldModelRequest(userMetaData, ticket, shape);
            request_1.default.populateJSON(request, jsonObject);
        }
        else {
            throw new invalidjsonerror_1.default();
        }
        return request;
    };
    /**
     * Get what type of request this is.
     */
    WorldModelRequest.prototype.getType = function () {
        return WorldModelRequest.TYPE;
    };
    Object.defineProperty(WorldModelRequest.prototype, "shape", {
        /**
         * Get the shape.
         */
        get: function () {
            return this._shape;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert this response object to JSON.
     */
    WorldModelRequest.prototype.toJSON = function () {
        var json = _super.prototype.toJSON.call(this);
        json.shape = this._shape.toJSON();
        return json;
    };
    WorldModelRequest.TYPE = "worldmodelrequest";
    return WorldModelRequest;
}(authenticatedrequest_1.default));
exports.WorldModelRequest = WorldModelRequest;
exports.default = WorldModelRequest;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3Qvd29ybGRtb2RlbHJlcXVlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBQTBEO0FBRTFELHlEQUFvRDtBQUNwRCw2Q0FBd0M7QUFDeEMsNENBQXlDO0FBRXpDLHFDQUFnQztBQUNoQyxpRUFBNEQ7QUFDNUQscUZBQWdGO0FBQ2hGLCtGQUEwRjtBQUUxRjs7O0dBR0c7QUFDSDtJQUF1QyxxQ0FBb0I7SUFnQ3ZELDJCQUFhLFlBQTBCLEVBQUUsTUFBYyxFQUFFLEtBQVk7UUFBckUsWUFDSSxrQkFBTSxZQUFZLEVBQUUsTUFBTSxDQUFDLFNBTTlCO1FBTEcsRUFBRSxDQUFDLENBQUUsS0FBTSxDQUFDLENBQUMsQ0FBQztZQUNWLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSx5Q0FBK0IsRUFBRSxDQUFDO1FBQ2hELENBQUM7O0lBQ0wsQ0FBQztJQWpDRDs7Ozs7T0FLRztJQUNXLDBCQUFRLEdBQXRCLFVBQXVCLFVBQWU7UUFFbEMsSUFBSSxPQUEwQixDQUFDO1FBRS9CLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEtBQUssaUJBQWlCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUM5QyxNQUFNLElBQUksb0NBQTBCLEVBQUUsQ0FBQztRQUMzQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxZQUFZLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxVQUFVLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQztZQUU1RSxJQUFJLFlBQVksR0FBRyxzQkFBWSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEUsSUFBSSxNQUFNLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELElBQUksS0FBSyxHQUFHLGVBQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2xELE9BQU8sR0FBRyxJQUFJLGlCQUFpQixDQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFFLENBQUM7WUFDL0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBRTlDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSwwQkFBZ0IsRUFBRSxDQUFDO1FBQ2pDLENBQUM7UUFDRCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFXRDs7T0FFRztJQUNJLG1DQUFPLEdBQWQ7UUFDSSxNQUFNLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFLRCxzQkFBVyxvQ0FBSztRQUhoQjs7V0FFRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQzs7O09BQUE7SUFFRDs7T0FFRztJQUNJLGtDQUFNLEdBQWI7UUFDSSxJQUFJLElBQUksR0FBNEIsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDbkQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQTVEYSxzQkFBSSxHQUFHLG1CQUFtQixDQUFDO0lBOEQ3Qyx3QkFBQztDQWhFRCxBQWdFQyxDQWhFc0MsOEJBQW9CLEdBZ0UxRDtBQWhFWSw4Q0FBaUI7QUFrRTlCLGtCQUFlLGlCQUFpQixDQUFDIiwiZmlsZSI6InByb3RvY29sL3JlcXVlc3Qvd29ybGRtb2RlbHJlcXVlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQXV0aGVudGljYXRlZFJlcXVlc3QgZnJvbSBcIi4vYXV0aGVudGljYXRlZHJlcXVlc3RcIjtcclxuaW1wb3J0IFNoYXBlIGZyb20gXCIuLi8uLi9tYXRoL3NoYXBlXCI7XHJcbmltcG9ydCBVc2VyTWV0YURhdGEgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJtZXRhZGF0YVwiO1xyXG5pbXBvcnQgVGlja2V0IGZyb20gXCIuLi8uLi9tb2RlbC90aWNrZXRcIjtcclxuaW1wb3J0IHtDb21tb259IGZyb20gXCIuLi8uLi9tYXRoL2NvbW1vblwiO1xyXG5pbXBvcnQgQ29udmVydGVyIGZyb20gXCIuLi9jb252ZXJ0ZXJcIjtcclxuaW1wb3J0IFJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdFwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXF1ZXN0RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVxdWVzdGVycm9yXCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcblxyXG4vKipcclxuICogUmVxdWVzdHMgYSBzdWJzZXQgb2YgdGhlIHdvcmxkIG1vZGVsIHdob3NlIG9iamVjdCBtb2RlbHMgaW50ZXJzZWN0XHJcbiAqIHRoZSBnaXZlbiBzaGFwZS5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBXb3JsZE1vZGVsUmVxdWVzdCBleHRlbmRzIEF1dGhlbnRpY2F0ZWRSZXF1ZXN0IHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcIndvcmxkbW9kZWxyZXF1ZXN0XCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfc2hhcGU6IFNoYXBlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBhIEpTT04gb2JqZWN0IHRvIGEgV29ybGRNb2RlbFJlcXVlc3QuXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGpzb25PYmplY3RcclxuICAgICAqIEByZXR1cm5zIHtXb3JsZE1vZGVsUmVxdWVzdH1cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqZWN0OiBhbnkpOiBXb3JsZE1vZGVsUmVxdWVzdCB7XHJcblxyXG4gICAgICAgIGxldCByZXF1ZXN0OiBXb3JsZE1vZGVsUmVxdWVzdDtcclxuXHJcbiAgICAgICAgaWYgKGpzb25PYmplY3QudHlwZSAhPT0gV29ybGRNb2RlbFJlcXVlc3QuVFlQRSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORm9yUmVxdWVzdEVycm9yKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICgganNvbk9iamVjdC51c2VyTWV0YURhdGEgJiYganNvbk9iamVjdC50aWNrZXQgJiYganNvbk9iamVjdC5zaGFwZSApIHtcclxuXHJcbiAgICAgICAgICAgIGxldCB1c2VyTWV0YURhdGEgPSBVc2VyTWV0YURhdGEuZnJvbUpTT04oanNvbk9iamVjdC51c2VyTWV0YURhdGEpO1xyXG4gICAgICAgICAgICBsZXQgdGlja2V0ID0gVGlja2V0LmZyb21KU09OKGpzb25PYmplY3QudGlja2V0KTtcclxuICAgICAgICAgICAgbGV0IHNoYXBlID0gQ29tbW9uLmNvbnZlcnRTaGFwZShqc29uT2JqZWN0LnNoYXBlKTtcclxuICAgICAgICAgICAgcmVxdWVzdCA9IG5ldyBXb3JsZE1vZGVsUmVxdWVzdCggdXNlck1ldGFEYXRhLCB0aWNrZXQsIHNoYXBlICk7XHJcbiAgICAgICAgICAgIFJlcXVlc3QucG9wdWxhdGVKU09OKHJlcXVlc3QsIGpzb25PYmplY3QpO1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZEpTT05FcnJvcigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVxdWVzdDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvciggdXNlck1ldGFEYXRhOiBVc2VyTWV0YURhdGEsIHRpY2tldDogVGlja2V0LCBzaGFwZTogU2hhcGUgKSB7XHJcbiAgICAgICAgc3VwZXIodXNlck1ldGFEYXRhLCB0aWNrZXQpO1xyXG4gICAgICAgIGlmICggc2hhcGUgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3NoYXBlID0gc2hhcGU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgd2hhdCB0eXBlIG9mIHJlcXVlc3QgdGhpcyBpcy5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFR5cGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gV29ybGRNb2RlbFJlcXVlc3QuVFlQRTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgc2hhcGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXQgc2hhcGUoKTogU2hhcGUge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zaGFwZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyByZXNwb25zZSBvYmplY3QgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IGpzb246IHsgW2tleTogc3RyaW5nXTogYW55OyB9ID0gc3VwZXIudG9KU09OKCk7XHJcbiAgICAgICAganNvbi5zaGFwZSA9IHRoaXMuX3NoYXBlLnRvSlNPTigpO1xyXG4gICAgICAgIHJldHVybiBqc29uO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgV29ybGRNb2RlbFJlcXVlc3Q7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
