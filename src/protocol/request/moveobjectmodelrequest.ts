import World from "../../model/world";
import UserMetaData from "../../model/usermetadata";
import ObjectModel from "../../model/objectmodel";
import Request from "./request";
import Ticket from "../../model/ticket";
import AuthenticatedRequest from "./authenticatedrequest";
import Point from "../../math/point";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForRequestError from "../../error/invalidjsonforrequesterror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";

/**
 * Move a oject model in the world.
 */
export class MoveObjectModelRequest extends AuthenticatedRequest {

    public static TYPE = "moveobjectmodelrequest";

    private _objectModelId: string;
    private _endPoint: Point;

    /**
     * Convert a JSON object to a request.
     */
    public static fromJSON(jsonObject: any): MoveObjectModelRequest {
        let request;
        if ( jsonObject.type !== MoveObjectModelRequest.TYPE ) {
            throw new InvalidJSONForRequestError();
        }
        if ( jsonObject.ticket && jsonObject.userMetaData &&
             jsonObject.objectModelId && jsonObject.endPoint ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let objectModelId = jsonObject.objectModelId;
            let endPoint = Point.fromJSON(jsonObject.endPoint);
            request = new MoveObjectModelRequest(userMetaData, ticket, objectModelId, endPoint);
            Request.populateJSON(request, jsonObject);

        } else {
            throw new InvalidJSONError();
        }
        return request;
    }

    constructor( userMetaData: UserMetaData, ticket: Ticket, objectModelId: string, endPoint: Point ) {
        super(userMetaData, ticket);
        if ( objectModelId && endPoint ) {
            this._objectModelId = objectModelId;
            this._endPoint = endPoint;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Get request type.
     */
    public getType(): string {
        return MoveObjectModelRequest.TYPE;
    }

    /**
     * Get the object model id to move.
     */
    public get objectModelId(): string {
        return this._objectModelId;
    }

    /**
     * Get the location to move the object model to.
     */
    public get endPoint(): Point {
        return this._endPoint;
    }

    /**
     * Get JSON object that represents this request.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any } = super.toJSON();
        json.objectModelId = this._objectModelId;
        json.endPoint = this._endPoint.toJSON();
        return json;
    }

}

export default MoveObjectModelRequest;
