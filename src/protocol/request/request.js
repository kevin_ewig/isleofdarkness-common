"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("../../uuid");
var Request = /** @class */ (function () {
    function Request() {
        this._requestId = uuid_1.default.getUuid();
    }
    /**
     * Populate request object with fields from JSON object.
     * @param response
     * @param jsonObject
     */
    Request.populateJSON = function (request, jsonObject) {
        request.requestId = jsonObject.requestId;
        return request;
    };
    Object.defineProperty(Request.prototype, "requestId", {
        /**
         * Get the request id for this request.
         */
        get: function () {
            return this._requestId;
        },
        /**
         * Set the request id for this request.
         * @param requestId
         */
        set: function (requestId) {
            this._requestId = requestId;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert JSON to the specific Request object.
     */
    Request.prototype.toJSON = function () {
        var result = {};
        result.type = this.getType();
        if (this._requestId) {
            result.requestId = this._requestId;
        }
        return result;
    };
    return Request;
}());
exports.Request = Request;
exports.default = Request;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3QvcmVxdWVzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG1DQUE4QjtBQUU5QjtJQWNJO1FBQ0ksSUFBSSxDQUFDLFVBQVUsR0FBRyxjQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDckMsQ0FBQztJQVpEOzs7O09BSUc7SUFDVyxvQkFBWSxHQUExQixVQUEyQixPQUFnQixFQUFFLFVBQWU7UUFDeEQsT0FBTyxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDO1FBQ3pDLE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQVNELHNCQUFJLDhCQUFTO1FBSGI7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzNCLENBQUM7UUFFRDs7O1dBR0c7YUFDSCxVQUFjLFNBQWlCO1lBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBQ2hDLENBQUM7OztPQVJBO0lBZUQ7O09BRUc7SUFDSSx3QkFBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLEVBQUUsQ0FBQztRQUN6QyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM3QixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsVUFBVyxDQUFDLENBQUMsQ0FBQztZQUNwQixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDdkMsQ0FBQztRQUNELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVMLGNBQUM7QUFBRCxDQWxEQSxBQWtEQyxJQUFBO0FBbERxQiwwQkFBTztBQW9EN0Isa0JBQWUsT0FBTyxDQUFDIiwiZmlsZSI6InByb3RvY29sL3JlcXVlc3QvcmVxdWVzdC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBVdWlkIGZyb20gXCIuLi8uLi91dWlkXCI7XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgUmVxdWVzdCB7XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVxdWVzdElkOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQb3B1bGF0ZSByZXF1ZXN0IG9iamVjdCB3aXRoIGZpZWxkcyBmcm9tIEpTT04gb2JqZWN0LlxyXG4gICAgICogQHBhcmFtIHJlc3BvbnNlXHJcbiAgICAgKiBAcGFyYW0ganNvbk9iamVjdFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIHBvcHVsYXRlSlNPTihyZXF1ZXN0OiBSZXF1ZXN0LCBqc29uT2JqZWN0OiBhbnkpOiBSZXF1ZXN0IHtcclxuICAgICAgICByZXF1ZXN0LnJlcXVlc3RJZCA9IGpzb25PYmplY3QucmVxdWVzdElkO1xyXG4gICAgICAgIHJldHVybiByZXF1ZXN0O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuX3JlcXVlc3RJZCA9IFV1aWQuZ2V0VXVpZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSByZXF1ZXN0IGlkIGZvciB0aGlzIHJlcXVlc3QuXHJcbiAgICAgKi9cclxuICAgIGdldCByZXF1ZXN0SWQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcmVxdWVzdElkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSByZXF1ZXN0IGlkIGZvciB0aGlzIHJlcXVlc3QuXHJcbiAgICAgKiBAcGFyYW0gcmVxdWVzdElkXHJcbiAgICAgKi9cclxuICAgIHNldCByZXF1ZXN0SWQocmVxdWVzdElkOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLl9yZXF1ZXN0SWQgPSByZXF1ZXN0SWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIHJlcXVlc3QgdHlwZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGFic3RyYWN0IGdldFR5cGUoKTogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBKU09OIHRvIHRoZSBzcGVjaWZpYyBSZXF1ZXN0IG9iamVjdC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSB7fTtcclxuICAgICAgICByZXN1bHQudHlwZSA9IHRoaXMuZ2V0VHlwZSgpO1xyXG4gICAgICAgIGlmICggdGhpcy5fcmVxdWVzdElkICkge1xyXG4gICAgICAgICAgICByZXN1bHQucmVxdWVzdElkID0gdGhpcy5fcmVxdWVzdElkO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUmVxdWVzdDtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
