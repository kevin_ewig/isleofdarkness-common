import World from "../../model/world";
import UserMetaData from "../../model/usermetadata";
import ObjectModel from "../../model/objectmodel";
import Request from "./request";
import Ticket from "../../model/ticket";
import AuthenticatedRequest from "./authenticatedrequest";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForRequestError from "../../error/invalidjsonforrequesterror";

/**
 * Adds an avatar to the world.
 */
export class AddAvatarRequest extends AuthenticatedRequest {

    public static TYPE = "addavatarrequest";

    private _objectModel: ObjectModel;

    /**
     * Convert a JSON object to a request.
     * @param jsonObject
     * @returns {AddObjectModelCommand}
     */
    public static fromJSON(jsonObject: any): AddAvatarRequest {
        let request;
        if ( jsonObject.type !== AddAvatarRequest.TYPE ) {
            throw new InvalidJSONForRequestError();
        }
        if ( jsonObject.ticket && jsonObject.userMetaData ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);

            request = new AddAvatarRequest(userMetaData, ticket);
            Request.populateJSON(request, jsonObject);

        } else {
            throw new InvalidJSONError();
        }
        return request;
    }

    constructor( userMetaData: UserMetaData, ticket: Ticket ) {
        super(userMetaData, ticket);
    }

    /**
     * Get command type.
     */
    public getType(): string {
        return AddAvatarRequest.TYPE;
    }

    /**
     * Get JSON object that represents this command.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = super.toJSON();
        return json;
    }

}

export default AddAvatarRequest;
