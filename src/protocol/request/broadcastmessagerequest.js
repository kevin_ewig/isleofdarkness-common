"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var authenticatedrequest_1 = require("./authenticatedrequest");
var ticket_1 = require("../../model/ticket");
var usermetadata_1 = require("../../model/usermetadata");
var request_1 = require("./request");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforrequesterror_1 = require("../../error/invalidjsonforrequesterror");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
var BroadcastMessageRequest = /** @class */ (function (_super) {
    __extends(BroadcastMessageRequest, _super);
    function BroadcastMessageRequest(userMetaData, ticket, message) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        if (message) {
            _this._message = message;
        }
        else {
            throw new missingconstructorargumenterror_1.default();
        }
        return _this;
    }
    /**
     * Convert a JSON object to a BroadcastMessageRequest.
     * @param jsonObject
     */
    BroadcastMessageRequest.fromJSON = function (jsonObject) {
        var request;
        if (jsonObject.type !== BroadcastMessageRequest.TYPE) {
            throw new invalidjsonforrequesterror_1.default();
        }
        else if (jsonObject.userMetaData && jsonObject.ticket && jsonObject.requestId) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            request = new BroadcastMessageRequest(userMetaData, ticket, jsonObject.message);
            request_1.default.populateJSON(request, jsonObject);
        }
        else {
            throw new invalidjsonerror_1.default();
        }
        return request;
    };
    Object.defineProperty(BroadcastMessageRequest.prototype, "message", {
        /**
         * Get message from the client that is sent to the server to broadcast.
         * @param message
         */
        get: function () {
            return this._message;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Get request type.
     */
    BroadcastMessageRequest.prototype.getType = function () {
        return BroadcastMessageRequest.TYPE;
    };
    /**
     * Convert this response object to JSON.
     */
    BroadcastMessageRequest.prototype.toJSON = function () {
        var json = _super.prototype.toJSON.call(this);
        json.message = this._message;
        return json;
    };
    BroadcastMessageRequest.TYPE = "broadcastmessagerequest";
    return BroadcastMessageRequest;
}(authenticatedrequest_1.default));
exports.BroadcastMessageRequest = BroadcastMessageRequest;
exports.default = BroadcastMessageRequest;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3QvYnJvYWRjYXN0bWVzc2FnZXJlcXVlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBQTBEO0FBQzFELDZDQUF3QztBQUN4Qyx5REFBb0Q7QUFDcEQscUNBQWdDO0FBQ2hDLGlFQUE0RDtBQUM1RCxxRkFBZ0Y7QUFDaEYsK0ZBQTBGO0FBRTFGO0lBQTZDLDJDQUFvQjtJQThCN0QsaUNBQVksWUFBMEIsRUFBRSxNQUFjLEVBQUUsT0FBZTtRQUF2RSxZQUNJLGtCQUFNLFlBQVksRUFBRSxNQUFNLENBQUMsU0FNOUI7UUFMRyxFQUFFLENBQUMsQ0FBRSxPQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ1osS0FBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7UUFDNUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLHlDQUErQixFQUFFLENBQUM7UUFDaEQsQ0FBQzs7SUFDTCxDQUFDO0lBL0JEOzs7T0FHRztJQUNXLGdDQUFRLEdBQXRCLFVBQXVCLFVBQWU7UUFFbEMsSUFBSSxPQUFnQyxDQUFDO1FBRXJDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxJQUFJLEtBQUssdUJBQXVCLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNyRCxNQUFNLElBQUksb0NBQTBCLEVBQUUsQ0FBQztRQUMzQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxZQUFZLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxVQUFVLENBQUMsU0FBVSxDQUFDLENBQUMsQ0FBQztZQUVoRixJQUFJLFlBQVksR0FBRyxzQkFBWSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEUsSUFBSSxNQUFNLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELE9BQU8sR0FBRyxJQUFJLHVCQUF1QixDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2hGLGlCQUFPLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztRQUU5QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksMEJBQWdCLEVBQUUsQ0FBQztRQUNqQyxDQUFDO1FBRUQsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBZUQsc0JBQUksNENBQU87UUFKWDs7O1dBR0c7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pCLENBQUM7OztPQUFBO0lBRUQ7O09BRUc7SUFDSSx5Q0FBTyxHQUFkO1FBQ0ksTUFBTSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQztJQUN4QyxDQUFDO0lBRUQ7O09BRUc7SUFDSSx3Q0FBTSxHQUFiO1FBQ0ksSUFBSSxJQUFJLEdBQTRCLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ25ELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM3QixNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUEzRGEsNEJBQUksR0FBRyx5QkFBeUIsQ0FBQztJQTZEbkQsOEJBQUM7Q0EvREQsQUErREMsQ0EvRDRDLDhCQUFvQixHQStEaEU7QUEvRFksMERBQXVCO0FBaUVwQyxrQkFBZSx1QkFBdUIsQ0FBQyIsImZpbGUiOiJwcm90b2NvbC9yZXF1ZXN0L2Jyb2FkY2FzdG1lc3NhZ2VyZXF1ZXN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEF1dGhlbnRpY2F0ZWRSZXF1ZXN0IGZyb20gXCIuL2F1dGhlbnRpY2F0ZWRyZXF1ZXN0XCI7XHJcbmltcG9ydCBUaWNrZXQgZnJvbSBcIi4uLy4uL21vZGVsL3RpY2tldFwiO1xyXG5pbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuLi8uLi9tb2RlbC91c2VybWV0YWRhdGFcIjtcclxuaW1wb3J0IFJlcXVlc3QgZnJvbSBcIi4vcmVxdWVzdFwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXF1ZXN0RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVxdWVzdGVycm9yXCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgQnJvYWRjYXN0TWVzc2FnZVJlcXVlc3QgZXh0ZW5kcyBBdXRoZW50aWNhdGVkUmVxdWVzdCB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJicm9hZGNhc3RtZXNzYWdlcmVxdWVzdFwiO1xyXG5cclxuICAgIHByaXZhdGUgX21lc3NhZ2U6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgYSBKU09OIG9iamVjdCB0byBhIEJyb2FkY2FzdE1lc3NhZ2VSZXF1ZXN0LlxyXG4gICAgICogQHBhcmFtIGpzb25PYmplY3RcclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqZWN0OiBhbnkpOiBCcm9hZGNhc3RNZXNzYWdlUmVxdWVzdCB7XHJcblxyXG4gICAgICAgIGxldCByZXF1ZXN0OiBCcm9hZGNhc3RNZXNzYWdlUmVxdWVzdDtcclxuXHJcbiAgICAgICAgaWYgKCBqc29uT2JqZWN0LnR5cGUgIT09IEJyb2FkY2FzdE1lc3NhZ2VSZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkZvclJlcXVlc3RFcnJvcigpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIGpzb25PYmplY3QudXNlck1ldGFEYXRhICYmIGpzb25PYmplY3QudGlja2V0ICYmIGpzb25PYmplY3QucmVxdWVzdElkICkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHVzZXJNZXRhRGF0YSA9IFVzZXJNZXRhRGF0YS5mcm9tSlNPTihqc29uT2JqZWN0LnVzZXJNZXRhRGF0YSk7XHJcbiAgICAgICAgICAgIGxldCB0aWNrZXQgPSBUaWNrZXQuZnJvbUpTT04oanNvbk9iamVjdC50aWNrZXQpO1xyXG4gICAgICAgICAgICByZXF1ZXN0ID0gbmV3IEJyb2FkY2FzdE1lc3NhZ2VSZXF1ZXN0KHVzZXJNZXRhRGF0YSwgdGlja2V0LCBqc29uT2JqZWN0Lm1lc3NhZ2UpO1xyXG4gICAgICAgICAgICBSZXF1ZXN0LnBvcHVsYXRlSlNPTihyZXF1ZXN0LCBqc29uT2JqZWN0KTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORXJyb3IoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByZXF1ZXN0O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgbWVzc2FnZTogc3RyaW5nKSB7XHJcbiAgICAgICAgc3VwZXIodXNlck1ldGFEYXRhLCB0aWNrZXQpO1xyXG4gICAgICAgIGlmICggbWVzc2FnZSApIHtcclxuICAgICAgICAgICAgdGhpcy5fbWVzc2FnZSA9IG1lc3NhZ2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgbWVzc2FnZSBmcm9tIHRoZSBjbGllbnQgdGhhdCBpcyBzZW50IHRvIHRoZSBzZXJ2ZXIgdG8gYnJvYWRjYXN0LlxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2VcclxuICAgICAqL1xyXG4gICAgZ2V0IG1lc3NhZ2UoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbWVzc2FnZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCByZXF1ZXN0IHR5cGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRUeXBlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEJyb2FkY2FzdE1lc3NhZ2VSZXF1ZXN0LlRZUEU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgcmVzcG9uc2Ugb2JqZWN0IHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCBqc29uOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHN1cGVyLnRvSlNPTigpO1xyXG4gICAgICAgIGpzb24ubWVzc2FnZSA9IHRoaXMuX21lc3NhZ2U7XHJcbiAgICAgICAgcmV0dXJuIGpzb247XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBCcm9hZGNhc3RNZXNzYWdlUmVxdWVzdDtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
