import World from "../../model/world";
import UserMetaData from "../../model/usermetadata";
import ObjectModel from "../../model/objectmodel";
import Request from "./request";
import Ticket from "../../model/ticket";
import AuthenticatedRequest from "./authenticatedrequest";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForRequestError from "../../error/invalidjsonforrequesterror";

/**
 * Adds an avatar to the world.
 */
export class RemoveAvatarRequest extends AuthenticatedRequest {

    public static TYPE = "removeavatarrequest";

    /**
     * Convert a JSON object to a request.
     * @param jsonObject
     * @returns {RemoveAvatarRequest}
     */
    public static fromJSON(jsonObject: any): RemoveAvatarRequest {
        let request;
        if ( jsonObject.type !== RemoveAvatarRequest.TYPE ) {
            throw new InvalidJSONForRequestError();
        }
        if ( jsonObject.ticket && jsonObject.userMetaData ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);

            request = new RemoveAvatarRequest(userMetaData, ticket);
            Request.populateJSON(request, jsonObject);

        } else {
            throw new InvalidJSONError();
        }
        return request;
    }

    constructor( userMetaData: UserMetaData, ticket: Ticket ) {
        super(userMetaData, ticket);
    }

    /**
     * Get command type.
     */
    public getType(): string {
        return RemoveAvatarRequest.TYPE;
    }

    /**
     * Get JSON object that represents this command.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = super.toJSON();
        return json;
    }

}

export default RemoveAvatarRequest;
