import UserMetaData from "../../model/usermetadata";
import Ticket from "../../model/ticket";
import Request from "./request";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";

/**
 * A request that comes from an already-authenticated user.
 */
export abstract class AuthenticatedRequest extends Request {

    public static TYPE = "authenticatedrequest";

    private _userMetaData: UserMetaData;
    private _ticket: Ticket;

    constructor(userMetaData: UserMetaData, ticket: Ticket) {
        super();
        if ( !userMetaData || !ticket ) {
            throw new MissingConstructorArgumentError();
        } else {
            this._userMetaData = userMetaData;
            this._ticket = ticket;
        }
    }

    /**
     * Get some information about the user making this request.
     */
    public getUserMetaData(): UserMetaData {
        return this._userMetaData;
    }

    /**
     * Get authentication ticket of this authenticated request.
     */
    public getTicket(): Ticket {
        return this._ticket;
    }

    /**
     * Get JSON object that represents this request.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = super.toJSON();
        if ( this.getUserMetaData() ) {
            result.userMetaData = this.getUserMetaData().toJSON();
        }
        if ( this.getTicket() ) {
            result.ticket = this.getTicket().toJSON();
        }
        return result;
    }


}

export default AuthenticatedRequest;
