import World from "../../model/world";
import UserMetaData from "../../model/usermetadata";
import ObjectModel from "../../model/objectmodel";
import Request from "./request";
import Ticket from "../../model/ticket";
import AuthenticatedRequest from "./authenticatedrequest";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForRequestError from "../../error/invalidjsonforrequesterror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";

/**
 * Adds a oject model to the world.
 */
export class AddObjectModelRequest extends AuthenticatedRequest {

    public static TYPE = "addobjectmodelrequest";

    private _objectModel: ObjectModel;

    /**
     * Convert a JSON object to a request.
     * @param jsonObject
     * @returns {AddObjectModelCommand}
     */
    public static fromJSON(jsonObject: any): AddObjectModelRequest {
        let request;
        if ( jsonObject.type !== AddObjectModelRequest.TYPE ) {
            throw new InvalidJSONForRequestError();
        }
        if ( jsonObject.objectModel && jsonObject.ticket && jsonObject.userMetaData ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let obj = ObjectModel.fromJSON(jsonObject.objectModel);
            let ticket = Ticket.fromJSON(jsonObject.ticket);

            request = new AddObjectModelRequest(userMetaData, ticket, obj);
            Request.populateJSON(request, jsonObject);

        } else {
            throw new InvalidJSONError();
        }
        return request;
    }

    constructor( userMetaData: UserMetaData, ticket: Ticket, objectModel: ObjectModel ) {
        super(userMetaData, ticket);
        if ( objectModel ) {
            this._objectModel = objectModel;
        } else {
            throw new MissingConstructorArgumentError();
        }

    }

    /**
     * Get command type.
     */
    public getType(): string {
        return AddObjectModelRequest.TYPE;
    }

    /**
     * Get object model to add.
     */
    public get ObjectModel(): ObjectModel {
        return this._objectModel;
    }

    /**
     * Get JSON object that represents this command.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = super.toJSON();
        json.objectModel = this._objectModel.toJSON();
        return json;
    }

}

export default AddObjectModelRequest;
