import AuthenticatedRequest from "./authenticatedrequest";
import Ticket from "../../model/ticket";
import UserMetaData from "../../model/usermetadata";
import Request from "./request";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForRequestError from "../../error/invalidjsonforrequesterror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";

export class OpenConnectionRequest extends AuthenticatedRequest {

    public static TYPE = "openconnectionrequest";

    /**
     * Convert a JSON object to OpenConnectionRequest.
     * @param jsonObject
     */
    public static fromJSON(jsonObject: any): OpenConnectionRequest {

        let request: OpenConnectionRequest;

        if ( jsonObject.type !== OpenConnectionRequest.TYPE ) {
            throw new InvalidJSONForRequestError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket && jsonObject.requestId ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            request = new OpenConnectionRequest(userMetaData, ticket);
            Request.populateJSON(request, jsonObject);

        } else {
            throw new InvalidJSONError();
        }

        return request;

    }

    constructor(userMetaData: UserMetaData, ticket: Ticket) {
        super(userMetaData, ticket);
    }

    public getType(): string {
        return OpenConnectionRequest.TYPE;
    }


    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = super.toJSON();
        return json;
    }

}

export default OpenConnectionRequest;
