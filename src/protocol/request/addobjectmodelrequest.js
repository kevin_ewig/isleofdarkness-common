"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usermetadata_1 = require("../../model/usermetadata");
var objectmodel_1 = require("../../model/objectmodel");
var request_1 = require("./request");
var ticket_1 = require("../../model/ticket");
var authenticatedrequest_1 = require("./authenticatedrequest");
var invalidjsonerror_1 = require("../../error/invalidjsonerror");
var invalidjsonforrequesterror_1 = require("../../error/invalidjsonforrequesterror");
var missingconstructorargumenterror_1 = require("../../error/missingconstructorargumenterror");
/**
 * Adds a oject model to the world.
 */
var AddObjectModelRequest = /** @class */ (function (_super) {
    __extends(AddObjectModelRequest, _super);
    function AddObjectModelRequest(userMetaData, ticket, objectModel) {
        var _this = _super.call(this, userMetaData, ticket) || this;
        if (objectModel) {
            _this._objectModel = objectModel;
        }
        else {
            throw new missingconstructorargumenterror_1.default();
        }
        return _this;
    }
    /**
     * Convert a JSON object to a request.
     * @param jsonObject
     * @returns {AddObjectModelCommand}
     */
    AddObjectModelRequest.fromJSON = function (jsonObject) {
        var request;
        if (jsonObject.type !== AddObjectModelRequest.TYPE) {
            throw new invalidjsonforrequesterror_1.default();
        }
        if (jsonObject.objectModel && jsonObject.ticket && jsonObject.userMetaData) {
            var userMetaData = usermetadata_1.default.fromJSON(jsonObject.userMetaData);
            var obj = objectmodel_1.default.fromJSON(jsonObject.objectModel);
            var ticket = ticket_1.default.fromJSON(jsonObject.ticket);
            request = new AddObjectModelRequest(userMetaData, ticket, obj);
            request_1.default.populateJSON(request, jsonObject);
        }
        else {
            throw new invalidjsonerror_1.default();
        }
        return request;
    };
    /**
     * Get command type.
     */
    AddObjectModelRequest.prototype.getType = function () {
        return AddObjectModelRequest.TYPE;
    };
    Object.defineProperty(AddObjectModelRequest.prototype, "ObjectModel", {
        /**
         * Get object model to add.
         */
        get: function () {
            return this._objectModel;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Get JSON object that represents this command.
     */
    AddObjectModelRequest.prototype.toJSON = function () {
        var json = _super.prototype.toJSON.call(this);
        json.objectModel = this._objectModel.toJSON();
        return json;
    };
    AddObjectModelRequest.TYPE = "addobjectmodelrequest";
    return AddObjectModelRequest;
}(authenticatedrequest_1.default));
exports.AddObjectModelRequest = AddObjectModelRequest;
exports.default = AddObjectModelRequest;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3RvY29sL3JlcXVlc3QvYWRkb2JqZWN0bW9kZWxyZXF1ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUNBLHlEQUFvRDtBQUNwRCx1REFBa0Q7QUFDbEQscUNBQWdDO0FBQ2hDLDZDQUF3QztBQUN4QywrREFBMEQ7QUFDMUQsaUVBQTREO0FBQzVELHFGQUFnRjtBQUNoRiwrRkFBMEY7QUFFMUY7O0dBRUc7QUFDSDtJQUEyQyx5Q0FBb0I7SUErQjNELCtCQUFhLFlBQTBCLEVBQUUsTUFBYyxFQUFFLFdBQXdCO1FBQWpGLFlBQ0ksa0JBQU0sWUFBWSxFQUFFLE1BQU0sQ0FBQyxTQU85QjtRQU5HLEVBQUUsQ0FBQyxDQUFFLFdBQVksQ0FBQyxDQUFDLENBQUM7WUFDaEIsS0FBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUM7UUFDcEMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLHlDQUErQixFQUFFLENBQUM7UUFDaEQsQ0FBQzs7SUFFTCxDQUFDO0lBakNEOzs7O09BSUc7SUFDVyw4QkFBUSxHQUF0QixVQUF1QixVQUFlO1FBQ2xDLElBQUksT0FBTyxDQUFDO1FBQ1osRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLElBQUksS0FBSyxxQkFBcUIsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ25ELE1BQU0sSUFBSSxvQ0FBMEIsRUFBRSxDQUFDO1FBQzNDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsV0FBVyxJQUFJLFVBQVUsQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLFlBQWEsQ0FBQyxDQUFDLENBQUM7WUFFM0UsSUFBSSxZQUFZLEdBQUcsc0JBQVksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xFLElBQUksR0FBRyxHQUFHLHFCQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN2RCxJQUFJLE1BQU0sR0FBRyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFaEQsT0FBTyxHQUFHLElBQUkscUJBQXFCLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztZQUMvRCxpQkFBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFFOUMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLDBCQUFnQixFQUFFLENBQUM7UUFDakMsQ0FBQztRQUNELE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQVlEOztPQUVHO0lBQ0ksdUNBQU8sR0FBZDtRQUNJLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7SUFDdEMsQ0FBQztJQUtELHNCQUFXLDhDQUFXO1FBSHRCOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUM3QixDQUFDOzs7T0FBQTtJQUVEOztPQUVHO0lBQ0ksc0NBQU0sR0FBYjtRQUNJLElBQUksSUFBSSxHQUE0QixpQkFBTSxNQUFNLFdBQUUsQ0FBQztRQUNuRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDOUMsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBNURhLDBCQUFJLEdBQUcsdUJBQXVCLENBQUM7SUE4RGpELDRCQUFDO0NBaEVELEFBZ0VDLENBaEUwQyw4QkFBb0IsR0FnRTlEO0FBaEVZLHNEQUFxQjtBQWtFbEMsa0JBQWUscUJBQXFCLENBQUMiLCJmaWxlIjoicHJvdG9jb2wvcmVxdWVzdC9hZGRvYmplY3Rtb2RlbHJlcXVlc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgV29ybGQgZnJvbSBcIi4uLy4uL21vZGVsL3dvcmxkXCI7XHJcbmltcG9ydCBVc2VyTWV0YURhdGEgZnJvbSBcIi4uLy4uL21vZGVsL3VzZXJtZXRhZGF0YVwiO1xyXG5pbXBvcnQgT2JqZWN0TW9kZWwgZnJvbSBcIi4uLy4uL21vZGVsL29iamVjdG1vZGVsXCI7XHJcbmltcG9ydCBSZXF1ZXN0IGZyb20gXCIuL3JlcXVlc3RcIjtcclxuaW1wb3J0IFRpY2tldCBmcm9tIFwiLi4vLi4vbW9kZWwvdGlja2V0XCI7XHJcbmltcG9ydCBBdXRoZW50aWNhdGVkUmVxdWVzdCBmcm9tIFwiLi9hdXRoZW50aWNhdGVkcmVxdWVzdFwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05FcnJvciBmcm9tIFwiLi4vLi4vZXJyb3IvaW52YWxpZGpzb25lcnJvclwiO1xyXG5pbXBvcnQgSW52YWxpZEpTT05Gb3JSZXF1ZXN0RXJyb3IgZnJvbSBcIi4uLy4uL2Vycm9yL2ludmFsaWRqc29uZm9ycmVxdWVzdGVycm9yXCI7XHJcbmltcG9ydCBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIGZyb20gXCIuLi8uLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcblxyXG4vKipcclxuICogQWRkcyBhIG9qZWN0IG1vZGVsIHRvIHRoZSB3b3JsZC5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBBZGRPYmplY3RNb2RlbFJlcXVlc3QgZXh0ZW5kcyBBdXRoZW50aWNhdGVkUmVxdWVzdCB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJhZGRvYmplY3Rtb2RlbHJlcXVlc3RcIjtcclxuXHJcbiAgICBwcml2YXRlIF9vYmplY3RNb2RlbDogT2JqZWN0TW9kZWw7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IGEgSlNPTiBvYmplY3QgdG8gYSByZXF1ZXN0LlxyXG4gICAgICogQHBhcmFtIGpzb25PYmplY3RcclxuICAgICAqIEByZXR1cm5zIHtBZGRPYmplY3RNb2RlbENvbW1hbmR9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iamVjdDogYW55KTogQWRkT2JqZWN0TW9kZWxSZXF1ZXN0IHtcclxuICAgICAgICBsZXQgcmVxdWVzdDtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gQWRkT2JqZWN0TW9kZWxSZXF1ZXN0LlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBJbnZhbGlkSlNPTkZvclJlcXVlc3RFcnJvcigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIGpzb25PYmplY3Qub2JqZWN0TW9kZWwgJiYganNvbk9iamVjdC50aWNrZXQgJiYganNvbk9iamVjdC51c2VyTWV0YURhdGEgKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgdXNlck1ldGFEYXRhID0gVXNlck1ldGFEYXRhLmZyb21KU09OKGpzb25PYmplY3QudXNlck1ldGFEYXRhKTtcclxuICAgICAgICAgICAgbGV0IG9iaiA9IE9iamVjdE1vZGVsLmZyb21KU09OKGpzb25PYmplY3Qub2JqZWN0TW9kZWwpO1xyXG4gICAgICAgICAgICBsZXQgdGlja2V0ID0gVGlja2V0LmZyb21KU09OKGpzb25PYmplY3QudGlja2V0KTtcclxuXHJcbiAgICAgICAgICAgIHJlcXVlc3QgPSBuZXcgQWRkT2JqZWN0TW9kZWxSZXF1ZXN0KHVzZXJNZXRhRGF0YSwgdGlja2V0LCBvYmopO1xyXG4gICAgICAgICAgICBSZXF1ZXN0LnBvcHVsYXRlSlNPTihyZXF1ZXN0LCBqc29uT2JqZWN0KTtcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRKU09ORXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlcXVlc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHVzZXJNZXRhRGF0YTogVXNlck1ldGFEYXRhLCB0aWNrZXQ6IFRpY2tldCwgb2JqZWN0TW9kZWw6IE9iamVjdE1vZGVsICkge1xyXG4gICAgICAgIHN1cGVyKHVzZXJNZXRhRGF0YSwgdGlja2V0KTtcclxuICAgICAgICBpZiAoIG9iamVjdE1vZGVsICkge1xyXG4gICAgICAgICAgICB0aGlzLl9vYmplY3RNb2RlbCA9IG9iamVjdE1vZGVsO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCBjb21tYW5kIHR5cGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRUeXBlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEFkZE9iamVjdE1vZGVsUmVxdWVzdC5UWVBFO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IG9iamVjdCBtb2RlbCB0byBhZGQuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXQgT2JqZWN0TW9kZWwoKTogT2JqZWN0TW9kZWwge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9vYmplY3RNb2RlbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCBKU09OIG9iamVjdCB0aGF0IHJlcHJlc2VudHMgdGhpcyBjb21tYW5kLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHsgW2tleTogc3RyaW5nXTogYW55OyB9IHtcclxuICAgICAgICBsZXQganNvbjogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICBqc29uLm9iamVjdE1vZGVsID0gdGhpcy5fb2JqZWN0TW9kZWwudG9KU09OKCk7XHJcbiAgICAgICAgcmV0dXJuIGpzb247XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBBZGRPYmplY3RNb2RlbFJlcXVlc3Q7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
