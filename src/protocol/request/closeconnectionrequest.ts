import Ticket from "../../model/ticket";
import UserMetaData from "../../model/usermetadata";
import AuthenticatedRequest from "./authenticatedrequest";
import Request from "./request";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForRequestError from "../../error/invalidjsonforrequesterror";

export class CloseConnectionRequest extends AuthenticatedRequest {

    public static TYPE = "closeconnectionrequest";

    private _result: boolean;

    /**
     * Convert JSON object to CloseConnectionRequest.
     * @param jsonObject
     */
    public static fromJSON(jsonObject: any): CloseConnectionRequest {
        let request: CloseConnectionRequest;
        if ( jsonObject.type !== CloseConnectionRequest.TYPE ) {
            throw new InvalidJSONForRequestError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket && jsonObject.requestId ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let result = jsonObject.result;
            request = new CloseConnectionRequest(userMetaData, ticket, result);
            Request.populateJSON(request, jsonObject);

        } else {
            throw new InvalidJSONError();
        }
        return request;
    }

    constructor(userMetaData: UserMetaData, ticket: Ticket, result: boolean) {
        super(userMetaData, ticket);
        this._result = result;
    }

    public getType(): string {
        return CloseConnectionRequest.TYPE;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = super.toJSON();
        json.result = this._result;
        return json;
    }

}

export default CloseConnectionRequest;
