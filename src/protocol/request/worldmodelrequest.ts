import AuthenticatedRequest from "./authenticatedrequest";
import Shape from "../../math/shape";
import UserMetaData from "../../model/usermetadata";
import Ticket from "../../model/ticket";
import {Common} from "../../math/common";
import Converter from "../converter";
import Request from "./request";
import InvalidJSONError from "../../error/invalidjsonerror";
import InvalidJSONForRequestError from "../../error/invalidjsonforrequesterror";
import MissingConstructorArgumentError from "../../error/missingconstructorargumenterror";

/**
 * Requests a subset of the world model whose object models intersect
 * the given shape.
 */
export class WorldModelRequest extends AuthenticatedRequest {

    public static TYPE = "worldmodelrequest";

    private _shape: Shape;

    /**
     * Convert a JSON object to a WorldModelRequest.
     *
     * @param jsonObject
     * @returns {WorldModelRequest}
     */
    public static fromJSON(jsonObject: any): WorldModelRequest {

        let request: WorldModelRequest;

        if (jsonObject.type !== WorldModelRequest.TYPE ) {
            throw new InvalidJSONForRequestError();
        } else if ( jsonObject.userMetaData && jsonObject.ticket && jsonObject.shape ) {

            let userMetaData = UserMetaData.fromJSON(jsonObject.userMetaData);
            let ticket = Ticket.fromJSON(jsonObject.ticket);
            let shape = Common.convertShape(jsonObject.shape);
            request = new WorldModelRequest( userMetaData, ticket, shape );
            Request.populateJSON(request, jsonObject);

        } else {
            throw new InvalidJSONError();
        }
        return request;
    }

    constructor( userMetaData: UserMetaData, ticket: Ticket, shape: Shape ) {
        super(userMetaData, ticket);
        if ( shape ) {
            this._shape = shape;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Get what type of request this is.
     */
    public getType(): string {
        return WorldModelRequest.TYPE;
    }

    /**
     * Get the shape.
     */
    public get shape(): Shape {
        return this._shape;
    }

    /**
     * Convert this response object to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = super.toJSON();
        json.shape = this._shape.toJSON();
        return json;
    }

}

export default WorldModelRequest;
