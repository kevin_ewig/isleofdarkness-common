import BroadcastMessageRequest from "./request/broadcastmessagerequest";
import OpenConnectionRequest from "./request/openconnectionrequest";
import CloseConnectionRequest from "./request/closeconnectionrequest";
import WorldModelRequest from "./request/worldmodelrequest";
import AddObjectModelRequest from "./request/addobjectmodelrequest";
import AddAvatarRequest from "./request/addavatarrequest";
import MoveObjectModelRequest from "./request/moveobjectmodelrequest";
import RemoveAvatarRequest from "./request/removeavatarrequest";

import BroadcastMessageResponse from "./response/broadcastmessageresponse";
import OpenConnectionResponse from "./response/openconnectionresponse";
import WorldModelResponse from "./response/worldmodelresponse";
import InvalidRequestResponse from "./response/invalidrequestresponse";
import AddObjectModelResponse from "./response/addobjectmodelresponse";
import MoveObjectModelResponse from "./response/moveobjectmodelresponse";
import CloseConnectionResponse from "./response/closeconnectionresponse";
import AddAvatarResponse from "./response/addavatarresponse";
import ErrorResponse from "./response/errorresponse";
import OkResponse from "./response/okresponse";
import RemoveAvatarResponse from "./response/removeavatarresponse";

import Response from "./response/response";
import Request from "./request/request";

export class Converter {

    /**
     * Convert a string to a Response object.
     * @param value
     * @constructor
     */
    public static ConvertResponse(value: string): Response|null {
        let responseObject: Response|null = null;
        try {

            let jsonObject: any = JSON.parse(value);
            let jsonObjectType: string = jsonObject.type;

            if ( jsonObjectType === AddAvatarResponse.TYPE ) {
                responseObject = AddAvatarResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === AddObjectModelResponse.TYPE ) {
                responseObject = AddObjectModelResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === BroadcastMessageResponse.TYPE ) {
                responseObject = BroadcastMessageResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === OpenConnectionResponse.TYPE ) {
                responseObject = OpenConnectionResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === WorldModelResponse.TYPE ) {
                responseObject = WorldModelResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === InvalidRequestResponse.TYPE ) {
                responseObject = InvalidRequestResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === CloseConnectionResponse.TYPE ) {
                responseObject = CloseConnectionResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === MoveObjectModelResponse.TYPE ) {
                responseObject = MoveObjectModelResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === ErrorResponse.TYPE ) {
                responseObject = ErrorResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === OkResponse.TYPE ) {
                responseObject = OkResponse.fromJSON(jsonObject);
            } else if ( jsonObjectType === RemoveAvatarResponse.TYPE ) {
                responseObject = RemoveAvatarResponse.fromJSON(jsonObject);
            }

        } catch ( error ) {
            throw new Error("Problem converting " + value + " to Response");
        }
        return responseObject;
    }

    /**
     * Convert a string to a Request object.
     * @param value
     * @constructor
     */
    public static ConvertRequest(value: string): Request|null {
        let requestObject: Request|null = null;
        try {

            let jsonObject: any = JSON.parse(value);
            let jsonObjectType: string = jsonObject.type;

            if ( jsonObjectType === AddAvatarRequest.TYPE ) {
                requestObject = AddAvatarRequest.fromJSON(jsonObject);
            } else if ( jsonObjectType === BroadcastMessageRequest.TYPE ) {
                requestObject = BroadcastMessageRequest.fromJSON(jsonObject);
            } else if ( jsonObjectType === OpenConnectionRequest.TYPE ) {
                requestObject = OpenConnectionRequest.fromJSON(jsonObject);
            } else if ( jsonObjectType === CloseConnectionRequest.TYPE ) {
                requestObject = CloseConnectionRequest.fromJSON(jsonObject);
            } else if ( jsonObjectType === WorldModelRequest.TYPE ) {
                requestObject = WorldModelRequest.fromJSON(jsonObject);
            } else if ( jsonObjectType === AddObjectModelRequest.TYPE ) {
                requestObject = AddObjectModelRequest.fromJSON(jsonObject);
            } else if ( jsonObjectType === MoveObjectModelRequest.TYPE ) {
                requestObject = MoveObjectModelRequest.fromJSON(jsonObject);
            } else if ( jsonObjectType === RemoveAvatarRequest.TYPE ) {
                requestObject = RemoveAvatarRequest.fromJSON(jsonObject);
            }

        } catch ( error ) {
            throw new Error("Problem converting " + value + " to Request. [" + error.message + "]");
        }
        return requestObject;
    }

}

export default Converter;
