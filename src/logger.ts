export class Logger {

    public static error( className: string, message: string ) {
        Logger.logMessage( "ERROR", className, message );
    }

    public static warn( className: string, message: string ) {
        Logger.logMessage( "WARN", className, message );
    }

    public static info( className: string, message: string ) {
        Logger.logMessage( "INFO", className, message );
    }

    public static debug( className: string, message: string ) {
        Logger.logMessage( "DEBUG", className, message );
    }

    public static logMessage(type: string, className: string, message: string) {
        console.log("[" + type + "] " + Logger.getDateAsString() + " [" + className + "] " + message );
    }

    public static getDateAsString() {
        let now = new Date();
        let monthStr = now.getMonth() >= 9 ? now.getMonth() + 1 : ("0" + (now.getMonth() + 1));
        let dateStr = now.getDate() >= 9 ? now.getDate() : ("0" + now.getDate());
        let hoursStr = now.getHours() >= 9 ? now.getHours() : ("0" + now.getHours());
        let minuteStr = now.getMinutes() >= 9 ? now.getMinutes() : ("0" + now.getMinutes());
        let secondStr = now.getSeconds() >= 9 ? now.getSeconds() : ("0" + now.getSeconds());
        let dformat = [monthStr, dateStr, now.getFullYear()].join("/") + " " +
            [hoursStr, minuteStr, secondStr].join(":");
        return dformat;
    }

}

export default Logger;
