export class MissingConstructorArgumentError extends Error {

    constructor(errorMsg?: string) {
        super(errorMsg == null ? "Missing constructor argument" : "Missing constructor: " + errorMsg);
    }

}

export default MissingConstructorArgumentError;
