export class InvalidJSONForRequestError extends Error {

    constructor(errorMsg?: string) {
        super(errorMsg == null ? "Invalid JSON For Request Error" : "Invalid JSON For Request Error: " + errorMsg);
    }

}

export default InvalidJSONForRequestError;
