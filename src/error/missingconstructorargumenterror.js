"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var MissingConstructorArgumentError = /** @class */ (function (_super) {
    __extends(MissingConstructorArgumentError, _super);
    function MissingConstructorArgumentError(errorMsg) {
        return _super.call(this, errorMsg == null ? "Missing constructor argument" : "Missing constructor: " + errorMsg) || this;
    }
    return MissingConstructorArgumentError;
}(Error));
exports.MissingConstructorArgumentError = MissingConstructorArgumentError;
exports.default = MissingConstructorArgumentError;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVycm9yL21pc3Npbmdjb25zdHJ1Y3RvcmFyZ3VtZW50ZXJyb3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUE7SUFBcUQsbURBQUs7SUFFdEQseUNBQVksUUFBaUI7ZUFDekIsa0JBQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsOEJBQThCLENBQUMsQ0FBQyxDQUFDLHVCQUF1QixHQUFHLFFBQVEsQ0FBQztJQUNqRyxDQUFDO0lBRUwsc0NBQUM7QUFBRCxDQU5BLEFBTUMsQ0FOb0QsS0FBSyxHQU16RDtBQU5ZLDBFQUErQjtBQVE1QyxrQkFBZSwrQkFBK0IsQ0FBQyIsImZpbGUiOiJlcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgZXh0ZW5kcyBFcnJvciB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZXJyb3JNc2c/OiBzdHJpbmcpIHtcclxuICAgICAgICBzdXBlcihlcnJvck1zZyA9PSBudWxsID8gXCJNaXNzaW5nIGNvbnN0cnVjdG9yIGFyZ3VtZW50XCIgOiBcIk1pc3NpbmcgY29uc3RydWN0b3I6IFwiICsgZXJyb3JNc2cpO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvcjtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
