export class InvalidJSONError extends Error {

    constructor(errorMsg?: string) {
        super(errorMsg == null ? "Invalid JSON Error" : "Invalid JSON Error: " + errorMsg);
    }

}

export default InvalidJSONError;
