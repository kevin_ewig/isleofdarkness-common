export class InvalidJSONForResponseError extends Error {

    constructor(errorMsg?: string) {
        super(errorMsg == null ? "Invalid JSON For Response error" : "Invalid JSON For Response error: " + errorMsg);
    }

}

export default InvalidJSONForResponseError;
