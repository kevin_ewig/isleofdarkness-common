"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var InvalidJSONError = /** @class */ (function (_super) {
    __extends(InvalidJSONError, _super);
    function InvalidJSONError(errorMsg) {
        return _super.call(this, errorMsg == null ? "Invalid JSON Error" : "Invalid JSON Error: " + errorMsg) || this;
    }
    return InvalidJSONError;
}(Error));
exports.InvalidJSONError = InvalidJSONError;
exports.default = InvalidJSONError;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVycm9yL2ludmFsaWRqc29uZXJyb3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUE7SUFBc0Msb0NBQUs7SUFFdkMsMEJBQVksUUFBaUI7ZUFDekIsa0JBQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixHQUFHLFFBQVEsQ0FBQztJQUN0RixDQUFDO0lBRUwsdUJBQUM7QUFBRCxDQU5BLEFBTUMsQ0FOcUMsS0FBSyxHQU0xQztBQU5ZLDRDQUFnQjtBQVE3QixrQkFBZSxnQkFBZ0IsQ0FBQyIsImZpbGUiOiJlcnJvci9pbnZhbGlkanNvbmVycm9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEludmFsaWRKU09ORXJyb3IgZXh0ZW5kcyBFcnJvciB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZXJyb3JNc2c/OiBzdHJpbmcpIHtcclxuICAgICAgICBzdXBlcihlcnJvck1zZyA9PSBudWxsID8gXCJJbnZhbGlkIEpTT04gRXJyb3JcIiA6IFwiSW52YWxpZCBKU09OIEVycm9yOiBcIiArIGVycm9yTXNnKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEludmFsaWRKU09ORXJyb3I7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
