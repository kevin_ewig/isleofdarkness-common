"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var InvalidJSONForRequestError = /** @class */ (function (_super) {
    __extends(InvalidJSONForRequestError, _super);
    function InvalidJSONForRequestError(errorMsg) {
        return _super.call(this, errorMsg == null ? "Invalid JSON For Request Error" : "Invalid JSON For Request Error: " + errorMsg) || this;
    }
    return InvalidJSONForRequestError;
}(Error));
exports.InvalidJSONForRequestError = InvalidJSONForRequestError;
exports.default = InvalidJSONForRequestError;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVycm9yL2ludmFsaWRqc29uZm9ycmVxdWVzdGVycm9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBO0lBQWdELDhDQUFLO0lBRWpELG9DQUFZLFFBQWlCO2VBQ3pCLGtCQUFNLFFBQVEsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQyxrQ0FBa0MsR0FBRyxRQUFRLENBQUM7SUFDOUcsQ0FBQztJQUVMLGlDQUFDO0FBQUQsQ0FOQSxBQU1DLENBTitDLEtBQUssR0FNcEQ7QUFOWSxnRUFBMEI7QUFRdkMsa0JBQWUsMEJBQTBCLENBQUMiLCJmaWxlIjoiZXJyb3IvaW52YWxpZGpzb25mb3JyZXF1ZXN0ZXJyb3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgSW52YWxpZEpTT05Gb3JSZXF1ZXN0RXJyb3IgZXh0ZW5kcyBFcnJvciB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZXJyb3JNc2c/OiBzdHJpbmcpIHtcclxuICAgICAgICBzdXBlcihlcnJvck1zZyA9PSBudWxsID8gXCJJbnZhbGlkIEpTT04gRm9yIFJlcXVlc3QgRXJyb3JcIiA6IFwiSW52YWxpZCBKU09OIEZvciBSZXF1ZXN0IEVycm9yOiBcIiArIGVycm9yTXNnKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEludmFsaWRKU09ORm9yUmVxdWVzdEVycm9yO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
