"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var InvalidJSONForResponseError = /** @class */ (function (_super) {
    __extends(InvalidJSONForResponseError, _super);
    function InvalidJSONForResponseError(errorMsg) {
        return _super.call(this, errorMsg == null ? "Invalid JSON For Response error" : "Invalid JSON For Response error: " + errorMsg) || this;
    }
    return InvalidJSONForResponseError;
}(Error));
exports.InvalidJSONForResponseError = InvalidJSONForResponseError;
exports.default = InvalidJSONForResponseError;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVycm9yL2ludmFsaWRqc29uZm9ycmVzcG9uc2VlcnJvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQTtJQUFpRCwrQ0FBSztJQUVsRCxxQ0FBWSxRQUFpQjtlQUN6QixrQkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLENBQUMsbUNBQW1DLEdBQUcsUUFBUSxDQUFDO0lBQ2hILENBQUM7SUFFTCxrQ0FBQztBQUFELENBTkEsQUFNQyxDQU5nRCxLQUFLLEdBTXJEO0FBTlksa0VBQTJCO0FBUXhDLGtCQUFlLDJCQUEyQixDQUFDIiwiZmlsZSI6ImVycm9yL2ludmFsaWRqc29uZm9ycmVzcG9uc2VlcnJvci5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBJbnZhbGlkSlNPTkZvclJlc3BvbnNlRXJyb3IgZXh0ZW5kcyBFcnJvciB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZXJyb3JNc2c/OiBzdHJpbmcpIHtcclxuICAgICAgICBzdXBlcihlcnJvck1zZyA9PSBudWxsID8gXCJJbnZhbGlkIEpTT04gRm9yIFJlc3BvbnNlIGVycm9yXCIgOiBcIkludmFsaWQgSlNPTiBGb3IgUmVzcG9uc2UgZXJyb3I6IFwiICsgZXJyb3JNc2cpO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgSW52YWxpZEpTT05Gb3JSZXNwb25zZUVycm9yO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
