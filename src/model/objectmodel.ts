import Shape from "../math/shape";
import {Common} from "../math/common";
import Point from "../math/Point";
import { MissingConstructorArgumentError } from "../error/missingconstructorargumenterror";

/**
 * An object that is rendered in the world.
 */
export class ObjectModel {

    public static TYPE = "objectmodel";

    private _id: string;
    private _shape: Shape;
    private _worldName: string;
    private _objectType: ObjectModelType;
    private _modelName: string;
    private _displayName: string;
    private _modelTexture: string;
    private _ownerUserId: string;

    /**
     * Convert JSON object to Object Model object.
     */
    public static fromJSON(jsonObject: any) {

        if ( !jsonObject ) {
            throw new Error("Cannot convert undefined");
        }

        if ( !ObjectModel.isCorrectType(jsonObject) ) {
            throw new Error("Cannot convert. Invalid type.");
        }

        return ObjectModel.fromJSONNotStrict(jsonObject);

    }

    /**
     * Convert JSON object to Object Model object without checking for type.
     * Used by base classes.
     */
    public static fromJSONNotStrict(jsonObject: any) {

        if ( !jsonObject ) {
            throw new Error("Cannot convert undefined");
        }

        let id: string = jsonObject.id;
        let shape: Shape = Common.convertShape(jsonObject.shape);
        let modelName: string = jsonObject.modelName;
        let modelTexture: string = jsonObject.modelTexture;
        let ownerUserId: string = jsonObject.ownerUserId;
        let worldName: string = jsonObject.worldName;
        let displayName: string = jsonObject.displayName;

        let objType: ObjectModelType = ObjectModelType.Passable;
        if (jsonObject.objectType) {
            let objTypeAsInt: number = parseInt(jsonObject.objectType, 10);
            objType = objTypeAsInt;
        }

        let objModel = new ObjectModel(id, shape, modelName);
        objModel.modelTexture = modelTexture;
        objModel.shape = shape;
        objModel.ownerUserId = ownerUserId;
        objModel.objectType = objType;
        objModel.worldName = worldName;
        objModel.displayName = displayName;
        return objModel;

    }

    /**
     * Check if this json object has the correct object model type.
     * @param jsonObject
     */
    public static isCorrectType(jsonObject: any) {
        if ( jsonObject.type !== ObjectModel.TYPE ) {
            return false;
        }
        return true;
    }

    constructor(id: string, shape: Shape, modelName: string) {
        if ( id && modelName && shape ) {
            this._id = id;
            this._shape = shape;
            this._modelName = modelName;
            this._objectType = ObjectModelType.Passable;
        } else {
            throw new MissingConstructorArgumentError("Missing: id=" + id + " shape=" + shape + " modelName=" + modelName);
        }
    }

    /**
     * A unique id of this model.
     * @returns {string}
     */
    public get id(): string {
        return this._id;
    }

    /**
     * Bounding shape of this model.
     */
    public set shape(shape: Shape) {
        this._shape = shape;
    }

    /**
     * Bounding shape of thi model.
     * @returns {Shape}
     */
    public get shape(): Shape {
        return this._shape;
    }

    /**
     * The name of the world the object model is in.
     */
    public set worldName( value: string ) {
        this._worldName = value;
    }

    /**
     * The name of the world the object model is in.
     * @returns {string}
     */
    public get worldName(): string {
        return this._worldName;
    }

    /**
     * A distinct name of the object model.
     */
    public set displayName( value: string ) {
        this._displayName = value;
    }

    /**
     * A distinct name of the object model.
     * @returns {string}
     */
    public get displayName(): string {
        return this._displayName;
    }

    /**
     * The name of the model. This is the 3d file used by this model.
     */
    public set modelName( value: string ) {
        this._modelName = value;
    }

    /**
     * The name of the model. This is the 3d file used by this model.
     * @returns {string}
     */
    public get modelName(): string {
        return this._modelName;
    }

    /**
     * Set the texture of the model. (Optional)
     * If no texture specified then engine will use the default texture assigned in the model.
     * @param value
     */
    public set modelTexture(value: string) {
        this._modelTexture = value;
    }

    /**
     * Get the texture of the model. (Optional)
     * If no texture specified then engine will use the default texture assigned in the model.
     * @param value
     */
    public get modelTexture() {
        return this._modelTexture;
    }

    /**
     * Set the user id of the owner of this object.
     * @param value
     */
    public set ownerUserId(value: string) {
        this._ownerUserId = value;
    }

    /**
     * Get the user id of the owner of this object.
     * @param value
     */
    public get ownerUserId() {
        return this._ownerUserId;
    }

    /**
     * This object can be either passable, climbable or impassable. The type
     * determines how other objects behave and interact around this one.
     */
    public set objectType(value: ObjectModelType) {
        this._objectType = value;
    }

    /**
     * This object can be either passable, climbable or impassable. The type
     * determines how other objects behave and interact around this one.
     * @returns {ObjectModelType}
     */
    public get objectType(): ObjectModelType {
        return this._objectType;
    }

    /**
     * Get the 2-dimensional section coordinate location of this object model.
     * At this point the section coordinate is just the coordinate of the shape
     * divided by a constant number.
     */
    public getSectionCoordinate(): Point {
        let shape: Shape = this._shape;
        let position: Point = shape.getPosition();
        return position.getSectionCoordinate();
    }

    /**
     * Convert this object model to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = {};
        result.id = this._id;
        result.shape = this.shape.toJSON();
        result.modelName = this._modelName;
        result.modelTexture = this._modelTexture;
        result.ownerUserId = this._ownerUserId;
        result.type = ObjectModel.TYPE;
        result.objectType = this._objectType;
        result.worldName = this._worldName;
        result.displayName = this._displayName;
        return result;
    }
}

/**
 * This determines how an avatar can interact with an object model in the world.
 */
export enum ObjectModelType {
    Passable = 1,
    Impassable = 2,
    Climbable = 3
}

export default ObjectModel;
