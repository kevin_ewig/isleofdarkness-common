import ObjectModel from "./objectmodel";
import Shape from "../math/shape";
import Intersecter from "../math/intersecter";
import UserMetaData from "./usermetadata";
import { AnimatedCharacter } from "./animatedcharacter";

export class World {

    private _idDictionary: { [id: string]: ObjectModel };
    private _users: Array<UserMetaData>;

    /**
     * Convert JSON object to a World object.
     * @param jsonObject
     */
    public static fromJSON(jsonObject: any): World {

        let world: World;

        if ( jsonObject.objectModels && jsonObject.users ) {
            world = new World();
            for ( let index = 0; index < jsonObject.objectModels.length; index++ ) {
                let objModelJson = jsonObject.objectModels[index];
                if ( ObjectModel.isCorrectType(objModelJson) ) {
                    let objModel = ObjectModel.fromJSON(objModelJson);
                    world.addObjectModel( objModel );
                } else if ( AnimatedCharacter.isCorrectType(objModelJson) ) {
                    let objModel = AnimatedCharacter.fromJSON(objModelJson);
                    world.addObjectModel( objModel );
                }
            }

            for ( let index = 0; index < jsonObject.users.length; index++ ) {
                let userJson = jsonObject.users[index];
                let usermeta = UserMetaData.fromJSON(userJson);
                world.addUserMetaData( usermeta );
            }
        } else {
            throw new Error("Invalid JSON");
        }

        return world;

    }

    constructor() {
        this._idDictionary = {};
        this._users = [];
    }

    /**
     * Add an object model to this world.
     * @param objModel The object model.
     */
    public addObjectModel( objModel: ObjectModel ) {
        let id = objModel.id;
        this._idDictionary[id] = objModel;
    }

    /**
     * Add a user to the world.
     */
    public addUserMetaData( userobject: UserMetaData ) {
        this._users.push( userobject );
    }

    /**
     * Get all items that intersect a given shape.
     * @param shape The shape shape used to filter out object model intersection.
     */
    public getAllIntersection( shape: Shape ): ObjectModel[] {

        // List of all objects that intersect with the given shape.
        let resultList = new Array<ObjectModel>();

        for (let key in this._idDictionary) {
            if ( key ) {
                let tempObjectModel: ObjectModel = this._idDictionary[key];

                // This determines if we have intersected on the x-y plane. Now check if we can intersect the
                // object in the z-plane.
                let intersect = Intersecter.isShapeIntersectShape(shape, tempObjectModel.shape);
                if ( intersect ) {
                    resultList.push( tempObjectModel );
                }
            }
        }

        return resultList;

    }

    /**
     * List all object models in the world. Used primarily for debugging and generating the JSON file.
     */
    public getAllObjectModels(): ObjectModel[] {
        let array = new Array<ObjectModel>();
        for ( let key in this._idDictionary ) {
            if ( this._idDictionary.hasOwnProperty(key) ) {
                let objectModel = this._idDictionary[key];
                array.push(objectModel);
            }
        }
        return array;
    }

    /**
     * List all users in the world. Used primarily for debugging and generating the JSON file.
     */
    public getAllUserMetaData(): UserMetaData[] {
        return this._users;
    }

    /**
     * Get object model by id.
     * @param id
     */
    public getObjectModel(id: string): ObjectModel {
        return this._idDictionary[id];
    }

    /**
     * Remove an object model to this world.
     * @param id The object model id.
     */
    public removeObjectModel( id: string ) {

        let objModel = this._idDictionary[id];

        // Remove the object from the id lookup.
        delete this._idDictionary[id];

    }

    /**
     * Convert this world into JSON.
     */
    public toJSON() {

        let arrayOfUsers = [];

        // Convert the users.
        for ( let index = 0; index < this._users.length; index++ ) {
            let objectModelJson = this._users[index].toJSON();
            arrayOfUsers.push(objectModelJson);
        }

        // Convert the object models.
        let arrayOfObjectModels = this.getAllObjectModels();
        let array = [];

        for ( let index = 0; index < arrayOfObjectModels.length; index++ ) {
            let objectModelJson = arrayOfObjectModels[index].toJSON();
            array.push(objectModelJson);
        }

        return {
            "objectModels" : array,
            "users": arrayOfUsers
        };

    }

}

export default World;
