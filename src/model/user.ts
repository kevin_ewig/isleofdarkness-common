import {UserMetaData} from "./usermetadata";
import Point from "../math/point";
import { MissingConstructorArgumentError } from "../error/missingconstructorargumenterror";

export class User {

    private static TYPE = "user";

    private _userId: string;
    private _name: string;
    private _email: string;
    private _createdDate: Date;
    private _lastLoggedInDate: Date;
    private _language: string;
    private _disabled = false;
    private _pictureUrl: string;
    private _avatarObjectModelId: string;

    /**
     * Convert a JSON object to a User object.
     * @param jsonObject
     * @returns {User}
     */
    public static fromJSON(jsonObject: any) {
        if ( jsonObject.userId && jsonObject.name ) {
            let userId = jsonObject.userId;
            let user: User = new User(userId, jsonObject.name);
            if ( jsonObject.createdDate ) {
                user.createdDate = new Date(jsonObject.createdDate);
            }
            if ( jsonObject.email ) {
                user.email = jsonObject.email;
            }
            if ( jsonObject.language ) {
                user.language = jsonObject.language;
            }
            if ( jsonObject.lastLoggedInDate ) {
                user.lastLoggedInDate = new Date(jsonObject.lastLoggedInDate);
            }
            if ( jsonObject.pictureUrl ) {
                user.pictureUrl = jsonObject.pictureUrl;
            }
            if ( jsonObject.avatarObjectModelId ) {
                user.avatarObjectModelId = jsonObject.avatarObjectModelId;
            }
            if ( jsonObject.disabled ) {
                user.disabled = jsonObject.disabled === true ? true : false;
            }
            return user;
        } else {
            throw new Error("Unable to convert to user object");
        }
    }

    constructor( userId: string, name: string ) {
        if ( userId && name ) {
            this._userId = userId;
            this._name = name;
        } else {
            throw new MissingConstructorArgumentError("userid=" + userId + " name=" + name);
        }
    }

    get createdDate() {
        return this._createdDate;
    }

    set createdDate(value: Date) {
        this._createdDate = value;
    }

    get email() {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    /**
     * Get meta-data associated with this user. Used to return basic and simple user information. To be used along with
     * messages, and other response objects.
     * @returns {UserMetaData}
     */
    public getMetaData(): UserMetaData {
        return new UserMetaData(this._userId, this._name);
    }

    get language() {
        return this._language;
    }

    set language(value: string) {
        this._language = value;
    }

    get lastLoggedInDate() {
        return this._lastLoggedInDate;
    }

    set lastLoggedInDate(value: Date) {
        this._lastLoggedInDate = value;
    }

    get disabled() {
        return this._disabled;
    }

    set disabled(value: boolean) {
        this._disabled = value;
    }

    get pictureUrl() {
        return this._pictureUrl;
    }

    set pictureUrl(value: string) {
        this._pictureUrl = value;
    }

    get avatarObjectModelId() {
        return this._avatarObjectModelId;
    }

    set avatarObjectModelId(value: string) {
        this._avatarObjectModelId = value;
    }

    get userId() {
        return this._userId;
    }

    get name() {
        return this._name;
    }

    /**
     * Convert user object into JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = {};
        json.userId = this._userId;
        json.name = this._name;
        json.createdDate = this._createdDate;
        json.email = this._email;
        json.language = this._language;
        json.lastLoggedInDate = this._lastLoggedInDate;
        json.pictureUrl = this._pictureUrl;
        json.disabled = this._disabled;
        json.avatarObjectModelId = this._avatarObjectModelId;
        return json;
    }

}

export default User;
