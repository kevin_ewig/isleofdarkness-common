import Shape from "../math/shape";
import {Common} from "../math/common";
import AnimationProfile from "./animationprofile";
import { MissingConstructorArgumentError } from "../error/missingconstructorargumenterror";

/**
 * A base profile for a specific character (avatar). This is used to provide a default
 * template for a user to select. This character has no owner. This is not to be used
 * or added to the world.
 */
export class BaseAnimatedCharacter {

    public static TYPE = "baseanimatedcharacter";

    private _id: string;
    private _shape: Shape;
    private _modelName: string;
    private _modelTexture: string;
    private _speed: number;
    private _animationProfile: AnimationProfile;

    /**
     * Convert JSON object to Object Model object.
     */
    public static fromJSON(jsonObject: any) {

        if ( !jsonObject ) {
            throw new Error("Cannot convert undefined");
        }

        let type = jsonObject.type;
        if ( type !== BaseAnimatedCharacter.TYPE ) {
            throw new Error("Cannot convert. Invalid type.");
        }

        let id: string = jsonObject.id;
        let shape: Shape = Common.convertShape(jsonObject.shape);
        let modelName: string = jsonObject.modelName;
        let modelTexture: string = jsonObject.modelTexture;
        let animationProfile: AnimationProfile = AnimationProfile.fromJSON(jsonObject.animationprofile);

        let speed = 0;
        if ( jsonObject.speed ) {
            speed = parseInt(jsonObject.speed, 10);
        }

        let charProfile = new BaseAnimatedCharacter(id, shape, modelName, animationProfile);
        charProfile.modelTexture = modelTexture;
        charProfile.speed = speed;
        return charProfile;

    }

    constructor(id: string, shape: Shape, modelName: string, animationProfile: AnimationProfile ) {
        if ( id && modelName && shape && animationProfile ) {
            this._id = id;
            this._modelName = modelName;
            this._shape = shape;
            this._animationProfile = animationProfile;
        } else {
            throw new MissingConstructorArgumentError("id=" + id + " shape=" + shape +
            " modelname=" + modelName + " animationprofile=" + animationProfile);
        }
    }

    /**
     * A unique id of this model.
     * @returns {string}
     */
    public get id(): string {
        return this._id;
    }

    /**
     * Bounding shape of thi model.
     * @returns {Shape}
     */
    public get shape(): Shape {
        return this._shape;
    }

    /**
     * Set how fast this model will move.
     * @param value
     */
    public set speed(value: number) {
        this._speed = value;
    }

    /**
     * Get how fast this model will move.
     * @param value
     */
    public get speed() {
        return this._speed;
    }

    /**
     * The name of the model. This is the 3d file used by this model.
     * @returns {string}
     */
    public set modelName( value: string ) {
        this._modelName = value;
    }

    /**
     * The name of the model. This is the 3d file used by this model.
     * @returns {string}
     */
    public get modelName(): string {
        return this._modelName;
    }

    /**
     * Set the texture of the model. (Optional)
     * If no texture specified then engine will use the default texture assigned in the model.
     * @param value
     */
    public set modelTexture(value: string) {
        this._modelTexture = value;
    }

    /**
     * Get the texture of the model. (Optional)
     * If no texture specified then engine will use the default texture assigned in the model.
     * @param value
     */
    public get modelTexture() {
        return this._modelTexture;
    }

    /**
     * Return the animation profile used to animate this character.
     */
    public get animationProfile() {
        return this._animationProfile;
    }

    /**
     * Convert this object model to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = {};
        result.id = this._id;
        result.shape = this.shape.toJSON();
        result.modelName = this._modelName;
        result.modelTexture = this._modelTexture;
        result.speed = this._speed;
        result.animationprofile = this._animationProfile.toJSON();
        result.type = BaseAnimatedCharacter.TYPE;
        return result;
    }

}

export default BaseAnimatedCharacter;
