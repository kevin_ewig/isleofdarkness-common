"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var objectmodel_1 = require("./objectmodel");
var point_1 = require("../math/point");
var animationprofile_1 = require("./animationprofile");
var TWEEN = require("@tweenjs/tween.js");
var AnimatedCharacter = /** @class */ (function (_super) {
    __extends(AnimatedCharacter, _super);
    function AnimatedCharacter(id, shape, modelName) {
        var _this = _super.call(this, id, shape, modelName) || this;
        _this._speed = AnimatedCharacter._DEFAULT_SPEED;
        return _this;
    }
    /**
     * Convert JSON object to Object Model object.
     */
    AnimatedCharacter.fromJSON = function (jsonObject) {
        if (!jsonObject) {
            throw new Error("Cannot convert undefined");
        }
        if (!AnimatedCharacter.isCorrectType(jsonObject)) {
            throw new Error("Cannot convert. Invalid type.");
        }
        // Extract speed.
        var speed = AnimatedCharacter._DEFAULT_SPEED;
        if (jsonObject.speed) {
            speed = parseInt(jsonObject.speed, 10);
        }
        // Copy all values into a new Animated Character object.
        var source = objectmodel_1.default.fromJSONNotStrict(jsonObject);
        var animatedChar = new AnimatedCharacter(source.id, source.shape, source.modelName);
        Object.assign(animatedChar, source);
        if (animatedChar) {
            animatedChar.speed = speed;
            var points = [];
            if (jsonObject.path && jsonObject.dateTweenStart) {
                for (var index = 0; index < jsonObject.path.length; index++) {
                    points.push(point_1.default.fromJSON(jsonObject.path[index]));
                }
                var dateTweenStart = new Date(jsonObject.dateTweenStart);
                animatedChar.setPath(points, dateTweenStart);
            }
        }
        // Set animation profile if it exists.
        if (jsonObject.animationProfile) {
            animatedChar.animationProfile = animationprofile_1.default.fromJSON(jsonObject.animationProfile);
        }
        return animatedChar;
    };
    /**
     * Check if this json object has the correct animated character type.
     * @param jsonObject
     */
    AnimatedCharacter.isCorrectType = function (jsonObject) {
        if (jsonObject.type !== AnimatedCharacter.TYPE) {
            return false;
        }
        return true;
    };
    /**
     * Get the path this object will travel.
     */
    AnimatedCharacter.prototype.getPath = function () {
        return this._path;
    };
    /**
     * Set path made up of points.
     * @param points
     */
    AnimatedCharacter.prototype.setPath = function (points, dateTweenStart) {
        // Stop tween if it started.
        if (this._tween) {
            this._tween.stop();
        }
        var startPos = this.shape;
        var xArray = [];
        var yArray = [];
        var zArray = [];
        for (var i = 0; i < points.length; i++) {
            xArray[i] = points[i].x;
            yArray[i] = points[i].y;
            zArray[i] = points[i].z;
        }
        // Set points globally
        this._path = points;
        var totalTravelTime = points.length * this._speed;
        this._tween = new TWEEN.Tween(startPos).
            to({ x: xArray, y: yArray, z: zArray }, totalTravelTime).
            easing(TWEEN.Easing.Linear.None);
        // Call this function on tween update.
        if (this._onTweenUpdatedFunction) {
            this._tween.onUpdate(this._onTweenUpdatedFunction);
        }
        // Call this function on tween stop.
        if (this._onTweenCompleteFunction) {
            var self_1 = this;
            this._tween.onComplete(function () {
                var tweenObject = this._object;
                self_1._onTweenCompleteFunction(tweenObject);
            });
        }
        // Set date tween start -- if there is any.
        this._dateTweenStart = new Date();
        if (dateTweenStart) {
            this._dateTweenStart = dateTweenStart;
        }
        // No delays.
        this._tween.delay(0);
        // Start moving.
        this._tween.start(this._dateTweenStart.getTime());
    };
    Object.defineProperty(AnimatedCharacter.prototype, "speed", {
        /**
         * Get how fast this model will move.
         * @param value
         */
        get: function () {
            return this._speed;
        },
        /**
         * Set how fast this model will move.
         * @param value
         */
        set: function (value) {
            this._speed = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimatedCharacter.prototype, "animationProfile", {
        /**
         * Get the animation values for walk, run, idle, etc.
         * @param value
         */
        get: function () {
            return this._animationProfile;
        },
        /**
         * Set the animation values for walk, run, idle, etc.
         * @param value
         */
        set: function (value) {
            this._animationProfile = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimatedCharacter.prototype, "dateTweenStart", {
        /**
         * Get date/time tween started.
         * @param value
         */
        get: function () {
            return this._dateTweenStart;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimatedCharacter.prototype, "onTweenComplete", {
        /**
         * Set function that will be called after tween is complete.
         * @param value
         */
        set: function (completeFunction) {
            this._onTweenCompleteFunction = completeFunction;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimatedCharacter.prototype, "onTweenUpdate", {
        /**
         * Set function that will be called any time the tween is updated.
         * @param value
         */
        set: function (updateFunction) {
            this._onTweenUpdatedFunction = updateFunction;
        },
        enumerable: true,
        configurable: true
    });
    /**
    * Convert this object model to JSON.
    */
    AnimatedCharacter.prototype.toJSON = function () {
        var result = {};
        result = _super.prototype.toJSON.call(this);
        result.speed = this._speed;
        result.type = AnimatedCharacter.TYPE;
        if (this._path) {
            result.path = [];
            for (var index = 0; index < this._path.length; index++) {
                result.path.push(this._path[index].toJSON());
            }
        }
        if (this._dateTweenStart) {
            result.dateTweenStart = this._dateTweenStart;
        }
        if (this._animationProfile) {
            result.animationProfile = this._animationProfile;
        }
        return result;
    };
    AnimatedCharacter.TYPE = "animatedcharacter";
    AnimatedCharacter._DEFAULT_SPEED = 500;
    return AnimatedCharacter;
}(objectmodel_1.default));
exports.AnimatedCharacter = AnimatedCharacter;
exports.default = AnimatedCharacter;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL2FuaW1hdGVkY2hhcmFjdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLDZDQUF3QztBQUN4Qyx1Q0FBa0M7QUFFbEMsdURBQWtEO0FBRWxELElBQU0sS0FBSyxHQUFRLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0FBRWhEO0lBQXVDLHFDQUFXO0lBc0U5QywyQkFBWSxFQUFVLEVBQUUsS0FBWSxFQUFFLFNBQWlCO1FBQXZELFlBQ0ksa0JBQU0sRUFBRSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsU0FDOUI7UUFoRU8sWUFBTSxHQUFXLGlCQUFpQixDQUFDLGNBQWMsQ0FBQzs7SUFnRTFELENBQUM7SUEzREQ7O09BRUc7SUFDVywwQkFBUSxHQUF0QixVQUF1QixVQUFlO1FBRWxDLEVBQUUsQ0FBQyxDQUFFLENBQUMsVUFBVyxDQUFDLENBQUMsQ0FBQztZQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDaEQsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFFLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUNqRCxNQUFNLElBQUksS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUM7UUFDckQsQ0FBQztRQUVELGlCQUFpQjtRQUNqQixJQUFJLEtBQUssR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUM7UUFDN0MsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLEtBQU0sQ0FBQyxDQUFDLENBQUM7WUFDckIsS0FBSyxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNDLENBQUM7UUFFRCx3REFBd0Q7UUFDeEQsSUFBSSxNQUFNLEdBQUcscUJBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQXNCLENBQUM7UUFDNUUsSUFBSSxZQUFZLEdBQXNCLElBQUksaUJBQWlCLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN2RyxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsQ0FBQztRQUVwQyxFQUFFLENBQUMsQ0FBRSxZQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLFlBQVksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBRTNCLElBQUksTUFBTSxHQUFZLEVBQUUsQ0FBQztZQUN6QixFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsSUFBSSxJQUFJLFVBQVUsQ0FBQyxjQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUNqRCxHQUFHLENBQUMsQ0FBRSxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFHLENBQUM7b0JBQzVELE1BQU0sQ0FBQyxJQUFJLENBQUUsZUFBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUUsQ0FBQztnQkFDMUQsQ0FBQztnQkFDRCxJQUFJLGNBQWMsR0FBUyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQy9ELFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1lBQ2pELENBQUM7UUFDTCxDQUFDO1FBRUQsc0NBQXNDO1FBQ3RDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxnQkFBaUIsQ0FBQyxDQUFDLENBQUM7WUFDaEMsWUFBWSxDQUFDLGdCQUFnQixHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUMzRixDQUFDO1FBRUQsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUV4QixDQUFDO0lBRUQ7OztPQUdHO0lBQ1csK0JBQWEsR0FBM0IsVUFBNEIsVUFBZTtRQUN2QyxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsSUFBSSxLQUFLLGlCQUFpQixDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDL0MsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBTUQ7O09BRUc7SUFDSSxtQ0FBTyxHQUFkO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLG1DQUFPLEdBQWQsVUFBZ0IsTUFBZSxFQUFFLGNBQXFCO1FBRWxELDRCQUE0QjtRQUM1QixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsTUFBTyxDQUFDLENBQUMsQ0FBQztZQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3ZCLENBQUM7UUFFRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQzFCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUNoQixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1lBQ3ZDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzVCLENBQUM7UUFFRCxzQkFBc0I7UUFDdEIsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUM7UUFFcEIsSUFBSSxlQUFlLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2xELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNuQyxFQUFFLENBQUUsRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxFQUFFLGVBQWUsQ0FBRTtZQUMxRCxNQUFNLENBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFFLENBQUM7UUFFdkMsc0NBQXNDO1FBQ3RDLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyx1QkFBd0IsQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFFLENBQUM7UUFDekQsQ0FBQztRQUVELG9DQUFvQztRQUNwQyxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsd0JBQXlCLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLElBQUksTUFBSSxHQUFRLElBQUksQ0FBQztZQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBRTtnQkFDcEIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDL0IsTUFBSSxDQUFDLHdCQUF3QixDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQy9DLENBQUMsQ0FDQSxDQUFDO1FBQ04sQ0FBQztRQUVELDJDQUEyQztRQUMzQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDbEMsRUFBRSxDQUFDLENBQUUsY0FBZSxDQUFDLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQztRQUMxQyxDQUFDO1FBRUQsYUFBYTtRQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXJCLGdCQUFnQjtRQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFdEQsQ0FBQztJQU1ELHNCQUFXLG9DQUFLO1FBSWhCOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQztRQWREOzs7V0FHRzthQUNILFVBQWlCLEtBQWE7WUFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDeEIsQ0FBQzs7O09BQUE7SUFjRCxzQkFBVywrQ0FBZ0I7UUFJM0I7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ2xDLENBQUM7UUFkRDs7O1dBR0c7YUFDSCxVQUE0QixLQUF1QjtZQUMvQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLENBQUM7OztPQUFBO0lBY0Qsc0JBQVcsNkNBQWM7UUFKekI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUNoQyxDQUFDOzs7T0FBQTtJQU1ELHNCQUFXLDhDQUFlO1FBSjFCOzs7V0FHRzthQUNILFVBQTJCLGdCQUEwQjtZQUNqRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDckQsQ0FBQzs7O09BQUE7SUFNRCxzQkFBVyw0Q0FBYTtRQUp4Qjs7O1dBR0c7YUFDSCxVQUF5QixjQUF3QjtZQUM3QyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsY0FBYyxDQUFDO1FBQ2xELENBQUM7OztPQUFBO0lBRUE7O01BRUU7SUFDSSxrQ0FBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLEVBQUUsQ0FBQztRQUN6QyxNQUFNLEdBQUcsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDeEIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzNCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1FBRXJDLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxLQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2YsTUFBTSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDakIsR0FBRyxDQUFDLENBQUUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRyxDQUFDO2dCQUN2RCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFFLENBQUM7WUFDbkQsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsZUFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDekIsTUFBTSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQ2pELENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsaUJBQWtCLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDckQsQ0FBQztRQUVELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQTFOYSxzQkFBSSxHQUFHLG1CQUFtQixDQUFDO0lBQzFCLGdDQUFjLEdBQUcsR0FBRyxDQUFDO0lBMk54Qyx3QkFBQztDQTlORCxBQThOQyxDQTlOc0MscUJBQVcsR0E4TmpEO0FBOU5ZLDhDQUFpQjtBQWdPOUIsa0JBQWUsaUJBQWlCLENBQUMiLCJmaWxlIjoibW9kZWwvYW5pbWF0ZWRjaGFyYWN0ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgT2JqZWN0TW9kZWwgZnJvbSBcIi4vb2JqZWN0bW9kZWxcIjtcclxuaW1wb3J0IFBvaW50IGZyb20gXCIuLi9tYXRoL3BvaW50XCI7XHJcbmltcG9ydCBTaGFwZSBmcm9tIFwiLi4vbWF0aC9zaGFwZVwiO1xyXG5pbXBvcnQgQW5pbWF0aW9uUHJvZmlsZSBmcm9tIFwiLi9hbmltYXRpb25wcm9maWxlXCI7XHJcblxyXG5jb25zdCBUV0VFTjogYW55ID0gcmVxdWlyZShcIkB0d2VlbmpzL3R3ZWVuLmpzXCIpO1xyXG5cclxuZXhwb3J0IGNsYXNzIEFuaW1hdGVkQ2hhcmFjdGVyIGV4dGVuZHMgT2JqZWN0TW9kZWwge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgVFlQRSA9IFwiYW5pbWF0ZWRjaGFyYWN0ZXJcIjtcclxuICAgIHByaXZhdGUgc3RhdGljIF9ERUZBVUxUX1NQRUVEID0gNTAwO1xyXG5cclxuICAgIHByaXZhdGUgX3BhdGg6IFBvaW50W107XHJcbiAgICBwcml2YXRlIF90d2VlbjogYW55O1xyXG4gICAgcHJpdmF0ZSBfZGF0ZVR3ZWVuU3RhcnQ6IERhdGU7XHJcbiAgICBwcml2YXRlIF9zcGVlZDogbnVtYmVyID0gQW5pbWF0ZWRDaGFyYWN0ZXIuX0RFRkFVTFRfU1BFRUQ7XHJcbiAgICBwcml2YXRlIF9vblR3ZWVuVXBkYXRlZEZ1bmN0aW9uOiBGdW5jdGlvbjtcclxuICAgIHByaXZhdGUgX29uVHdlZW5Db21wbGV0ZUZ1bmN0aW9uOiBGdW5jdGlvbjtcclxuICAgIHByaXZhdGUgX2FuaW1hdGlvblByb2ZpbGU6IEFuaW1hdGlvblByb2ZpbGU7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IEpTT04gb2JqZWN0IHRvIE9iamVjdCBNb2RlbCBvYmplY3QuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iamVjdDogYW55KSB7XHJcblxyXG4gICAgICAgIGlmICggIWpzb25PYmplY3QgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbm5vdCBjb252ZXJ0IHVuZGVmaW5lZFwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggIUFuaW1hdGVkQ2hhcmFjdGVyLmlzQ29ycmVjdFR5cGUoanNvbk9iamVjdCkgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbm5vdCBjb252ZXJ0LiBJbnZhbGlkIHR5cGUuXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gRXh0cmFjdCBzcGVlZC5cclxuICAgICAgICBsZXQgc3BlZWQgPSBBbmltYXRlZENoYXJhY3Rlci5fREVGQVVMVF9TUEVFRDtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3Quc3BlZWQgKSB7XHJcbiAgICAgICAgICAgIHNwZWVkID0gcGFyc2VJbnQoanNvbk9iamVjdC5zcGVlZCwgMTApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQ29weSBhbGwgdmFsdWVzIGludG8gYSBuZXcgQW5pbWF0ZWQgQ2hhcmFjdGVyIG9iamVjdC5cclxuICAgICAgICBsZXQgc291cmNlID0gT2JqZWN0TW9kZWwuZnJvbUpTT05Ob3RTdHJpY3QoanNvbk9iamVjdCkgYXMgQW5pbWF0ZWRDaGFyYWN0ZXI7XHJcbiAgICAgICAgbGV0IGFuaW1hdGVkQ2hhcjogQW5pbWF0ZWRDaGFyYWN0ZXIgPSBuZXcgQW5pbWF0ZWRDaGFyYWN0ZXIoc291cmNlLmlkLCBzb3VyY2Uuc2hhcGUsIHNvdXJjZS5tb2RlbE5hbWUpO1xyXG4gICAgICAgIE9iamVjdC5hc3NpZ24oYW5pbWF0ZWRDaGFyLCBzb3VyY2UpO1xyXG5cclxuICAgICAgICBpZiAoIGFuaW1hdGVkQ2hhciApIHtcclxuICAgICAgICAgICAgYW5pbWF0ZWRDaGFyLnNwZWVkID0gc3BlZWQ7XHJcblxyXG4gICAgICAgICAgICBsZXQgcG9pbnRzOiBQb2ludFtdID0gW107XHJcbiAgICAgICAgICAgIGlmICgganNvbk9iamVjdC5wYXRoICYmIGpzb25PYmplY3QuZGF0ZVR3ZWVuU3RhcnQgKSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKCBsZXQgaW5kZXggPSAwOyBpbmRleCA8IGpzb25PYmplY3QucGF0aC5sZW5ndGg7IGluZGV4KysgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9pbnRzLnB1c2goIFBvaW50LmZyb21KU09OKGpzb25PYmplY3QucGF0aFtpbmRleF0pICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsZXQgZGF0ZVR3ZWVuU3RhcnQ6IERhdGUgPSBuZXcgRGF0ZShqc29uT2JqZWN0LmRhdGVUd2VlblN0YXJ0KTtcclxuICAgICAgICAgICAgICAgIGFuaW1hdGVkQ2hhci5zZXRQYXRoKHBvaW50cywgZGF0ZVR3ZWVuU3RhcnQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTZXQgYW5pbWF0aW9uIHByb2ZpbGUgaWYgaXQgZXhpc3RzLlxyXG4gICAgICAgIGlmICgganNvbk9iamVjdC5hbmltYXRpb25Qcm9maWxlICkge1xyXG4gICAgICAgICAgICBhbmltYXRlZENoYXIuYW5pbWF0aW9uUHJvZmlsZSA9IEFuaW1hdGlvblByb2ZpbGUuZnJvbUpTT04oanNvbk9iamVjdC5hbmltYXRpb25Qcm9maWxlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBhbmltYXRlZENoYXI7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgdGhpcyBqc29uIG9iamVjdCBoYXMgdGhlIGNvcnJlY3QgYW5pbWF0ZWQgY2hhcmFjdGVyIHR5cGUuXHJcbiAgICAgKiBAcGFyYW0ganNvbk9iamVjdFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGlzQ29ycmVjdFR5cGUoanNvbk9iamVjdDogYW55KSB7XHJcbiAgICAgICAgaWYgKCBqc29uT2JqZWN0LnR5cGUgIT09IEFuaW1hdGVkQ2hhcmFjdGVyLlRZUEUgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoaWQ6IHN0cmluZywgc2hhcGU6IFNoYXBlLCBtb2RlbE5hbWU6IHN0cmluZykge1xyXG4gICAgICAgIHN1cGVyKGlkLCBzaGFwZSwgbW9kZWxOYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgcGF0aCB0aGlzIG9iamVjdCB3aWxsIHRyYXZlbC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFBhdGgoKTogUG9pbnRbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BhdGg7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgcGF0aCBtYWRlIHVwIG9mIHBvaW50cy5cclxuICAgICAqIEBwYXJhbSBwb2ludHNcclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldFBhdGgoIHBvaW50czogUG9pbnRbXSwgZGF0ZVR3ZWVuU3RhcnQ/OiBEYXRlICk6IHZvaWQge1xyXG5cclxuICAgICAgICAvLyBTdG9wIHR3ZWVuIGlmIGl0IHN0YXJ0ZWQuXHJcbiAgICAgICAgaWYgKCB0aGlzLl90d2VlbiApIHtcclxuICAgICAgICAgICAgdGhpcy5fdHdlZW4uc3RvcCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IHN0YXJ0UG9zID0gdGhpcy5zaGFwZTtcclxuICAgICAgICBsZXQgeEFycmF5ID0gW107XHJcbiAgICAgICAgbGV0IHlBcnJheSA9IFtdO1xyXG4gICAgICAgIGxldCB6QXJyYXkgPSBbXTtcclxuICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBwb2ludHMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgICAgIHhBcnJheVtpXSA9IHBvaW50c1tpXS54O1xyXG4gICAgICAgICAgICB5QXJyYXlbaV0gPSBwb2ludHNbaV0ueTtcclxuICAgICAgICAgICAgekFycmF5W2ldID0gcG9pbnRzW2ldLno7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTZXQgcG9pbnRzIGdsb2JhbGx5XHJcbiAgICAgICAgdGhpcy5fcGF0aCA9IHBvaW50cztcclxuXHJcbiAgICAgICAgbGV0IHRvdGFsVHJhdmVsVGltZSA9IHBvaW50cy5sZW5ndGggKiB0aGlzLl9zcGVlZDtcclxuICAgICAgICB0aGlzLl90d2VlbiA9IG5ldyBUV0VFTi5Ud2VlbihzdGFydFBvcykuXHJcbiAgICAgICAgICAgIHRvKCB7IHg6IHhBcnJheSwgeTogeUFycmF5LCB6OiB6QXJyYXkgfSwgdG90YWxUcmF2ZWxUaW1lICkuXHJcbiAgICAgICAgICAgIGVhc2luZyggVFdFRU4uRWFzaW5nLkxpbmVhci5Ob25lICk7XHJcblxyXG4gICAgICAgIC8vIENhbGwgdGhpcyBmdW5jdGlvbiBvbiB0d2VlbiB1cGRhdGUuXHJcbiAgICAgICAgaWYgKCB0aGlzLl9vblR3ZWVuVXBkYXRlZEZ1bmN0aW9uICkge1xyXG4gICAgICAgICAgICB0aGlzLl90d2Vlbi5vblVwZGF0ZSggdGhpcy5fb25Ud2VlblVwZGF0ZWRGdW5jdGlvbiApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQ2FsbCB0aGlzIGZ1bmN0aW9uIG9uIHR3ZWVuIHN0b3AuXHJcbiAgICAgICAgaWYgKCB0aGlzLl9vblR3ZWVuQ29tcGxldGVGdW5jdGlvbiApIHtcclxuICAgICAgICAgICAgbGV0IHNlbGY6IGFueSA9IHRoaXM7XHJcbiAgICAgICAgICAgIHRoaXMuX3R3ZWVuLm9uQ29tcGxldGUoIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHR3ZWVuT2JqZWN0ID0gdGhpcy5fb2JqZWN0O1xyXG4gICAgICAgICAgICAgICAgc2VsZi5fb25Ud2VlbkNvbXBsZXRlRnVuY3Rpb24odHdlZW5PYmplY3QpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTZXQgZGF0ZSB0d2VlbiBzdGFydCAtLSBpZiB0aGVyZSBpcyBhbnkuXHJcbiAgICAgICAgdGhpcy5fZGF0ZVR3ZWVuU3RhcnQgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIGlmICggZGF0ZVR3ZWVuU3RhcnQgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RhdGVUd2VlblN0YXJ0ID0gZGF0ZVR3ZWVuU3RhcnQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBObyBkZWxheXMuXHJcbiAgICAgICAgdGhpcy5fdHdlZW4uZGVsYXkoMCk7XHJcblxyXG4gICAgICAgIC8vIFN0YXJ0IG1vdmluZy5cclxuICAgICAgICB0aGlzLl90d2Vlbi5zdGFydCh0aGlzLl9kYXRlVHdlZW5TdGFydC5nZXRUaW1lKCkpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCBob3cgZmFzdCB0aGlzIG1vZGVsIHdpbGwgbW92ZS5cclxuICAgICAqIEBwYXJhbSB2YWx1ZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc2V0IHNwZWVkKHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9zcGVlZCA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGhvdyBmYXN0IHRoaXMgbW9kZWwgd2lsbCBtb3ZlLlxyXG4gICAgICogQHBhcmFtIHZhbHVlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXQgc3BlZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NwZWVkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSBhbmltYXRpb24gdmFsdWVzIGZvciB3YWxrLCBydW4sIGlkbGUsIGV0Yy5cclxuICAgICAqIEBwYXJhbSB2YWx1ZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc2V0IGFuaW1hdGlvblByb2ZpbGUodmFsdWU6IEFuaW1hdGlvblByb2ZpbGUpIHtcclxuICAgICAgICB0aGlzLl9hbmltYXRpb25Qcm9maWxlID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIGFuaW1hdGlvbiB2YWx1ZXMgZm9yIHdhbGssIHJ1biwgaWRsZSwgZXRjLlxyXG4gICAgICogQHBhcmFtIHZhbHVlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXQgYW5pbWF0aW9uUHJvZmlsZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYW5pbWF0aW9uUHJvZmlsZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCBkYXRlL3RpbWUgdHdlZW4gc3RhcnRlZC5cclxuICAgICAqIEBwYXJhbSB2YWx1ZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0IGRhdGVUd2VlblN0YXJ0KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kYXRlVHdlZW5TdGFydDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCBmdW5jdGlvbiB0aGF0IHdpbGwgYmUgY2FsbGVkIGFmdGVyIHR3ZWVuIGlzIGNvbXBsZXRlLlxyXG4gICAgICogQHBhcmFtIHZhbHVlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzZXQgb25Ud2VlbkNvbXBsZXRlKGNvbXBsZXRlRnVuY3Rpb246IEZ1bmN0aW9uKSB7XHJcbiAgICAgICAgdGhpcy5fb25Ud2VlbkNvbXBsZXRlRnVuY3Rpb24gPSBjb21wbGV0ZUZ1bmN0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IGZ1bmN0aW9uIHRoYXQgd2lsbCBiZSBjYWxsZWQgYW55IHRpbWUgdGhlIHR3ZWVuIGlzIHVwZGF0ZWQuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldCBvblR3ZWVuVXBkYXRlKHVwZGF0ZUZ1bmN0aW9uOiBGdW5jdGlvbikge1xyXG4gICAgICAgIHRoaXMuX29uVHdlZW5VcGRhdGVkRnVuY3Rpb24gPSB1cGRhdGVGdW5jdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgb2JqZWN0IG1vZGVsIHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IHsgW2tleTogc3RyaW5nXTogYW55OyB9ID0ge307XHJcbiAgICAgICAgcmVzdWx0ID0gc3VwZXIudG9KU09OKCk7XHJcbiAgICAgICAgcmVzdWx0LnNwZWVkID0gdGhpcy5fc3BlZWQ7XHJcbiAgICAgICAgcmVzdWx0LnR5cGUgPSBBbmltYXRlZENoYXJhY3Rlci5UWVBFO1xyXG5cclxuICAgICAgICBpZiAoIHRoaXMuX3BhdGggKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdC5wYXRoID0gW107XHJcbiAgICAgICAgICAgIGZvciAoIGxldCBpbmRleCA9IDA7IGluZGV4IDwgdGhpcy5fcGF0aC5sZW5ndGg7IGluZGV4KysgKSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHQucGF0aC5wdXNoKCB0aGlzLl9wYXRoW2luZGV4XS50b0pTT04oKSApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIHRoaXMuX2RhdGVUd2VlblN0YXJ0ICkge1xyXG4gICAgICAgICAgICByZXN1bHQuZGF0ZVR3ZWVuU3RhcnQgPSB0aGlzLl9kYXRlVHdlZW5TdGFydDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICggdGhpcy5fYW5pbWF0aW9uUHJvZmlsZSApIHtcclxuICAgICAgICAgICAgcmVzdWx0LmFuaW1hdGlvblByb2ZpbGUgPSB0aGlzLl9hbmltYXRpb25Qcm9maWxlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEFuaW1hdGVkQ2hhcmFjdGVyO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
