import { MissingConstructorArgumentError } from "../error/missingconstructorargumenterror";

export class AnimationProfile {

    private _id: string;
    private _name: string;
    private _idle: string;
    private _run: string;
    private _walk: string;
    private _die: string;

    /**
     * Convert JSON object to Animation Profile object.
     */
    public static fromJSON(jsonObject: any) {

        if ( !jsonObject ) {
            throw new Error("Cannot convert undefined");
        }

        let id: string = jsonObject.id;
        let name: string = jsonObject.name;
        let idle: string = jsonObject.idle;
        let run: string = jsonObject.run;
        let walk: string = jsonObject.walk;
        let die: string = jsonObject.die;

        let animationProfile = new AnimationProfile(id, name, idle, run, walk, die);
        return animationProfile;

    }

    constructor( id: string, name: string, idle: string, run: string, walk: string, die: string ) {
        if ( id && name ) {
            this._id = id;
            this._name = name;
            this._idle = idle;
            this._run = run;
            this._walk = walk;
            this._die = die;
        } else {
            throw new MissingConstructorArgumentError("id=" + id + ", name=" + name);
        }
    }

    /**
     * Animation profile id.
     */
    public get id() {
        return this._id;
    }

    /**
     * Animation profile name. This is the name of the animated character model.
     */
    public get name() {
        return this._name;
    }

    /**
     * Action name for idle animation.
     */
    public get idle() {
        return this._idle;
    }

    /**
     * Action name for run animation.
     */
    public get run() {
        return this._run;
    }

    /**
     * Action name for walk animation.
     */
    public get walk() {
        return this._walk;
    }

    /**
     * Action name for die animation.
     */
    public get die() {
        return this._die;
    }

    /**
     * Convert this animation profile to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = {};
        result.id = this._id;
        result.name = this._name;
        result.idle = this._idle;
        result.run = this._run;
        result.walk = this._walk;
        result.die = this._die;
        return result;
    }

}

export default AnimationProfile;
