import uuid from "../uuid";

export class Ticket {

    private static readonly HEADER_TICKETID = "x-ticketid";
    private static readonly HEADER_TICKETEXPIRYDATE = "x-ticketexpiry";
    private static readonly HEADER_USERID = "x-tickeruserid";

    private _ticketId: string;
    private _expirationDate: Date;
    private _userId: string;

    /**
     * For Client: Set ticket data to perform a request to the server.
     * @param ticket The ticket data.
     * @param xhr The JQuery Ajax XHR object.
     */
    public static toRequest(ticket: Ticket, xhr: any) {
        xhr.setRequestHeader(Ticket.HEADER_TICKETID, ticket.ticketId);
        xhr.setRequestHeader(Ticket.HEADER_TICKETEXPIRYDATE, ticket.expirationDate.getTime());
        xhr.setRequestHeader(Ticket.HEADER_USERID, ticket.userId);
    }

    /**
     * For Client: Get ticket data from request.
     * @param response
     */
    public static fromResponse(xhr: any): Ticket|null {

        if ( !xhr ) {
            return null;
        }
        if ( !xhr.getAllResponseHeaders ) {
            return null;
        }

        let headers = xhr.getAllResponseHeaders();
        let arr = headers.trim().split(/[\r\n]+/);
        let headerMap: { [key: string]: any; } = {};
        for ( let i = 0; i < arr.length; i++ ) {
            let line = arr[i];
            let parts = line.split(": ");
            let key = parts[0];
            let value = parts[1];
            headerMap[key] = value;
        }

        let userId = headerMap[Ticket.HEADER_USERID];
        let ticketId = headerMap[Ticket.HEADER_TICKETID];
        let ticketExpiry = new Date();
        let ticks = parseInt(headerMap[Ticket.HEADER_TICKETEXPIRYDATE], 10);
        ticketExpiry.setTime(ticks);
        return new Ticket(userId, ticketId, ticketExpiry);

    }

    /**
     * For Server: Get data from HTTP Express request and convert to Ticket.
     * @param requestHeaders The Express request headers (request.headers)
     */
    public static fromRequest(requestHeaders: any): Ticket|null {
        if ( !requestHeaders ) {
            return null;
        }
        let ticketId: string = requestHeaders[Ticket.HEADER_TICKETID];
        let ticketUserId: string = requestHeaders[Ticket.HEADER_USERID];

        let ticketExpiry: string = requestHeaders[Ticket.HEADER_TICKETEXPIRYDATE];
        let ticketExpiryInt: number = parseInt(ticketExpiry, 10);
        let expiryDate = new Date();
        expiryDate.setTime(ticketExpiryInt);
        return new Ticket(ticketUserId, ticketId, expiryDate);
    }

    /**
     * For Server: Set ticket data in the server to respond to a request.
     * @param ticket The ticket data.
     * @param response Express response object.
     */
    public static toResponse(ticket: Ticket, response: any): void {
        response.set(Ticket.HEADER_TICKETID, ticket.ticketId);
        response.set(Ticket.HEADER_TICKETEXPIRYDATE, ticket.expirationDate.getTime());
        response.set(Ticket.HEADER_USERID, ticket.userId );
    }

    /**
     * Convert json object to ticket.
     * @param jsonObject
     * @returns {Ticket}
     */
    public static fromJSON(jsonObject: any): Ticket {
        let ticket = null;
        if ( jsonObject.ticketId && jsonObject.expirationDate && jsonObject.userId ) {
            let expiryDate: Date;
            if ( typeof jsonObject.expirationDate === "string") {
                expiryDate = new Date(jsonObject.expirationDate);
            } else {
                expiryDate = jsonObject.expiryDate;
            }
            ticket = new Ticket( jsonObject.userId, jsonObject.ticketId, expiryDate );
        } else {
            throw new Error("Cannot convert ticket");
        }
        return ticket;
    }

    /**
     * Create a new ticket. It is not stored anywhere yet though.
     */
    public static NewTicket( userId: string, numOfSecondsUntilExpiration: number ): Ticket {

        if ( !userId ) {
            throw new Error("User Id is required when creating new ticket");
        }
        if ( numOfSecondsUntilExpiration <= 0 ) {
            throw new Error("Expiration Time must be greater than 0");
        }

        let now: Date = new Date();
        let ticketId: any = uuid.getUuid();
        let ticketExpiration: Date = new Date();
        ticketExpiration.setTime( now.getTime() + numOfSecondsUntilExpiration * 1000 );

        let ticket = new Ticket(userId, ticketId, ticketExpiration);
        return ticket;

    }

    constructor(userId: string, ticketId: string, expirationDate: Date) {
        this._userId = userId;
        this._ticketId = ticketId;
        this._expirationDate = expirationDate;
    }

    /**
     * User Id that is associated with the ticket.
     * Ths ticket is unique to this user id.
     */
    get userId() {
        return this._userId;
    }

    /**
     * Unique identifier of this ticket.
     * @returns {string}
     */
    get ticketId() {
        return this._ticketId;
    }

    /**
     * Date and time when this ticket expires.
     * @returns {Date}
     */
    get expirationDate() {
        return this._expirationDate;
    }

    /**
     * Convert ticket to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = {};
        result.ticketId = this._ticketId;
        result.expirationDate = this._expirationDate;
        result.userId = this._userId;
        return result;
    }

}

export default Ticket;
