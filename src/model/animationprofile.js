"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var missingconstructorargumenterror_1 = require("../error/missingconstructorargumenterror");
var AnimationProfile = /** @class */ (function () {
    function AnimationProfile(id, name, idle, run, walk, die) {
        if (id && name) {
            this._id = id;
            this._name = name;
            this._idle = idle;
            this._run = run;
            this._walk = walk;
            this._die = die;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError("id=" + id + ", name=" + name);
        }
    }
    /**
     * Convert JSON object to Animation Profile object.
     */
    AnimationProfile.fromJSON = function (jsonObject) {
        if (!jsonObject) {
            throw new Error("Cannot convert undefined");
        }
        var id = jsonObject.id;
        var name = jsonObject.name;
        var idle = jsonObject.idle;
        var run = jsonObject.run;
        var walk = jsonObject.walk;
        var die = jsonObject.die;
        var animationProfile = new AnimationProfile(id, name, idle, run, walk, die);
        return animationProfile;
    };
    Object.defineProperty(AnimationProfile.prototype, "id", {
        /**
         * Animation profile id.
         */
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimationProfile.prototype, "name", {
        /**
         * Animation profile name. This is the name of the animated character model.
         */
        get: function () {
            return this._name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimationProfile.prototype, "idle", {
        /**
         * Action name for idle animation.
         */
        get: function () {
            return this._idle;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimationProfile.prototype, "run", {
        /**
         * Action name for run animation.
         */
        get: function () {
            return this._run;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimationProfile.prototype, "walk", {
        /**
         * Action name for walk animation.
         */
        get: function () {
            return this._walk;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnimationProfile.prototype, "die", {
        /**
         * Action name for die animation.
         */
        get: function () {
            return this._die;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert this animation profile to JSON.
     */
    AnimationProfile.prototype.toJSON = function () {
        var result = {};
        result.id = this._id;
        result.name = this._name;
        result.idle = this._idle;
        result.run = this._run;
        result.walk = this._walk;
        result.die = this._die;
        return result;
    };
    return AnimationProfile;
}());
exports.AnimationProfile = AnimationProfile;
exports.default = AnimationProfile;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL2FuaW1hdGlvbnByb2ZpbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw0RkFBMkY7QUFFM0Y7SUE4QkksMEJBQWEsRUFBVSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsR0FBVyxFQUFFLElBQVksRUFBRSxHQUFXO1FBQ3ZGLEVBQUUsQ0FBQyxDQUFFLEVBQUUsSUFBSSxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7WUFDZCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNsQixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztZQUNoQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNsQixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksaUVBQStCLENBQUMsS0FBSyxHQUFHLEVBQUUsR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDN0UsQ0FBQztJQUNMLENBQUM7SUFoQ0Q7O09BRUc7SUFDVyx5QkFBUSxHQUF0QixVQUF1QixVQUFlO1FBRWxDLEVBQUUsQ0FBQyxDQUFFLENBQUMsVUFBVyxDQUFDLENBQUMsQ0FBQztZQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDaEQsQ0FBQztRQUVELElBQUksRUFBRSxHQUFXLFVBQVUsQ0FBQyxFQUFFLENBQUM7UUFDL0IsSUFBSSxJQUFJLEdBQVcsVUFBVSxDQUFDLElBQUksQ0FBQztRQUNuQyxJQUFJLElBQUksR0FBVyxVQUFVLENBQUMsSUFBSSxDQUFDO1FBQ25DLElBQUksR0FBRyxHQUFXLFVBQVUsQ0FBQyxHQUFHLENBQUM7UUFDakMsSUFBSSxJQUFJLEdBQVcsVUFBVSxDQUFDLElBQUksQ0FBQztRQUNuQyxJQUFJLEdBQUcsR0FBVyxVQUFVLENBQUMsR0FBRyxDQUFDO1FBRWpDLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzVFLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztJQUU1QixDQUFDO0lBa0JELHNCQUFXLGdDQUFFO1FBSGI7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ3BCLENBQUM7OztPQUFBO0lBS0Qsc0JBQVcsa0NBQUk7UUFIZjs7V0FFRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFLRCxzQkFBVyxrQ0FBSTtRQUhmOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDOzs7T0FBQTtJQUtELHNCQUFXLGlDQUFHO1FBSGQ7O1dBRUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3JCLENBQUM7OztPQUFBO0lBS0Qsc0JBQVcsa0NBQUk7UUFIZjs7V0FFRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFLRCxzQkFBVyxpQ0FBRztRQUhkOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNyQixDQUFDOzs7T0FBQTtJQUVEOztPQUVHO0lBQ0ksaUNBQU0sR0FBYjtRQUNJLElBQUksTUFBTSxHQUE0QixFQUFFLENBQUM7UUFDekMsTUFBTSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ3JCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QixNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekIsTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3ZCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QixNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDdkIsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUwsdUJBQUM7QUFBRCxDQW5HQSxBQW1HQyxJQUFBO0FBbkdZLDRDQUFnQjtBQXFHN0Isa0JBQWUsZ0JBQWdCLENBQUMiLCJmaWxlIjoibW9kZWwvYW5pbWF0aW9ucHJvZmlsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgfSBmcm9tIFwiLi4vZXJyb3IvbWlzc2luZ2NvbnN0cnVjdG9yYXJndW1lbnRlcnJvclwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEFuaW1hdGlvblByb2ZpbGUge1xyXG5cclxuICAgIHByaXZhdGUgX2lkOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9uYW1lOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9pZGxlOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9ydW46IHN0cmluZztcclxuICAgIHByaXZhdGUgX3dhbGs6IHN0cmluZztcclxuICAgIHByaXZhdGUgX2RpZTogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBKU09OIG9iamVjdCB0byBBbmltYXRpb24gUHJvZmlsZSBvYmplY3QuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iamVjdDogYW55KSB7XHJcblxyXG4gICAgICAgIGlmICggIWpzb25PYmplY3QgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbm5vdCBjb252ZXJ0IHVuZGVmaW5lZFwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBpZDogc3RyaW5nID0ganNvbk9iamVjdC5pZDtcclxuICAgICAgICBsZXQgbmFtZTogc3RyaW5nID0ganNvbk9iamVjdC5uYW1lO1xyXG4gICAgICAgIGxldCBpZGxlOiBzdHJpbmcgPSBqc29uT2JqZWN0LmlkbGU7XHJcbiAgICAgICAgbGV0IHJ1bjogc3RyaW5nID0ganNvbk9iamVjdC5ydW47XHJcbiAgICAgICAgbGV0IHdhbGs6IHN0cmluZyA9IGpzb25PYmplY3Qud2FsaztcclxuICAgICAgICBsZXQgZGllOiBzdHJpbmcgPSBqc29uT2JqZWN0LmRpZTtcclxuXHJcbiAgICAgICAgbGV0IGFuaW1hdGlvblByb2ZpbGUgPSBuZXcgQW5pbWF0aW9uUHJvZmlsZShpZCwgbmFtZSwgaWRsZSwgcnVuLCB3YWxrLCBkaWUpO1xyXG4gICAgICAgIHJldHVybiBhbmltYXRpb25Qcm9maWxlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvciggaWQ6IHN0cmluZywgbmFtZTogc3RyaW5nLCBpZGxlOiBzdHJpbmcsIHJ1bjogc3RyaW5nLCB3YWxrOiBzdHJpbmcsIGRpZTogc3RyaW5nICkge1xyXG4gICAgICAgIGlmICggaWQgJiYgbmFtZSApIHtcclxuICAgICAgICAgICAgdGhpcy5faWQgPSBpZDtcclxuICAgICAgICAgICAgdGhpcy5fbmFtZSA9IG5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMuX2lkbGUgPSBpZGxlO1xyXG4gICAgICAgICAgICB0aGlzLl9ydW4gPSBydW47XHJcbiAgICAgICAgICAgIHRoaXMuX3dhbGsgPSB3YWxrO1xyXG4gICAgICAgICAgICB0aGlzLl9kaWUgPSBkaWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IoXCJpZD1cIiArIGlkICsgXCIsIG5hbWU9XCIgKyBuYW1lKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBbmltYXRpb24gcHJvZmlsZSBpZC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBpZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBbmltYXRpb24gcHJvZmlsZSBuYW1lLiBUaGlzIGlzIHRoZSBuYW1lIG9mIHRoZSBhbmltYXRlZCBjaGFyYWN0ZXIgbW9kZWwuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXQgbmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbmFtZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFjdGlvbiBuYW1lIGZvciBpZGxlIGFuaW1hdGlvbi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBpZGxlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pZGxlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWN0aW9uIG5hbWUgZm9yIHJ1biBhbmltYXRpb24uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXQgcnVuKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9ydW47XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBY3Rpb24gbmFtZSBmb3Igd2FsayBhbmltYXRpb24uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXQgd2FsaygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fd2FsaztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFjdGlvbiBuYW1lIGZvciBkaWUgYW5pbWF0aW9uLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0IGRpZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZGllO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB0aGlzIGFuaW1hdGlvbiBwcm9maWxlIHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IHsgW2tleTogc3RyaW5nXTogYW55OyB9ID0ge307XHJcbiAgICAgICAgcmVzdWx0LmlkID0gdGhpcy5faWQ7XHJcbiAgICAgICAgcmVzdWx0Lm5hbWUgPSB0aGlzLl9uYW1lO1xyXG4gICAgICAgIHJlc3VsdC5pZGxlID0gdGhpcy5faWRsZTtcclxuICAgICAgICByZXN1bHQucnVuID0gdGhpcy5fcnVuO1xyXG4gICAgICAgIHJlc3VsdC53YWxrID0gdGhpcy5fd2FsaztcclxuICAgICAgICByZXN1bHQuZGllID0gdGhpcy5fZGllO1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBBbmltYXRpb25Qcm9maWxlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
