"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("../math/point");
var UserMetaData = /** @class */ (function () {
    function UserMetaData(userId, userName) {
        this._userId = userId;
        this._userName = userName;
    }
    /**
     * Convert JSON object to a UserMetaData object.
     * @param jsonObject
     */
    UserMetaData.fromJSON = function (jsonObject) {
        if (!jsonObject) {
            throw new Error("Cannot convert undefined");
        }
        if (!jsonObject.hasOwnProperty("userId")) {
            throw new Error("No userId property");
        }
        if (!jsonObject.hasOwnProperty("userName")) {
            throw new Error("No userName property");
        }
        var usermetadata = new UserMetaData(jsonObject.userId, jsonObject.userName);
        if (jsonObject.sectionCoordinate) {
            usermetadata.sectionCoordinate = point_1.default.fromJSON(jsonObject.sectionCoorindate);
        }
        return usermetadata;
    };
    Object.defineProperty(UserMetaData.prototype, "userId", {
        get: function () {
            return this._userId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserMetaData.prototype, "userName", {
        get: function () {
            return this._userName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserMetaData.prototype, "sectionCoordinate", {
        get: function () {
            return this._sectionCoordinate;
        },
        set: function (point) {
            this._sectionCoordinate = point;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert user object into JSON.
     */
    UserMetaData.prototype.toJSON = function () {
        var json = {};
        json.userId = this._userId;
        json.userName = this._userName;
        if (this._sectionCoordinate) {
            json.sectionCoordinate = this._sectionCoordinate.toJSON();
        }
        return json;
    };
    return UserMetaData;
}());
exports.UserMetaData = UserMetaData;
exports.default = UserMetaData;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL3VzZXJtZXRhZGF0YS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHVDQUFrQztBQUVsQztJQWdDSSxzQkFBYSxNQUFjLEVBQUUsUUFBZ0I7UUFDekMsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7SUFDOUIsQ0FBQztJQTdCRDs7O09BR0c7SUFDVyxxQkFBUSxHQUF0QixVQUF1QixVQUFlO1FBRWxDLEVBQUUsQ0FBQyxDQUFFLENBQUMsVUFBVyxDQUFDLENBQUMsQ0FBQztZQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDaEQsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFFLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekMsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLE1BQU0sSUFBSSxLQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUM1QyxDQUFDO1FBRUQsSUFBSSxZQUFZLEdBQUcsSUFBSSxZQUFZLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUUsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLGlCQUFrQixDQUFDLENBQUMsQ0FBQztZQUNqQyxZQUFZLENBQUMsaUJBQWlCLEdBQUcsZUFBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNsRixDQUFDO1FBR0QsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUV4QixDQUFDO0lBT0Qsc0JBQUksZ0NBQU07YUFBVjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3hCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksa0NBQVE7YUFBWjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMkNBQWlCO2FBQXJCO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztRQUNuQyxDQUFDO2FBRUQsVUFBdUIsS0FBWTtZQUMvQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ3BDLENBQUM7OztPQUpBO0lBTUQ7O09BRUc7SUFDSSw2QkFBTSxHQUFiO1FBQ0ksSUFBSSxJQUFJLEdBQTRCLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQy9CLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxrQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUM5RCxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUwsbUJBQUM7QUFBRCxDQWxFQSxBQWtFQyxJQUFBO0FBbEVZLG9DQUFZO0FBb0V6QixrQkFBZSxZQUFZLENBQUMiLCJmaWxlIjoibW9kZWwvdXNlcm1ldGFkYXRhLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFBvaW50IGZyb20gXCIuLi9tYXRoL3BvaW50XCI7XHJcblxyXG5leHBvcnQgY2xhc3MgVXNlck1ldGFEYXRhIHtcclxuXHJcbiAgICBwcml2YXRlIF91c2VySWQ6IHN0cmluZztcclxuICAgIHByaXZhdGUgX3VzZXJOYW1lOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9zZWN0aW9uQ29vcmRpbmF0ZTogUG9pbnQ7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IEpTT04gb2JqZWN0IHRvIGEgVXNlck1ldGFEYXRhIG9iamVjdC5cclxuICAgICAqIEBwYXJhbSBqc29uT2JqZWN0XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbk9iamVjdDogYW55KTogVXNlck1ldGFEYXRhIHtcclxuXHJcbiAgICAgICAgaWYgKCAhanNvbk9iamVjdCApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGNvbnZlcnQgdW5kZWZpbmVkXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoICFqc29uT2JqZWN0Lmhhc093blByb3BlcnR5KFwidXNlcklkXCIpICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyB1c2VySWQgcHJvcGVydHlcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggIWpzb25PYmplY3QuaGFzT3duUHJvcGVydHkoXCJ1c2VyTmFtZVwiKSApIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTm8gdXNlck5hbWUgcHJvcGVydHlcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdXNlcm1ldGFkYXRhID0gbmV3IFVzZXJNZXRhRGF0YShqc29uT2JqZWN0LnVzZXJJZCwganNvbk9iamVjdC51c2VyTmFtZSk7XHJcbiAgICAgICAgaWYgKCBqc29uT2JqZWN0LnNlY3Rpb25Db29yZGluYXRlICkge1xyXG4gICAgICAgICAgICB1c2VybWV0YWRhdGEuc2VjdGlvbkNvb3JkaW5hdGUgPSBQb2ludC5mcm9tSlNPTihqc29uT2JqZWN0LnNlY3Rpb25Db29yaW5kYXRlKTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICByZXR1cm4gdXNlcm1ldGFkYXRhO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvciggdXNlcklkOiBzdHJpbmcsIHVzZXJOYW1lOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgdGhpcy5fdXNlcklkID0gdXNlcklkO1xyXG4gICAgICAgIHRoaXMuX3VzZXJOYW1lID0gdXNlck5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHVzZXJJZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdXNlcklkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCB1c2VyTmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdXNlck5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHNlY3Rpb25Db29yZGluYXRlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zZWN0aW9uQ29vcmRpbmF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgc2VjdGlvbkNvb3JkaW5hdGUoIHBvaW50OiBQb2ludCApIHtcclxuICAgICAgICB0aGlzLl9zZWN0aW9uQ29vcmRpbmF0ZSA9IHBvaW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCB1c2VyIG9iamVjdCBpbnRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCBqc29uOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSA9IHt9O1xyXG4gICAgICAgIGpzb24udXNlcklkID0gdGhpcy5fdXNlcklkO1xyXG4gICAgICAgIGpzb24udXNlck5hbWUgPSB0aGlzLl91c2VyTmFtZTtcclxuICAgICAgICBpZiAoIHRoaXMuX3NlY3Rpb25Db29yZGluYXRlICkge1xyXG4gICAgICAgICAgICBqc29uLnNlY3Rpb25Db29yZGluYXRlID0gdGhpcy5fc2VjdGlvbkNvb3JkaW5hdGUudG9KU09OKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBqc29uO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgVXNlck1ldGFEYXRhO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
