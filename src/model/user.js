"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var usermetadata_1 = require("./usermetadata");
var missingconstructorargumenterror_1 = require("../error/missingconstructorargumenterror");
var User = /** @class */ (function () {
    function User(userId, name) {
        this._disabled = false;
        if (userId && name) {
            this._userId = userId;
            this._name = name;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError("userid=" + userId + " name=" + name);
        }
    }
    /**
     * Convert a JSON object to a User object.
     * @param jsonObject
     * @returns {User}
     */
    User.fromJSON = function (jsonObject) {
        if (jsonObject.userId && jsonObject.name) {
            var userId = jsonObject.userId;
            var user = new User(userId, jsonObject.name);
            if (jsonObject.createdDate) {
                user.createdDate = new Date(jsonObject.createdDate);
            }
            if (jsonObject.email) {
                user.email = jsonObject.email;
            }
            if (jsonObject.language) {
                user.language = jsonObject.language;
            }
            if (jsonObject.lastLoggedInDate) {
                user.lastLoggedInDate = new Date(jsonObject.lastLoggedInDate);
            }
            if (jsonObject.pictureUrl) {
                user.pictureUrl = jsonObject.pictureUrl;
            }
            if (jsonObject.avatarObjectModelId) {
                user.avatarObjectModelId = jsonObject.avatarObjectModelId;
            }
            if (jsonObject.disabled) {
                user.disabled = jsonObject.disabled === true ? true : false;
            }
            return user;
        }
        else {
            throw new Error("Unable to convert to user object");
        }
    };
    Object.defineProperty(User.prototype, "createdDate", {
        get: function () {
            return this._createdDate;
        },
        set: function (value) {
            this._createdDate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (value) {
            this._email = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Get meta-data associated with this user. Used to return basic and simple user information. To be used along with
     * messages, and other response objects.
     * @returns {UserMetaData}
     */
    User.prototype.getMetaData = function () {
        return new usermetadata_1.UserMetaData(this._userId, this._name);
    };
    Object.defineProperty(User.prototype, "language", {
        get: function () {
            return this._language;
        },
        set: function (value) {
            this._language = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "lastLoggedInDate", {
        get: function () {
            return this._lastLoggedInDate;
        },
        set: function (value) {
            this._lastLoggedInDate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "disabled", {
        get: function () {
            return this._disabled;
        },
        set: function (value) {
            this._disabled = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "pictureUrl", {
        get: function () {
            return this._pictureUrl;
        },
        set: function (value) {
            this._pictureUrl = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "avatarObjectModelId", {
        get: function () {
            return this._avatarObjectModelId;
        },
        set: function (value) {
            this._avatarObjectModelId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "userId", {
        get: function () {
            return this._userId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "name", {
        get: function () {
            return this._name;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert user object into JSON.
     */
    User.prototype.toJSON = function () {
        var json = {};
        json.userId = this._userId;
        json.name = this._name;
        json.createdDate = this._createdDate;
        json.email = this._email;
        json.language = this._language;
        json.lastLoggedInDate = this._lastLoggedInDate;
        json.pictureUrl = this._pictureUrl;
        json.disabled = this._disabled;
        json.avatarObjectModelId = this._avatarObjectModelId;
        return json;
    };
    User.TYPE = "user";
    return User;
}());
exports.User = User;
exports.default = User;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL3VzZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwrQ0FBNEM7QUFFNUMsNEZBQTJGO0FBRTNGO0lBa0RJLGNBQWEsTUFBYyxFQUFFLElBQVk7UUF4Q2pDLGNBQVMsR0FBRyxLQUFLLENBQUM7UUF5Q3RCLEVBQUUsQ0FBQyxDQUFFLE1BQU0sSUFBSSxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxpRUFBK0IsQ0FBQyxTQUFTLEdBQUcsTUFBTSxHQUFHLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUNwRixDQUFDO0lBQ0wsQ0FBQztJQTNDRDs7OztPQUlHO0lBQ1csYUFBUSxHQUF0QixVQUF1QixVQUFlO1FBQ2xDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQztZQUMvQixJQUFJLElBQUksR0FBUyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25ELEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxXQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN4RCxDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLEtBQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQztZQUNsQyxDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLFFBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUN4QyxDQUFDO1lBQ0QsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLGdCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ2xFLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsVUFBVyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsVUFBVSxDQUFDO1lBQzVDLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsbUJBQW9CLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsVUFBVSxDQUFDLG1CQUFtQixDQUFDO1lBQzlELENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBRSxVQUFVLENBQUMsUUFBUyxDQUFDLENBQUMsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDaEUsQ0FBQztZQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1FBQ3hELENBQUM7SUFDTCxDQUFDO0lBV0Qsc0JBQUksNkJBQVc7YUFBZjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzdCLENBQUM7YUFFRCxVQUFnQixLQUFXO1lBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzlCLENBQUM7OztPQUpBO0lBTUQsc0JBQUksdUJBQUs7YUFBVDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUM7YUFFRCxVQUFVLEtBQWE7WUFDbkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDeEIsQ0FBQzs7O09BSkE7SUFNRDs7OztPQUlHO0lBQ0ksMEJBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsSUFBSSwyQkFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCxzQkFBSSwwQkFBUTthQUFaO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDMUIsQ0FBQzthQUVELFVBQWEsS0FBYTtZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUMzQixDQUFDOzs7T0FKQTtJQU1ELHNCQUFJLGtDQUFnQjthQUFwQjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDbEMsQ0FBQzthQUVELFVBQXFCLEtBQVc7WUFDNUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztRQUNuQyxDQUFDOzs7T0FKQTtJQU1ELHNCQUFJLDBCQUFRO2FBQVo7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDO2FBRUQsVUFBYSxLQUFjO1lBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQzNCLENBQUM7OztPQUpBO0lBTUQsc0JBQUksNEJBQVU7YUFBZDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVCLENBQUM7YUFFRCxVQUFlLEtBQWE7WUFDeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDN0IsQ0FBQzs7O09BSkE7SUFNRCxzQkFBSSxxQ0FBbUI7YUFBdkI7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDO1FBQ3JDLENBQUM7YUFFRCxVQUF3QixLQUFhO1lBQ2pDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFLLENBQUM7UUFDdEMsQ0FBQzs7O09BSkE7SUFNRCxzQkFBSSx3QkFBTTthQUFWO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDeEIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxzQkFBSTthQUFSO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFFRDs7T0FFRztJQUNJLHFCQUFNLEdBQWI7UUFDSSxJQUFJLElBQUksR0FBNEIsRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUMvQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDbkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQy9CLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7UUFDckQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBakpjLFNBQUksR0FBRyxNQUFNLENBQUM7SUFtSmpDLFdBQUM7Q0FySkQsQUFxSkMsSUFBQTtBQXJKWSxvQkFBSTtBQXVKakIsa0JBQWUsSUFBSSxDQUFDIiwiZmlsZSI6Im1vZGVsL3VzZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1VzZXJNZXRhRGF0YX0gZnJvbSBcIi4vdXNlcm1ldGFkYXRhXCI7XHJcbmltcG9ydCBQb2ludCBmcm9tIFwiLi4vbWF0aC9wb2ludFwiO1xyXG5pbXBvcnQgeyBNaXNzaW5nQ29uc3RydWN0b3JBcmd1bWVudEVycm9yIH0gZnJvbSBcIi4uL2Vycm9yL21pc3Npbmdjb25zdHJ1Y3RvcmFyZ3VtZW50ZXJyb3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBVc2VyIHtcclxuXHJcbiAgICBwcml2YXRlIHN0YXRpYyBUWVBFID0gXCJ1c2VyXCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfdXNlcklkOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9uYW1lOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9lbWFpbDogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBfY3JlYXRlZERhdGU6IERhdGU7XHJcbiAgICBwcml2YXRlIF9sYXN0TG9nZ2VkSW5EYXRlOiBEYXRlO1xyXG4gICAgcHJpdmF0ZSBfbGFuZ3VhZ2U6IHN0cmluZztcclxuICAgIHByaXZhdGUgX2Rpc2FibGVkID0gZmFsc2U7XHJcbiAgICBwcml2YXRlIF9waWN0dXJlVXJsOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9hdmF0YXJPYmplY3RNb2RlbElkOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IGEgSlNPTiBvYmplY3QgdG8gYSBVc2VyIG9iamVjdC5cclxuICAgICAqIEBwYXJhbSBqc29uT2JqZWN0XHJcbiAgICAgKiBAcmV0dXJucyB7VXNlcn1cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uT2JqZWN0OiBhbnkpIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudXNlcklkICYmIGpzb25PYmplY3QubmFtZSApIHtcclxuICAgICAgICAgICAgbGV0IHVzZXJJZCA9IGpzb25PYmplY3QudXNlcklkO1xyXG4gICAgICAgICAgICBsZXQgdXNlcjogVXNlciA9IG5ldyBVc2VyKHVzZXJJZCwganNvbk9iamVjdC5uYW1lKTtcclxuICAgICAgICAgICAgaWYgKCBqc29uT2JqZWN0LmNyZWF0ZWREYXRlICkge1xyXG4gICAgICAgICAgICAgICAgdXNlci5jcmVhdGVkRGF0ZSA9IG5ldyBEYXRlKGpzb25PYmplY3QuY3JlYXRlZERhdGUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICgganNvbk9iamVjdC5lbWFpbCApIHtcclxuICAgICAgICAgICAgICAgIHVzZXIuZW1haWwgPSBqc29uT2JqZWN0LmVtYWlsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICgganNvbk9iamVjdC5sYW5ndWFnZSApIHtcclxuICAgICAgICAgICAgICAgIHVzZXIubGFuZ3VhZ2UgPSBqc29uT2JqZWN0Lmxhbmd1YWdlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICgganNvbk9iamVjdC5sYXN0TG9nZ2VkSW5EYXRlICkge1xyXG4gICAgICAgICAgICAgICAgdXNlci5sYXN0TG9nZ2VkSW5EYXRlID0gbmV3IERhdGUoanNvbk9iamVjdC5sYXN0TG9nZ2VkSW5EYXRlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIGpzb25PYmplY3QucGljdHVyZVVybCApIHtcclxuICAgICAgICAgICAgICAgIHVzZXIucGljdHVyZVVybCA9IGpzb25PYmplY3QucGljdHVyZVVybDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIGpzb25PYmplY3QuYXZhdGFyT2JqZWN0TW9kZWxJZCApIHtcclxuICAgICAgICAgICAgICAgIHVzZXIuYXZhdGFyT2JqZWN0TW9kZWxJZCA9IGpzb25PYmplY3QuYXZhdGFyT2JqZWN0TW9kZWxJZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIGpzb25PYmplY3QuZGlzYWJsZWQgKSB7XHJcbiAgICAgICAgICAgICAgICB1c2VyLmRpc2FibGVkID0ganNvbk9iamVjdC5kaXNhYmxlZCA9PT0gdHJ1ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gdXNlcjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJVbmFibGUgdG8gY29udmVydCB0byB1c2VyIG9iamVjdFwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHVzZXJJZDogc3RyaW5nLCBuYW1lOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgaWYgKCB1c2VySWQgJiYgbmFtZSApIHtcclxuICAgICAgICAgICAgdGhpcy5fdXNlcklkID0gdXNlcklkO1xyXG4gICAgICAgICAgICB0aGlzLl9uYW1lID0gbmFtZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvcihcInVzZXJpZD1cIiArIHVzZXJJZCArIFwiIG5hbWU9XCIgKyBuYW1lKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGNyZWF0ZWREYXRlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jcmVhdGVkRGF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgY3JlYXRlZERhdGUodmFsdWU6IERhdGUpIHtcclxuICAgICAgICB0aGlzLl9jcmVhdGVkRGF0ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBlbWFpbCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZW1haWw7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IGVtYWlsKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLl9lbWFpbCA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IG1ldGEtZGF0YSBhc3NvY2lhdGVkIHdpdGggdGhpcyB1c2VyLiBVc2VkIHRvIHJldHVybiBiYXNpYyBhbmQgc2ltcGxlIHVzZXIgaW5mb3JtYXRpb24uIFRvIGJlIHVzZWQgYWxvbmcgd2l0aFxyXG4gICAgICogbWVzc2FnZXMsIGFuZCBvdGhlciByZXNwb25zZSBvYmplY3RzLlxyXG4gICAgICogQHJldHVybnMge1VzZXJNZXRhRGF0YX1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldE1ldGFEYXRhKCk6IFVzZXJNZXRhRGF0YSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBVc2VyTWV0YURhdGEodGhpcy5fdXNlcklkLCB0aGlzLl9uYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbGFuZ3VhZ2UoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xhbmd1YWdlO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBsYW5ndWFnZSh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fbGFuZ3VhZ2UgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbGFzdExvZ2dlZEluRGF0ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbGFzdExvZ2dlZEluRGF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgbGFzdExvZ2dlZEluRGF0ZSh2YWx1ZTogRGF0ZSkge1xyXG4gICAgICAgIHRoaXMuX2xhc3RMb2dnZWRJbkRhdGUgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzYWJsZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2Rpc2FibGVkO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBkaXNhYmxlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuX2Rpc2FibGVkID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHBpY3R1cmVVcmwoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3BpY3R1cmVVcmw7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IHBpY3R1cmVVcmwodmFsdWU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX3BpY3R1cmVVcmwgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgYXZhdGFyT2JqZWN0TW9kZWxJZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYXZhdGFyT2JqZWN0TW9kZWxJZDtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgYXZhdGFyT2JqZWN0TW9kZWxJZCh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fYXZhdGFyT2JqZWN0TW9kZWxJZCA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCB1c2VySWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJJZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbmFtZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdXNlciBvYmplY3QgaW50byBKU09OLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHsgW2tleTogc3RyaW5nXTogYW55OyB9IHtcclxuICAgICAgICBsZXQganNvbjogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSB7fTtcclxuICAgICAgICBqc29uLnVzZXJJZCA9IHRoaXMuX3VzZXJJZDtcclxuICAgICAgICBqc29uLm5hbWUgPSB0aGlzLl9uYW1lO1xyXG4gICAgICAgIGpzb24uY3JlYXRlZERhdGUgPSB0aGlzLl9jcmVhdGVkRGF0ZTtcclxuICAgICAgICBqc29uLmVtYWlsID0gdGhpcy5fZW1haWw7XHJcbiAgICAgICAganNvbi5sYW5ndWFnZSA9IHRoaXMuX2xhbmd1YWdlO1xyXG4gICAgICAgIGpzb24ubGFzdExvZ2dlZEluRGF0ZSA9IHRoaXMuX2xhc3RMb2dnZWRJbkRhdGU7XHJcbiAgICAgICAganNvbi5waWN0dXJlVXJsID0gdGhpcy5fcGljdHVyZVVybDtcclxuICAgICAgICBqc29uLmRpc2FibGVkID0gdGhpcy5fZGlzYWJsZWQ7XHJcbiAgICAgICAganNvbi5hdmF0YXJPYmplY3RNb2RlbElkID0gdGhpcy5fYXZhdGFyT2JqZWN0TW9kZWxJZDtcclxuICAgICAgICByZXR1cm4ganNvbjtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFVzZXI7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
