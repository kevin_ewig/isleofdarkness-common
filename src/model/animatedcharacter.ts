import ObjectModel from "./objectmodel";
import Point from "../math/point";
import Shape from "../math/shape";
import AnimationProfile from "./animationprofile";

const TWEEN: any = require("@tweenjs/tween.js");

export class AnimatedCharacter extends ObjectModel {

    public static TYPE = "animatedcharacter";
    private static _DEFAULT_SPEED = 500;

    private _path: Point[];
    private _tween: any;
    private _dateTweenStart: Date;
    private _speed: number = AnimatedCharacter._DEFAULT_SPEED;
    private _onTweenUpdatedFunction: Function;
    private _onTweenCompleteFunction: Function;
    private _animationProfile: AnimationProfile;

    /**
     * Convert JSON object to Object Model object.
     */
    public static fromJSON(jsonObject: any) {

        if ( !jsonObject ) {
            throw new Error("Cannot convert undefined");
        }

        if ( !AnimatedCharacter.isCorrectType(jsonObject) ) {
            throw new Error("Cannot convert. Invalid type.");
        }

        // Extract speed.
        let speed = AnimatedCharacter._DEFAULT_SPEED;
        if ( jsonObject.speed ) {
            speed = parseInt(jsonObject.speed, 10);
        }

        // Copy all values into a new Animated Character object.
        let source = ObjectModel.fromJSONNotStrict(jsonObject) as AnimatedCharacter;
        let animatedChar: AnimatedCharacter = new AnimatedCharacter(source.id, source.shape, source.modelName);
        Object.assign(animatedChar, source);

        if ( animatedChar ) {
            animatedChar.speed = speed;

            let points: Point[] = [];
            if ( jsonObject.path && jsonObject.dateTweenStart ) {
                for ( let index = 0; index < jsonObject.path.length; index++ ) {
                    points.push( Point.fromJSON(jsonObject.path[index]) );
                }
                let dateTweenStart: Date = new Date(jsonObject.dateTweenStart);
                animatedChar.setPath(points, dateTweenStart);
            }
        }

        // Set animation profile if it exists.
        if ( jsonObject.animationProfile ) {
            animatedChar.animationProfile = AnimationProfile.fromJSON(jsonObject.animationProfile);
        }

        return animatedChar;

    }

    /**
     * Check if this json object has the correct animated character type.
     * @param jsonObject
     */
    public static isCorrectType(jsonObject: any) {
        if ( jsonObject.type !== AnimatedCharacter.TYPE ) {
            return false;
        }
        return true;
    }

    constructor(id: string, shape: Shape, modelName: string) {
        super(id, shape, modelName);
    }

    /**
     * Get the path this object will travel.
     */
    public getPath(): Point[] {
        return this._path;
    }

    /**
     * Set path made up of points.
     * @param points
     */
    public setPath( points: Point[], dateTweenStart?: Date ): void {

        // Stop tween if it started.
        if ( this._tween ) {
            this._tween.stop();
        }

        let startPos = this.shape;
        let xArray = [];
        let yArray = [];
        let zArray = [];
        for ( let i = 0; i < points.length; i++ ) {
            xArray[i] = points[i].x;
            yArray[i] = points[i].y;
            zArray[i] = points[i].z;
        }

        // Set points globally
        this._path = points;

        let totalTravelTime = points.length * this._speed;
        this._tween = new TWEEN.Tween(startPos).
            to( { x: xArray, y: yArray, z: zArray }, totalTravelTime ).
            easing( TWEEN.Easing.Linear.None );

        // Call this function on tween update.
        if ( this._onTweenUpdatedFunction ) {
            this._tween.onUpdate( this._onTweenUpdatedFunction );
        }

        // Call this function on tween stop.
        if ( this._onTweenCompleteFunction ) {
            let self: any = this;
            this._tween.onComplete( function() {
                let tweenObject = this._object;
                self._onTweenCompleteFunction(tweenObject);
            }
            );
        }

        // Set date tween start -- if there is any.
        this._dateTweenStart = new Date();
        if ( dateTweenStart ) {
            this._dateTweenStart = dateTweenStart;
        }

        // No delays.
        this._tween.delay(0);

        // Start moving.
        this._tween.start(this._dateTweenStart.getTime());

    }

    /**
     * Set how fast this model will move.
     * @param value
     */
    public set speed(value: number) {
        this._speed = value;
    }

    /**
     * Get how fast this model will move.
     * @param value
     */
    public get speed() {
        return this._speed;
    }

    /**
     * Set the animation values for walk, run, idle, etc.
     * @param value
     */
    public set animationProfile(value: AnimationProfile) {
        this._animationProfile = value;
    }

    /**
     * Get the animation values for walk, run, idle, etc.
     * @param value
     */
    public get animationProfile() {
        return this._animationProfile;
    }

    /**
     * Get date/time tween started.
     * @param value
     */
    public get dateTweenStart() {
        return this._dateTweenStart;
    }

    /**
     * Set function that will be called after tween is complete.
     * @param value
     */
    public set onTweenComplete(completeFunction: Function) {
        this._onTweenCompleteFunction = completeFunction;
    }

    /**
     * Set function that will be called any time the tween is updated.
     * @param value
     */
    public set onTweenUpdate(updateFunction: Function) {
        this._onTweenUpdatedFunction = updateFunction;
    }

     /**
     * Convert this object model to JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let result: { [key: string]: any; } = {};
        result = super.toJSON();
        result.speed = this._speed;
        result.type = AnimatedCharacter.TYPE;

        if ( this._path ) {
            result.path = [];
            for ( let index = 0; index < this._path.length; index++ ) {
                result.path.push( this._path[index].toJSON() );
            }
        }

        if ( this._dateTweenStart ) {
            result.dateTweenStart = this._dateTweenStart;
        }

        if ( this._animationProfile ) {
            result.animationProfile = this._animationProfile;
        }

        return result;
    }

}

export default AnimatedCharacter;
