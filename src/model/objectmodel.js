"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("../math/common");
var missingconstructorargumenterror_1 = require("../error/missingconstructorargumenterror");
/**
 * An object that is rendered in the world.
 */
var ObjectModel = /** @class */ (function () {
    function ObjectModel(id, shape, modelName) {
        if (id && modelName && shape) {
            this._id = id;
            this._shape = shape;
            this._modelName = modelName;
            this._objectType = ObjectModelType.Passable;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError("Missing: id=" + id + " shape=" + shape + " modelName=" + modelName);
        }
    }
    /**
     * Convert JSON object to Object Model object.
     */
    ObjectModel.fromJSON = function (jsonObject) {
        if (!jsonObject) {
            throw new Error("Cannot convert undefined");
        }
        if (!ObjectModel.isCorrectType(jsonObject)) {
            throw new Error("Cannot convert. Invalid type.");
        }
        return ObjectModel.fromJSONNotStrict(jsonObject);
    };
    /**
     * Convert JSON object to Object Model object without checking for type.
     * Used by base classes.
     */
    ObjectModel.fromJSONNotStrict = function (jsonObject) {
        if (!jsonObject) {
            throw new Error("Cannot convert undefined");
        }
        var id = jsonObject.id;
        var shape = common_1.Common.convertShape(jsonObject.shape);
        var modelName = jsonObject.modelName;
        var modelTexture = jsonObject.modelTexture;
        var ownerUserId = jsonObject.ownerUserId;
        var worldName = jsonObject.worldName;
        var displayName = jsonObject.displayName;
        var objType = ObjectModelType.Passable;
        if (jsonObject.objectType) {
            var objTypeAsInt = parseInt(jsonObject.objectType, 10);
            objType = objTypeAsInt;
        }
        var objModel = new ObjectModel(id, shape, modelName);
        objModel.modelTexture = modelTexture;
        objModel.shape = shape;
        objModel.ownerUserId = ownerUserId;
        objModel.objectType = objType;
        objModel.worldName = worldName;
        objModel.displayName = displayName;
        return objModel;
    };
    /**
     * Check if this json object has the correct object model type.
     * @param jsonObject
     */
    ObjectModel.isCorrectType = function (jsonObject) {
        if (jsonObject.type !== ObjectModel.TYPE) {
            return false;
        }
        return true;
    };
    Object.defineProperty(ObjectModel.prototype, "id", {
        /**
         * A unique id of this model.
         * @returns {string}
         */
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel.prototype, "shape", {
        /**
         * Bounding shape of thi model.
         * @returns {Shape}
         */
        get: function () {
            return this._shape;
        },
        /**
         * Bounding shape of this model.
         */
        set: function (shape) {
            this._shape = shape;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel.prototype, "worldName", {
        /**
         * The name of the world the object model is in.
         * @returns {string}
         */
        get: function () {
            return this._worldName;
        },
        /**
         * The name of the world the object model is in.
         */
        set: function (value) {
            this._worldName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel.prototype, "displayName", {
        /**
         * A distinct name of the object model.
         * @returns {string}
         */
        get: function () {
            return this._displayName;
        },
        /**
         * A distinct name of the object model.
         */
        set: function (value) {
            this._displayName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel.prototype, "modelName", {
        /**
         * The name of the model. This is the 3d file used by this model.
         * @returns {string}
         */
        get: function () {
            return this._modelName;
        },
        /**
         * The name of the model. This is the 3d file used by this model.
         */
        set: function (value) {
            this._modelName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel.prototype, "modelTexture", {
        /**
         * Get the texture of the model. (Optional)
         * If no texture specified then engine will use the default texture assigned in the model.
         * @param value
         */
        get: function () {
            return this._modelTexture;
        },
        /**
         * Set the texture of the model. (Optional)
         * If no texture specified then engine will use the default texture assigned in the model.
         * @param value
         */
        set: function (value) {
            this._modelTexture = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel.prototype, "ownerUserId", {
        /**
         * Get the user id of the owner of this object.
         * @param value
         */
        get: function () {
            return this._ownerUserId;
        },
        /**
         * Set the user id of the owner of this object.
         * @param value
         */
        set: function (value) {
            this._ownerUserId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel.prototype, "objectType", {
        /**
         * This object can be either passable, climbable or impassable. The type
         * determines how other objects behave and interact around this one.
         * @returns {ObjectModelType}
         */
        get: function () {
            return this._objectType;
        },
        /**
         * This object can be either passable, climbable or impassable. The type
         * determines how other objects behave and interact around this one.
         */
        set: function (value) {
            this._objectType = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Get the 2-dimensional section coordinate location of this object model.
     * At this point the section coordinate is just the coordinate of the shape
     * divided by a constant number.
     */
    ObjectModel.prototype.getSectionCoordinate = function () {
        var shape = this._shape;
        var position = shape.getPosition();
        return position.getSectionCoordinate();
    };
    /**
     * Convert this object model to JSON.
     */
    ObjectModel.prototype.toJSON = function () {
        var result = {};
        result.id = this._id;
        result.shape = this.shape.toJSON();
        result.modelName = this._modelName;
        result.modelTexture = this._modelTexture;
        result.ownerUserId = this._ownerUserId;
        result.type = ObjectModel.TYPE;
        result.objectType = this._objectType;
        result.worldName = this._worldName;
        result.displayName = this._displayName;
        return result;
    };
    ObjectModel.TYPE = "objectmodel";
    return ObjectModel;
}());
exports.ObjectModel = ObjectModel;
/**
 * This determines how an avatar can interact with an object model in the world.
 */
var ObjectModelType;
(function (ObjectModelType) {
    ObjectModelType[ObjectModelType["Passable"] = 1] = "Passable";
    ObjectModelType[ObjectModelType["Impassable"] = 2] = "Impassable";
    ObjectModelType[ObjectModelType["Climbable"] = 3] = "Climbable";
})(ObjectModelType = exports.ObjectModelType || (exports.ObjectModelType = {}));
exports.default = ObjectModel;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL29iamVjdG1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EseUNBQXNDO0FBRXRDLDRGQUEyRjtBQUUzRjs7R0FFRztBQUNIO0lBNEVJLHFCQUFZLEVBQVUsRUFBRSxLQUFZLEVBQUUsU0FBaUI7UUFDbkQsRUFBRSxDQUFDLENBQUUsRUFBRSxJQUFJLFNBQVMsSUFBSSxLQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO1lBQ2QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7WUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQUMsUUFBUSxDQUFDO1FBQ2hELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxpRUFBK0IsQ0FBQyxjQUFjLEdBQUcsRUFBRSxHQUFHLFNBQVMsR0FBRyxLQUFLLEdBQUcsYUFBYSxHQUFHLFNBQVMsQ0FBQyxDQUFDO1FBQ25ILENBQUM7SUFDTCxDQUFDO0lBeEVEOztPQUVHO0lBQ1csb0JBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUVsQyxFQUFFLENBQUMsQ0FBRSxDQUFDLFVBQVcsQ0FBQyxDQUFDLENBQUM7WUFDaEIsTUFBTSxJQUFJLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBQ2hELENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBRSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLE1BQU0sSUFBSSxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUNyRCxDQUFDO1FBRUQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUVyRCxDQUFDO0lBRUQ7OztPQUdHO0lBQ1csNkJBQWlCLEdBQS9CLFVBQWdDLFVBQWU7UUFFM0MsRUFBRSxDQUFDLENBQUUsQ0FBQyxVQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLE1BQU0sSUFBSSxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUNoRCxDQUFDO1FBRUQsSUFBSSxFQUFFLEdBQVcsVUFBVSxDQUFDLEVBQUUsQ0FBQztRQUMvQixJQUFJLEtBQUssR0FBVSxlQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6RCxJQUFJLFNBQVMsR0FBVyxVQUFVLENBQUMsU0FBUyxDQUFDO1FBQzdDLElBQUksWUFBWSxHQUFXLFVBQVUsQ0FBQyxZQUFZLENBQUM7UUFDbkQsSUFBSSxXQUFXLEdBQVcsVUFBVSxDQUFDLFdBQVcsQ0FBQztRQUNqRCxJQUFJLFNBQVMsR0FBVyxVQUFVLENBQUMsU0FBUyxDQUFDO1FBQzdDLElBQUksV0FBVyxHQUFXLFVBQVUsQ0FBQyxXQUFXLENBQUM7UUFFakQsSUFBSSxPQUFPLEdBQW9CLGVBQWUsQ0FBQyxRQUFRLENBQUM7UUFDeEQsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDeEIsSUFBSSxZQUFZLEdBQVcsUUFBUSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDL0QsT0FBTyxHQUFHLFlBQVksQ0FBQztRQUMzQixDQUFDO1FBRUQsSUFBSSxRQUFRLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNyRCxRQUFRLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztRQUNyQyxRQUFRLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUN2QixRQUFRLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUNuQyxRQUFRLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztRQUM5QixRQUFRLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMvQixRQUFRLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUNuQyxNQUFNLENBQUMsUUFBUSxDQUFDO0lBRXBCLENBQUM7SUFFRDs7O09BR0c7SUFDVyx5QkFBYSxHQUEzQixVQUE0QixVQUFlO1FBQ3ZDLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxJQUFJLEtBQUssV0FBVyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUM7WUFDekMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBaUJELHNCQUFXLDJCQUFFO1FBSmI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNwQixDQUFDOzs7T0FBQTtJQUtELHNCQUFXLDhCQUFLO1FBSWhCOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQztRQWJEOztXQUVHO2FBQ0gsVUFBaUIsS0FBWTtZQUN6QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQWFELHNCQUFXLGtDQUFTO1FBSXBCOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDM0IsQ0FBQztRQWJEOztXQUVHO2FBQ0gsVUFBc0IsS0FBYTtZQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQWFELHNCQUFXLG9DQUFXO1FBSXRCOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDN0IsQ0FBQztRQWJEOztXQUVHO2FBQ0gsVUFBd0IsS0FBYTtZQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQWFELHNCQUFXLGtDQUFTO1FBSXBCOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDM0IsQ0FBQztRQWJEOztXQUVHO2FBQ0gsVUFBc0IsS0FBYTtZQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQWVELHNCQUFXLHFDQUFZO1FBSXZCOzs7O1dBSUc7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO1FBQzlCLENBQUM7UUFoQkQ7Ozs7V0FJRzthQUNILFVBQXdCLEtBQWE7WUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDL0IsQ0FBQzs7O09BQUE7SUFlRCxzQkFBVyxvQ0FBVztRQUl0Qjs7O1dBR0c7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzdCLENBQUM7UUFkRDs7O1dBR0c7YUFDSCxVQUF1QixLQUFhO1lBQ2hDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzlCLENBQUM7OztPQUFBO0lBY0Qsc0JBQVcsbUNBQVU7UUFJckI7Ozs7V0FJRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDNUIsQ0FBQztRQWZEOzs7V0FHRzthQUNILFVBQXNCLEtBQXNCO1lBQ3hDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQzdCLENBQUM7OztPQUFBO0lBV0Q7Ozs7T0FJRztJQUNJLDBDQUFvQixHQUEzQjtRQUNJLElBQUksS0FBSyxHQUFVLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDL0IsSUFBSSxRQUFRLEdBQVUsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztJQUMzQyxDQUFDO0lBRUQ7O09BRUc7SUFDSSw0QkFBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLEVBQUUsQ0FBQztRQUN6QyxNQUFNLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDckIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUNuQyxNQUFNLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDekMsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ3ZDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQztRQUMvQixNQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDckMsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUN2QyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUF0T2EsZ0JBQUksR0FBRyxhQUFhLENBQUM7SUF1T3ZDLGtCQUFDO0NBek9ELEFBeU9DLElBQUE7QUF6T1ksa0NBQVc7QUEyT3hCOztHQUVHO0FBQ0gsSUFBWSxlQUlYO0FBSkQsV0FBWSxlQUFlO0lBQ3ZCLDZEQUFZLENBQUE7SUFDWixpRUFBYyxDQUFBO0lBQ2QsK0RBQWEsQ0FBQTtBQUNqQixDQUFDLEVBSlcsZUFBZSxHQUFmLHVCQUFlLEtBQWYsdUJBQWUsUUFJMUI7QUFFRCxrQkFBZSxXQUFXLENBQUMiLCJmaWxlIjoibW9kZWwvb2JqZWN0bW9kZWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2hhcGUgZnJvbSBcIi4uL21hdGgvc2hhcGVcIjtcclxuaW1wb3J0IHtDb21tb259IGZyb20gXCIuLi9tYXRoL2NvbW1vblwiO1xyXG5pbXBvcnQgUG9pbnQgZnJvbSBcIi4uL21hdGgvUG9pbnRcIjtcclxuaW1wb3J0IHsgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvciB9IGZyb20gXCIuLi9lcnJvci9taXNzaW5nY29uc3RydWN0b3Jhcmd1bWVudGVycm9yXCI7XHJcblxyXG4vKipcclxuICogQW4gb2JqZWN0IHRoYXQgaXMgcmVuZGVyZWQgaW4gdGhlIHdvcmxkLlxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIE9iamVjdE1vZGVsIHtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEUgPSBcIm9iamVjdG1vZGVsXCI7XHJcblxyXG4gICAgcHJpdmF0ZSBfaWQ6IHN0cmluZztcclxuICAgIHByaXZhdGUgX3NoYXBlOiBTaGFwZTtcclxuICAgIHByaXZhdGUgX3dvcmxkTmFtZTogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBfb2JqZWN0VHlwZTogT2JqZWN0TW9kZWxUeXBlO1xyXG4gICAgcHJpdmF0ZSBfbW9kZWxOYW1lOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9kaXNwbGF5TmFtZTogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBfbW9kZWxUZXh0dXJlOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9vd25lclVzZXJJZDogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBKU09OIG9iamVjdCB0byBPYmplY3QgTW9kZWwgb2JqZWN0LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSkge1xyXG5cclxuICAgICAgICBpZiAoICFqc29uT2JqZWN0ICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgY29udmVydCB1bmRlZmluZWRcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoICFPYmplY3RNb2RlbC5pc0NvcnJlY3RUeXBlKGpzb25PYmplY3QpICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgY29udmVydC4gSW52YWxpZCB0eXBlLlwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBPYmplY3RNb2RlbC5mcm9tSlNPTk5vdFN0cmljdChqc29uT2JqZWN0KTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IEpTT04gb2JqZWN0IHRvIE9iamVjdCBNb2RlbCBvYmplY3Qgd2l0aG91dCBjaGVja2luZyBmb3IgdHlwZS5cclxuICAgICAqIFVzZWQgYnkgYmFzZSBjbGFzc2VzLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OTm90U3RyaWN0KGpzb25PYmplY3Q6IGFueSkge1xyXG5cclxuICAgICAgICBpZiAoICFqc29uT2JqZWN0ICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgY29udmVydCB1bmRlZmluZWRcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgaWQ6IHN0cmluZyA9IGpzb25PYmplY3QuaWQ7XHJcbiAgICAgICAgbGV0IHNoYXBlOiBTaGFwZSA9IENvbW1vbi5jb252ZXJ0U2hhcGUoanNvbk9iamVjdC5zaGFwZSk7XHJcbiAgICAgICAgbGV0IG1vZGVsTmFtZTogc3RyaW5nID0ganNvbk9iamVjdC5tb2RlbE5hbWU7XHJcbiAgICAgICAgbGV0IG1vZGVsVGV4dHVyZTogc3RyaW5nID0ganNvbk9iamVjdC5tb2RlbFRleHR1cmU7XHJcbiAgICAgICAgbGV0IG93bmVyVXNlcklkOiBzdHJpbmcgPSBqc29uT2JqZWN0Lm93bmVyVXNlcklkO1xyXG4gICAgICAgIGxldCB3b3JsZE5hbWU6IHN0cmluZyA9IGpzb25PYmplY3Qud29ybGROYW1lO1xyXG4gICAgICAgIGxldCBkaXNwbGF5TmFtZTogc3RyaW5nID0ganNvbk9iamVjdC5kaXNwbGF5TmFtZTtcclxuXHJcbiAgICAgICAgbGV0IG9ialR5cGU6IE9iamVjdE1vZGVsVHlwZSA9IE9iamVjdE1vZGVsVHlwZS5QYXNzYWJsZTtcclxuICAgICAgICBpZiAoanNvbk9iamVjdC5vYmplY3RUeXBlKSB7XHJcbiAgICAgICAgICAgIGxldCBvYmpUeXBlQXNJbnQ6IG51bWJlciA9IHBhcnNlSW50KGpzb25PYmplY3Qub2JqZWN0VHlwZSwgMTApO1xyXG4gICAgICAgICAgICBvYmpUeXBlID0gb2JqVHlwZUFzSW50O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IG9iak1vZGVsID0gbmV3IE9iamVjdE1vZGVsKGlkLCBzaGFwZSwgbW9kZWxOYW1lKTtcclxuICAgICAgICBvYmpNb2RlbC5tb2RlbFRleHR1cmUgPSBtb2RlbFRleHR1cmU7XHJcbiAgICAgICAgb2JqTW9kZWwuc2hhcGUgPSBzaGFwZTtcclxuICAgICAgICBvYmpNb2RlbC5vd25lclVzZXJJZCA9IG93bmVyVXNlcklkO1xyXG4gICAgICAgIG9iak1vZGVsLm9iamVjdFR5cGUgPSBvYmpUeXBlO1xyXG4gICAgICAgIG9iak1vZGVsLndvcmxkTmFtZSA9IHdvcmxkTmFtZTtcclxuICAgICAgICBvYmpNb2RlbC5kaXNwbGF5TmFtZSA9IGRpc3BsYXlOYW1lO1xyXG4gICAgICAgIHJldHVybiBvYmpNb2RlbDtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVjayBpZiB0aGlzIGpzb24gb2JqZWN0IGhhcyB0aGUgY29ycmVjdCBvYmplY3QgbW9kZWwgdHlwZS5cclxuICAgICAqIEBwYXJhbSBqc29uT2JqZWN0XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgaXNDb3JyZWN0VHlwZShqc29uT2JqZWN0OiBhbnkpIHtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3QudHlwZSAhPT0gT2JqZWN0TW9kZWwuVFlQRSApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihpZDogc3RyaW5nLCBzaGFwZTogU2hhcGUsIG1vZGVsTmFtZTogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKCBpZCAmJiBtb2RlbE5hbWUgJiYgc2hhcGUgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2lkID0gaWQ7XHJcbiAgICAgICAgICAgIHRoaXMuX3NoYXBlID0gc2hhcGU7XHJcbiAgICAgICAgICAgIHRoaXMuX21vZGVsTmFtZSA9IG1vZGVsTmFtZTtcclxuICAgICAgICAgICAgdGhpcy5fb2JqZWN0VHlwZSA9IE9iamVjdE1vZGVsVHlwZS5QYXNzYWJsZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgTWlzc2luZ0NvbnN0cnVjdG9yQXJndW1lbnRFcnJvcihcIk1pc3Npbmc6IGlkPVwiICsgaWQgKyBcIiBzaGFwZT1cIiArIHNoYXBlICsgXCIgbW9kZWxOYW1lPVwiICsgbW9kZWxOYW1lKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIHVuaXF1ZSBpZCBvZiB0aGlzIG1vZGVsLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBpZCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pZDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEJvdW5kaW5nIHNoYXBlIG9mIHRoaXMgbW9kZWwuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzZXQgc2hhcGUoc2hhcGU6IFNoYXBlKSB7XHJcbiAgICAgICAgdGhpcy5fc2hhcGUgPSBzaGFwZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEJvdW5kaW5nIHNoYXBlIG9mIHRoaSBtb2RlbC5cclxuICAgICAqIEByZXR1cm5zIHtTaGFwZX1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBzaGFwZSgpOiBTaGFwZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NoYXBlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhlIG5hbWUgb2YgdGhlIHdvcmxkIHRoZSBvYmplY3QgbW9kZWwgaXMgaW4uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzZXQgd29ybGROYW1lKCB2YWx1ZTogc3RyaW5nICkge1xyXG4gICAgICAgIHRoaXMuX3dvcmxkTmFtZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhlIG5hbWUgb2YgdGhlIHdvcmxkIHRoZSBvYmplY3QgbW9kZWwgaXMgaW4uXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0IHdvcmxkTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl93b3JsZE5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIGRpc3RpbmN0IG5hbWUgb2YgdGhlIG9iamVjdCBtb2RlbC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldCBkaXNwbGF5TmFtZSggdmFsdWU6IHN0cmluZyApIHtcclxuICAgICAgICB0aGlzLl9kaXNwbGF5TmFtZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQSBkaXN0aW5jdCBuYW1lIG9mIHRoZSBvYmplY3QgbW9kZWwuXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0IGRpc3BsYXlOYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2Rpc3BsYXlOYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhlIG5hbWUgb2YgdGhlIG1vZGVsLiBUaGlzIGlzIHRoZSAzZCBmaWxlIHVzZWQgYnkgdGhpcyBtb2RlbC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldCBtb2RlbE5hbWUoIHZhbHVlOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgdGhpcy5fbW9kZWxOYW1lID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgbmFtZSBvZiB0aGUgbW9kZWwuIFRoaXMgaXMgdGhlIDNkIGZpbGUgdXNlZCBieSB0aGlzIG1vZGVsLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBtb2RlbE5hbWUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbW9kZWxOYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSB0ZXh0dXJlIG9mIHRoZSBtb2RlbC4gKE9wdGlvbmFsKVxyXG4gICAgICogSWYgbm8gdGV4dHVyZSBzcGVjaWZpZWQgdGhlbiBlbmdpbmUgd2lsbCB1c2UgdGhlIGRlZmF1bHQgdGV4dHVyZSBhc3NpZ25lZCBpbiB0aGUgbW9kZWwuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldCBtb2RlbFRleHR1cmUodmFsdWU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX21vZGVsVGV4dHVyZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSB0ZXh0dXJlIG9mIHRoZSBtb2RlbC4gKE9wdGlvbmFsKVxyXG4gICAgICogSWYgbm8gdGV4dHVyZSBzcGVjaWZpZWQgdGhlbiBlbmdpbmUgd2lsbCB1c2UgdGhlIGRlZmF1bHQgdGV4dHVyZSBhc3NpZ25lZCBpbiB0aGUgbW9kZWwuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBtb2RlbFRleHR1cmUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21vZGVsVGV4dHVyZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCB0aGUgdXNlciBpZCBvZiB0aGUgb3duZXIgb2YgdGhpcyBvYmplY3QuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldCBvd25lclVzZXJJZCh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fb3duZXJVc2VySWQgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgdXNlciBpZCBvZiB0aGUgb3duZXIgb2YgdGhpcyBvYmplY3QuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBvd25lclVzZXJJZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fb3duZXJVc2VySWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGlzIG9iamVjdCBjYW4gYmUgZWl0aGVyIHBhc3NhYmxlLCBjbGltYmFibGUgb3IgaW1wYXNzYWJsZS4gVGhlIHR5cGVcclxuICAgICAqIGRldGVybWluZXMgaG93IG90aGVyIG9iamVjdHMgYmVoYXZlIGFuZCBpbnRlcmFjdCBhcm91bmQgdGhpcyBvbmUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzZXQgb2JqZWN0VHlwZSh2YWx1ZTogT2JqZWN0TW9kZWxUeXBlKSB7XHJcbiAgICAgICAgdGhpcy5fb2JqZWN0VHlwZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhpcyBvYmplY3QgY2FuIGJlIGVpdGhlciBwYXNzYWJsZSwgY2xpbWJhYmxlIG9yIGltcGFzc2FibGUuIFRoZSB0eXBlXHJcbiAgICAgKiBkZXRlcm1pbmVzIGhvdyBvdGhlciBvYmplY3RzIGJlaGF2ZSBhbmQgaW50ZXJhY3QgYXJvdW5kIHRoaXMgb25lLlxyXG4gICAgICogQHJldHVybnMge09iamVjdE1vZGVsVHlwZX1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBvYmplY3RUeXBlKCk6IE9iamVjdE1vZGVsVHlwZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX29iamVjdFR5cGU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIDItZGltZW5zaW9uYWwgc2VjdGlvbiBjb29yZGluYXRlIGxvY2F0aW9uIG9mIHRoaXMgb2JqZWN0IG1vZGVsLlxyXG4gICAgICogQXQgdGhpcyBwb2ludCB0aGUgc2VjdGlvbiBjb29yZGluYXRlIGlzIGp1c3QgdGhlIGNvb3JkaW5hdGUgb2YgdGhlIHNoYXBlXHJcbiAgICAgKiBkaXZpZGVkIGJ5IGEgY29uc3RhbnQgbnVtYmVyLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0U2VjdGlvbkNvb3JkaW5hdGUoKTogUG9pbnQge1xyXG4gICAgICAgIGxldCBzaGFwZTogU2hhcGUgPSB0aGlzLl9zaGFwZTtcclxuICAgICAgICBsZXQgcG9zaXRpb246IFBvaW50ID0gc2hhcGUuZ2V0UG9zaXRpb24oKTtcclxuICAgICAgICByZXR1cm4gcG9zaXRpb24uZ2V0U2VjdGlvbkNvb3JkaW5hdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgdGhpcyBvYmplY3QgbW9kZWwgdG8gSlNPTi5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHRvSlNPTigpOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0gPSB7fTtcclxuICAgICAgICByZXN1bHQuaWQgPSB0aGlzLl9pZDtcclxuICAgICAgICByZXN1bHQuc2hhcGUgPSB0aGlzLnNoYXBlLnRvSlNPTigpO1xyXG4gICAgICAgIHJlc3VsdC5tb2RlbE5hbWUgPSB0aGlzLl9tb2RlbE5hbWU7XHJcbiAgICAgICAgcmVzdWx0Lm1vZGVsVGV4dHVyZSA9IHRoaXMuX21vZGVsVGV4dHVyZTtcclxuICAgICAgICByZXN1bHQub3duZXJVc2VySWQgPSB0aGlzLl9vd25lclVzZXJJZDtcclxuICAgICAgICByZXN1bHQudHlwZSA9IE9iamVjdE1vZGVsLlRZUEU7XHJcbiAgICAgICAgcmVzdWx0Lm9iamVjdFR5cGUgPSB0aGlzLl9vYmplY3RUeXBlO1xyXG4gICAgICAgIHJlc3VsdC53b3JsZE5hbWUgPSB0aGlzLl93b3JsZE5hbWU7XHJcbiAgICAgICAgcmVzdWx0LmRpc3BsYXlOYW1lID0gdGhpcy5fZGlzcGxheU5hbWU7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFRoaXMgZGV0ZXJtaW5lcyBob3cgYW4gYXZhdGFyIGNhbiBpbnRlcmFjdCB3aXRoIGFuIG9iamVjdCBtb2RlbCBpbiB0aGUgd29ybGQuXHJcbiAqL1xyXG5leHBvcnQgZW51bSBPYmplY3RNb2RlbFR5cGUge1xyXG4gICAgUGFzc2FibGUgPSAxLFxyXG4gICAgSW1wYXNzYWJsZSA9IDIsXHJcbiAgICBDbGltYmFibGUgPSAzXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE9iamVjdE1vZGVsO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
