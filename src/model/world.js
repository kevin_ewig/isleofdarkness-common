"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var objectmodel_1 = require("./objectmodel");
var intersecter_1 = require("../math/intersecter");
var usermetadata_1 = require("./usermetadata");
var animatedcharacter_1 = require("./animatedcharacter");
var World = /** @class */ (function () {
    function World() {
        this._idDictionary = {};
        this._users = [];
    }
    /**
     * Convert JSON object to a World object.
     * @param jsonObject
     */
    World.fromJSON = function (jsonObject) {
        var world;
        if (jsonObject.objectModels && jsonObject.users) {
            world = new World();
            for (var index = 0; index < jsonObject.objectModels.length; index++) {
                var objModelJson = jsonObject.objectModels[index];
                if (objectmodel_1.default.isCorrectType(objModelJson)) {
                    var objModel = objectmodel_1.default.fromJSON(objModelJson);
                    world.addObjectModel(objModel);
                }
                else if (animatedcharacter_1.AnimatedCharacter.isCorrectType(objModelJson)) {
                    var objModel = animatedcharacter_1.AnimatedCharacter.fromJSON(objModelJson);
                    world.addObjectModel(objModel);
                }
            }
            for (var index = 0; index < jsonObject.users.length; index++) {
                var userJson = jsonObject.users[index];
                var usermeta = usermetadata_1.default.fromJSON(userJson);
                world.addUserMetaData(usermeta);
            }
        }
        else {
            throw new Error("Invalid JSON");
        }
        return world;
    };
    /**
     * Add an object model to this world.
     * @param objModel The object model.
     */
    World.prototype.addObjectModel = function (objModel) {
        var id = objModel.id;
        this._idDictionary[id] = objModel;
    };
    /**
     * Add a user to the world.
     */
    World.prototype.addUserMetaData = function (userobject) {
        this._users.push(userobject);
    };
    /**
     * Get all items that intersect a given shape.
     * @param shape The shape shape used to filter out object model intersection.
     */
    World.prototype.getAllIntersection = function (shape) {
        // List of all objects that intersect with the given shape.
        var resultList = new Array();
        for (var key in this._idDictionary) {
            if (key) {
                var tempObjectModel = this._idDictionary[key];
                // This determines if we have intersected on the x-y plane. Now check if we can intersect the
                // object in the z-plane.
                var intersect = intersecter_1.default.isShapeIntersectShape(shape, tempObjectModel.shape);
                if (intersect) {
                    resultList.push(tempObjectModel);
                }
            }
        }
        return resultList;
    };
    /**
     * List all object models in the world. Used primarily for debugging and generating the JSON file.
     */
    World.prototype.getAllObjectModels = function () {
        var array = new Array();
        for (var key in this._idDictionary) {
            if (this._idDictionary.hasOwnProperty(key)) {
                var objectModel = this._idDictionary[key];
                array.push(objectModel);
            }
        }
        return array;
    };
    /**
     * List all users in the world. Used primarily for debugging and generating the JSON file.
     */
    World.prototype.getAllUserMetaData = function () {
        return this._users;
    };
    /**
     * Get object model by id.
     * @param id
     */
    World.prototype.getObjectModel = function (id) {
        return this._idDictionary[id];
    };
    /**
     * Remove an object model to this world.
     * @param id The object model id.
     */
    World.prototype.removeObjectModel = function (id) {
        var objModel = this._idDictionary[id];
        // Remove the object from the id lookup.
        delete this._idDictionary[id];
    };
    /**
     * Convert this world into JSON.
     */
    World.prototype.toJSON = function () {
        var arrayOfUsers = [];
        // Convert the users.
        for (var index = 0; index < this._users.length; index++) {
            var objectModelJson = this._users[index].toJSON();
            arrayOfUsers.push(objectModelJson);
        }
        // Convert the object models.
        var arrayOfObjectModels = this.getAllObjectModels();
        var array = [];
        for (var index = 0; index < arrayOfObjectModels.length; index++) {
            var objectModelJson = arrayOfObjectModels[index].toJSON();
            array.push(objectModelJson);
        }
        return {
            "objectModels": array,
            "users": arrayOfUsers
        };
    };
    return World;
}());
exports.World = World;
exports.default = World;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL3dvcmxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsNkNBQXdDO0FBRXhDLG1EQUE4QztBQUM5QywrQ0FBMEM7QUFDMUMseURBQXdEO0FBRXhEO0lBdUNJO1FBQ0ksSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQXJDRDs7O09BR0c7SUFDVyxjQUFRLEdBQXRCLFVBQXVCLFVBQWU7UUFFbEMsSUFBSSxLQUFZLENBQUM7UUFFakIsRUFBRSxDQUFDLENBQUUsVUFBVSxDQUFDLFlBQVksSUFBSSxVQUFVLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQztZQUNoRCxLQUFLLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztZQUNwQixHQUFHLENBQUMsQ0FBRSxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFHLENBQUM7Z0JBQ3BFLElBQUksWUFBWSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2xELEVBQUUsQ0FBQyxDQUFFLHFCQUFXLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBRSxDQUFDLENBQUMsQ0FBQztvQkFDNUMsSUFBSSxRQUFRLEdBQUcscUJBQVcsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ2xELEtBQUssQ0FBQyxjQUFjLENBQUUsUUFBUSxDQUFFLENBQUM7Z0JBQ3JDLENBQUM7Z0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLHFDQUFpQixDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3pELElBQUksUUFBUSxHQUFHLHFDQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDeEQsS0FBSyxDQUFDLGNBQWMsQ0FBRSxRQUFRLENBQUUsQ0FBQztnQkFDckMsQ0FBQztZQUNMLENBQUM7WUFFRCxHQUFHLENBQUMsQ0FBRSxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFHLENBQUM7Z0JBQzdELElBQUksUUFBUSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3ZDLElBQUksUUFBUSxHQUFHLHNCQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMvQyxLQUFLLENBQUMsZUFBZSxDQUFFLFFBQVEsQ0FBRSxDQUFDO1lBQ3RDLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7UUFFRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBRWpCLENBQUM7SUFPRDs7O09BR0c7SUFDSSw4QkFBYyxHQUFyQixVQUF1QixRQUFxQjtRQUN4QyxJQUFJLEVBQUUsR0FBRyxRQUFRLENBQUMsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDO0lBQ3RDLENBQUM7SUFFRDs7T0FFRztJQUNJLCtCQUFlLEdBQXRCLFVBQXdCLFVBQXdCO1FBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFFLFVBQVUsQ0FBRSxDQUFDO0lBQ25DLENBQUM7SUFFRDs7O09BR0c7SUFDSSxrQ0FBa0IsR0FBekIsVUFBMkIsS0FBWTtRQUVuQywyREFBMkQ7UUFDM0QsSUFBSSxVQUFVLEdBQUcsSUFBSSxLQUFLLEVBQWUsQ0FBQztRQUUxQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUNqQyxFQUFFLENBQUMsQ0FBRSxHQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNSLElBQUksZUFBZSxHQUFnQixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUUzRCw2RkFBNkY7Z0JBQzdGLHlCQUF5QjtnQkFDekIsSUFBSSxTQUFTLEdBQUcscUJBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoRixFQUFFLENBQUMsQ0FBRSxTQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNkLFVBQVUsQ0FBQyxJQUFJLENBQUUsZUFBZSxDQUFFLENBQUM7Z0JBQ3ZDLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUVELE1BQU0sQ0FBQyxVQUFVLENBQUM7SUFFdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksa0NBQWtCLEdBQXpCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxLQUFLLEVBQWUsQ0FBQztRQUNyQyxHQUFHLENBQUMsQ0FBRSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsYUFBYyxDQUFDLENBQUMsQ0FBQztZQUNuQyxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDNUIsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRDs7T0FFRztJQUNJLGtDQUFrQixHQUF6QjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7O09BR0c7SUFDSSw4QkFBYyxHQUFyQixVQUFzQixFQUFVO1FBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRDs7O09BR0c7SUFDSSxpQ0FBaUIsR0FBeEIsVUFBMEIsRUFBVTtRQUVoQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRXRDLHdDQUF3QztRQUN4QyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7SUFFbEMsQ0FBQztJQUVEOztPQUVHO0lBQ0ksc0JBQU0sR0FBYjtRQUVJLElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUV0QixxQkFBcUI7UUFDckIsR0FBRyxDQUFDLENBQUUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRyxDQUFDO1lBQ3hELElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDbEQsWUFBWSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBRUQsNkJBQTZCO1FBQzdCLElBQUksbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDcEQsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBRWYsR0FBRyxDQUFDLENBQUUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUcsQ0FBQztZQUNoRSxJQUFJLGVBQWUsR0FBRyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUMxRCxLQUFLLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ2hDLENBQUM7UUFFRCxNQUFNLENBQUM7WUFDSCxjQUFjLEVBQUcsS0FBSztZQUN0QixPQUFPLEVBQUUsWUFBWTtTQUN4QixDQUFDO0lBRU4sQ0FBQztJQUVMLFlBQUM7QUFBRCxDQTdKQSxBQTZKQyxJQUFBO0FBN0pZLHNCQUFLO0FBK0psQixrQkFBZSxLQUFLLENBQUMiLCJmaWxlIjoibW9kZWwvd29ybGQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgT2JqZWN0TW9kZWwgZnJvbSBcIi4vb2JqZWN0bW9kZWxcIjtcclxuaW1wb3J0IFNoYXBlIGZyb20gXCIuLi9tYXRoL3NoYXBlXCI7XHJcbmltcG9ydCBJbnRlcnNlY3RlciBmcm9tIFwiLi4vbWF0aC9pbnRlcnNlY3RlclwiO1xyXG5pbXBvcnQgVXNlck1ldGFEYXRhIGZyb20gXCIuL3VzZXJtZXRhZGF0YVwiO1xyXG5pbXBvcnQgeyBBbmltYXRlZENoYXJhY3RlciB9IGZyb20gXCIuL2FuaW1hdGVkY2hhcmFjdGVyXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgV29ybGQge1xyXG5cclxuICAgIHByaXZhdGUgX2lkRGljdGlvbmFyeTogeyBbaWQ6IHN0cmluZ106IE9iamVjdE1vZGVsIH07XHJcbiAgICBwcml2YXRlIF91c2VyczogQXJyYXk8VXNlck1ldGFEYXRhPjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgSlNPTiBvYmplY3QgdG8gYSBXb3JsZCBvYmplY3QuXHJcbiAgICAgKiBAcGFyYW0ganNvbk9iamVjdFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSk6IFdvcmxkIHtcclxuXHJcbiAgICAgICAgbGV0IHdvcmxkOiBXb3JsZDtcclxuXHJcbiAgICAgICAgaWYgKCBqc29uT2JqZWN0Lm9iamVjdE1vZGVscyAmJiBqc29uT2JqZWN0LnVzZXJzICkge1xyXG4gICAgICAgICAgICB3b3JsZCA9IG5ldyBXb3JsZCgpO1xyXG4gICAgICAgICAgICBmb3IgKCBsZXQgaW5kZXggPSAwOyBpbmRleCA8IGpzb25PYmplY3Qub2JqZWN0TW9kZWxzLmxlbmd0aDsgaW5kZXgrKyApIHtcclxuICAgICAgICAgICAgICAgIGxldCBvYmpNb2RlbEpzb24gPSBqc29uT2JqZWN0Lm9iamVjdE1vZGVsc1tpbmRleF07XHJcbiAgICAgICAgICAgICAgICBpZiAoIE9iamVjdE1vZGVsLmlzQ29ycmVjdFR5cGUob2JqTW9kZWxKc29uKSApIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgb2JqTW9kZWwgPSBPYmplY3RNb2RlbC5mcm9tSlNPTihvYmpNb2RlbEpzb24pO1xyXG4gICAgICAgICAgICAgICAgICAgIHdvcmxkLmFkZE9iamVjdE1vZGVsKCBvYmpNb2RlbCApO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICggQW5pbWF0ZWRDaGFyYWN0ZXIuaXNDb3JyZWN0VHlwZShvYmpNb2RlbEpzb24pICkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBvYmpNb2RlbCA9IEFuaW1hdGVkQ2hhcmFjdGVyLmZyb21KU09OKG9iak1vZGVsSnNvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgd29ybGQuYWRkT2JqZWN0TW9kZWwoIG9iak1vZGVsICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZvciAoIGxldCBpbmRleCA9IDA7IGluZGV4IDwganNvbk9iamVjdC51c2Vycy5sZW5ndGg7IGluZGV4KysgKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdXNlckpzb24gPSBqc29uT2JqZWN0LnVzZXJzW2luZGV4XTtcclxuICAgICAgICAgICAgICAgIGxldCB1c2VybWV0YSA9IFVzZXJNZXRhRGF0YS5mcm9tSlNPTih1c2VySnNvbik7XHJcbiAgICAgICAgICAgICAgICB3b3JsZC5hZGRVc2VyTWV0YURhdGEoIHVzZXJtZXRhICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIEpTT05cIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gd29ybGQ7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuX2lkRGljdGlvbmFyeSA9IHt9O1xyXG4gICAgICAgIHRoaXMuX3VzZXJzID0gW107XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBZGQgYW4gb2JqZWN0IG1vZGVsIHRvIHRoaXMgd29ybGQuXHJcbiAgICAgKiBAcGFyYW0gb2JqTW9kZWwgVGhlIG9iamVjdCBtb2RlbC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGFkZE9iamVjdE1vZGVsKCBvYmpNb2RlbDogT2JqZWN0TW9kZWwgKSB7XHJcbiAgICAgICAgbGV0IGlkID0gb2JqTW9kZWwuaWQ7XHJcbiAgICAgICAgdGhpcy5faWREaWN0aW9uYXJ5W2lkXSA9IG9iak1vZGVsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIGEgdXNlciB0byB0aGUgd29ybGQuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhZGRVc2VyTWV0YURhdGEoIHVzZXJvYmplY3Q6IFVzZXJNZXRhRGF0YSApIHtcclxuICAgICAgICB0aGlzLl91c2Vycy5wdXNoKCB1c2Vyb2JqZWN0ICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgYWxsIGl0ZW1zIHRoYXQgaW50ZXJzZWN0IGEgZ2l2ZW4gc2hhcGUuXHJcbiAgICAgKiBAcGFyYW0gc2hhcGUgVGhlIHNoYXBlIHNoYXBlIHVzZWQgdG8gZmlsdGVyIG91dCBvYmplY3QgbW9kZWwgaW50ZXJzZWN0aW9uLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0QWxsSW50ZXJzZWN0aW9uKCBzaGFwZTogU2hhcGUgKTogT2JqZWN0TW9kZWxbXSB7XHJcblxyXG4gICAgICAgIC8vIExpc3Qgb2YgYWxsIG9iamVjdHMgdGhhdCBpbnRlcnNlY3Qgd2l0aCB0aGUgZ2l2ZW4gc2hhcGUuXHJcbiAgICAgICAgbGV0IHJlc3VsdExpc3QgPSBuZXcgQXJyYXk8T2JqZWN0TW9kZWw+KCk7XHJcblxyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLl9pZERpY3Rpb25hcnkpIHtcclxuICAgICAgICAgICAgaWYgKCBrZXkgKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdGVtcE9iamVjdE1vZGVsOiBPYmplY3RNb2RlbCA9IHRoaXMuX2lkRGljdGlvbmFyeVtrZXldO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFRoaXMgZGV0ZXJtaW5lcyBpZiB3ZSBoYXZlIGludGVyc2VjdGVkIG9uIHRoZSB4LXkgcGxhbmUuIE5vdyBjaGVjayBpZiB3ZSBjYW4gaW50ZXJzZWN0IHRoZVxyXG4gICAgICAgICAgICAgICAgLy8gb2JqZWN0IGluIHRoZSB6LXBsYW5lLlxyXG4gICAgICAgICAgICAgICAgbGV0IGludGVyc2VjdCA9IEludGVyc2VjdGVyLmlzU2hhcGVJbnRlcnNlY3RTaGFwZShzaGFwZSwgdGVtcE9iamVjdE1vZGVsLnNoYXBlKTtcclxuICAgICAgICAgICAgICAgIGlmICggaW50ZXJzZWN0ICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdExpc3QucHVzaCggdGVtcE9iamVjdE1vZGVsICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByZXN1bHRMaXN0O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExpc3QgYWxsIG9iamVjdCBtb2RlbHMgaW4gdGhlIHdvcmxkLiBVc2VkIHByaW1hcmlseSBmb3IgZGVidWdnaW5nIGFuZCBnZW5lcmF0aW5nIHRoZSBKU09OIGZpbGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRBbGxPYmplY3RNb2RlbHMoKTogT2JqZWN0TW9kZWxbXSB7XHJcbiAgICAgICAgbGV0IGFycmF5ID0gbmV3IEFycmF5PE9iamVjdE1vZGVsPigpO1xyXG4gICAgICAgIGZvciAoIGxldCBrZXkgaW4gdGhpcy5faWREaWN0aW9uYXJ5ICkge1xyXG4gICAgICAgICAgICBpZiAoIHRoaXMuX2lkRGljdGlvbmFyeS5oYXNPd25Qcm9wZXJ0eShrZXkpICkge1xyXG4gICAgICAgICAgICAgICAgbGV0IG9iamVjdE1vZGVsID0gdGhpcy5faWREaWN0aW9uYXJ5W2tleV07XHJcbiAgICAgICAgICAgICAgICBhcnJheS5wdXNoKG9iamVjdE1vZGVsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gYXJyYXk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMaXN0IGFsbCB1c2VycyBpbiB0aGUgd29ybGQuIFVzZWQgcHJpbWFyaWx5IGZvciBkZWJ1Z2dpbmcgYW5kIGdlbmVyYXRpbmcgdGhlIEpTT04gZmlsZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldEFsbFVzZXJNZXRhRGF0YSgpOiBVc2VyTWV0YURhdGFbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IG9iamVjdCBtb2RlbCBieSBpZC5cclxuICAgICAqIEBwYXJhbSBpZFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0T2JqZWN0TW9kZWwoaWQ6IHN0cmluZyk6IE9iamVjdE1vZGVsIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faWREaWN0aW9uYXJ5W2lkXTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZSBhbiBvYmplY3QgbW9kZWwgdG8gdGhpcyB3b3JsZC5cclxuICAgICAqIEBwYXJhbSBpZCBUaGUgb2JqZWN0IG1vZGVsIGlkLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcmVtb3ZlT2JqZWN0TW9kZWwoIGlkOiBzdHJpbmcgKSB7XHJcblxyXG4gICAgICAgIGxldCBvYmpNb2RlbCA9IHRoaXMuX2lkRGljdGlvbmFyeVtpZF07XHJcblxyXG4gICAgICAgIC8vIFJlbW92ZSB0aGUgb2JqZWN0IGZyb20gdGhlIGlkIGxvb2t1cC5cclxuICAgICAgICBkZWxldGUgdGhpcy5faWREaWN0aW9uYXJ5W2lkXTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgd29ybGQgaW50byBKU09OLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdG9KU09OKCkge1xyXG5cclxuICAgICAgICBsZXQgYXJyYXlPZlVzZXJzID0gW107XHJcblxyXG4gICAgICAgIC8vIENvbnZlcnQgdGhlIHVzZXJzLlxyXG4gICAgICAgIGZvciAoIGxldCBpbmRleCA9IDA7IGluZGV4IDwgdGhpcy5fdXNlcnMubGVuZ3RoOyBpbmRleCsrICkge1xyXG4gICAgICAgICAgICBsZXQgb2JqZWN0TW9kZWxKc29uID0gdGhpcy5fdXNlcnNbaW5kZXhdLnRvSlNPTigpO1xyXG4gICAgICAgICAgICBhcnJheU9mVXNlcnMucHVzaChvYmplY3RNb2RlbEpzb24pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQ29udmVydCB0aGUgb2JqZWN0IG1vZGVscy5cclxuICAgICAgICBsZXQgYXJyYXlPZk9iamVjdE1vZGVscyA9IHRoaXMuZ2V0QWxsT2JqZWN0TW9kZWxzKCk7XHJcbiAgICAgICAgbGV0IGFycmF5ID0gW107XHJcblxyXG4gICAgICAgIGZvciAoIGxldCBpbmRleCA9IDA7IGluZGV4IDwgYXJyYXlPZk9iamVjdE1vZGVscy5sZW5ndGg7IGluZGV4KysgKSB7XHJcbiAgICAgICAgICAgIGxldCBvYmplY3RNb2RlbEpzb24gPSBhcnJheU9mT2JqZWN0TW9kZWxzW2luZGV4XS50b0pTT04oKTtcclxuICAgICAgICAgICAgYXJyYXkucHVzaChvYmplY3RNb2RlbEpzb24pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgXCJvYmplY3RNb2RlbHNcIiA6IGFycmF5LFxyXG4gICAgICAgICAgICBcInVzZXJzXCI6IGFycmF5T2ZVc2Vyc1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgV29ybGQ7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
