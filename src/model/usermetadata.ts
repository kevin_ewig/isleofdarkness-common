import Point from "../math/point";

export class UserMetaData {

    private _userId: string;
    private _userName: string;
    private _sectionCoordinate: Point;

    /**
     * Convert JSON object to a UserMetaData object.
     * @param jsonObject
     */
    public static fromJSON(jsonObject: any): UserMetaData {

        if ( !jsonObject ) {
            throw new Error("Cannot convert undefined");
        }
        if ( !jsonObject.hasOwnProperty("userId") ) {
            throw new Error("No userId property");
        }
        if ( !jsonObject.hasOwnProperty("userName") ) {
            throw new Error("No userName property");
        }

        let usermetadata = new UserMetaData(jsonObject.userId, jsonObject.userName);
        if ( jsonObject.sectionCoordinate ) {
            usermetadata.sectionCoordinate = Point.fromJSON(jsonObject.sectionCoorindate);
        }


        return usermetadata;

    }

    constructor( userId: string, userName: string ) {
        this._userId = userId;
        this._userName = userName;
    }

    get userId() {
        return this._userId;
    }

    get userName() {
        return this._userName;
    }

    get sectionCoordinate() {
        return this._sectionCoordinate;
    }

    set sectionCoordinate( point: Point ) {
        this._sectionCoordinate = point;
    }

    /**
     * Convert user object into JSON.
     */
    public toJSON(): { [key: string]: any; } {
        let json: { [key: string]: any; } = {};
        json.userId = this._userId;
        json.userName = this._userName;
        if ( this._sectionCoordinate ) {
            json.sectionCoordinate = this._sectionCoordinate.toJSON();
        }
        return json;
    }

}

export default UserMetaData;
