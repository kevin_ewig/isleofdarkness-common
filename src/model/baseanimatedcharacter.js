"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("../math/common");
var animationprofile_1 = require("./animationprofile");
var missingconstructorargumenterror_1 = require("../error/missingconstructorargumenterror");
/**
 * A base profile for a specific character (avatar). This is used to provide a default
 * template for a user to select. This character has no owner. This is not to be used
 * or added to the world.
 */
var BaseAnimatedCharacter = /** @class */ (function () {
    function BaseAnimatedCharacter(id, shape, modelName, animationProfile) {
        if (id && modelName && shape && animationProfile) {
            this._id = id;
            this._modelName = modelName;
            this._shape = shape;
            this._animationProfile = animationProfile;
        }
        else {
            throw new missingconstructorargumenterror_1.MissingConstructorArgumentError("id=" + id + " shape=" + shape +
                " modelname=" + modelName + " animationprofile=" + animationProfile);
        }
    }
    /**
     * Convert JSON object to Object Model object.
     */
    BaseAnimatedCharacter.fromJSON = function (jsonObject) {
        if (!jsonObject) {
            throw new Error("Cannot convert undefined");
        }
        var type = jsonObject.type;
        if (type !== BaseAnimatedCharacter.TYPE) {
            throw new Error("Cannot convert. Invalid type.");
        }
        var id = jsonObject.id;
        var shape = common_1.Common.convertShape(jsonObject.shape);
        var modelName = jsonObject.modelName;
        var modelTexture = jsonObject.modelTexture;
        var animationProfile = animationprofile_1.default.fromJSON(jsonObject.animationprofile);
        var speed = 0;
        if (jsonObject.speed) {
            speed = parseInt(jsonObject.speed, 10);
        }
        var charProfile = new BaseAnimatedCharacter(id, shape, modelName, animationProfile);
        charProfile.modelTexture = modelTexture;
        charProfile.speed = speed;
        return charProfile;
    };
    Object.defineProperty(BaseAnimatedCharacter.prototype, "id", {
        /**
         * A unique id of this model.
         * @returns {string}
         */
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseAnimatedCharacter.prototype, "shape", {
        /**
         * Bounding shape of thi model.
         * @returns {Shape}
         */
        get: function () {
            return this._shape;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseAnimatedCharacter.prototype, "speed", {
        /**
         * Get how fast this model will move.
         * @param value
         */
        get: function () {
            return this._speed;
        },
        /**
         * Set how fast this model will move.
         * @param value
         */
        set: function (value) {
            this._speed = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseAnimatedCharacter.prototype, "modelName", {
        /**
         * The name of the model. This is the 3d file used by this model.
         * @returns {string}
         */
        get: function () {
            return this._modelName;
        },
        /**
         * The name of the model. This is the 3d file used by this model.
         * @returns {string}
         */
        set: function (value) {
            this._modelName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseAnimatedCharacter.prototype, "modelTexture", {
        /**
         * Get the texture of the model. (Optional)
         * If no texture specified then engine will use the default texture assigned in the model.
         * @param value
         */
        get: function () {
            return this._modelTexture;
        },
        /**
         * Set the texture of the model. (Optional)
         * If no texture specified then engine will use the default texture assigned in the model.
         * @param value
         */
        set: function (value) {
            this._modelTexture = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseAnimatedCharacter.prototype, "animationProfile", {
        /**
         * Return the animation profile used to animate this character.
         */
        get: function () {
            return this._animationProfile;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Convert this object model to JSON.
     */
    BaseAnimatedCharacter.prototype.toJSON = function () {
        var result = {};
        result.id = this._id;
        result.shape = this.shape.toJSON();
        result.modelName = this._modelName;
        result.modelTexture = this._modelTexture;
        result.speed = this._speed;
        result.animationprofile = this._animationProfile.toJSON();
        result.type = BaseAnimatedCharacter.TYPE;
        return result;
    };
    BaseAnimatedCharacter.TYPE = "baseanimatedcharacter";
    return BaseAnimatedCharacter;
}());
exports.BaseAnimatedCharacter = BaseAnimatedCharacter;
exports.default = BaseAnimatedCharacter;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL2Jhc2VhbmltYXRlZGNoYXJhY3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHlDQUFzQztBQUN0Qyx1REFBa0Q7QUFDbEQsNEZBQTJGO0FBRTNGOzs7O0dBSUc7QUFDSDtJQTJDSSwrQkFBWSxFQUFVLEVBQUUsS0FBWSxFQUFFLFNBQWlCLEVBQUUsZ0JBQWtDO1FBQ3ZGLEVBQUUsQ0FBQyxDQUFFLEVBQUUsSUFBSSxTQUFTLElBQUksS0FBSyxJQUFJLGdCQUFpQixDQUFDLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztZQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1lBQzVCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxnQkFBZ0IsQ0FBQztRQUM5QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLElBQUksaUVBQStCLENBQUMsS0FBSyxHQUFHLEVBQUUsR0FBRyxTQUFTLEdBQUcsS0FBSztnQkFDeEUsYUFBYSxHQUFHLFNBQVMsR0FBRyxvQkFBb0IsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3pFLENBQUM7SUFDTCxDQUFDO0lBMUNEOztPQUVHO0lBQ1csOEJBQVEsR0FBdEIsVUFBdUIsVUFBZTtRQUVsQyxFQUFFLENBQUMsQ0FBRSxDQUFDLFVBQVcsQ0FBQyxDQUFDLENBQUM7WUFDaEIsTUFBTSxJQUFJLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBQ2hELENBQUM7UUFFRCxJQUFJLElBQUksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDO1FBQzNCLEVBQUUsQ0FBQyxDQUFFLElBQUksS0FBSyxxQkFBcUIsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sSUFBSSxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUNyRCxDQUFDO1FBRUQsSUFBSSxFQUFFLEdBQVcsVUFBVSxDQUFDLEVBQUUsQ0FBQztRQUMvQixJQUFJLEtBQUssR0FBVSxlQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6RCxJQUFJLFNBQVMsR0FBVyxVQUFVLENBQUMsU0FBUyxDQUFDO1FBQzdDLElBQUksWUFBWSxHQUFXLFVBQVUsQ0FBQyxZQUFZLENBQUM7UUFDbkQsSUFBSSxnQkFBZ0IsR0FBcUIsMEJBQWdCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBRWhHLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLEVBQUUsQ0FBQyxDQUFFLFVBQVUsQ0FBQyxLQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLEtBQUssR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMzQyxDQUFDO1FBRUQsSUFBSSxXQUFXLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BGLFdBQVcsQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ3hDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxXQUFXLENBQUM7SUFFdkIsQ0FBQztJQWtCRCxzQkFBVyxxQ0FBRTtRQUpiOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDcEIsQ0FBQzs7O09BQUE7SUFNRCxzQkFBVyx3Q0FBSztRQUpoQjs7O1dBR0c7YUFDSDtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUM7OztPQUFBO0lBTUQsc0JBQVcsd0NBQUs7UUFJaEI7OztXQUdHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QixDQUFDO1FBZEQ7OztXQUdHO2FBQ0gsVUFBaUIsS0FBYTtZQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQWNELHNCQUFXLDRDQUFTO1FBSXBCOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDM0IsQ0FBQztRQWREOzs7V0FHRzthQUNILFVBQXNCLEtBQWE7WUFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDNUIsQ0FBQzs7O09BQUE7SUFlRCxzQkFBVywrQ0FBWTtRQUl2Qjs7OztXQUlHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM5QixDQUFDO1FBaEJEOzs7O1dBSUc7YUFDSCxVQUF3QixLQUFhO1lBQ2pDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQy9CLENBQUM7OztPQUFBO0lBY0Qsc0JBQVcsbURBQWdCO1FBSDNCOztXQUVHO2FBQ0g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ2xDLENBQUM7OztPQUFBO0lBRUQ7O09BRUc7SUFDSSxzQ0FBTSxHQUFiO1FBQ0ksSUFBSSxNQUFNLEdBQTRCLEVBQUUsQ0FBQztRQUN6QyxNQUFNLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDckIsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUNuQyxNQUFNLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDekMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzNCLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDMUQsTUFBTSxDQUFDLElBQUksR0FBRyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7UUFDekMsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBM0lhLDBCQUFJLEdBQUcsdUJBQXVCLENBQUM7SUE2SWpELDRCQUFDO0NBL0lELEFBK0lDLElBQUE7QUEvSVksc0RBQXFCO0FBaUpsQyxrQkFBZSxxQkFBcUIsQ0FBQyIsImZpbGUiOiJtb2RlbC9iYXNlYW5pbWF0ZWRjaGFyYWN0ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2hhcGUgZnJvbSBcIi4uL21hdGgvc2hhcGVcIjtcclxuaW1wb3J0IHtDb21tb259IGZyb20gXCIuLi9tYXRoL2NvbW1vblwiO1xyXG5pbXBvcnQgQW5pbWF0aW9uUHJvZmlsZSBmcm9tIFwiLi9hbmltYXRpb25wcm9maWxlXCI7XHJcbmltcG9ydCB7IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgfSBmcm9tIFwiLi4vZXJyb3IvbWlzc2luZ2NvbnN0cnVjdG9yYXJndW1lbnRlcnJvclwiO1xyXG5cclxuLyoqXHJcbiAqIEEgYmFzZSBwcm9maWxlIGZvciBhIHNwZWNpZmljIGNoYXJhY3RlciAoYXZhdGFyKS4gVGhpcyBpcyB1c2VkIHRvIHByb3ZpZGUgYSBkZWZhdWx0XHJcbiAqIHRlbXBsYXRlIGZvciBhIHVzZXIgdG8gc2VsZWN0LiBUaGlzIGNoYXJhY3RlciBoYXMgbm8gb3duZXIuIFRoaXMgaXMgbm90IHRvIGJlIHVzZWRcclxuICogb3IgYWRkZWQgdG8gdGhlIHdvcmxkLlxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIEJhc2VBbmltYXRlZENoYXJhY3RlciB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBUWVBFID0gXCJiYXNlYW5pbWF0ZWRjaGFyYWN0ZXJcIjtcclxuXHJcbiAgICBwcml2YXRlIF9pZDogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBfc2hhcGU6IFNoYXBlO1xyXG4gICAgcHJpdmF0ZSBfbW9kZWxOYW1lOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9tb2RlbFRleHR1cmU6IHN0cmluZztcclxuICAgIHByaXZhdGUgX3NwZWVkOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9hbmltYXRpb25Qcm9maWxlOiBBbmltYXRpb25Qcm9maWxlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBKU09OIG9iamVjdCB0byBPYmplY3QgTW9kZWwgb2JqZWN0LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb25PYmplY3Q6IGFueSkge1xyXG5cclxuICAgICAgICBpZiAoICFqc29uT2JqZWN0ICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgY29udmVydCB1bmRlZmluZWRcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdHlwZSA9IGpzb25PYmplY3QudHlwZTtcclxuICAgICAgICBpZiAoIHR5cGUgIT09IEJhc2VBbmltYXRlZENoYXJhY3Rlci5UWVBFICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgY29udmVydC4gSW52YWxpZCB0eXBlLlwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBpZDogc3RyaW5nID0ganNvbk9iamVjdC5pZDtcclxuICAgICAgICBsZXQgc2hhcGU6IFNoYXBlID0gQ29tbW9uLmNvbnZlcnRTaGFwZShqc29uT2JqZWN0LnNoYXBlKTtcclxuICAgICAgICBsZXQgbW9kZWxOYW1lOiBzdHJpbmcgPSBqc29uT2JqZWN0Lm1vZGVsTmFtZTtcclxuICAgICAgICBsZXQgbW9kZWxUZXh0dXJlOiBzdHJpbmcgPSBqc29uT2JqZWN0Lm1vZGVsVGV4dHVyZTtcclxuICAgICAgICBsZXQgYW5pbWF0aW9uUHJvZmlsZTogQW5pbWF0aW9uUHJvZmlsZSA9IEFuaW1hdGlvblByb2ZpbGUuZnJvbUpTT04oanNvbk9iamVjdC5hbmltYXRpb25wcm9maWxlKTtcclxuXHJcbiAgICAgICAgbGV0IHNwZWVkID0gMDtcclxuICAgICAgICBpZiAoIGpzb25PYmplY3Quc3BlZWQgKSB7XHJcbiAgICAgICAgICAgIHNwZWVkID0gcGFyc2VJbnQoanNvbk9iamVjdC5zcGVlZCwgMTApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGNoYXJQcm9maWxlID0gbmV3IEJhc2VBbmltYXRlZENoYXJhY3RlcihpZCwgc2hhcGUsIG1vZGVsTmFtZSwgYW5pbWF0aW9uUHJvZmlsZSk7XHJcbiAgICAgICAgY2hhclByb2ZpbGUubW9kZWxUZXh0dXJlID0gbW9kZWxUZXh0dXJlO1xyXG4gICAgICAgIGNoYXJQcm9maWxlLnNwZWVkID0gc3BlZWQ7XHJcbiAgICAgICAgcmV0dXJuIGNoYXJQcm9maWxlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihpZDogc3RyaW5nLCBzaGFwZTogU2hhcGUsIG1vZGVsTmFtZTogc3RyaW5nLCBhbmltYXRpb25Qcm9maWxlOiBBbmltYXRpb25Qcm9maWxlICkge1xyXG4gICAgICAgIGlmICggaWQgJiYgbW9kZWxOYW1lICYmIHNoYXBlICYmIGFuaW1hdGlvblByb2ZpbGUgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2lkID0gaWQ7XHJcbiAgICAgICAgICAgIHRoaXMuX21vZGVsTmFtZSA9IG1vZGVsTmFtZTtcclxuICAgICAgICAgICAgdGhpcy5fc2hhcGUgPSBzaGFwZTtcclxuICAgICAgICAgICAgdGhpcy5fYW5pbWF0aW9uUHJvZmlsZSA9IGFuaW1hdGlvblByb2ZpbGU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IoXCJpZD1cIiArIGlkICsgXCIgc2hhcGU9XCIgKyBzaGFwZSArXHJcbiAgICAgICAgICAgIFwiIG1vZGVsbmFtZT1cIiArIG1vZGVsTmFtZSArIFwiIGFuaW1hdGlvbnByb2ZpbGU9XCIgKyBhbmltYXRpb25Qcm9maWxlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIHVuaXF1ZSBpZCBvZiB0aGlzIG1vZGVsLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBpZCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pZDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEJvdW5kaW5nIHNoYXBlIG9mIHRoaSBtb2RlbC5cclxuICAgICAqIEByZXR1cm5zIHtTaGFwZX1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBzaGFwZSgpOiBTaGFwZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NoYXBlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IGhvdyBmYXN0IHRoaXMgbW9kZWwgd2lsbCBtb3ZlLlxyXG4gICAgICogQHBhcmFtIHZhbHVlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzZXQgc3BlZWQodmFsdWU6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX3NwZWVkID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgaG93IGZhc3QgdGhpcyBtb2RlbCB3aWxsIG1vdmUuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBzcGVlZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3BlZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgbmFtZSBvZiB0aGUgbW9kZWwuIFRoaXMgaXMgdGhlIDNkIGZpbGUgdXNlZCBieSB0aGlzIG1vZGVsLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldCBtb2RlbE5hbWUoIHZhbHVlOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgdGhpcy5fbW9kZWxOYW1lID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgbmFtZSBvZiB0aGUgbW9kZWwuIFRoaXMgaXMgdGhlIDNkIGZpbGUgdXNlZCBieSB0aGlzIG1vZGVsLlxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBtb2RlbE5hbWUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbW9kZWxOYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSB0ZXh0dXJlIG9mIHRoZSBtb2RlbC4gKE9wdGlvbmFsKVxyXG4gICAgICogSWYgbm8gdGV4dHVyZSBzcGVjaWZpZWQgdGhlbiBlbmdpbmUgd2lsbCB1c2UgdGhlIGRlZmF1bHQgdGV4dHVyZSBhc3NpZ25lZCBpbiB0aGUgbW9kZWwuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgcHVibGljIHNldCBtb2RlbFRleHR1cmUodmFsdWU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX21vZGVsVGV4dHVyZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSB0ZXh0dXJlIG9mIHRoZSBtb2RlbC4gKE9wdGlvbmFsKVxyXG4gICAgICogSWYgbm8gdGV4dHVyZSBzcGVjaWZpZWQgdGhlbiBlbmdpbmUgd2lsbCB1c2UgdGhlIGRlZmF1bHQgdGV4dHVyZSBhc3NpZ25lZCBpbiB0aGUgbW9kZWwuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldCBtb2RlbFRleHR1cmUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21vZGVsVGV4dHVyZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiB0aGUgYW5pbWF0aW9uIHByb2ZpbGUgdXNlZCB0byBhbmltYXRlIHRoaXMgY2hhcmFjdGVyLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0IGFuaW1hdGlvblByb2ZpbGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FuaW1hdGlvblByb2ZpbGU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0IHRoaXMgb2JqZWN0IG1vZGVsIHRvIEpTT04uXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b0pTT04oKTogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0ge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IHsgW2tleTogc3RyaW5nXTogYW55OyB9ID0ge307XHJcbiAgICAgICAgcmVzdWx0LmlkID0gdGhpcy5faWQ7XHJcbiAgICAgICAgcmVzdWx0LnNoYXBlID0gdGhpcy5zaGFwZS50b0pTT04oKTtcclxuICAgICAgICByZXN1bHQubW9kZWxOYW1lID0gdGhpcy5fbW9kZWxOYW1lO1xyXG4gICAgICAgIHJlc3VsdC5tb2RlbFRleHR1cmUgPSB0aGlzLl9tb2RlbFRleHR1cmU7XHJcbiAgICAgICAgcmVzdWx0LnNwZWVkID0gdGhpcy5fc3BlZWQ7XHJcbiAgICAgICAgcmVzdWx0LmFuaW1hdGlvbnByb2ZpbGUgPSB0aGlzLl9hbmltYXRpb25Qcm9maWxlLnRvSlNPTigpO1xyXG4gICAgICAgIHJlc3VsdC50eXBlID0gQmFzZUFuaW1hdGVkQ2hhcmFjdGVyLlRZUEU7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEJhc2VBbmltYXRlZENoYXJhY3RlcjtcclxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
