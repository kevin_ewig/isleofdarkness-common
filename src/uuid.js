"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var shortid = require("shortid");
var Uuid = /** @class */ (function () {
    function Uuid() {
    }
    /**
     * Gets a UUID.
     */
    Uuid.getUuid = function () {
        return shortid.generate();
    };
    return Uuid;
}());
exports.Uuid = Uuid;
exports.default = Uuid;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInV1aWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7QUFFakM7SUFBQTtJQVNBLENBQUM7SUFQRzs7T0FFRztJQUNXLFlBQU8sR0FBckI7UUFDSSxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFFTCxXQUFDO0FBQUQsQ0FUQSxBQVNDLElBQUE7QUFUWSxvQkFBSTtBQVdqQixrQkFBZSxJQUFJLENBQUMiLCJmaWxlIjoidXVpZC5qcyIsInNvdXJjZXNDb250ZW50IjpbImxldCBzaG9ydGlkID0gcmVxdWlyZShcInNob3J0aWRcIik7XHJcblxyXG5leHBvcnQgY2xhc3MgVXVpZCB7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgVVVJRC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBnZXRVdWlkKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHNob3J0aWQuZ2VuZXJhdGUoKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFV1aWQ7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
