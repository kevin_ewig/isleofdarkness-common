let shortid = require("shortid");

export class Uuid {

    /**
     * Gets a UUID.
     */
    public static getUuid(): string {
        return shortid.generate();
    }

}

export default Uuid;
