import Shape from "../math/shape";
import MissingConstructorArgumentError from "../error/missingconstructorargumenterror";

export class Node {

    private _h: number;
    private _g: number;
    private _shape: Shape;
    private _parent: Node;
    private _objectModelId: string;

    constructor(objectModelId: string, shape: Shape) {
        if ( shape && objectModelId ) {
            this._shape = shape;
            this._objectModelId = objectModelId;
            this._h = 0;
            this._g = 0;
        } else {
            throw new MissingConstructorArgumentError();
        }
    }

    /**
     * Determine if two nodes are equal.
     */
    public equals(otherNode: Node) {
        if ( !otherNode ) {
            return false;
        }
        if ( otherNode.h === this.h && otherNode.g === this.g && otherNode.shape ) {
            if ( otherNode.shape.x === this.shape.x &&
                otherNode.shape.y === this.shape.y &&
                otherNode.shape.z === this.shape.z ) {
                return true;
            }
        }
        return false;
    }

    /**
     * The sum of G + H.
     */
    get f() { return this._g + this._h; }

    /**
     * Set the movement cost to move from the starting point A to a given square on the world.
     */
    set g(value: number) { this._g = value; }

    /**
     * Get the movement cost to move from the starting point A to a given square on the world.
     */
    get g() { return this._g; }

    /**
     * Get the object model id that is performing the pathfinding.
     */
    get objectModelId() {
        return this._objectModelId;
    }

    /**
     * Get the geometry associated with this node.
     * @returns {Shape}
     */
    get shape() {
        return this._shape;
    }

    /**
     * Return a unique hash value associated with this node.
     */
    public getHash(): string {
        return "[" + this._objectModelId + "=" + this.shape.x + "," + this.shape.y + "," + this.shape.z + "]";
    }

    /**
     * Set the estimated movement cost to move from that given square on the grid to the final destination.
     */
    set h(value: number) { this._h = value; }

    /**
     * Get the estimated movement cost to move from that given square on the grid to the final destination.
     */
    get h() { return this._h; }

    /**
     * Set the parent node. This is the node that generated this node.
     */
    set parent(value: Node) { this._parent = value; }

    /**
     * Get the parent node. This is the node that generated this node.
     */
    get parent() { return this._parent; }

    /**
     * Return a string value containing information about this node.
     */
    public toString(): string {
        return "[" + this.shape.x + "," + this.shape.y + "," + this.shape.z + "]" +
            "[ g = " + this.g + ", h = " + this.h + " f = " + this.f + " ]";
    }
}

export default Node;
