import {Node} from "./node";

/**
 * A list of closed nodes. These nodes will never be in the final pathfinding result.
 */
export class CloseList {

    private _list: Node[];
    private _hash: any;

    constructor() {
        this._list = new Array<Node>();
        this._hash = {};
    }

    /**
     * Get the f value of the node in the list. If node does not exist in the list then return undefined.
     * @param node
     * @returns {any}
     */
    public getFValue(node: Node): number {
        return this._hash[node.getHash()];
    }

    /**
     * Get the length of close list.
     * @returns {number}
     */
    public getLength(): number {
        return this._list.length;
    }

    /**
     * Check if this node position is already in the open list.
     * @param node
     * @returns {boolean}
     */
    public isNodeInList(node: Node): boolean {
        return this._hash[node.getHash()] !== undefined;
    }

    /**
     * Add a node to the closed list.
     * @param node
     */
    public push( node: Node ) {
        if ( !node ) {
            throw new Error("Invalid node");
        }
        this._list.push(node);
        this._hash[node.getHash()] = 1;
    }

}

export default CloseList;
