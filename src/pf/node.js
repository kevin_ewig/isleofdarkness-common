"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var missingconstructorargumenterror_1 = require("../error/missingconstructorargumenterror");
var Node = /** @class */ (function () {
    function Node(objectModelId, shape) {
        if (shape && objectModelId) {
            this._shape = shape;
            this._objectModelId = objectModelId;
            this._h = 0;
            this._g = 0;
        }
        else {
            throw new missingconstructorargumenterror_1.default();
        }
    }
    /**
     * Determine if two nodes are equal.
     */
    Node.prototype.equals = function (otherNode) {
        if (!otherNode) {
            return false;
        }
        if (otherNode.h === this.h && otherNode.g === this.g && otherNode.shape) {
            if (otherNode.shape.x === this.shape.x &&
                otherNode.shape.y === this.shape.y &&
                otherNode.shape.z === this.shape.z) {
                return true;
            }
        }
        return false;
    };
    Object.defineProperty(Node.prototype, "f", {
        /**
         * The sum of G + H.
         */
        get: function () { return this._g + this._h; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "g", {
        /**
         * Get the movement cost to move from the starting point A to a given square on the world.
         */
        get: function () { return this._g; },
        /**
         * Set the movement cost to move from the starting point A to a given square on the world.
         */
        set: function (value) { this._g = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "objectModelId", {
        /**
         * Get the object model id that is performing the pathfinding.
         */
        get: function () {
            return this._objectModelId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "shape", {
        /**
         * Get the geometry associated with this node.
         * @returns {Shape}
         */
        get: function () {
            return this._shape;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Return a unique hash value associated with this node.
     */
    Node.prototype.getHash = function () {
        return "[" + this._objectModelId + "=" + this.shape.x + "," + this.shape.y + "," + this.shape.z + "]";
    };
    Object.defineProperty(Node.prototype, "h", {
        /**
         * Get the estimated movement cost to move from that given square on the grid to the final destination.
         */
        get: function () { return this._h; },
        /**
         * Set the estimated movement cost to move from that given square on the grid to the final destination.
         */
        set: function (value) { this._h = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "parent", {
        /**
         * Get the parent node. This is the node that generated this node.
         */
        get: function () { return this._parent; },
        /**
         * Set the parent node. This is the node that generated this node.
         */
        set: function (value) { this._parent = value; },
        enumerable: true,
        configurable: true
    });
    /**
     * Return a string value containing information about this node.
     */
    Node.prototype.toString = function () {
        return "[" + this.shape.x + "," + this.shape.y + "," + this.shape.z + "]" +
            "[ g = " + this.g + ", h = " + this.h + " f = " + this.f + " ]";
    };
    return Node;
}());
exports.Node = Node;
exports.default = Node;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBmL25vZGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSw0RkFBdUY7QUFFdkY7SUFRSSxjQUFZLGFBQXFCLEVBQUUsS0FBWTtRQUMzQyxFQUFFLENBQUMsQ0FBRSxLQUFLLElBQUksYUFBYyxDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwQixJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQztZQUNwQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNaLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSx5Q0FBK0IsRUFBRSxDQUFDO1FBQ2hELENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxxQkFBTSxHQUFiLFVBQWMsU0FBZTtRQUN6QixFQUFFLENBQUMsQ0FBRSxDQUFDLFNBQVUsQ0FBQyxDQUFDLENBQUM7WUFDZixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxTQUFTLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxJQUFJLFNBQVMsQ0FBQyxLQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLEVBQUUsQ0FBQyxDQUFFLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbkMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RDLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDaEIsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFLRCxzQkFBSSxtQkFBQztRQUhMOztXQUVHO2FBQ0gsY0FBVSxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFLckMsc0JBQUksbUJBQUM7UUFFTDs7V0FFRzthQUNILGNBQVUsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBUjNCOztXQUVHO2FBQ0gsVUFBTSxLQUFhLElBQUksSUFBSSxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQVV6QyxzQkFBSSwrQkFBYTtRQUhqQjs7V0FFRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDL0IsQ0FBQzs7O09BQUE7SUFNRCxzQkFBSSx1QkFBSztRQUpUOzs7V0FHRzthQUNIO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQzs7O09BQUE7SUFFRDs7T0FFRztJQUNJLHNCQUFPLEdBQWQ7UUFDSSxNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxjQUFjLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQzFHLENBQUM7SUFLRCxzQkFBSSxtQkFBQztRQUVMOztXQUVHO2FBQ0gsY0FBVSxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFSM0I7O1dBRUc7YUFDSCxVQUFNLEtBQWEsSUFBSSxJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBVXpDLHNCQUFJLHdCQUFNO1FBRVY7O1dBRUc7YUFDSCxjQUFlLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztRQVJyQzs7V0FFRzthQUNILFVBQVcsS0FBVyxJQUFJLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFPakQ7O09BRUc7SUFDSSx1QkFBUSxHQUFmO1FBQ0ksTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEdBQUc7WUFDckUsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDO0lBQ3hFLENBQUM7SUFDTCxXQUFDO0FBQUQsQ0FwR0EsQUFvR0MsSUFBQTtBQXBHWSxvQkFBSTtBQXNHakIsa0JBQWUsSUFBSSxDQUFDIiwiZmlsZSI6InBmL25vZGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2hhcGUgZnJvbSBcIi4uL21hdGgvc2hhcGVcIjtcclxuaW1wb3J0IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IgZnJvbSBcIi4uL2Vycm9yL21pc3Npbmdjb25zdHJ1Y3RvcmFyZ3VtZW50ZXJyb3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBOb2RlIHtcclxuXHJcbiAgICBwcml2YXRlIF9oOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9nOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9zaGFwZTogU2hhcGU7XHJcbiAgICBwcml2YXRlIF9wYXJlbnQ6IE5vZGU7XHJcbiAgICBwcml2YXRlIF9vYmplY3RNb2RlbElkOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3Iob2JqZWN0TW9kZWxJZDogc3RyaW5nLCBzaGFwZTogU2hhcGUpIHtcclxuICAgICAgICBpZiAoIHNoYXBlICYmIG9iamVjdE1vZGVsSWQgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3NoYXBlID0gc2hhcGU7XHJcbiAgICAgICAgICAgIHRoaXMuX29iamVjdE1vZGVsSWQgPSBvYmplY3RNb2RlbElkO1xyXG4gICAgICAgICAgICB0aGlzLl9oID0gMDtcclxuICAgICAgICAgICAgdGhpcy5fZyA9IDA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IE1pc3NpbmdDb25zdHJ1Y3RvckFyZ3VtZW50RXJyb3IoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZXRlcm1pbmUgaWYgdHdvIG5vZGVzIGFyZSBlcXVhbC5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGVxdWFscyhvdGhlck5vZGU6IE5vZGUpIHtcclxuICAgICAgICBpZiAoICFvdGhlck5vZGUgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCBvdGhlck5vZGUuaCA9PT0gdGhpcy5oICYmIG90aGVyTm9kZS5nID09PSB0aGlzLmcgJiYgb3RoZXJOb2RlLnNoYXBlICkge1xyXG4gICAgICAgICAgICBpZiAoIG90aGVyTm9kZS5zaGFwZS54ID09PSB0aGlzLnNoYXBlLnggJiZcclxuICAgICAgICAgICAgICAgIG90aGVyTm9kZS5zaGFwZS55ID09PSB0aGlzLnNoYXBlLnkgJiZcclxuICAgICAgICAgICAgICAgIG90aGVyTm9kZS5zaGFwZS56ID09PSB0aGlzLnNoYXBlLnogKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgc3VtIG9mIEcgKyBILlxyXG4gICAgICovXHJcbiAgICBnZXQgZigpIHsgcmV0dXJuIHRoaXMuX2cgKyB0aGlzLl9oOyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIG1vdmVtZW50IGNvc3QgdG8gbW92ZSBmcm9tIHRoZSBzdGFydGluZyBwb2ludCBBIHRvIGEgZ2l2ZW4gc3F1YXJlIG9uIHRoZSB3b3JsZC5cclxuICAgICAqL1xyXG4gICAgc2V0IGcodmFsdWU6IG51bWJlcikgeyB0aGlzLl9nID0gdmFsdWU7IH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgbW92ZW1lbnQgY29zdCB0byBtb3ZlIGZyb20gdGhlIHN0YXJ0aW5nIHBvaW50IEEgdG8gYSBnaXZlbiBzcXVhcmUgb24gdGhlIHdvcmxkLlxyXG4gICAgICovXHJcbiAgICBnZXQgZygpIHsgcmV0dXJuIHRoaXMuX2c7IH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgb2JqZWN0IG1vZGVsIGlkIHRoYXQgaXMgcGVyZm9ybWluZyB0aGUgcGF0aGZpbmRpbmcuXHJcbiAgICAgKi9cclxuICAgIGdldCBvYmplY3RNb2RlbElkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9vYmplY3RNb2RlbElkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBnZW9tZXRyeSBhc3NvY2lhdGVkIHdpdGggdGhpcyBub2RlLlxyXG4gICAgICogQHJldHVybnMge1NoYXBlfVxyXG4gICAgICovXHJcbiAgICBnZXQgc2hhcGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NoYXBlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIGEgdW5pcXVlIGhhc2ggdmFsdWUgYXNzb2NpYXRlZCB3aXRoIHRoaXMgbm9kZS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldEhhc2goKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gXCJbXCIgKyB0aGlzLl9vYmplY3RNb2RlbElkICsgXCI9XCIgKyB0aGlzLnNoYXBlLnggKyBcIixcIiArIHRoaXMuc2hhcGUueSArIFwiLFwiICsgdGhpcy5zaGFwZS56ICsgXCJdXCI7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdGhlIGVzdGltYXRlZCBtb3ZlbWVudCBjb3N0IHRvIG1vdmUgZnJvbSB0aGF0IGdpdmVuIHNxdWFyZSBvbiB0aGUgZ3JpZCB0byB0aGUgZmluYWwgZGVzdGluYXRpb24uXHJcbiAgICAgKi9cclxuICAgIHNldCBoKHZhbHVlOiBudW1iZXIpIHsgdGhpcy5faCA9IHZhbHVlOyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIGVzdGltYXRlZCBtb3ZlbWVudCBjb3N0IHRvIG1vdmUgZnJvbSB0aGF0IGdpdmVuIHNxdWFyZSBvbiB0aGUgZ3JpZCB0byB0aGUgZmluYWwgZGVzdGluYXRpb24uXHJcbiAgICAgKi9cclxuICAgIGdldCBoKCkgeyByZXR1cm4gdGhpcy5faDsgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoZSBwYXJlbnQgbm9kZS4gVGhpcyBpcyB0aGUgbm9kZSB0aGF0IGdlbmVyYXRlZCB0aGlzIG5vZGUuXHJcbiAgICAgKi9cclxuICAgIHNldCBwYXJlbnQodmFsdWU6IE5vZGUpIHsgdGhpcy5fcGFyZW50ID0gdmFsdWU7IH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgcGFyZW50IG5vZGUuIFRoaXMgaXMgdGhlIG5vZGUgdGhhdCBnZW5lcmF0ZWQgdGhpcyBub2RlLlxyXG4gICAgICovXHJcbiAgICBnZXQgcGFyZW50KCkgeyByZXR1cm4gdGhpcy5fcGFyZW50OyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gYSBzdHJpbmcgdmFsdWUgY29udGFpbmluZyBpbmZvcm1hdGlvbiBhYm91dCB0aGlzIG5vZGUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyB0b1N0cmluZygpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBcIltcIiArIHRoaXMuc2hhcGUueCArIFwiLFwiICsgdGhpcy5zaGFwZS55ICsgXCIsXCIgKyB0aGlzLnNoYXBlLnogKyBcIl1cIiArXHJcbiAgICAgICAgICAgIFwiWyBnID0gXCIgKyB0aGlzLmcgKyBcIiwgaCA9IFwiICsgdGhpcy5oICsgXCIgZiA9IFwiICsgdGhpcy5mICsgXCIgXVwiO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBOb2RlO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
