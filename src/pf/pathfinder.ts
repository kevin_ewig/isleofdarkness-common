import Logger from "../logger";
import {ObjectModelType} from "../model/objectmodel";
import Intersecter from "../math/intersecter";
import Point from "../math/point";
import Shape from "../math/shape";
import ObjectModel from "../model/objectmodel";
import OpenList from "./openlist";
import CloseList from "./closelist";
import World from "../model/world";
import Node from "./node";

export class Pathfinder {

    private _openList: OpenList;
    private _closeList: CloseList;
    private _startingPoint: Point;
    private _step: number;
    private _destination: Point;
    private _world: World;
    private _nodeLimit: number;

    constructor(world: World, nodeLimit?: number) {
        this._world = world;
        if ( nodeLimit ) {
            this._nodeLimit = 0;
        } else {
            this._nodeLimit = 500;
        }
    }

    /**
     * Finds a path for the given geometry from the geometry's current point
     * to a certain given point.
     * @param geometry
     * @param destination
     * @param step This is the height distance or the height of the step of the
     * entity that is trying to find the path. It is proportional to the height of the
     * the entity. If the entity is big and tall, the height would be large. If the
     * entity is small and short, the height will be small.
     * @returns {Array}
     */
    public findPath(objectModel: ObjectModel, destination: Point, step?: number): Point[] {

        // Reset the open and closed list.
        this._openList = new OpenList();
        this._closeList = new CloseList();

        // Set up step.
        this._step = step ? step : 0;

        // Get the destination.
        let geometry: Shape = objectModel.shape;
        this._startingPoint = new Point(geometry.x, geometry.y, geometry.z);
        this._destination = destination.clone();

        // Add the first geometry as the first node on the list.
        let firstNode = new Node(objectModel.id, geometry.clone());
        firstNode.g = this.getCostFromSource(firstNode);
        firstNode.h = this.getCostToDestination(firstNode);

        // Add first node to the list of possibilities.
        this._openList.push(firstNode);

        // Find the path. Get the last node of the path.
        let lastNode = this.computePath();

        // Convert to points.
        let result: Point[]|null = new Array<Point>();
        if ( lastNode ) {
            result = this.convertNodeToPoints(lastNode);
        }
        return result;

    }

    /**
     * Recursively use the A* pathfinding algorithm to find the closest path.
     * Return null if path cannot be found.
     */
    private computePath(): Node|null {

        while ( this._openList.isNotEmpty() ) {

            // If we've already considered too many nodes and possibilities,
            // then just exit.
            if ( this._closeList.getLength() > this._nodeLimit ) {
                return null;
            }

            let qNode = this._openList.pop();
            if ( qNode === null ) {
                Logger.warn("Pathfinder", "No more nodes in the open list");
                return null;
            }

            let arrayOfNodes = this.getAdjacentNodes(qNode);
            for ( let i = 0; i < arrayOfNodes.length; i++ ) {

                let tempNode = arrayOfNodes[i];

                if ( Intersecter.isShapeIntersectPoint(tempNode.shape, this._destination) ) {
                    if ( tempNode.shape.z === this._destination.z ) {
                        return tempNode;
                    }
                }

                let skipSuccessor = false;

                // if a node with the same position as successor is in the OPEN list
                // which has a lower f than successor, skip this successor.
                if ( this._openList.isNodeInList(tempNode) ) {
                    if ( this._openList.getFValue(tempNode) <= tempNode.f ) {
                        skipSuccessor = true;
                    }
                }

                // if a node with the same position as successor is in the CLOSED list
                // which has a lower f than successor, skip this successor
                if ( this._closeList.isNodeInList(tempNode) ) {
                    skipSuccessor = true;
                }

                if ( !skipSuccessor ) {
                    this._openList.push(tempNode);
                }
            }

            this._closeList.push(qNode);
            this._openList.sort();

        }

        return null;

    }

    /**
     * Convert node into a list of points (for the path).
     */
    private convertNodeToPoints(node: Node): Point[] {
        let path = new Array<Point>();
        let tempNode = node;
        while ( tempNode != null ) {
            path.unshift( new Point(tempNode.shape.x, tempNode.shape.y, tempNode.shape.z) );
            tempNode = tempNode.parent;
        }
        return path;
    }

    /**
     * Get adjacent nodes.
     */
    private getAdjacentNodes( node: Node ): Array<Node> {

        let adjacentShapes = node.shape.getAdjacentShapes();

        // Add potential shapes to the open list. Ignore nodes that cannot be crossed or are in the closed list.
        let adjacentNodes = new Array<Node>();
        for ( let i = 0; i < adjacentShapes.length; i++ ) {

            let adjacentShape = adjacentShapes[i];
            let tempNode = new Node(node.objectModelId, adjacentShape);

            // Set the z value of the shape based on the shapes that this adjacent
            // shape intersects.
            let tempZHeight = this.getZHeight(tempNode);

            // Only allow this child node if the gap between the parent node is not greater than the step value.
            if ( Math.abs(tempZHeight - node.shape.z) <= this._step ) {

                adjacentShape.z = tempZHeight;
                tempNode.parent = node;
                tempNode.g = this.getCostFromSource(tempNode);
                tempNode.h = this.getCostToDestination(tempNode);

                // Only allow this child if the node is not blocked.
                if ( this.isNodeBlocked(tempNode) === false ) {
                    adjacentNodes.push(tempNode);
                }

            }

        }

        return adjacentNodes;

    }

    /**
     * Find the cost G between the current node to the starting point.
     */
    private getCostFromSource(node: Node): number {
        // return (node.parent === null) ? 1 : node.parent.g + 1;
        let x1 = node.shape.x;
        let x2 = this._startingPoint.x;
        let y1 = node.shape.y;
        let y2 = this._startingPoint.y;
        let z1 = node.shape.z;
        let z2 = this._startingPoint.z;
        let twodcost = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        let zcost = Math.abs(z2 - z1);
        if ( zcost > 0 ) { return zcost + twodcost * 0.01; }
        return twodcost;
    }

    /**
     * Find the cost H between the current node to the destination point.
     */
    private getCostToDestination(node: Node): number {
        let x1 = node.shape.x;
        let x2 = this._destination.x;
        let y1 = node.shape.y;
        let y2 = this._destination.y;
        let z1 = node.shape.z;
        let z2 = this._destination.z;
        let twodcost = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        let zcost = Math.abs(z2 - z1);
        if ( zcost > 0 ) { return zcost + twodcost * 0.01; }
        return twodcost;
    }

    /**
     * Get the Z height of a given node.
     */
    private getZHeight(node: Node): number {

        let shape = node.shape;
        let zValue = 0;

        let pointShape = new Point(shape.x, shape.y, shape.z);

        let intersectingObjectModels = this._world.getAllIntersection(shape);

        for ( let j = 0; j < intersectingObjectModels.length; j++ ) {

            let intersectObjectModel = intersectingObjectModels[j];
            let intersectShape = intersectObjectModel.shape;

            // If it is impassable then the isNodeBlocked method will filter it out.
            if ( intersectObjectModel.id !== node.objectModelId && intersectObjectModel.objectType !== ObjectModelType.Impassable ) {
                if ( Intersecter.isShapeIntersectPoint(intersectShape, pointShape) ) {

                    // We need to get the largest z value.
                    let tempZValue = intersectShape.getZRelativeToPoint(pointShape);
                    if ( tempZValue <= shape.z + this._step ) {
                        if ( zValue < tempZValue ) {
                            zValue = tempZValue;
                        }
                    }

                }
            }

        }

        return zValue;

    }

    /**
     * If the pathfinding node intersects with something that is impassable then the node is
     * considered blocked and not included in pathfinding. Otherwise it is included.
     */
    private isNodeBlocked(node: Node): boolean {
        let shape = node.shape;
        let intersectingObjectModels = this._world.getAllIntersection(shape);
        for ( let j = 0; j < intersectingObjectModels.length; j++ ) {
            let tempObjectModel = intersectingObjectModels[j];
            if ( tempObjectModel.id !== node.objectModelId && tempObjectModel.objectType === ObjectModelType.Impassable ) {
                return true;
            }
        }
        return false;
    }

}

export default Pathfinder;
