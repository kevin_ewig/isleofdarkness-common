"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A list of closed nodes. These nodes will never be in the final pathfinding result.
 */
var CloseList = /** @class */ (function () {
    function CloseList() {
        this._list = new Array();
        this._hash = {};
    }
    /**
     * Get the f value of the node in the list. If node does not exist in the list then return undefined.
     * @param node
     * @returns {any}
     */
    CloseList.prototype.getFValue = function (node) {
        return this._hash[node.getHash()];
    };
    /**
     * Get the length of close list.
     * @returns {number}
     */
    CloseList.prototype.getLength = function () {
        return this._list.length;
    };
    /**
     * Check if this node position is already in the open list.
     * @param node
     * @returns {boolean}
     */
    CloseList.prototype.isNodeInList = function (node) {
        return this._hash[node.getHash()] !== undefined;
    };
    /**
     * Add a node to the closed list.
     * @param node
     */
    CloseList.prototype.push = function (node) {
        if (!node) {
            throw new Error("Invalid node");
        }
        this._list.push(node);
        this._hash[node.getHash()] = 1;
    };
    return CloseList;
}());
exports.CloseList = CloseList;
exports.default = CloseList;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBmL2Nsb3NlbGlzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBOztHQUVHO0FBQ0g7SUFLSTtRQUNJLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxLQUFLLEVBQVEsQ0FBQztRQUMvQixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLDZCQUFTLEdBQWhCLFVBQWlCLElBQVU7UUFDdkIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVEOzs7T0FHRztJQUNJLDZCQUFTLEdBQWhCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQzdCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksZ0NBQVksR0FBbkIsVUFBb0IsSUFBVTtRQUMxQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsS0FBSyxTQUFTLENBQUM7SUFDcEQsQ0FBQztJQUVEOzs7T0FHRztJQUNJLHdCQUFJLEdBQVgsVUFBYSxJQUFVO1FBQ25CLEVBQUUsQ0FBQyxDQUFFLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNWLE1BQU0sSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUNELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFTCxnQkFBQztBQUFELENBaERBLEFBZ0RDLElBQUE7QUFoRFksOEJBQVM7QUFrRHRCLGtCQUFlLFNBQVMsQ0FBQyIsImZpbGUiOiJwZi9jbG9zZWxpc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05vZGV9IGZyb20gXCIuL25vZGVcIjtcclxuXHJcbi8qKlxyXG4gKiBBIGxpc3Qgb2YgY2xvc2VkIG5vZGVzLiBUaGVzZSBub2RlcyB3aWxsIG5ldmVyIGJlIGluIHRoZSBmaW5hbCBwYXRoZmluZGluZyByZXN1bHQuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgQ2xvc2VMaXN0IHtcclxuXHJcbiAgICBwcml2YXRlIF9saXN0OiBOb2RlW107XHJcbiAgICBwcml2YXRlIF9oYXNoOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5fbGlzdCA9IG5ldyBBcnJheTxOb2RlPigpO1xyXG4gICAgICAgIHRoaXMuX2hhc2ggPSB7fTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgZiB2YWx1ZSBvZiB0aGUgbm9kZSBpbiB0aGUgbGlzdC4gSWYgbm9kZSBkb2VzIG5vdCBleGlzdCBpbiB0aGUgbGlzdCB0aGVuIHJldHVybiB1bmRlZmluZWQuXHJcbiAgICAgKiBAcGFyYW0gbm9kZVxyXG4gICAgICogQHJldHVybnMge2FueX1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldEZWYWx1ZShub2RlOiBOb2RlKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faGFzaFtub2RlLmdldEhhc2goKV07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIGxlbmd0aCBvZiBjbG9zZSBsaXN0LlxyXG4gICAgICogQHJldHVybnMge251bWJlcn1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldExlbmd0aCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9saXN0Lmxlbmd0aDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIHRoaXMgbm9kZSBwb3NpdGlvbiBpcyBhbHJlYWR5IGluIHRoZSBvcGVuIGxpc3QuXHJcbiAgICAgKiBAcGFyYW0gbm9kZVxyXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBpc05vZGVJbkxpc3Qobm9kZTogTm9kZSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9oYXNoW25vZGUuZ2V0SGFzaCgpXSAhPT0gdW5kZWZpbmVkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIGEgbm9kZSB0byB0aGUgY2xvc2VkIGxpc3QuXHJcbiAgICAgKiBAcGFyYW0gbm9kZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcHVzaCggbm9kZTogTm9kZSApIHtcclxuICAgICAgICBpZiAoICFub2RlICkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIG5vZGVcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX2xpc3QucHVzaChub2RlKTtcclxuICAgICAgICB0aGlzLl9oYXNoW25vZGUuZ2V0SGFzaCgpXSA9IDE7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBDbG9zZUxpc3Q7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
