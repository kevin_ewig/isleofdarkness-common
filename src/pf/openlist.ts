import {Node} from "./node";

/**
 * A list of open nodes to be considered.
 */
export class OpenList {

    private _list: Node[];
    private _hash: any;

    constructor() {
        this._list = new Array<Node>();
        this._hash = {};
    }

    /**
     * Get the f value of the node in the list. If node does not exist in the list then return undefined.
     * @param node
     * @returns {any}
     */
    public getFValue(node: Node): number {
        return this._hash[node.getHash()];
    }

    /**
     * Check if this node position is already in the open list.
     * @param node
     * @returns {boolean}
     */
    public isNodeInList(node: Node): boolean {
        return this._hash[node.getHash()] !== undefined;
    }

    /**
     * Determine if this list is not empty.
     * @returns {boolean}
     */
    public isNotEmpty(): boolean {
        return this._list.length > 0;
    }

    /**
     * Add a node to the top of the list.
     * @param node
     */
    public push( node: Node ) {
        if ( !node ) {
            throw new Error("Invalid node");
        }
        this._list.push(node);
        this._hash[node.getHash()] = node.f;
    }

    /**
     * Removes the node on the top of the list.
     * Null if no nodes or list is empty.
     */
    public pop(): Node|null {
        let node = this._list.shift();
        if ( node ) {
            delete this._hash[node.getHash()];
            return node;
        }
        return null;
    }

    /**
     * Sort this list. The node with the lowest cost (f value) will be at the top.
     */
    public sort() {
        this._list.sort( this.comparer );
    }

    /**
     * Compare function for nodes. This is used for sorting.
     * @param nodeA
     * @param nodeB
     * @returns {number}
     */
    private comparer( nodeA: Node, nodeB: Node ) {
        return nodeA.f - nodeB.f;
    }

}

export default OpenList;
