"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A list of open nodes to be considered.
 */
var OpenList = /** @class */ (function () {
    function OpenList() {
        this._list = new Array();
        this._hash = {};
    }
    /**
     * Get the f value of the node in the list. If node does not exist in the list then return undefined.
     * @param node
     * @returns {any}
     */
    OpenList.prototype.getFValue = function (node) {
        return this._hash[node.getHash()];
    };
    /**
     * Check if this node position is already in the open list.
     * @param node
     * @returns {boolean}
     */
    OpenList.prototype.isNodeInList = function (node) {
        return this._hash[node.getHash()] !== undefined;
    };
    /**
     * Determine if this list is not empty.
     * @returns {boolean}
     */
    OpenList.prototype.isNotEmpty = function () {
        return this._list.length > 0;
    };
    /**
     * Add a node to the top of the list.
     * @param node
     */
    OpenList.prototype.push = function (node) {
        if (!node) {
            throw new Error("Invalid node");
        }
        this._list.push(node);
        this._hash[node.getHash()] = node.f;
    };
    /**
     * Removes the node on the top of the list.
     * Null if no nodes or list is empty.
     */
    OpenList.prototype.pop = function () {
        var node = this._list.shift();
        if (node) {
            delete this._hash[node.getHash()];
            return node;
        }
        return null;
    };
    /**
     * Sort this list. The node with the lowest cost (f value) will be at the top.
     */
    OpenList.prototype.sort = function () {
        this._list.sort(this.comparer);
    };
    /**
     * Compare function for nodes. This is used for sorting.
     * @param nodeA
     * @param nodeB
     * @returns {number}
     */
    OpenList.prototype.comparer = function (nodeA, nodeB) {
        return nodeA.f - nodeB.f;
    };
    return OpenList;
}());
exports.OpenList = OpenList;
exports.default = OpenList;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBmL29wZW5saXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7O0dBRUc7QUFDSDtJQUtJO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLEtBQUssRUFBUSxDQUFDO1FBQy9CLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksNEJBQVMsR0FBaEIsVUFBaUIsSUFBVTtRQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLCtCQUFZLEdBQW5CLFVBQW9CLElBQVU7UUFDMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssU0FBUyxDQUFDO0lBQ3BELENBQUM7SUFFRDs7O09BR0c7SUFDSSw2QkFBVSxHQUFqQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVEOzs7T0FHRztJQUNJLHVCQUFJLEdBQVgsVUFBYSxJQUFVO1FBQ25CLEVBQUUsQ0FBQyxDQUFFLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNWLE1BQU0sSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUNELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksc0JBQUcsR0FBVjtRQUNJLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDOUIsRUFBRSxDQUFDLENBQUUsSUFBSyxDQUFDLENBQUMsQ0FBQztZQUNULE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNJLHVCQUFJLEdBQVg7UUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFFLENBQUM7SUFDckMsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ssMkJBQVEsR0FBaEIsVUFBa0IsS0FBVyxFQUFFLEtBQVc7UUFDdEMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUwsZUFBQztBQUFELENBOUVBLEFBOEVDLElBQUE7QUE5RVksNEJBQVE7QUFnRnJCLGtCQUFlLFFBQVEsQ0FBQyIsImZpbGUiOiJwZi9vcGVubGlzdC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Tm9kZX0gZnJvbSBcIi4vbm9kZVwiO1xyXG5cclxuLyoqXHJcbiAqIEEgbGlzdCBvZiBvcGVuIG5vZGVzIHRvIGJlIGNvbnNpZGVyZWQuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgT3Blbkxpc3Qge1xyXG5cclxuICAgIHByaXZhdGUgX2xpc3Q6IE5vZGVbXTtcclxuICAgIHByaXZhdGUgX2hhc2g6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLl9saXN0ID0gbmV3IEFycmF5PE5vZGU+KCk7XHJcbiAgICAgICAgdGhpcy5faGFzaCA9IHt9O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBmIHZhbHVlIG9mIHRoZSBub2RlIGluIHRoZSBsaXN0LiBJZiBub2RlIGRvZXMgbm90IGV4aXN0IGluIHRoZSBsaXN0IHRoZW4gcmV0dXJuIHVuZGVmaW5lZC5cclxuICAgICAqIEBwYXJhbSBub2RlXHJcbiAgICAgKiBAcmV0dXJucyB7YW55fVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0RlZhbHVlKG5vZGU6IE5vZGUpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9oYXNoW25vZGUuZ2V0SGFzaCgpXTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIHRoaXMgbm9kZSBwb3NpdGlvbiBpcyBhbHJlYWR5IGluIHRoZSBvcGVuIGxpc3QuXHJcbiAgICAgKiBAcGFyYW0gbm9kZVxyXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBpc05vZGVJbkxpc3Qobm9kZTogTm9kZSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9oYXNoW25vZGUuZ2V0SGFzaCgpXSAhPT0gdW5kZWZpbmVkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGV0ZXJtaW5lIGlmIHRoaXMgbGlzdCBpcyBub3QgZW1wdHkuXHJcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGlzTm90RW1wdHkoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xpc3QubGVuZ3RoID4gMDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZCBhIG5vZGUgdG8gdGhlIHRvcCBvZiB0aGUgbGlzdC5cclxuICAgICAqIEBwYXJhbSBub2RlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBwdXNoKCBub2RlOiBOb2RlICkge1xyXG4gICAgICAgIGlmICggIW5vZGUgKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgbm9kZVwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fbGlzdC5wdXNoKG5vZGUpO1xyXG4gICAgICAgIHRoaXMuX2hhc2hbbm9kZS5nZXRIYXNoKCldID0gbm9kZS5mO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVtb3ZlcyB0aGUgbm9kZSBvbiB0aGUgdG9wIG9mIHRoZSBsaXN0LlxyXG4gICAgICogTnVsbCBpZiBubyBub2RlcyBvciBsaXN0IGlzIGVtcHR5LlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcG9wKCk6IE5vZGV8bnVsbCB7XHJcbiAgICAgICAgbGV0IG5vZGUgPSB0aGlzLl9saXN0LnNoaWZ0KCk7XHJcbiAgICAgICAgaWYgKCBub2RlICkge1xyXG4gICAgICAgICAgICBkZWxldGUgdGhpcy5faGFzaFtub2RlLmdldEhhc2goKV07XHJcbiAgICAgICAgICAgIHJldHVybiBub2RlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNvcnQgdGhpcyBsaXN0LiBUaGUgbm9kZSB3aXRoIHRoZSBsb3dlc3QgY29zdCAoZiB2YWx1ZSkgd2lsbCBiZSBhdCB0aGUgdG9wLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc29ydCgpIHtcclxuICAgICAgICB0aGlzLl9saXN0LnNvcnQoIHRoaXMuY29tcGFyZXIgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbXBhcmUgZnVuY3Rpb24gZm9yIG5vZGVzLiBUaGlzIGlzIHVzZWQgZm9yIHNvcnRpbmcuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUFcclxuICAgICAqIEBwYXJhbSBub2RlQlxyXG4gICAgICogQHJldHVybnMge251bWJlcn1cclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBjb21wYXJlciggbm9kZUE6IE5vZGUsIG5vZGVCOiBOb2RlICkge1xyXG4gICAgICAgIHJldHVybiBub2RlQS5mIC0gbm9kZUIuZjtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE9wZW5MaXN0O1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
