"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("../logger");
var objectmodel_1 = require("../model/objectmodel");
var intersecter_1 = require("../math/intersecter");
var point_1 = require("../math/point");
var openlist_1 = require("./openlist");
var closelist_1 = require("./closelist");
var node_1 = require("./node");
var Pathfinder = /** @class */ (function () {
    function Pathfinder(world, nodeLimit) {
        this._world = world;
        if (nodeLimit) {
            this._nodeLimit = 0;
        }
        else {
            this._nodeLimit = 500;
        }
    }
    /**
     * Finds a path for the given geometry from the geometry's current point
     * to a certain given point.
     * @param geometry
     * @param destination
     * @param step This is the height distance or the height of the step of the
     * entity that is trying to find the path. It is proportional to the height of the
     * the entity. If the entity is big and tall, the height would be large. If the
     * entity is small and short, the height will be small.
     * @returns {Array}
     */
    Pathfinder.prototype.findPath = function (objectModel, destination, step) {
        // Reset the open and closed list.
        this._openList = new openlist_1.default();
        this._closeList = new closelist_1.default();
        // Set up step.
        this._step = step ? step : 0;
        // Get the destination.
        var geometry = objectModel.shape;
        this._startingPoint = new point_1.default(geometry.x, geometry.y, geometry.z);
        this._destination = destination.clone();
        // Add the first geometry as the first node on the list.
        var firstNode = new node_1.default(objectModel.id, geometry.clone());
        firstNode.g = this.getCostFromSource(firstNode);
        firstNode.h = this.getCostToDestination(firstNode);
        // Add first node to the list of possibilities.
        this._openList.push(firstNode);
        // Find the path. Get the last node of the path.
        var lastNode = this.computePath();
        // Convert to points.
        var result = new Array();
        if (lastNode) {
            result = this.convertNodeToPoints(lastNode);
        }
        return result;
    };
    /**
     * Recursively use the A* pathfinding algorithm to find the closest path.
     * Return null if path cannot be found.
     */
    Pathfinder.prototype.computePath = function () {
        while (this._openList.isNotEmpty()) {
            // If we've already considered too many nodes and possibilities,
            // then just exit.
            if (this._closeList.getLength() > this._nodeLimit) {
                return null;
            }
            var qNode = this._openList.pop();
            if (qNode === null) {
                logger_1.default.warn("Pathfinder", "No more nodes in the open list");
                return null;
            }
            var arrayOfNodes = this.getAdjacentNodes(qNode);
            for (var i = 0; i < arrayOfNodes.length; i++) {
                var tempNode = arrayOfNodes[i];
                if (intersecter_1.default.isShapeIntersectPoint(tempNode.shape, this._destination)) {
                    if (tempNode.shape.z === this._destination.z) {
                        return tempNode;
                    }
                }
                var skipSuccessor = false;
                // if a node with the same position as successor is in the OPEN list
                // which has a lower f than successor, skip this successor.
                if (this._openList.isNodeInList(tempNode)) {
                    if (this._openList.getFValue(tempNode) <= tempNode.f) {
                        skipSuccessor = true;
                    }
                }
                // if a node with the same position as successor is in the CLOSED list
                // which has a lower f than successor, skip this successor
                if (this._closeList.isNodeInList(tempNode)) {
                    skipSuccessor = true;
                }
                if (!skipSuccessor) {
                    this._openList.push(tempNode);
                }
            }
            this._closeList.push(qNode);
            this._openList.sort();
        }
        return null;
    };
    /**
     * Convert node into a list of points (for the path).
     */
    Pathfinder.prototype.convertNodeToPoints = function (node) {
        var path = new Array();
        var tempNode = node;
        while (tempNode != null) {
            path.unshift(new point_1.default(tempNode.shape.x, tempNode.shape.y, tempNode.shape.z));
            tempNode = tempNode.parent;
        }
        return path;
    };
    /**
     * Get adjacent nodes.
     */
    Pathfinder.prototype.getAdjacentNodes = function (node) {
        var adjacentShapes = node.shape.getAdjacentShapes();
        // Add potential shapes to the open list. Ignore nodes that cannot be crossed or are in the closed list.
        var adjacentNodes = new Array();
        for (var i = 0; i < adjacentShapes.length; i++) {
            var adjacentShape = adjacentShapes[i];
            var tempNode = new node_1.default(node.objectModelId, adjacentShape);
            // Set the z value of the shape based on the shapes that this adjacent
            // shape intersects.
            var tempZHeight = this.getZHeight(tempNode);
            // Only allow this child node if the gap between the parent node is not greater than the step value.
            if (Math.abs(tempZHeight - node.shape.z) <= this._step) {
                adjacentShape.z = tempZHeight;
                tempNode.parent = node;
                tempNode.g = this.getCostFromSource(tempNode);
                tempNode.h = this.getCostToDestination(tempNode);
                // Only allow this child if the node is not blocked.
                if (this.isNodeBlocked(tempNode) === false) {
                    adjacentNodes.push(tempNode);
                }
            }
        }
        return adjacentNodes;
    };
    /**
     * Find the cost G between the current node to the starting point.
     */
    Pathfinder.prototype.getCostFromSource = function (node) {
        // return (node.parent === null) ? 1 : node.parent.g + 1;
        var x1 = node.shape.x;
        var x2 = this._startingPoint.x;
        var y1 = node.shape.y;
        var y2 = this._startingPoint.y;
        var z1 = node.shape.z;
        var z2 = this._startingPoint.z;
        var twodcost = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        var zcost = Math.abs(z2 - z1);
        if (zcost > 0) {
            return zcost + twodcost * 0.01;
        }
        return twodcost;
    };
    /**
     * Find the cost H between the current node to the destination point.
     */
    Pathfinder.prototype.getCostToDestination = function (node) {
        var x1 = node.shape.x;
        var x2 = this._destination.x;
        var y1 = node.shape.y;
        var y2 = this._destination.y;
        var z1 = node.shape.z;
        var z2 = this._destination.z;
        var twodcost = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        var zcost = Math.abs(z2 - z1);
        if (zcost > 0) {
            return zcost + twodcost * 0.01;
        }
        return twodcost;
    };
    /**
     * Get the Z height of a given node.
     */
    Pathfinder.prototype.getZHeight = function (node) {
        var shape = node.shape;
        var zValue = 0;
        var pointShape = new point_1.default(shape.x, shape.y, shape.z);
        var intersectingObjectModels = this._world.getAllIntersection(shape);
        for (var j = 0; j < intersectingObjectModels.length; j++) {
            var intersectObjectModel = intersectingObjectModels[j];
            var intersectShape = intersectObjectModel.shape;
            // If it is impassable then the isNodeBlocked method will filter it out.
            if (intersectObjectModel.id !== node.objectModelId && intersectObjectModel.objectType !== objectmodel_1.ObjectModelType.Impassable) {
                if (intersecter_1.default.isShapeIntersectPoint(intersectShape, pointShape)) {
                    // We need to get the largest z value.
                    var tempZValue = intersectShape.getZRelativeToPoint(pointShape);
                    if (tempZValue <= shape.z + this._step) {
                        if (zValue < tempZValue) {
                            zValue = tempZValue;
                        }
                    }
                }
            }
        }
        return zValue;
    };
    /**
     * If the pathfinding node intersects with something that is impassable then the node is
     * considered blocked and not included in pathfinding. Otherwise it is included.
     */
    Pathfinder.prototype.isNodeBlocked = function (node) {
        var shape = node.shape;
        var intersectingObjectModels = this._world.getAllIntersection(shape);
        for (var j = 0; j < intersectingObjectModels.length; j++) {
            var tempObjectModel = intersectingObjectModels[j];
            if (tempObjectModel.id !== node.objectModelId && tempObjectModel.objectType === objectmodel_1.ObjectModelType.Impassable) {
                return true;
            }
        }
        return false;
    };
    return Pathfinder;
}());
exports.Pathfinder = Pathfinder;
exports.default = Pathfinder;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBmL3BhdGhmaW5kZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxvQ0FBK0I7QUFDL0Isb0RBQXFEO0FBQ3JELG1EQUE4QztBQUM5Qyx1Q0FBa0M7QUFHbEMsdUNBQWtDO0FBQ2xDLHlDQUFvQztBQUVwQywrQkFBMEI7QUFFMUI7SUFVSSxvQkFBWSxLQUFZLEVBQUUsU0FBa0I7UUFDeEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsRUFBRSxDQUFDLENBQUUsU0FBVSxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1FBQ3hCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDO1FBQzFCLENBQUM7SUFDTCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7T0FVRztJQUNJLDZCQUFRLEdBQWYsVUFBZ0IsV0FBd0IsRUFBRSxXQUFrQixFQUFFLElBQWE7UUFFdkUsa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxrQkFBUSxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLG1CQUFTLEVBQUUsQ0FBQztRQUVsQyxlQUFlO1FBQ2YsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRTdCLHVCQUF1QjtRQUN2QixJQUFJLFFBQVEsR0FBVSxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxlQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUV4Qyx3REFBd0Q7UUFDeEQsSUFBSSxTQUFTLEdBQUcsSUFBSSxjQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsRUFBRSxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUMzRCxTQUFTLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoRCxTQUFTLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVuRCwrQ0FBK0M7UUFDL0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFL0IsZ0RBQWdEO1FBQ2hELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUVsQyxxQkFBcUI7UUFDckIsSUFBSSxNQUFNLEdBQWlCLElBQUksS0FBSyxFQUFTLENBQUM7UUFDOUMsRUFBRSxDQUFDLENBQUUsUUFBUyxDQUFDLENBQUMsQ0FBQztZQUNiLE1BQU0sR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDaEQsQ0FBQztRQUNELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFFbEIsQ0FBQztJQUVEOzs7T0FHRztJQUNLLGdDQUFXLEdBQW5CO1FBRUksT0FBUSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxFQUFHLENBQUM7WUFFbkMsZ0VBQWdFO1lBQ2hFLGtCQUFrQjtZQUNsQixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxHQUFHLElBQUksQ0FBQyxVQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNsRCxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2hCLENBQUM7WUFFRCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxDQUFFLEtBQUssS0FBSyxJQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixnQkFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsZ0NBQWdDLENBQUMsQ0FBQztnQkFDNUQsTUFBTSxDQUFDLElBQUksQ0FBQztZQUNoQixDQUFDO1lBRUQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hELEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO2dCQUU3QyxJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRS9CLEVBQUUsQ0FBQyxDQUFFLHFCQUFXLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFFLENBQUMsQ0FBQyxDQUFDO29CQUN6RSxFQUFFLENBQUMsQ0FBRSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQzdDLE1BQU0sQ0FBQyxRQUFRLENBQUM7b0JBQ3BCLENBQUM7Z0JBQ0wsQ0FBQztnQkFFRCxJQUFJLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBRTFCLG9FQUFvRTtnQkFDcEUsMkRBQTJEO2dCQUMzRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzFDLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxDQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUNyRCxhQUFhLEdBQUcsSUFBSSxDQUFDO29CQUN6QixDQUFDO2dCQUNMLENBQUM7Z0JBRUQsc0VBQXNFO2dCQUN0RSwwREFBMEQ7Z0JBQzFELEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBRSxDQUFDLENBQUMsQ0FBQztvQkFDM0MsYUFBYSxHQUFHLElBQUksQ0FBQztnQkFDekIsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBRSxDQUFDLGFBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQ25CLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNsQyxDQUFDO1lBQ0wsQ0FBQztZQUVELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFMUIsQ0FBQztRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFFaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ssd0NBQW1CLEdBQTNCLFVBQTRCLElBQVU7UUFDbEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxLQUFLLEVBQVMsQ0FBQztRQUM5QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDcEIsT0FBUSxRQUFRLElBQUksSUFBSSxFQUFHLENBQUM7WUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBRSxJQUFJLGVBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7WUFDaEYsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDL0IsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0sscUNBQWdCLEdBQXhCLFVBQTBCLElBQVU7UUFFaEMsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXBELHdHQUF3RztRQUN4RyxJQUFJLGFBQWEsR0FBRyxJQUFJLEtBQUssRUFBUSxDQUFDO1FBQ3RDLEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1lBRS9DLElBQUksYUFBYSxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLFFBQVEsR0FBRyxJQUFJLGNBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBRTNELHNFQUFzRTtZQUN0RSxvQkFBb0I7WUFDcEIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUU1QyxvR0FBb0c7WUFDcEcsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQztnQkFFdkQsYUFBYSxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUM7Z0JBQzlCLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixRQUFRLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDOUMsUUFBUSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBRWpELG9EQUFvRDtnQkFDcEQsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsS0FBSyxLQUFNLENBQUMsQ0FBQyxDQUFDO29CQUMzQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNqQyxDQUFDO1lBRUwsQ0FBQztRQUVMLENBQUM7UUFFRCxNQUFNLENBQUMsYUFBYSxDQUFDO0lBRXpCLENBQUM7SUFFRDs7T0FFRztJQUNLLHNDQUFpQixHQUF6QixVQUEwQixJQUFVO1FBQ2hDLHlEQUF5RDtRQUN6RCxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUN0QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUMvQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUN0QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUMvQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUN0QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUMvQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDOUIsRUFBRSxDQUFDLENBQUUsS0FBSyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFBQyxNQUFNLENBQUMsS0FBSyxHQUFHLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFBQyxDQUFDO1FBQ3BELE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVEOztPQUVHO0lBQ0sseUNBQW9CLEdBQTVCLFVBQTZCLElBQVU7UUFDbkMsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDdEIsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDN0IsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDdEIsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDN0IsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDdEIsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDN0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQzlCLEVBQUUsQ0FBQyxDQUFFLEtBQUssR0FBRyxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQUMsQ0FBQztRQUNwRCxNQUFNLENBQUMsUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFFRDs7T0FFRztJQUNLLCtCQUFVLEdBQWxCLFVBQW1CLElBQVU7UUFFekIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN2QixJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFZixJQUFJLFVBQVUsR0FBRyxJQUFJLGVBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXRELElBQUksd0JBQXdCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVyRSxHQUFHLENBQUMsQ0FBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLHdCQUF3QixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1lBRXpELElBQUksb0JBQW9CLEdBQUcsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkQsSUFBSSxjQUFjLEdBQUcsb0JBQW9CLENBQUMsS0FBSyxDQUFDO1lBRWhELHdFQUF3RTtZQUN4RSxFQUFFLENBQUMsQ0FBRSxvQkFBb0IsQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLGFBQWEsSUFBSSxvQkFBb0IsQ0FBQyxVQUFVLEtBQUssNkJBQWUsQ0FBQyxVQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNySCxFQUFFLENBQUMsQ0FBRSxxQkFBVyxDQUFDLHFCQUFxQixDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUUsQ0FBQyxDQUFDLENBQUM7b0JBRWxFLHNDQUFzQztvQkFDdEMsSUFBSSxVQUFVLEdBQUcsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNoRSxFQUFFLENBQUMsQ0FBRSxVQUFVLElBQUksS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQzt3QkFDdkMsRUFBRSxDQUFDLENBQUUsTUFBTSxHQUFHLFVBQVcsQ0FBQyxDQUFDLENBQUM7NEJBQ3hCLE1BQU0sR0FBRyxVQUFVLENBQUM7d0JBQ3hCLENBQUM7b0JBQ0wsQ0FBQztnQkFFTCxDQUFDO1lBQ0wsQ0FBQztRQUVMLENBQUM7UUFFRCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBRWxCLENBQUM7SUFFRDs7O09BR0c7SUFDSyxrQ0FBYSxHQUFyQixVQUFzQixJQUFVO1FBQzVCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdkIsSUFBSSx3QkFBd0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JFLEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsd0JBQXdCLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDekQsSUFBSSxlQUFlLEdBQUcsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEQsRUFBRSxDQUFDLENBQUUsZUFBZSxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsYUFBYSxJQUFJLGVBQWUsQ0FBQyxVQUFVLEtBQUssNkJBQWUsQ0FBQyxVQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUMzRyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2hCLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUwsaUJBQUM7QUFBRCxDQXhRQSxBQXdRQyxJQUFBO0FBeFFZLGdDQUFVO0FBMFF2QixrQkFBZSxVQUFVLENBQUMiLCJmaWxlIjoicGYvcGF0aGZpbmRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBMb2dnZXIgZnJvbSBcIi4uL2xvZ2dlclwiO1xyXG5pbXBvcnQge09iamVjdE1vZGVsVHlwZX0gZnJvbSBcIi4uL21vZGVsL29iamVjdG1vZGVsXCI7XHJcbmltcG9ydCBJbnRlcnNlY3RlciBmcm9tIFwiLi4vbWF0aC9pbnRlcnNlY3RlclwiO1xyXG5pbXBvcnQgUG9pbnQgZnJvbSBcIi4uL21hdGgvcG9pbnRcIjtcclxuaW1wb3J0IFNoYXBlIGZyb20gXCIuLi9tYXRoL3NoYXBlXCI7XHJcbmltcG9ydCBPYmplY3RNb2RlbCBmcm9tIFwiLi4vbW9kZWwvb2JqZWN0bW9kZWxcIjtcclxuaW1wb3J0IE9wZW5MaXN0IGZyb20gXCIuL29wZW5saXN0XCI7XHJcbmltcG9ydCBDbG9zZUxpc3QgZnJvbSBcIi4vY2xvc2VsaXN0XCI7XHJcbmltcG9ydCBXb3JsZCBmcm9tIFwiLi4vbW9kZWwvd29ybGRcIjtcclxuaW1wb3J0IE5vZGUgZnJvbSBcIi4vbm9kZVwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIFBhdGhmaW5kZXIge1xyXG5cclxuICAgIHByaXZhdGUgX29wZW5MaXN0OiBPcGVuTGlzdDtcclxuICAgIHByaXZhdGUgX2Nsb3NlTGlzdDogQ2xvc2VMaXN0O1xyXG4gICAgcHJpdmF0ZSBfc3RhcnRpbmdQb2ludDogUG9pbnQ7XHJcbiAgICBwcml2YXRlIF9zdGVwOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9kZXN0aW5hdGlvbjogUG9pbnQ7XHJcbiAgICBwcml2YXRlIF93b3JsZDogV29ybGQ7XHJcbiAgICBwcml2YXRlIF9ub2RlTGltaXQ6IG51bWJlcjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcih3b3JsZDogV29ybGQsIG5vZGVMaW1pdD86IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX3dvcmxkID0gd29ybGQ7XHJcbiAgICAgICAgaWYgKCBub2RlTGltaXQgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX25vZGVMaW1pdCA9IDA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5fbm9kZUxpbWl0ID0gNTAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmRzIGEgcGF0aCBmb3IgdGhlIGdpdmVuIGdlb21ldHJ5IGZyb20gdGhlIGdlb21ldHJ5J3MgY3VycmVudCBwb2ludFxyXG4gICAgICogdG8gYSBjZXJ0YWluIGdpdmVuIHBvaW50LlxyXG4gICAgICogQHBhcmFtIGdlb21ldHJ5XHJcbiAgICAgKiBAcGFyYW0gZGVzdGluYXRpb25cclxuICAgICAqIEBwYXJhbSBzdGVwIFRoaXMgaXMgdGhlIGhlaWdodCBkaXN0YW5jZSBvciB0aGUgaGVpZ2h0IG9mIHRoZSBzdGVwIG9mIHRoZVxyXG4gICAgICogZW50aXR5IHRoYXQgaXMgdHJ5aW5nIHRvIGZpbmQgdGhlIHBhdGguIEl0IGlzIHByb3BvcnRpb25hbCB0byB0aGUgaGVpZ2h0IG9mIHRoZVxyXG4gICAgICogdGhlIGVudGl0eS4gSWYgdGhlIGVudGl0eSBpcyBiaWcgYW5kIHRhbGwsIHRoZSBoZWlnaHQgd291bGQgYmUgbGFyZ2UuIElmIHRoZVxyXG4gICAgICogZW50aXR5IGlzIHNtYWxsIGFuZCBzaG9ydCwgdGhlIGhlaWdodCB3aWxsIGJlIHNtYWxsLlxyXG4gICAgICogQHJldHVybnMge0FycmF5fVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZmluZFBhdGgob2JqZWN0TW9kZWw6IE9iamVjdE1vZGVsLCBkZXN0aW5hdGlvbjogUG9pbnQsIHN0ZXA/OiBudW1iZXIpOiBQb2ludFtdIHtcclxuXHJcbiAgICAgICAgLy8gUmVzZXQgdGhlIG9wZW4gYW5kIGNsb3NlZCBsaXN0LlxyXG4gICAgICAgIHRoaXMuX29wZW5MaXN0ID0gbmV3IE9wZW5MaXN0KCk7XHJcbiAgICAgICAgdGhpcy5fY2xvc2VMaXN0ID0gbmV3IENsb3NlTGlzdCgpO1xyXG5cclxuICAgICAgICAvLyBTZXQgdXAgc3RlcC5cclxuICAgICAgICB0aGlzLl9zdGVwID0gc3RlcCA/IHN0ZXAgOiAwO1xyXG5cclxuICAgICAgICAvLyBHZXQgdGhlIGRlc3RpbmF0aW9uLlxyXG4gICAgICAgIGxldCBnZW9tZXRyeTogU2hhcGUgPSBvYmplY3RNb2RlbC5zaGFwZTtcclxuICAgICAgICB0aGlzLl9zdGFydGluZ1BvaW50ID0gbmV3IFBvaW50KGdlb21ldHJ5LngsIGdlb21ldHJ5LnksIGdlb21ldHJ5LnopO1xyXG4gICAgICAgIHRoaXMuX2Rlc3RpbmF0aW9uID0gZGVzdGluYXRpb24uY2xvbmUoKTtcclxuXHJcbiAgICAgICAgLy8gQWRkIHRoZSBmaXJzdCBnZW9tZXRyeSBhcyB0aGUgZmlyc3Qgbm9kZSBvbiB0aGUgbGlzdC5cclxuICAgICAgICBsZXQgZmlyc3ROb2RlID0gbmV3IE5vZGUob2JqZWN0TW9kZWwuaWQsIGdlb21ldHJ5LmNsb25lKCkpO1xyXG4gICAgICAgIGZpcnN0Tm9kZS5nID0gdGhpcy5nZXRDb3N0RnJvbVNvdXJjZShmaXJzdE5vZGUpO1xyXG4gICAgICAgIGZpcnN0Tm9kZS5oID0gdGhpcy5nZXRDb3N0VG9EZXN0aW5hdGlvbihmaXJzdE5vZGUpO1xyXG5cclxuICAgICAgICAvLyBBZGQgZmlyc3Qgbm9kZSB0byB0aGUgbGlzdCBvZiBwb3NzaWJpbGl0aWVzLlxyXG4gICAgICAgIHRoaXMuX29wZW5MaXN0LnB1c2goZmlyc3ROb2RlKTtcclxuXHJcbiAgICAgICAgLy8gRmluZCB0aGUgcGF0aC4gR2V0IHRoZSBsYXN0IG5vZGUgb2YgdGhlIHBhdGguXHJcbiAgICAgICAgbGV0IGxhc3ROb2RlID0gdGhpcy5jb21wdXRlUGF0aCgpO1xyXG5cclxuICAgICAgICAvLyBDb252ZXJ0IHRvIHBvaW50cy5cclxuICAgICAgICBsZXQgcmVzdWx0OiBQb2ludFtdfG51bGwgPSBuZXcgQXJyYXk8UG9pbnQ+KCk7XHJcbiAgICAgICAgaWYgKCBsYXN0Tm9kZSApIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gdGhpcy5jb252ZXJ0Tm9kZVRvUG9pbnRzKGxhc3ROb2RlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZWN1cnNpdmVseSB1c2UgdGhlIEEqIHBhdGhmaW5kaW5nIGFsZ29yaXRobSB0byBmaW5kIHRoZSBjbG9zZXN0IHBhdGguXHJcbiAgICAgKiBSZXR1cm4gbnVsbCBpZiBwYXRoIGNhbm5vdCBiZSBmb3VuZC5cclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBjb21wdXRlUGF0aCgpOiBOb2RlfG51bGwge1xyXG5cclxuICAgICAgICB3aGlsZSAoIHRoaXMuX29wZW5MaXN0LmlzTm90RW1wdHkoKSApIHtcclxuXHJcbiAgICAgICAgICAgIC8vIElmIHdlJ3ZlIGFscmVhZHkgY29uc2lkZXJlZCB0b28gbWFueSBub2RlcyBhbmQgcG9zc2liaWxpdGllcyxcclxuICAgICAgICAgICAgLy8gdGhlbiBqdXN0IGV4aXQuXHJcbiAgICAgICAgICAgIGlmICggdGhpcy5fY2xvc2VMaXN0LmdldExlbmd0aCgpID4gdGhpcy5fbm9kZUxpbWl0ICkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBxTm9kZSA9IHRoaXMuX29wZW5MaXN0LnBvcCgpO1xyXG4gICAgICAgICAgICBpZiAoIHFOb2RlID09PSBudWxsICkge1xyXG4gICAgICAgICAgICAgICAgTG9nZ2VyLndhcm4oXCJQYXRoZmluZGVyXCIsIFwiTm8gbW9yZSBub2RlcyBpbiB0aGUgb3BlbiBsaXN0XCIpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBhcnJheU9mTm9kZXMgPSB0aGlzLmdldEFkamFjZW50Tm9kZXMocU5vZGUpO1xyXG4gICAgICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBhcnJheU9mTm9kZXMubGVuZ3RoOyBpKysgKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IHRlbXBOb2RlID0gYXJyYXlPZk5vZGVzW2ldO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICggSW50ZXJzZWN0ZXIuaXNTaGFwZUludGVyc2VjdFBvaW50KHRlbXBOb2RlLnNoYXBlLCB0aGlzLl9kZXN0aW5hdGlvbikgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCB0ZW1wTm9kZS5zaGFwZS56ID09PSB0aGlzLl9kZXN0aW5hdGlvbi56ICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGVtcE5vZGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGxldCBza2lwU3VjY2Vzc29yID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gaWYgYSBub2RlIHdpdGggdGhlIHNhbWUgcG9zaXRpb24gYXMgc3VjY2Vzc29yIGlzIGluIHRoZSBPUEVOIGxpc3RcclxuICAgICAgICAgICAgICAgIC8vIHdoaWNoIGhhcyBhIGxvd2VyIGYgdGhhbiBzdWNjZXNzb3IsIHNraXAgdGhpcyBzdWNjZXNzb3IuXHJcbiAgICAgICAgICAgICAgICBpZiAoIHRoaXMuX29wZW5MaXN0LmlzTm9kZUluTGlzdCh0ZW1wTm9kZSkgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCB0aGlzLl9vcGVuTGlzdC5nZXRGVmFsdWUodGVtcE5vZGUpIDw9IHRlbXBOb2RlLmYgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNraXBTdWNjZXNzb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBpZiBhIG5vZGUgd2l0aCB0aGUgc2FtZSBwb3NpdGlvbiBhcyBzdWNjZXNzb3IgaXMgaW4gdGhlIENMT1NFRCBsaXN0XHJcbiAgICAgICAgICAgICAgICAvLyB3aGljaCBoYXMgYSBsb3dlciBmIHRoYW4gc3VjY2Vzc29yLCBza2lwIHRoaXMgc3VjY2Vzc29yXHJcbiAgICAgICAgICAgICAgICBpZiAoIHRoaXMuX2Nsb3NlTGlzdC5pc05vZGVJbkxpc3QodGVtcE5vZGUpICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNraXBTdWNjZXNzb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICggIXNraXBTdWNjZXNzb3IgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fb3Blbkxpc3QucHVzaCh0ZW1wTm9kZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2Nsb3NlTGlzdC5wdXNoKHFOb2RlKTtcclxuICAgICAgICAgICAgdGhpcy5fb3Blbkxpc3Quc29ydCgpO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgbm9kZSBpbnRvIGEgbGlzdCBvZiBwb2ludHMgKGZvciB0aGUgcGF0aCkuXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgY29udmVydE5vZGVUb1BvaW50cyhub2RlOiBOb2RlKTogUG9pbnRbXSB7XHJcbiAgICAgICAgbGV0IHBhdGggPSBuZXcgQXJyYXk8UG9pbnQ+KCk7XHJcbiAgICAgICAgbGV0IHRlbXBOb2RlID0gbm9kZTtcclxuICAgICAgICB3aGlsZSAoIHRlbXBOb2RlICE9IG51bGwgKSB7XHJcbiAgICAgICAgICAgIHBhdGgudW5zaGlmdCggbmV3IFBvaW50KHRlbXBOb2RlLnNoYXBlLngsIHRlbXBOb2RlLnNoYXBlLnksIHRlbXBOb2RlLnNoYXBlLnopICk7XHJcbiAgICAgICAgICAgIHRlbXBOb2RlID0gdGVtcE5vZGUucGFyZW50O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcGF0aDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCBhZGphY2VudCBub2Rlcy5cclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBnZXRBZGphY2VudE5vZGVzKCBub2RlOiBOb2RlICk6IEFycmF5PE5vZGU+IHtcclxuXHJcbiAgICAgICAgbGV0IGFkamFjZW50U2hhcGVzID0gbm9kZS5zaGFwZS5nZXRBZGphY2VudFNoYXBlcygpO1xyXG5cclxuICAgICAgICAvLyBBZGQgcG90ZW50aWFsIHNoYXBlcyB0byB0aGUgb3BlbiBsaXN0LiBJZ25vcmUgbm9kZXMgdGhhdCBjYW5ub3QgYmUgY3Jvc3NlZCBvciBhcmUgaW4gdGhlIGNsb3NlZCBsaXN0LlxyXG4gICAgICAgIGxldCBhZGphY2VudE5vZGVzID0gbmV3IEFycmF5PE5vZGU+KCk7XHJcbiAgICAgICAgZm9yICggbGV0IGkgPSAwOyBpIDwgYWRqYWNlbnRTaGFwZXMubGVuZ3RoOyBpKysgKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgYWRqYWNlbnRTaGFwZSA9IGFkamFjZW50U2hhcGVzW2ldO1xyXG4gICAgICAgICAgICBsZXQgdGVtcE5vZGUgPSBuZXcgTm9kZShub2RlLm9iamVjdE1vZGVsSWQsIGFkamFjZW50U2hhcGUpO1xyXG5cclxuICAgICAgICAgICAgLy8gU2V0IHRoZSB6IHZhbHVlIG9mIHRoZSBzaGFwZSBiYXNlZCBvbiB0aGUgc2hhcGVzIHRoYXQgdGhpcyBhZGphY2VudFxyXG4gICAgICAgICAgICAvLyBzaGFwZSBpbnRlcnNlY3RzLlxyXG4gICAgICAgICAgICBsZXQgdGVtcFpIZWlnaHQgPSB0aGlzLmdldFpIZWlnaHQodGVtcE5vZGUpO1xyXG5cclxuICAgICAgICAgICAgLy8gT25seSBhbGxvdyB0aGlzIGNoaWxkIG5vZGUgaWYgdGhlIGdhcCBiZXR3ZWVuIHRoZSBwYXJlbnQgbm9kZSBpcyBub3QgZ3JlYXRlciB0aGFuIHRoZSBzdGVwIHZhbHVlLlxyXG4gICAgICAgICAgICBpZiAoIE1hdGguYWJzKHRlbXBaSGVpZ2h0IC0gbm9kZS5zaGFwZS56KSA8PSB0aGlzLl9zdGVwICkge1xyXG5cclxuICAgICAgICAgICAgICAgIGFkamFjZW50U2hhcGUueiA9IHRlbXBaSGVpZ2h0O1xyXG4gICAgICAgICAgICAgICAgdGVtcE5vZGUucGFyZW50ID0gbm9kZTtcclxuICAgICAgICAgICAgICAgIHRlbXBOb2RlLmcgPSB0aGlzLmdldENvc3RGcm9tU291cmNlKHRlbXBOb2RlKTtcclxuICAgICAgICAgICAgICAgIHRlbXBOb2RlLmggPSB0aGlzLmdldENvc3RUb0Rlc3RpbmF0aW9uKHRlbXBOb2RlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBPbmx5IGFsbG93IHRoaXMgY2hpbGQgaWYgdGhlIG5vZGUgaXMgbm90IGJsb2NrZWQuXHJcbiAgICAgICAgICAgICAgICBpZiAoIHRoaXMuaXNOb2RlQmxvY2tlZCh0ZW1wTm9kZSkgPT09IGZhbHNlICkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFkamFjZW50Tm9kZXMucHVzaCh0ZW1wTm9kZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGFkamFjZW50Tm9kZXM7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRmluZCB0aGUgY29zdCBHIGJldHdlZW4gdGhlIGN1cnJlbnQgbm9kZSB0byB0aGUgc3RhcnRpbmcgcG9pbnQuXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgZ2V0Q29zdEZyb21Tb3VyY2Uobm9kZTogTm9kZSk6IG51bWJlciB7XHJcbiAgICAgICAgLy8gcmV0dXJuIChub2RlLnBhcmVudCA9PT0gbnVsbCkgPyAxIDogbm9kZS5wYXJlbnQuZyArIDE7XHJcbiAgICAgICAgbGV0IHgxID0gbm9kZS5zaGFwZS54O1xyXG4gICAgICAgIGxldCB4MiA9IHRoaXMuX3N0YXJ0aW5nUG9pbnQueDtcclxuICAgICAgICBsZXQgeTEgPSBub2RlLnNoYXBlLnk7XHJcbiAgICAgICAgbGV0IHkyID0gdGhpcy5fc3RhcnRpbmdQb2ludC55O1xyXG4gICAgICAgIGxldCB6MSA9IG5vZGUuc2hhcGUuejtcclxuICAgICAgICBsZXQgejIgPSB0aGlzLl9zdGFydGluZ1BvaW50Lno7XHJcbiAgICAgICAgbGV0IHR3b2Rjb3N0ID0gTWF0aC5zcXJ0KCh4MiAtIHgxKSAqICh4MiAtIHgxKSArICh5MiAtIHkxKSAqICh5MiAtIHkxKSk7XHJcbiAgICAgICAgbGV0IHpjb3N0ID0gTWF0aC5hYnMoejIgLSB6MSk7XHJcbiAgICAgICAgaWYgKCB6Y29zdCA+IDAgKSB7IHJldHVybiB6Y29zdCArIHR3b2Rjb3N0ICogMC4wMTsgfVxyXG4gICAgICAgIHJldHVybiB0d29kY29zdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmQgdGhlIGNvc3QgSCBiZXR3ZWVuIHRoZSBjdXJyZW50IG5vZGUgdG8gdGhlIGRlc3RpbmF0aW9uIHBvaW50LlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGdldENvc3RUb0Rlc3RpbmF0aW9uKG5vZGU6IE5vZGUpOiBudW1iZXIge1xyXG4gICAgICAgIGxldCB4MSA9IG5vZGUuc2hhcGUueDtcclxuICAgICAgICBsZXQgeDIgPSB0aGlzLl9kZXN0aW5hdGlvbi54O1xyXG4gICAgICAgIGxldCB5MSA9IG5vZGUuc2hhcGUueTtcclxuICAgICAgICBsZXQgeTIgPSB0aGlzLl9kZXN0aW5hdGlvbi55O1xyXG4gICAgICAgIGxldCB6MSA9IG5vZGUuc2hhcGUuejtcclxuICAgICAgICBsZXQgejIgPSB0aGlzLl9kZXN0aW5hdGlvbi56O1xyXG4gICAgICAgIGxldCB0d29kY29zdCA9IE1hdGguc3FydCgoeDIgLSB4MSkgKiAoeDIgLSB4MSkgKyAoeTIgLSB5MSkgKiAoeTIgLSB5MSkpO1xyXG4gICAgICAgIGxldCB6Y29zdCA9IE1hdGguYWJzKHoyIC0gejEpO1xyXG4gICAgICAgIGlmICggemNvc3QgPiAwICkgeyByZXR1cm4gemNvc3QgKyB0d29kY29zdCAqIDAuMDE7IH1cclxuICAgICAgICByZXR1cm4gdHdvZGNvc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIFogaGVpZ2h0IG9mIGEgZ2l2ZW4gbm9kZS5cclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBnZXRaSGVpZ2h0KG5vZGU6IE5vZGUpOiBudW1iZXIge1xyXG5cclxuICAgICAgICBsZXQgc2hhcGUgPSBub2RlLnNoYXBlO1xyXG4gICAgICAgIGxldCB6VmFsdWUgPSAwO1xyXG5cclxuICAgICAgICBsZXQgcG9pbnRTaGFwZSA9IG5ldyBQb2ludChzaGFwZS54LCBzaGFwZS55LCBzaGFwZS56KTtcclxuXHJcbiAgICAgICAgbGV0IGludGVyc2VjdGluZ09iamVjdE1vZGVscyA9IHRoaXMuX3dvcmxkLmdldEFsbEludGVyc2VjdGlvbihzaGFwZSk7XHJcblxyXG4gICAgICAgIGZvciAoIGxldCBqID0gMDsgaiA8IGludGVyc2VjdGluZ09iamVjdE1vZGVscy5sZW5ndGg7IGorKyApIHtcclxuXHJcbiAgICAgICAgICAgIGxldCBpbnRlcnNlY3RPYmplY3RNb2RlbCA9IGludGVyc2VjdGluZ09iamVjdE1vZGVsc1tqXTtcclxuICAgICAgICAgICAgbGV0IGludGVyc2VjdFNoYXBlID0gaW50ZXJzZWN0T2JqZWN0TW9kZWwuc2hhcGU7XHJcblxyXG4gICAgICAgICAgICAvLyBJZiBpdCBpcyBpbXBhc3NhYmxlIHRoZW4gdGhlIGlzTm9kZUJsb2NrZWQgbWV0aG9kIHdpbGwgZmlsdGVyIGl0IG91dC5cclxuICAgICAgICAgICAgaWYgKCBpbnRlcnNlY3RPYmplY3RNb2RlbC5pZCAhPT0gbm9kZS5vYmplY3RNb2RlbElkICYmIGludGVyc2VjdE9iamVjdE1vZGVsLm9iamVjdFR5cGUgIT09IE9iamVjdE1vZGVsVHlwZS5JbXBhc3NhYmxlICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBJbnRlcnNlY3Rlci5pc1NoYXBlSW50ZXJzZWN0UG9pbnQoaW50ZXJzZWN0U2hhcGUsIHBvaW50U2hhcGUpICkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBXZSBuZWVkIHRvIGdldCB0aGUgbGFyZ2VzdCB6IHZhbHVlLlxyXG4gICAgICAgICAgICAgICAgICAgIGxldCB0ZW1wWlZhbHVlID0gaW50ZXJzZWN0U2hhcGUuZ2V0WlJlbGF0aXZlVG9Qb2ludChwb2ludFNoYXBlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIHRlbXBaVmFsdWUgPD0gc2hhcGUueiArIHRoaXMuX3N0ZXAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICggelZhbHVlIDwgdGVtcFpWYWx1ZSApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHpWYWx1ZSA9IHRlbXBaVmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHpWYWx1ZTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJZiB0aGUgcGF0aGZpbmRpbmcgbm9kZSBpbnRlcnNlY3RzIHdpdGggc29tZXRoaW5nIHRoYXQgaXMgaW1wYXNzYWJsZSB0aGVuIHRoZSBub2RlIGlzXHJcbiAgICAgKiBjb25zaWRlcmVkIGJsb2NrZWQgYW5kIG5vdCBpbmNsdWRlZCBpbiBwYXRoZmluZGluZy4gT3RoZXJ3aXNlIGl0IGlzIGluY2x1ZGVkLlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGlzTm9kZUJsb2NrZWQobm9kZTogTm9kZSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBzaGFwZSA9IG5vZGUuc2hhcGU7XHJcbiAgICAgICAgbGV0IGludGVyc2VjdGluZ09iamVjdE1vZGVscyA9IHRoaXMuX3dvcmxkLmdldEFsbEludGVyc2VjdGlvbihzaGFwZSk7XHJcbiAgICAgICAgZm9yICggbGV0IGogPSAwOyBqIDwgaW50ZXJzZWN0aW5nT2JqZWN0TW9kZWxzLmxlbmd0aDsgaisrICkge1xyXG4gICAgICAgICAgICBsZXQgdGVtcE9iamVjdE1vZGVsID0gaW50ZXJzZWN0aW5nT2JqZWN0TW9kZWxzW2pdO1xyXG4gICAgICAgICAgICBpZiAoIHRlbXBPYmplY3RNb2RlbC5pZCAhPT0gbm9kZS5vYmplY3RNb2RlbElkICYmIHRlbXBPYmplY3RNb2RlbC5vYmplY3RUeXBlID09PSBPYmplY3RNb2RlbFR5cGUuSW1wYXNzYWJsZSApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFBhdGhmaW5kZXI7XHJcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
